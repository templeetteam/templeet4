<?php

define("TRACE_BUILD",FALSE);


Function buildcode()
{
  global $server_value;
  global $support_page,$support_index,$support_pathinfo,$support_querystring;
  global $templeettestdir;
  
  $templeettestdir=gettempleettestdir();
  
  @unlink("templeet.php");
  @rename("templeet.php.backup","templeet.php");
  
  @unlink(".htaccess");
  @rename(".htaccess.backup",".htaccess");
  
  fill_server_array();
      
  $testcode=builddefaultcode(); 
  if (@file_put_contents("templeet/code.txt",$testcode)===FALSE)
    {
      print "error|1|";
      exit;
    }
  
  @copy("templeet/code.txt","templeet/test/code.txt");
  @copy("templeet/buildcode2.txt","templeet/test/buildcode2.txt");
  
  $res=checkcode($testcode);
  
  if ($res!==0)
    {
      print "error|3|$res";
      exit;
    }
      
  $testcode=preg_replace('/_SERVER_TEST/','_SERVER',$testcode);
	
  $templeetfile="templeet/templeet.php";
  $tmp=@file_get_contents($templeetfile);
  if ($tmp===FALSE)
    {
      print "error|2|$templeetfile"; 
      exit;	
    }

  preg_match('/(.*\/\/BEGIN-GETPATH[\r\n]*).*(\/\/END-GETPATH.*)/s',$tmp,$res); 
  $tmp=$res[1].$testcode.$res[2];

  if (@file_put_contents($templeetfile,$tmp)===FALSE)
    {
      print "error|1|$templeetfile"; 
      exit;	
    }
  print "ok";
  exit();
}

Function fill_server_array()
{
  global $server_value,$server_array,$fields;
  global $support_page,$support_index,$support_pathinfo,$support_querystring;
  global $templeettestdir;
  
  $server_array=array();
  $fields=array();
  
  if (!@include($templeettestdir."respage.php"))
      $support_page=FALSE;
    else
      $support_page=TRUE;

  if (!@include($templeettestdir."resindex.php"))
      $support_index=FALSE;
    else
      $support_index=TRUE;

  if (!@include($templeettestdir."respathinfo.php"))
      $support_pathinfo=FALSE;
    else
      $support_pathinfo=TRUE;

  if (!@include($templeettestdir."restempleet.php"))
      $support_querystring=FALSE;
    else
      $support_querystring=TRUE;
    
    
  trace_build("server_value:",$server_value,"==\n");  
  if ($support_page)
    {
      list($server_array['page'],$fields['page'])=fill_method_array($server_value['page'],'/'.addcslashes($templeettestdir,"'\\\/").'test\d\/'.'JRDKGCTDYEZ/','JRDKGCTDYEZ');
      uasort($server_array['page'],'callback_valuelength');
    }
    
  if ($support_index)
    {
      list($server_array['index'],$fields['index'])=fill_method_array($server_value['index'],'/'.addcslashes($templeettestdir,"'\\\/").'test\d'.'\//','KLOHTFGRED');
      uasort($server_array['index'],'callback_valuelength');
    }
       
    
  if ($support_pathinfo)
    {
      list($server_array['pathinfo'],$fields['pathinfo'])=fill_method_array($server_value['pathinfo'],'/'.addcslashes($templeettestdir,"'\\\/").'test\d\/'.'JRDKGCTDYEZ/','JRDKGCTDYEZ');
      uasort($server_array['pathinfo'],'callback_valuelength');
    }
    
  if ($support_querystring)
    {
      list($server_array['querystring'],$fields['querystring'])=fill_method_array($server_value['querystring'],'/'.addcslashes($templeettestdir,"'\\\/").'test\d\/'.'JRDKGCTDYEZ/','JRDKGCTDYEZ');
      uasort($server_array['querystring'],'callback_valuelength');
    }
  
  trace_build("fill_server_array:\n",$server_array,$fields);
}

Function callback_valuelength($a,$b)
{
  $l1=strlen($a);
  $l2=strlen($b);
  
  if ($l1==$l2)
    return 0;
   
  if ($l1<$l2)
    return -1;
    
  return +1;     
}

Function builddefaultcode()
{
  global $server_value,$server_array,$fields;
  global $support_page,$support_index,$support_pathinfo,$support_querystring;
  
  if (!$support_querystring)
    return "";
    
  $tmparr=$server_array['querystring'];  
  foreach ($tmparr as $key => $value)
    {      
      if (
          (isset($server_array['page'][$key]) && $server_array['page'][$key]==$value) &&
          (isset($server_array['index'][$key])&& $server_array['index'][$key]==$value) &&
          (isset($server_array['pathinfo'][$key]) && $server_array['pathinfo'][$key]==$value)
         )
        {
          if (isset($server_array['page'][$key])) unset($server_array['page'][$key]);
          if (isset($server_array['index'][$key])) unset($server_array['index'][$key]);
          if (isset($server_array['pathinfo'][$key])) unset($server_array['pathinfo'][$key]);
          if (isset($server_array['querystring'][$key])) unset($server_array['querystring'][$key]);
        }       
    }
  $tmp_server_value=$server_value;
  $tmp_server_array=$server_array;  

#  print "error: ";
#  print "<pre>";
#  debug_print_r("builddefaultcode tmp_server_array:",$tmp_server_array,"==\n");
#  print_r($fields);
#  print "</pre>";
#  exit;



/*
  $trace="";
  foreach ($server_value as $type => $test_array)
    {
       $trace.=",$type";
    }
  $trace.="\n";  
  foreach ($server_array as $method => $test_array)
    {
      foreach ($test_array as $env => $reg)
        {
          $trace.="$method $env";
          foreach ($server_value as $type => $tmp)
            {
              if (isset($server_value[$type][$env]) && preg_match($reg,$server_value[$type][$env]))
                  $trace.=",X";
                else
                  $trace.=",";
            }
 
          $trace.="\n";
        }
    }

  file_put_contents("trace.csv",$trace);
*/

  $method_array=array();
  
  $n=0;
  $newmethod=1;
  while ($newmethod)
    {
#    debug_print_r("loop newmethod\n\n");
      $newmethod=0;
      reset($tmp_server_array);
      while ($newmethod==0 && list($method,$test_array)=each($tmp_server_array))
        {
#          print "method:$method==\n";
          reset($test_array);  
          while ($newmethod==0 && list($server_var,$test)=each($test_array))
            {
               $testok=1;
               reset($tmp_server_value);
               while ($testok && list($test_method,)=each($tmp_server_value))
                 {
#                 debug_print_r("server_var:",$server_var," test_method:",$test_method,"==\n");
#                       debug_print_r("test:",$test,"===",(isset($tmp_server_value[$test_method][$server_var])?$tmp_server_value[$test_method][$server_var]:"not set"),"===\n");
                   if ($method==$test_method)
                       {
                         if (!preg_match($test,$tmp_server_value[$test_method][$server_var]))
                           {
#                             debug_print_r("method == not testok \n");
#                             print '$reg=';var_export($test);print ";\n";
#                             print '$value=';var_export($tmp_server_value[$test_method][$server_var]);print ";\n";
                             $testok=0;
                           }  
                       }
                     else
                       {
                         if (isset($tmp_server_value[$test_method][$server_var]) && preg_match($test,$tmp_server_value[$test_method][$server_var]))
                           {
#                              debug_print_r("method != not testok \n");
                            $testok=0;
                           }  
                       }  
                 }
               if ($testok)
                 {
                   $newmethod=1;
                   $method_array[$method]=array($server_var => $test);
                   unset($tmp_server_value[$method]);
                   unset($tmp_server_array[$method]);
#                   debug_print_r("newmethod:",$method,"==\n\n");
                 } 
            }
            
        }          
    }
  
#  print "method_array:\n";
#  print_r($method_array);
         
  $testcode="\$pathfromquerystring=0;\n\$pathinfo=0;\n\$querystring='';\nif (getpost('_redo')!='') \n  \$path=getpost('_redo');\n";
  

  foreach ($method_array as $method => $array_test)  
    {
//    debug_print_r("methodloop:",$method,"==\n");
    
      $teststring="";
      foreach ($array_test as $var => $test)  
        {
          if (isset($fields[$method][$var]))
              $teststring.="&& isset(\$_SERVER_TEST['$var']) && preg_match('".$test."',\$_SERVER_TEST['$var'],\$res_$var) \n";
            else
              $teststring.="&& isset(\$_SERVER_TEST['$var']) && preg_match('".$test."',\$_SERVER_TEST['$var']) \n";
        }
      $testcode.="else\nif (".substr($teststring,3).")\n  { /* $method */\n";
      
      $qs=0;
      $path=0;
      $qs_varname="";
      $path_varname="";   
      
      foreach ($array_test as $var => $test)  
        {
          if (isset($fields[$method][$var]))
            {
              if (!$path && isset($fields[$method][$var]['path']))      
                {
                  $path=1;      
                  $testcode.="    \$path=\$res_${var}[".$fields[$method][$var]['path']."];\n";
                }
              if (!$qs && isset($fields[$method][$var]['qs']))      
                {
                  $qs=1;      
                  if ($fields[$method][$var]['qs']==1)
                      $testcode.="    \$querystring=\$res_${var}[1];\n";
                    else  
                      $testcode.="    if (isset(\$res_${var}[2])) \$querystring=\$res_${var}[2];\n";
                }
            }     
        }
      
      if (!$path && $method!="querystring")
        {
          foreach ($fields[$method] as $var => $value)        
            {
              if ($qs)
                  {
                    if (
                         isset($fields[$method][$var]['path']) &&
                         (
                           $path_varname=="" ||
                           (
                             isset($fields[$method][$path_varname]['qs']) &&
                             !isset($fields[$method][$var]['qs']) 
                           )  
                         )
                       )      
                      {
                        $path_varname=$var;      
                      }
                  }
                else
                  {
                    if (
                         isset($fields[$method][$var]['path']) &&
                         (
                           $path_varname=="" ||
                           (
                             !isset($fields[$method][$path_varname]['qs']) &&
                             isset($fields[$method][$var]['qs']) 
                           )  
                         )
                       )      
                      {
                        $path_varname=$var;      
                      }
                         
                  }              
            }
        }

      if (!$qs)
        {
          if ($path_varname!="" && isset($fields[$method][$path_varname]['qs']))
              {
                $qs_varname=$path_varname;
              }
            else
              {
                      
                foreach ($fields[$method] as $var => $value)        
                  {
                    if ($qs)
                        {
                          if (
                               isset($fields[$method][$var]['qs']) &&
                               (
                                 $qs_varname=="" ||
                                 (
                                   isset($fields[$method][$qs_varname]['path']) &&
                                  !isset($fields[$method][$var]['path']) 
                                 )  
                               )
                             )      
                            {
                              $qs_varname=$var;      
                            }
                        }
                      else
                        {
                          if (
                               isset($fields[$method][$var]['qs']) &&
                               (
                                 $qs_varname=="" ||
                                 (
                                   !isset($fields[$method][$qs_varname]['path']) &&
                                   isset($fields[$method][$var]['path']) 
                                 )  
                               )
                             )      
                            {
                              $qs_varname=$var;      
                            }
                         
                        }              
                  }
                      
              }        
        }
        
      if ($path_varname==$qs_varname)  
        {
          if ($path_varname!="")
            {
              $testcode.="    preg_match('".$server_array[$method][$path_varname]."',\$_SERVER_TEST['$path_varname'],\$res);\n";
              $testcode.="    \$path=\$res[".$fields[$method][$path_varname]['path']."];\n";
              $testcode.="    if (isset(\$res[".$fields[$method][$qs_varname]['qs']."])) \$querystring=\$res[".$fields[$method][$qs_varname]['qs']."];\n";
              $path_varname="";
              $qs_varname="";
            }      
        }

      if ($qs_varname!="")
        {
          $testcode.="    preg_match('".$server_array[$method][$qs_varname]."',\$_SERVER_TEST['$qs_varname'],\$res);\n";
          $testcode.="    if (isset(\$res[".$fields[$method][$qs_varname]['qs']."])) \$querystring=\$res[".$fields[$method][$qs_varname]['qs']."]; \n";
        }                    
      if ($path_varname!="")
        {
          $testcode.="    preg_match('".$server_array[$method][$path_varname]."',\$_SERVER_TEST['$path_varname'],\$res);\n";
          $testcode.="    \$path=\$res[".$fields[$method][$path_varname]['path']."];\n";
        }  
        
      if ($method=='querystring')
        {
          $testcode.=<<<EOF
if (!preg_match("/^(?:file=)?(.*?)(?:&.*)?$/",\$querystring,\$res))
die("can't get path from querystring");
else
\$path=\$res[1];

\$pathfromquerystring=1;

EOF;

        }     
        
      if ($method=='pathinfo')
        {
          $testcode.="    \$pathinfo=1;\n";      
        }                 
      
      $testcode.="}\n";
    }  
 
  return $testcode;
}

Function checkcode($code)
{
  global $server_value;
  global $support_page,$support_index,$support_pathinfo;
  global $templeettestdir;
 
# print "server_value:\n";
# print_r($server_value);
 
 
  if ($support_page)
    {
      $_SERVER_TEST=$server_value['page'];
      unset($path);
      $res=@eval($code);
      if ($res===FALSE)
        return "parse error page: ".$code;
      if (!isset($path) || !preg_match("/".addcslashes($templeettestdir,"'\\\/")."test\d\/JRDKGCTDYEZ/",$path) || $pathfromquerystring!=0 || $pathinfo!=0)
	      return "page";
    }
    
  if ($support_index)
    {
      $_SERVER_TEST=$server_value['index'];
      unset($path);
      $res=@eval($code);
      if ($res===FALSE)
        return "parse error index: ".$code;
      if (!isset($path) || !preg_match("/".addcslashes($templeettestdir,"'\\\/")."test\d\//",$path) || $pathfromquerystring!=0 || $pathinfo!=0)
	      return "index";
    }
    
  if ($support_pathinfo)
    {
      $_SERVER_TEST=$server_value['pathinfo'];
      unset($path);
      $res=@eval($code);
      if ($res===FALSE)
        return "parse error pathinfo: ".$code;
      if (!isset($path) || $path!="JRDKGCTDYEZ" || $pathfromquerystring!=0 || $pathinfo==0)
	      return "pathinfo";
    }
    
  $_SERVER_TEST=$server_value['querystring'];
  unset($path);
  $res=eval($code);
  if ($res===FALSE)
    return "parse error querystring: ".$code;
 if (!isset($path) || $path!="RSYTEZJHGI" || $pathfromquerystring==0 || $pathinfo!=0)
    return "querystring";
  return 0;
}

Function fill_method_array($arr,$pattern,$replacement)
{
//debug_print_r("pattern:",$pattern,"===\n");
  $res=array();
  $res_field=array();
  
  reset($arr);
  while(list($name,$value)=each($arr))
    {
      if (is_string($value))
        {
          $value=preg_replace($pattern,$replacement,$value);
                
          $found_path=preg_match('/(?:JRDKGCTDYEZ|KLOHTFGRED)/',$value);
          $found_qs=preg_match('|RSYTEZJHGI|',$value);
              
          if ($found_path || 
              $found_qs || 
              preg_match('|templeet\.php|',$value) ||
              (preg_match('/^\d{3}$/',$value) && preg_match('/STATUS/',$name)))
            {
              $res[$name]='/^'.
                  preg_replace(
		   	            array('|KLOHTFGRED|','|JRDKGCTDYEZ|','|\?RSYTEZJHGI|','|RSYTEZJHGI|','|https?:|','|templeet\.php|'),
			              array('((?:[^\/]+\/)*)','([^\?]*?)','(?:\?(.*))?','(.*)','https?:','(?:templeet\.php|index\.php)'),
			              addcslashes($value,'\/\\')).
                  '$/';
                  
                  
//               debug_print_r("res:",$res[$name],"===$name===",$value,"===\n");   

	      if ($found_path && $found_qs)
		      $res_field[$name]=array('path'=>1,'qs'=>2);
        elseif ($found_path)  
		      $res_field[$name]=array('path'=>1);
        elseif ($found_qs)  
		      $res_field[$name]=array('qs'=>1);
            }
        }
    }
        
//    print "fill_method_array:\n";
    
//    print_r($arr);
//    print_r($res);
//    print_r($res_field);
        
  return array($res,$res_field);
        
}

function trace_build() {
  
  if (TRACE_BUILD)
    call_user_func_array('debug_print_r', func_get_args());

}
