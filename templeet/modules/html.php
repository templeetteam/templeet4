<?php
/*
 * This code has been developed by:
 *
 * Pascal COURTOIS
 *
 */

function return_makeselect($array)
{
  $name = current($array);
  next($array);
  $name = eval_list($name);

  $default = current($array);
  next($array);
  $default = eval_list($default);

  $res = '';

  $val = current($array);
  next($array);
  $val = eval_list($val);

  $onchange = '';

  if (!is_array($val))
    throw new TempleetError("makeselect: 3rd parameter must be an array");

  $nokey = 0;
  if (key($val) === 0) {
      if (array_key_last($val) === count($val) - 1)
        $nokey = 1;
      reset($val);
  }

  while (list($key, $value) = each($val)) {
    if ($nokey)
      $key = $value;

    if ($key == $default)
      $res .= "<option value=\"" . htmlentities($key) . "\" selected=\"selected\">" . htmlentities($value) . "</option>\n";
    else
      $res .= "<option value=\"" . htmlentities($key) . "\" >" . htmlentities($value) . "</option>\n";
  }

  if (list(, $val) = each($array)) {
    $val = eval_list($val);
    $onchange = "onchange=\"$val\" ";
  }

  return "<select name=\"$name\" $onchange>\n$res</select>";
}

function return_makeselectnum($array)
{
  $name = current($array);
  next($array);
  $name = eval_list($name);

  $default = current($array);
  next($array);
  $default = eval_list($default);

  $min = current($array);
  next($array);
  $min = eval_list($min);

  $max = current($array);
  next($array);
  $max = eval_list($max);

  $onchange = '';
  if (list(, $val) = each($array)) {
    $val = eval_list($val);
    $onchange = "onchange=\"$val\" ";
  }

  if (!is_numeric($min) || !is_numeric($max))
    return "min and max must be integers";

  if ($min > $max)
    return "min must be smaller than max";

  $res = '';
  while ($min <= $max) {
    if ($min == $default)
      $res .= "<option value=\"$min\" selected=\"selected\">$min</option>\n";
    else
      $res .= "<option value=\"$min\" >$min</option>\n";
    $min++;
  }

  return "<select name=\"$name\" $onchange>\n$res</select>";
}

function return_makehidden($array)
{
  $val = current($array);
  next($array);
  $val = eval_list($val);

  if (!is_array($val))
    throw new TempleetError("makehidden: parameter must be an array");

  $res = '';
  while (list($key, $value) = each($val)) {
    $res .= "<input type=\"hidden\" name=\"" . htmlentities($key) . "\" value=\"" . htmlentities($value) . "\" />\n";
  }
  return $res;
}

function html_return()
{
  return array('makeselect', 'makeselectnum', 'makehidden');
}
