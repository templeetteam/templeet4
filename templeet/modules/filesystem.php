<?php
/*
 * This code has been developed by:
 *
 * Fabien Penso
 * Pascal COURTOIS
 *
 */


function return_sendfile($array)
{
  global $sendfile;

  $val = current($array);
  next($array);
  $filename = eval_list($val);

  if (isset($sendfile))
    return -2;
  $sendfile = fopen($filename, 'rb');

  if ($sendfile === false)
    return -1;

  return 0;
}

function return_writefile($array)
{
  global $config;

  $val = current($array);
  next($array);
  $file = eval_list($val);
  $val = current($array);
  next($array);
  $text = eval_list($val);

  if (list(, $val) = each($array)) {
    $umask = eval_list($val);
    umask($umask);
  } else
    umask(022);

  $value = '.' . rand();

  $m_error = 0;

  while (file_exists($file . '.lock')) {
    clearstatcache();
    usleep(100);
  }
  touch($file . '.lock');
  if (!$fd = @fopen($file . $value, 'wb'))
    return -1;

  $txt_len = strlen($text);

  if (@fputs($fd, $text, $txt_len) != $txt_len)
    $m_error = -2;
  @fclose($fd);

  // this results in race condition on windows
  if ($config['windows']) @unlink($file);

  @rename($file . $value, $file);
  @unlink($file . '.lock');

  umask(022);

  return $m_error;
}

function filesystem_recursivedelete($dir1, $dir2, $dirmask, $filemask, $rmdir)
{
  $handle = @opendir("$dir1$dir2");
  if (!$handle)
    return;
  while ($name = readdir($handle)) {
    if ($name != '.' && $name != '..') {
      if (is_dir("$dir1$dir2/$name")) {
        if (!@preg_match($dirmask, "$dir2/$name"))
          filesystem_recursivedelete($dir1, "$dir2/$name", $dirmask, $filemask, $rmdir);
      } elseif (is_file("$dir1$dir2/$name")) {
        if (@preg_match($filemask, "$dir2/$name")) {
          @unlink("$dir1$dir2/$name");
        }
      }
    }
  }
  closedir($handle);
  if ($rmdir)
    @rmdir("$dir1$dir2");
}

function return_deletemask($array)
{
  global $config;

  $dir = current($array);
  next($array);
  $dir = eval_list($dir);
  if (empty($dir))
    throw new TempleetError("function deletemask: base directory (first argument) can't be empty\n");

  if (substr($dir, -1, 1) == "/")
    $dir = substr($dir, 0, -1);

  if ($dir == '')
    $dir = "/.";

  $dirmask = current($array);
  next($array);
  $dirmask = eval_list($dirmask);

  if (empty($dirmask))
    $dirmask = "/^$/";

  $filemask = current($array);
  next($array);
  $filemask = eval_list($filemask);

  if (empty($filemask))
    $filemask = "/.*/";
  elseif (!preg_match('/^(\W).*\1[imsxeADSUXJu]*$/', $filemask))
    $filemask = "/" . addcslashes($filemask, "/.") . "/";

  $rmdir = current($array);
  next($array);
  if (isset($rmdir))
    $rmdir = (bool)eval_list($rmdir);
  else
    $rmdir = 0;

  filesystem_recursivedelete($dir, '', $dirmask, $filemask, $rmdir);
}

function return_deletepath($array)
{
  global $config;

  $path = current($array);
  next($array);
  $path = eval_list($path);
  if (empty($path))
    throw new TempleetError("function deletedir: path can't be empty\n");

  if (is_file($path))
    unlink($path);
  elseif (is_dir($path))
    filesystem_recursivedelete($path, '', "/^$/", "/.*/", 1);
}

function safe_copy($from, $to)
{
  global $safecopysign;
  if (!isset($safecopysign))
    $safecopysign = array();

  $tmp = strrpos($to, '/');
  if ($to[$tmp] == '/')
    $tmp++;
  $filename = substr($to, $tmp);
  $directory = substr($to, 0, $tmp);

  if (
    !isset($safecopysign[$directory]) &&
    file_exists("$directory/.file.sign")
  ) {
    if ($fd = @fopen("$directory/.file.sign", 'rb')) {
      $output = '';
      while ($tmp = fread($fd, 65536)) {
        $output .= $tmp;
      }
      fclose($fd);
    }
    $safecopysign[$directory] = unserialize($output);
  } else {
    $safecopysign[$directory] = array();
  }

  $checksum = md5_file($from);

  if (
    file_exists($to) &&
    isset($safecopysign[$directory][$filename]) &&
    $safecopysign[$directory][$filename] != @md5_file($to)
  ) {
    $tmp = strrpos($filename, '.');
    if ($to[$tmp] == '.')
      $tmp++;
    $rfile = substr($filename, 0, $tmp);
    $vfile = substr($filename, $tmp);

    $filename = $rfile . '-update' . $vfile;
  }

  @copy($from, $directory . '/' . $filename);

  if (!$decperms = @fileperms($from)) {
    templeet_chmod($directory . '/' . $filename, 0644);
  } else {
    $octalperms = substr(sprintf('%o', $decperms), -4);
    templeet_chmod($directory . '/' . $filename, octdec($octalperms));
  }


  $safecopysign[$directory][$filename] = $checksum;

  return_writefile(array("writefile", "$directory/.file.sign", serialize($safecopysign[$directory])));
  return $directory . $filename;
}

function recursive_markforcopy($from, $relativepath)
{

  $listfile_array = array();
  if (is_dir($from . $relativepath)) {
    $handle = opendir($from . $relativepath);
    while (($file = readdir($handle)) !== false) {
      if ($file == '.' || $file == '..') continue;
      if (is_dir($from . $relativepath . $file)) {
        $tmp = recursive_markforcopy($from, $relativepath . $file . "/");
        if ((is_countable($tmp) ? count($tmp) : 0) == 0)
          array_push($listfile_array, $relativepath . $file . "/");
        else
          $listfile_array = array_merge($listfile_array, $tmp);
      } elseif (is_file($from . $relativepath . $file))
        array_push($listfile_array, $relativepath . $file);
    }

    closedir($handle);
  }

  return $listfile_array;
}


function return_copy($array)
{

  $val = current($array);
  next($array);
  $from = eval_list($val);

  $val = current($array);
  next($array);
  $to = eval_list($val);

  $val = current($array);
  next($array);
  $safe = eval_list($val);

  $listfile_array = array();
  if (is_dir($from)) {
    if (substr($from, -1, 1) == "/")
      $from = substr($from, 0, -1);

    if (substr($to, -1, 1) == "/")
      $to = substr($to, 0, -1);

    if (file_exists($to) && !is_dir($to))
      return -1;

    $listfiles = recursive_markforcopy($from, "/");
    foreach ($listfiles as $file) {
      $dest = $to . $file;
      if (substr($dest, -1, 1) == "/") {
        createdir($dest);
        continue;
      }

      $dirto = dirname($dest);
      if (!file_exists($dirto))
        createdir($dirto);

      if (!is_dir($dirto)) {
        if ($safe)
          return array();
        else
          return;
      }
      if ($safe)
        $listfile_array[] = safe_copy($from . $file, $dest);
      else
        @copy($from . $file, $dest);
    }
  } elseif (is_file($from)) {
    $file = $from;
    if (file_exists($to) && is_dir($to)) {
      $shortname = basename($file);
      if (substr($to, -1, 1) != "/")
        $to .= "/";

      $dest = $to . $shortname;
    } else {
      $dest = $to;
    }
    if ($safe)
      $listfile_array[] = safe_copy($file, $dest);
    else
      @copy($file, $dest);
  }

  if ($safe)
    return $listfile_array;
}



function getwebfile($url, $dir = '', $redirectnum = 10)
{
  global $global_var, $config;

  if (!preg_match("#^(http:|https:)?(?://([^/:]+)(?::(\d+))?)?(.+)#", $url, $res))
    return -3;

  $filename = '';
  if (ini_get("allow_url_fopen")) {
    $fpread = fopen($url, "r");
    if ($fpread !== FALSE && preg_match('/HTTP\/\d\.\d (\d\d\d)/', $http_response_header[0], $res))
      $global_var->getwebfile_status = (int)$res[1];
    else
      return $global_var->getwebfile_status = -1;

    if (is_dir($dir)) {

      end($http_response_header);
      while ($filename == '' && !preg_match("#^HTTP/#", current($http_response_header))) {
        if (preg_match('/^Content-disposition: attachment;\s+filename=(\S+)/i', current($http_response_header), $tmp_array)) {
          $filename = $tmp_array[1];
          $filename = preg_replace('/"(.*)"/', "$1", $filename);
        }
        prev($http_response_header);
      }
    }
  } else {
    if ($res[1] == "https:")
      return -4;

    if ($res[2] == '') {
      preg_match("#//(.*)#", $config['site_url'], $res2);
      $host = $res2[1];
    } else
      $host = $res[2];

    if ($res[3] == '')
      $port = 80;
    else
      $port = $res[3];

    $uri = $res[4];

    $fpread = @fsockopen($host, $port, $errno, $errstr, 10);
    if (!$fpread)
      return $global_var->getwebfile_status = -1;
    fputs($fpread, "GET " . $uri . " HTTP/1.0\r\n");
    fputs($fpread, "Host: " . $host . "\r\n\r\n");

    $out = fgets($fpread);
    if (preg_match('/HTTP\/\d\.\d (\d\d\d)/', $out, $res))
      $global_var->getwebfile_status = (int)$res[1];
    else
      return $global_var->getwebfile_status = -1;

    $redirect = $global_var->getwebfile_status == 301 ||
      $global_var->getwebfile_status == 302 ||
      $global_var->getwebfile_status == 307;

    if ($redirectnum < 0) {
      $global_var->getwebfile_status = 310;
      return '';
    }

    while (!feof($fpread)) {
      $out = fgets($fpread);
      if (preg_match('/^Content-disposition: attachment;\s+filename=(\S+)/i', $out, $tmp_array)) {
        $filename = $tmp_array[1];
        $filename = preg_replace('/"(.*)"/', "$1", $filename);
      }
      if ($redirect && preg_match('/^Location: ([^\r\n]*)/i', $out, $tmp_array)) {
        return getwebfile($tmp_array[1], $dir, $redirectnum - 1);
      }
      if ($out == "\r\n")
        break;
    }

    if ($redirect) {
      $global_var->getwebfile_status = 410;
      return '';
    }
  }

  if (preg_match('/\.php\w?$/', $filename))
    $filename .= ".txt";

  if ($dir == '') {
    $out = '';
    while (!feof($fpread)) {
      $tmp = fread($fpread, 65536);
      $out .= $tmp;
    }
    fclose($fpread);
    return $out;
  }

  if (is_dir($dir)) {
    if ($dir != '' && $dir[strlen($dir) - 1] != "/")
      $dir .= "/";

    if ($filename == '') {
      $tmp = substr($uri, strrpos($uri, '/') + 1);
      if (strpos($tmp, '?'))
        $tmp = substr($tmp, 0, strpos($tmp, '?'));
      if ($tmp == '/' || $tmp == '')
        $tmp = 'index.html';

      $filename = $tmp;
    }

    if (file_exists($dir . $filename)) {
      $filename .= '~';
      if (file_exists($dir . $filename)) {
        $i = 1;
        while (file_exists($dir . $filename . $i))
          $i++;
        $filename .= $i;
      }
    }

    $fullname = $dir . $filename;
  } else
    $fullname = $dir;

  if (!$fd = @fopen($fullname, 'wb'))
    return -2;

  while (!feof($fpread)) {
    $tmp = fread($fpread, 65536);
    fwrite($fd, $tmp);
  }
  @fclose($fd);
  return $filename;
}

function filesystem_return()
{
  return array('sendfile', 'writefile', 'deletemask', 'deletepath', 'copy', 'getwebfile');
}
