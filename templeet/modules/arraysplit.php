<?php
/*
 * This code has been developed by:
 *
 * Pascal COURTOIS
 *
 */

function return_arraysplit($array)
{
  global $global_var;

  $val = current($array);
  next($array);
  $splitarray = eval_list($val);

  if (!isset($splitarray) ||  !is_array($splitarray))
    return;

  while (list(, $name) = each($array)) {
    return_setref(array($name, current(eval_list(each($splitarray)))));
  }

  return;
}

function arraysplit_return()
{
  return array('arraysplit');
}
