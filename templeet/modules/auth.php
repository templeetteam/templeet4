<?php
/*
 * This module handles generic authentication
 *
 * This code has been developed by:
 *
 * Pascal Courtois
 *
 */

define("AUTH_ENTROPY", 32);
define("POST_COOKIE", "TEMPLEET_POST");

class auth
{
  public static int $uid = -1;
  public static int $userspace = -1;
  public static string $login = '';
  public static int $admin = 0;
  public static int $userspace_admin = 0;
  public static string $userpattern = "\w\.\-\+@";
  public static string $passpattern = " -\x7E";
  public static array $priv = array();
  public static $handle;
  public static $filehandle;
  public static $config;
  public static $profile;

  public static function create($type)
  {
    global $config;
    if (include_once($config['modulesdir'] . "auth/auth_" . $type . '.php')) {
      $classname = 'class_auth_' . $type;
      return new $classname();
    }
  }

  static function getpriv($autharea)
  {
    if (auth::$admin)
      return 10;

    if ($autharea == "ADMIN")
      return 0;

    if (auth::$uid < 0)
      return 0;

    if (!preg_match("/^(\{(\d*)\})?(.+)/", $autharea, $res))
      return -1;

    if ($res[1] == "{}")
      $userspace = auth::$userspace;
    else {
      if ($res[2] == '')
        $userspace = 0;
      else
        $userspace = $res[2];
    }

    if (auth::$userspace_admin && $userspace == auth::$userspace)
      return 10;

    if ($userspace == 0)
      $tmp_userspace = '';
    else
      $tmp_userspace = "{" . $res[2] . "}";
    $autharea = $res[3];

    if (isset(auth::$priv["${tmp_userspace}_DEL_$autharea"]))
      return 10;

    $max = 0;
    if (isset(auth::$priv["$tmp_userspace$autharea"]))
      $max = auth::$priv["$tmp_userspace$autharea"];
    if (isset(auth::$priv["${tmp_userspace}_ED_$autharea"]) && $max < auth::$priv["${tmp_userspace}_ED_$autharea"])
      $max = auth::$priv["${tmp_userspace}_ED_$autharea"];

    return $max;
  }
} // class auth

global $global_var;
if (!isset($global_var)) 
    $global_var = new stdClass();
$global_var->auth_uid = -1;

function return_setauth($expr)
{
  /*
   * ~setauth(userspace,login,passwd)
   */
  global $global_var;

  $val = current($expr);
  next($expr);
  $userspace = (int)eval_list($val);

  $val = current($expr);
  next($expr);
  $user = strtolower(eval_list($val));

  $val = current($expr);
  next($expr);
  $pass = eval_list($val);

  $val = current($expr);
  next($expr);
  $remember = eval_list($val);
  if ($remember != 1)
    $remember = 0;

  $user_info = auth_check($userspace, $user, $pass);

  if (is_array($user_info)) {
    $secretkey = auth_getsecretkey();

    auth::$uid = $user_info['uid'];
    auth::$login = $user;
    auth::$userspace = $userspace;
    $global_var->auth_userspace = $userspace;
    $global_var->auth_user = $user_info['login'];
    $global_var->auth_uid = $user_info['uid'];
    if (auth::$uid != 0)
      $global_var->auth_email = $user_info['email'];
    else {
      global $config;
      $global_var->auth_email = $config["emailadmin"];
    }
    $global_var->auth_nickname = $user_info['nickname'];
    auth::$priv = $user_info['priv'];

    auth_setcookie($userspace, $user, $user_info['uid'], $remember, $secretkey);

    return 0;
  } else {
    /*
           * Error while login
           */
    return $user_info;
  }
}

function return_getauth($expr)
{
  /*
   *  ~getauth(autharea,requestpriv,authpage,noprivpage)
   */

  global $config, $global_var, $cache;

  $val = current($expr);
  next($expr);
  $autharea = eval_list($val);

  $val = current($expr);
  next($expr);
  $requestpriv = eval_list($val);

  $val = current($expr);
  next($expr);
  if (isset($val))
    $authpage = eval_list($val);
  else
    $authpage = $config['default_auth_form'] . $global_var->dotlang;

  if (headers_sent()) {
    echo "~getauth() function must be the very first function called in a template";
    templeet_exit();
  }

  $cache = 0;
  $global_var->HTTP_nocache = 1;

  $authcookie = '';
  if (isset($_COOKIE[$config['authcookie']]))
    $authcookie = $_COOKIE[$config['authcookie']];

  if (
    $authcookie == '' ||
    (!preg_match("/^(\d+):((?:\d+\|)?[" . auth::$userpattern . "]+):(\d+):(\d+):(\d):(\w{16})(\w{40})$/", $authcookie, $arrayres)
      &&
      !preg_match("/^(\d+):((?:\d+\|)?[" . auth::$userpattern . "]+):(\d+):(\d+):(\d):(BADAUTH)$/", $authcookie, $arrayres))
  ) {
    if ($autharea == '' && $requestpriv < 0)
      return;
    auth_gotoauthform($authpage);
  }

  if ($arrayres[6] == 'BADAUTH') {
    $global_var->lastauth_user = $arrayres[2];
    $global_var->lastauth_userspace = $arrayres[1];
    if ($autharea == '' && $requestpriv < 0)
      return;
    auth_gotoauthform($authpage);
  } else {
    $secretkey = auth_getsecretkey();
    // verify cookie validity
    if (
      sha1($arrayres[1] . ":" . $arrayres[2] . ":" . $arrayres[3] . ":" . $arrayres[4] . ":" . $arrayres[5] . ":" . $arrayres[6] . ":" . $secretkey) != $arrayres[7] ||
      (($arrayres[4] < time()) && ($arrayres[4] != 0))
    ) {
      auth_setcookie($arrayres[1], $arrayres[2], (int)($arrayres[3]), $arrayres[5], -1);
      if ($autharea == '' && $requestpriv < 0)
        return;
      auth_gotoauthform($authpage);
    }

    auth::$userspace = $arrayres[1];
    auth::$login = $arrayres[2];
    auth::$uid = (int)($arrayres[3]);

    $user_info = auth_getinfo(auth::$uid);
    if (is_array($user_info) && isset($user_info['valid']) && $user_info['valid'] > 0) {
      auth::$priv = $user_info['priv'];
      $global_var->auth_userspace = auth::$userspace = $user_info['userspace'];
      auth::$userspace_admin = auth::$userspace == auth::$uid;
      auth::$admin = (auth::$uid == 0 || !empty($user_info['priv']['ADMIN']));

      $global_var->auth_uid = auth::$uid;
      $global_var->auth_user = auth::$login;
      if (auth::$uid != 0)
        $global_var->auth_email = $user_info['email'];
      else
        $global_var->auth_email = $config["emailadmin"];
      $global_var->auth_nickname = $user_info['nickname'];

      auth_setcookie(auth::$userspace, auth::$login, auth::$uid, $arrayres[5], $secretkey);

      if (
        auth::$admin || $autharea == '' || preg_match("/^\{" . auth::$uid . "\}/", $autharea) ||
        preg_match("/^\{" . auth::$userspace . "\}$/", $autharea) ||
        auth::getpriv($autharea) >= $requestpriv
      ) {

        if (isset($_COOKIE[POST_COOKIE])) {
          if (get_magic_quotes_gpc())
            $post = stripslashes($_COOKIE[POST_COOKIE]);
          else
            $post = $_COOKIE[POST_COOKIE];

          $tmparray = @unserialize($post);
          if ($tmparray !== FALSE)
            while (list($key, $value) = each($tmparray)) {
              if ($value != "deleted")
                $_POST[$key] = $value;
            }
          setcookie(POST_COOKIE, '', 0, $config['dir_installed']);
        }
      } else {
        $val = current($expr);
        next($expr);
        if (isset($val))
          $noprivpage = eval_list($val);
        else
          $noprivpage = $config['default_no_priv'] . $global_var->dotlang;

        include_once($config['modulesdir'] . 'url.php');
        header('Location: ' . return_absolute_templeet(array()) . preg_replace("|^/|", '', $noprivpage));
        templeet_exit();
      }
    } else {
      unset($_COOKIE[$config['authcookie']]);
      auth_setcookie(auth::$userspace, auth::$login, auth::$uid, $arrayres[5], -1);
      auth_gotoauthform($authpage);
    }
  }

  return;
}

function auth_setcookie($userspace, $login, $uid, $remember, $secretkey)
{

  global $config;

  $seed = auth_mkrdstr(16);

  $expirationdate = 0;
  if ($remember) {
    if (isset($config['authcookierememberexpire']))
      $expiration = $config['authcookierememberexpire'];
    else
      $expiration = 600;
  } else {
    if (isset($config['authcookieexpire']))
      $expiration = $config['authcookieexpire'];
    else
      $expiration = 0;
  }

  if ($expiration != 0)
    $expirationdate = time() + $expiration;
  else
    $expirationdate = 0;

  if ($expirationdate > 2147483647)
    $expirationdate = 2147483647;

  if ($remember)
    $expirationcookie = $expirationdate;
  else
    $expirationcookie = 0;

  if ($secretkey < 0) {
    if ($secretkey < -1 || (isset($config['removeauthcookie']) && $config['removeauthcookie']))
      setcookie($config['authcookie'], '', 0, $config['dir_installed'], $config['auth_domain']);
    else
      setcookie($config['authcookie'], "$userspace:$login:$uid:$expirationdate:$remember:BADAUTH", $expirationcookie, $config['dir_installed'], $config['auth_domain']);

    setcookie($config['authcookie'] . "_priv", '', 0, $config['dir_installed'], $config['auth_domain']);
    setcookie($config['authcookie'] . "_nick", '', 0, $config['dir_installed'], $config['auth_domain']);
  } else {
    $value = "$userspace:$login:$uid:$expirationdate:$remember:" . $seed . sha1("$userspace:$login:$uid:$expirationdate:$remember:$seed:$secretkey");
    setcookie($config['authcookie'], $value, $expirationcookie, $config['dir_installed'], $config['auth_domain']);

    global $config, $global_var;

    reset(auth::$priv);
    $priv = '';
    while (list($key, $value) = each(auth::$priv))
      $priv .= "($key;$value)";

    if (auth::$admin && !isset(auth::$priv['ADMIN']))
      $priv .= "(ADMIN;1)";
    if (!auth::$admin && auth::$userspace_admin && !isset(auth::$priv['{}ADMIN']))
      $priv .= "({}ADMIN;1)";

    setcookie($config['authcookie'] . "_priv", $priv, $expirationcookie, $config['dir_installed'], $config['auth_domain']);
    setcookie(
      $config['authcookie'] . "_nick",
      $global_var->auth_user . ":" . $global_var->auth_nickname . ":" . $global_var->auth_uid,
      $expirationcookie,
      $config['dir_installed'],
      $config['auth_domain']
    );
  }
}

function auth_getconfig()
{
  global $config;

  if (isset(auth::$config))
    return;
  include_once($config['modulesdir'] . "fieldfileaccess.php");

  auth::$config = ffa_readfile($config['authconfigfile']);
  if (!is_array(auth::$config))
    throw new TempleetError('Error in auth_getconfig. Bad format for authconfigfile');
}

function auth_getsecretkey()
{
  global $config;

  if (!isset(auth::$config['key']) || !is_string(auth::$config['key']) || strlen(auth::$config['key']) != AUTH_ENTROPY) {
    auth_getconfig();

    if (is_array(auth::$config) && (!isset(auth::$config['key']) || strlen(auth::$config['key']) != AUTH_ENTROPY)) {
      $savauthconfig = 'templeet/auth/config_ori.php';
      if (file_exists($savauthconfig))
        throw new TempleetError("$savauthconfig already exists. Can't write backup config file.");

      auth::$config['key'] = auth_mkrdstr(AUTH_ENTROPY);

      ffa_writefile($savauthconfig, auth::$config);

      include_once($config['modulesdir'] . "authedit.php");
      $res = authedit::writeconfigfile();
      if ($res != 0)
        throw new TempleetError("Can't write auth config file.");
    }
  }

  return auth::$config['key'];
}

function return_auth_userspace($expr)
{
  include_authmodule();
  $res = auth::$handle->userspace();
  if (is_array($res))
    $res[0] = "admin";
  return $res;
}

function auth_mkrdstr($nb)
{
  $charset = "ABCDEFGHIJKLMNOPQSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  $charsetlength = strlen($charset);
  $res = '';
  $i = 0;
  while ($i < $nb) {
    $res .= $charset[crand(0, $charsetlength - 1)];
    $i++;
  }
  return $res;
}

function auth_check($userspace, $user, $pass)
{
  global $config;

  $userspace = (int)$userspace;
  if ($userspace == 0 && strtolower($user) == 'admin' && return_auth_getmethod() != "file") {
    include_once($config['modulesdir'] . "auth/auth_file.php");

    if (!isset(auth::$filehandle))
      auth::$filehandle = new class_auth_file;
    return auth::$filehandle->checkauth($userspace, $user, $pass);
  } else {
    include_authmodule();
    return auth::$handle->checkauth($userspace, $user, $pass);
  }
}

function auth_getinfo($user, $userspace = -1)
{
  global $config;
  if (is_int($user) && $user == 0 && return_auth_getmethod() != "file") {
    include_once($config['modulesdir'] . "auth/auth_file.php");

    if (!isset(auth::$filehandle))
      auth::$filehandle = new class_auth_file;

    return auth::$filehandle->getinfo($user, $userspace);
  } else {
    include_authmodule();
    return auth::$handle->getinfo($user, $userspace);
  }
}

function auth_getbyuid($uid, $field)
{
  include_authmodule();

  $uid = (int)$uid;

  if ($uid == 0) {
    $info = auth::$filehandle->getinfo($uid);
  } else
    $info = auth::$handle->getinfo($uid);

  if (!is_array($info))
    return $info;

  if (
    $field == "login" ||
    $field == "nickname" ||
    $field == "userspace"
  ) {
    if (!isset($info[$field]))
      return "field:$field not found";

    return $info[$field];
  }

  if (auth::$admin || (auth::$userspace_admin && auth::$userspace == $info["userspace"])) {
    if (
      $field == "valid" ||
      $field == "email" ||
      $field == "ipaddr" ||
      $field == "creation"
    ) {
      if (!isset($info[$field]))
        return "field:$field not found";

      return $info[$field];
    }

    if ($field == "area") {
      $priv_area = $info["priv"];
      foreach ($priv_area as $zone => $value) {
        preg_match('/^(?:{([^}]+)})?(.*)/', $zone, $res);

        if ($res[1] == '') {
          $area_userspace = '';
        } else {
          $area_userspace = "{" . $res[1] . "}";
        }

        $area_name = $res[2];

        if (!auth::$admin && ($res[1] != auth::$userspace || !auth::$userspace_admin)) {
          if (substr($area_name, 0, 5) == "_DEL_") {
            unset($priv_area[$zone]);
          } else {
            if (preg_match('/^_ED_(.*)/', $area_name, $res)) {
              if (
                !isset(auth::$priv["${area_userspace}_DEL_" . $res[1]]) ||
                auth::$priv["${area_userspace}_DEL_" . $res[1]] == 0
              ) {
                unset($priv_area[$zone]);
              }
            } else {
              if (
                (!isset(auth::$priv["${area_userspace}_ED_" . $area_name]) ||
                  auth::$priv["${area_userspace}_ED_" . $area_name] == 0) &&
                (!isset(auth::$priv["${area_userspace}_DEL_" . $area_name]) ||
                  auth::$priv["${area_userspace}_DEL_" . $area_name] == 0)
              ) {
                unset($priv_area[$zone]);
              }
            }
          }
        }
      }
      return $priv_area;
    }
  }

  return '';
}

function return_auth_getbyuid($expr)
{
  $val = current($expr);
  next($expr);
  $uid = eval_list($val);

  $val = current($expr);
  next($expr);
  $field = eval_list($val);

  return auth_getbyuid($uid, $field);
}

function auth_gotoauthform($authpage, $error = 0)
{
  global $config, $path, $global_var;

  if ($authpage[0] == "!") {
    $tmp = substr($authpage, 1);

    include_once($config['modulesdir'] . 'url.php');
    header('Location: ' . return_absolute_templeet(array()) . preg_replace("|^/|", '', $tmp));
    templeet_exit();
  }

  if (!isset($_COOKIE[POST_COOKIE]) || $_COOKIE[POST_COOKIE] == '') {
    $tmparray = array();
    if (isset($_POST) && is_array($_POST))
      foreach ($_POST as $key => $value) {
        if (!preg_match('/^auth_/', $key))
          $tmparray[$key] = $value;
      }

    setcookie(POST_COOKIE, serialize($tmparray), 0, $config['dir_installed'], $config['auth_domain']);
  }

  $path = $authpage;
  parsehtml2template();
  if ($global_var->template == '')
    http_error(403);
  else {
    $tmp = parsetemplate($global_var->template);
    setheaders($tmp);
    echo $tmp;
  }
  templeet_exit();
}

function include_authmodule()
{
  if (empty(auth::$handle)) {
    global $config;

    return_auth_getmethod();

    $authmodulename = $config['modulesdir'] . "auth/auth_" . auth::$config['method']['type'] . ".php";

    if (!@file_exists($authmodulename)) {
      echo "no authentication module of that name: $authmodulename";
      templeet_exit();
    }
    include_once($authmodulename);

    $classname = "class_auth_" . auth::$config['method']['type'];
    auth::$handle = new $classname;
  }

  if (empty(auth::$filehandle)) {
    $authmodulename = $config['modulesdir'] . "auth/auth_file.php";

    if (!@file_exists($authmodulename)) {
      echo "no authentication module of that name: $authmodulename";
      templeet_exit();
    }
    include_once($authmodulename);

    auth::$filehandle = new class_auth_file;
  }
}

function return_auth_getmethod($expr = 0)
{
  auth_getconfig();
  if (!isset(auth::$config['method']) || !is_array(auth::$config['method'])) {
    auth::$config['method'] = array();
  }

  if (!isset(auth::$config['method']['type'])) {
    auth::$config['method']['type'] = "file";
  }
  return auth::$config['method']['type'];
}

function return_auth_signout($expr)
{
  global $global_var;

  auth::$admin = 0;
  auth::$userspace_admin = 0;
  auth::$priv = array();

  $removecookie = current($expr);
  next($expr);

  if (isset($removecookie))
    $removecookie = eval_list($removecookie);
  else
    $removecookie = FALSE;

  if (auth::$uid >= 0) {
    if ($removecookie)
      $signout = -2;
    else
      $signout = -1;
    auth_setcookie(auth::$userspace, auth::$login, auth::$uid, 1, $signout);
  }

  auth::$uid = -1;
  auth::$login = -1;
  $global_var->auth_user = '';
  $global_var->auth_email = '';
  $global_var->auth_uid = -1;
  $global_var->auth_nickname = '';
  return;
}

function return_getpriv($expr)
{
  /*
   *  ~getpriv(autharea)
   */
  $autharea = current($expr);
  next($expr);

  $res = auth::getpriv(eval_list($autharea));
  return $res;
}

function return_getprofile($expr)
{
  $val = current($expr);
  next($expr);
  if (!isset($val)) {
    $uid = auth::$uid;
  } else {
    $uid = eval_list($val);
    if (!is_int($uid) && !ctype_digit($uid))
      return -12;

    $uid = (int)$uid;
  }

  if (auth::$uid != $uid) {
    if (!auth::$admin) {
      if (!auth::$userspace_admin)
        return -107;
      $user_info = auth_getinfo($uid);
      if (!is_array($user_info))
        return -1;
      if ($user_info['userspace'] != auth::$userspace)
        return -107;
    }
    include_authmodule();
    return auth::$handle->getprofile($uid);
  }

  if (!isset(auth::$profile)) {
    if (auth::$uid == 0)
      auth::$profile = auth::$filehandle->getprofile(auth::$uid);
    else
      auth::$profile = auth::$handle->getprofile(auth::$uid);
  }

  return auth::$profile;
}

function return_auth_editpriv()
{
  if (auth::$uid < 0)
    return 0;

  if (auth::$admin || auth::$userspace_admin || (isset(auth::$priv['CREATEUSER']) && auth::$priv['CREATEUSER'] > 0))
    return 2;

  $useredit = '';
  reset(auth::$priv);
  while (list($area, $priv) = each(auth::$priv)) {
    if ($priv > 0) {
      preg_match('/^(?:{([^}]+)})?(.*)/', $area, $res);
      if (substr($res[2], 0, 5) == "_DEL_" || substr($res[2], 0, 4) == "_ED_") {
        if ($res[1] == '')
          return 2;

        if ($useredit == '')
          $useredit = $res[1];
        else {
          if ($useredit != $res[1])
            return 2;
        }
      }
    }
  }

  return 0;
}

function return_auth_emailislogin()
{
  global $config;

  auth_getconfig();

  return (isset(auth::$config['account']['emailislogin']) && auth::$config['account']['emailislogin']);
}

function auth_return()
{
  return array(
    'getauth', 'auth_signout', 'getpriv', 'auth_getmethod', 'getprofile',
    'auth_editpriv', 'setauth', 'auth_getbyuid', 'auth_userspace', 'auth_emailislogin'
  );
}
