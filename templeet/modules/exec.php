<?php
/*
 * This code has been developed by:
 *
 * Pascal COURTOIS
 *
 */

function return_passthru($array)
{
  global $config;

  $val = current($array);
  next($array);
  $command = eval_list($val);


  ob_start();
  passthru($command);
  $contents = ob_get_contents();
  ob_end_clean();

  return $contents;
}

function exec_return()
{
  return array('passthru');
}
