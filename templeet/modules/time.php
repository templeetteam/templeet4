<?php
/*
 * This code has been developed by:
 *
 * Fabien Penso
 *
 * This module contains functions which deal with time.
 *
 * Note: all the timestamps are 14 char long.
 * ~timestamp2hhmm takes a timestamp and returns it in the "hh:mm" form
 * ~timestamp2hhmmss takes a timestamp and returns it in the "hh:mm:ss" form
 * ~timestamp2month takes a timestamp and returns its month
 * ~timestamp2year takes a timestamp and returns its year
 * ~timestamp2day takes a timestamp and returns its day
 * ~format_timestamp takes a timestamp and formats it with strftime() function
 * ~format_unixtimestamp takes an unix timestamp and formats it with strftime() function
 */

function uniformize_timestamp($t)
{
  return preg_replace('/^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/', '$1$2$3$4$5$6', $t);
}

function return_timestamp2hhmm($array)
{
  $val = current($array);
  next($array);
  $tmp = uniformize_timestamp(eval_list($val));

  return substr($tmp, 8, 2) . ':' . substr($tmp, 10, 2);
}

function return_timestamp2hhmmss($array)
{
  $val = current($array);
  next($array);
  $tmp = uniformize_timestamp(eval_list($val));

  return substr($tmp, 8, 2) . ':' . substr($tmp, 10, 2) . ':' . substr($tmp, 12, 2);
}

function return_timestamp2month($array)
{
  $val = current($array);
  next($array);
  $tmp = uniformize_timestamp(eval_list($val));

  return substr($tmp, 4, 2);
}

function return_timestamp2year($array)
{
  $val = current($array);
  next($array);
  $tmp = uniformize_timestamp(eval_list($val));

  return substr($tmp, 0, 4);
}

function return_timestamp2day($array)
{
  $val = current($array);
  next($array);
  $tmp = uniformize_timestamp(eval_list($val));

  return substr($tmp, 6, 2);
}

function return_format_timestamp($array)
{
  $a = current($array);
  next($array);
  $b = current($array);
  next($array);
  $format = eval_list($a);
  $ts = uniformize_timestamp(eval_list($b));

  return strftime($format,
          mktime(intval(substr($ts,8,2)),
          intval(substr($ts,10,2)),
          intval(substr($ts,12,2)),
          intval(substr($ts,4,2)),
          intval(substr($ts,6,2)),
          strlen(trim($ts)) > 0 ? intval(substr($ts,0,4)) : false));
}

function return_format_unixtimestamp($array)
{
  $a = current($array);
  next($array);
  $b = current($array);
  next($array);
  $format = eval_list($a);
  $ts = eval_list($b);

  return strftime($format, $ts);
}

function return_now()
{
  return date('YmdHis');
}

function time_return()
{
  return array(
    'timestamp2day', 'timestamp2hhmmss', 'timestamp2month',
    'timestamp2year', 'format_timestamp', 'format_unixtimestamp',
    'timestamp2hhmm', 'now'
  );
}
