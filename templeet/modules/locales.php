<?php
/*
 * functions relative to locales
 *
 */

function tpl_strftime($format, $timestamp)
{
  global $config, $global_var;

  if (is_null($timestamp))
    $timestamp = time();
  if ($config["windows"]) {
    $tmp = strftime($format, $timestamp);

    if (isset($global_var->HTTP_charset) && $global_var->HTTP_charset != "utf-8") {
      if (function_exists("iconv") && $global_var->HTTP_charset != "iso-8859-1")
        $tmp = @iconv("iso-8859-1", $global_var->HTTP_charset . "//TRANSLIT", $tmp);
    } else
      $tmp = utf8_encode($tmp);

    return $tmp;
  }

  return strftime($format, $timestamp);
}

function locales_return()
{
  return array('tpl_strftime');
}
