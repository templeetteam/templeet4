<?php
/*
 * This code has been developed by:
 *
 * Pascal COURTOIS
 *
 */

global $nestedlstree;
$nestedlstree = 0;

global $treewalk;
$treewalk = array();

class ls_tree
{
  function sort_date($a, $b)
  {
    return ($a["stat"]["mtime"] == $b["stat"]["mtime"] ? strcmp($a["file"], $b["file"]) : ($a["stat"]["mtime"] > $b["stat"]["mtime"] ? 1 : -1));
  }

  function sort_rdate($a, $b)
  {
    return ($a["stat"]["mtime"] == $b["stat"]["mtime"] ? -strcmp($a["file"], $b["file"]) : ($a["stat"]["mtime"] < $b["stat"]["mtime"] ? 1 : -1));
  }

  function sort_num($a, $b)
  {
    return ((int) $a["file"] == (int) $b["file"] ? 0 : ((int)$a["file"] > (int)$b["file"] ? 1 : -1));
  }

  function sort_rnum($a, $b)
  {
    return ((int) $a["file"] == (int) $b["file"] ? 0 : ((int)$a["file"] > (int)$b["file"] ? -1 : 1));
  }

  function sort_str($a, $b)
  {
    return strcmp($a["file"], $b["file"]);
  }

  function sort_rstr($a, $b)
  {
    return -strcmp($a["file"], $b["file"]);
  }

  function ls_tree_sort(&$array, $sort)
  {
    switch ($sort) {
      case 'M':
        usort($array, array("ls_tree", "sort_date"));
        break;

      case 'MR':
        usort($array, array("ls_tree", "sort_rdate"));
        break;

      case 'S':
        usort($array, array("ls_tree", "sort_str"));
        break;

      case 'SR':
        usort($array, array("ls_tree", "sort_rstr"));
        break;

      case 'N':
        usort($array, array("ls_tree", "sort_num"));
        break;

      case 'NR':
        usort($array, array("ls_tree", "sort_rnum"));
        break;

      case 'RR':
        shuffle($array);
        break;
    }
  }

  function ls_tree1($dir, $depth = 0)
  {
    global $treewalk, $nestedlstree;

    $output = '';
    $handle = opendir($treewalk[$nestedlstree]["dir"] . $dir);
    if (!$handle)
      return '';

    $allfile = 0;
    $totalfile = 0;
    $dirarray = array();
    while ($a = readdir($handle)) {
      if ($a != '.' && $a != '..') {
        $allfile++;
        if (
          (is_dir($treewalk[$nestedlstree]["dir"] . $dir . "/$a") && preg_match($treewalk[$nestedlstree]["dirmask"], "$dir/$a", $matches)) ||
          (is_file($treewalk[$nestedlstree]["dir"] . $dir . "/$a") && preg_match($treewalk[$nestedlstree]["filemask"], $a, $matches))
        ) {
          $totalfile++;
          $tmp = array();
          $tmp["stat"] = stat($treewalk[$nestedlstree]["dir"] . $dir . "/$a");
          $tmp["file"] = $a;
          $tmp["matches"] = $matches;
          $dirarray[] = $tmp;
        }
      }
    }
    closedir($handle);
    $treewalk[$nestedlstree]['allfile'] = $allfile;
    $treewalk[$nestedlstree]['totalfile'] = $totalfile;

    if ($treewalk[$nestedlstree]["dirsort"] != '') {
      $arraydir = array();
      $arrayfile = array();
      foreach ($dirarray as $file) {
        if (0040000 & $file["stat"]["mode"])
          $arraydir[] = $file;
        else
          $arrayfile[] = $file;
      }

      self::ls_tree_sort($arraydir, $treewalk[$nestedlstree]["sort"]);
      self::ls_tree_sort($arrayfile, $treewalk[$nestedlstree]["sort"]);
      if ($treewalk[$nestedlstree]["dirsort"] == "DF")
        $dirarray = array_merge($arraydir, $arrayfile);
      else
        $dirarray = array_merge($arrayfile, $arraydir);
    } else
      self::ls_tree_sort($dirarray, $treewalk[$nestedlstree]["sort"]);

    if ((is_countable($dirarray) ? count($dirarray) : 0) == 0 && $depth == 0) {
      if (isset($treewalk[$nestedlstree]["elements"]["LTT"]))
        $output .= eval_list($treewalk[$nestedlstree]["elements"]["LTT"]);

      if (isset($treewalk[$nestedlstree]["elements"]["LD"]))
        $output .= eval_list($treewalk[$nestedlstree]["elements"]["LD"]);

      if (isset($treewalk[$nestedlstree]["elements"]["LTB"]))
        $output .= eval_list($treewalk[$nestedlstree]["elements"]["LTB"]);
    } elseif ((is_countable($dirarray) ? count($dirarray) : 0) == 1 && $depth == 0 && isset($treewalk[$nestedlstree]["elements"]["L1"])) {
      if (isset($treewalk[$nestedlstree]["elements"]["LTT"]))
        $output .= eval_list($treewalk[$nestedlstree]["elements"]["LTT"]);

      if (isset($treewalk[$nestedlstree]["elements"]["L1"]))
        $output .= eval_list($treewalk[$nestedlstree]["elements"]["L1"]);

      if (isset($treewalk[$nestedlstree]["elements"]["LTB"]))
        $output .= eval_list($treewalk[$nestedlstree]["elements"]["LTB"]);
    } else {
      $tcounter = 1;
      reset($dirarray);

      $treewalk[$nestedlstree]['tdepth'] = $depth;
      if ($depth == 0 && isset($treewalk[$nestedlstree]["elements"]["LF"]))
        $output .= eval_list($treewalk[$nestedlstree]["elements"]["LF"]);

      if (isset($treewalk[$nestedlstree]["elements"]["LTT"]))
        $output .= eval_list($treewalk[$nestedlstree]["elements"]["LTT"]);

      $key = key($dirarray);
      $currentfile = current($dirarray);
      next($dirarray);
      $treewalk[$nestedlstree]['counter']++;
      while (isset($currentfile)) {
        $treewalk[$nestedlstree]["file"] = $currentfile["file"];
        $treewalk[$nestedlstree]["stat"] = $currentfile["stat"];
        $treewalk[$nestedlstree]["matches"] = $currentfile["matches"];
        $treewalk[$nestedlstree]['tcounter'] = $tcounter;

        if (isset($treewalk[$nestedlstree]["elements"]["LM"]))
          $output .= eval_list($treewalk[$nestedlstree]["elements"]["LM"]);
        if (0040000 & $currentfile["stat"]["mode"]) {
          $output .= self::ls_tree1("$dir/" . $currentfile["file"], $depth + 1);

          $treewalk[$nestedlstree]['tdepth'] = $depth;
          $treewalk[$nestedlstree]["file"] = $currentfile["file"];
          $treewalk[$nestedlstree]["stat"] = $currentfile["stat"];
          $treewalk[$nestedlstree]["matches"] = $currentfile["matches"];
          $treewalk[$nestedlstree]['allfile'] = $allfile;
          $treewalk[$nestedlstree]['totalfile'] = $totalfile;
        }

        $treewalk[$nestedlstree]['tcounter'] = $tcounter;
        if (isset($treewalk[$nestedlstree]["elements"]["LP"])) {
          $output .= eval_list($treewalk[$nestedlstree]["elements"]["LP"]);
        }

        $key = key($dirarray);
        $currentfile = current($dirarray);
        next($dirarray);
        $treewalk[$nestedlstree]['counter']++;
        $tcounter++;
      }

      if (isset($treewalk[$nestedlstree]["elements"]["LTB"]))
        $output .= eval_list($treewalk[$nestedlstree]["elements"]["LTB"]);

      if ($depth == 0 && isset($treewalk[$nestedlstree]["elements"]["LL"]))
        $output .= eval_list($treewalk[$nestedlstree]["elements"]["LL"]);
    }

    return $output;
  }
}

function return_ls_tree($expr)
{
  global $treewalk, $nestedlstree;

  $dir = current($expr);
  next($expr);
  $dir = eval_list($dir);

  $dirmask = current($expr);
  next($expr);
  $dirmask = eval_list($dirmask);

  if ($dirmask == '')
    $dirmask = '//';
  elseif (@preg_match($dirmask, '', $res) === FALSE)
    return 'Error in dirmask regular expression in ls_tree !';


  $filemask = current($expr);
  next($expr);
  $filemask = eval_list($filemask);

  if ($filemask == '')
    $filemask = '//';
  elseif (@preg_match($filemask, '', $res) === FALSE)
    return 'Error in filemask regular expression in ls_tree !';

  $sort = current($expr);
  next($expr);
  $sort = eval_list($sort);

  $elements = array();
  while (list(, $val) = each($expr)) {
    $tmp_l = eval_list($val);

    if (
      $tmp_l == 'LM' ||
      $tmp_l == 'LF' ||
      $tmp_l == 'LP' ||
      $tmp_l == 'LL' ||
      $tmp_l == 'LTB' ||
      $tmp_l == 'LTT' ||
      $tmp_l == 'LE' ||
      $tmp_l == 'L1' ||
      $tmp_l == 'LD'
    ) {
      $val = current($expr);
      next($expr);
      if (!isset($val))
        throw new TempleetError("Error in ls_tree! You didn't specify a second argument");
      $elements[$tmp_l] = $val;
    } else
      throw new TempleetError("Error in ls_tree! You must use LM, LP, LF, LL, LE, L1, LTT, LTB  or LD as first argument");
  }

  if (!preg_match("/^(M|MR|S|SR|N|NR|RR|FS)((?:DF|DL)?)$/", $sort, $res))
    throw new TempleetError("Error in ls_tree! Bad sort flag");

  if ($dir == '')
    $dir = './';
  else {
    if ($dir[strlen($dir) - 1] != '/')
      $dir .= '/';
  }
  $nestedlstree++;
  $treewalk[$nestedlstree] = array(
    'dir' => $dir,
    'dirmask' => $dirmask,
    'filemask' => $filemask,
    'sort' => $res[1],
    'dirsort' => $res[2],
    'elements' => $elements,
    'counter' => 0,
  );

  $res = (new ls_tree())->ls_tree1('');
  unset($treewalk[$nestedlstree]);

  $nestedlstree--;
  return $res;
}

function ls_tree_fld($field)
{
  global $treewalk, $nestedlstree;
  if (isset($treewalk[$nestedlstree]['stat'][$field]))
    return $treewalk[$nestedlstree]['stat'][$field];

  switch ($field) {
    case 'file':
      return $treewalk[$nestedlstree]['file'];

    case 'rfile':
      if (preg_match('/(.*)\..*?$/', $treewalk[$nestedlstree]['file'], $res))
        return $res[1];
      else
        return $treewalk[$nestedlstree]['file'];

    case 'stat':
      return $treewalk[$nestedlstree]['stat'];

    case 'allfile':
    case 'totalfile':
    case 'counter':
    case 'matches':
    case 'tcounter':
    case 'tdepth':
      return $treewalk[$nestedlstree][$field];

    default:
      throw new TempleetError("Error in ls_tree_fld! unknown field: $field");
  }
}

function ls_tree_return()
{
  return array('ls_tree', 'ls_tree_fld');
}
