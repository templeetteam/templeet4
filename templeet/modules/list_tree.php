<?php

/**
 * This code has been developed by:
 * Fabien Penso
 */

if (!function_exists('return_fld')) {
  global $config;
  $file_tmp = $config['modulesdir'] . 'list.php';
  if (file_exists($file_tmp))
    require($file_tmp);
  else
    throw new TempleetError("list.php seems to be absent !\n");
}

function return_list_tree($array)
{
  global $config, $nestedlist, $sql_row, $list_gfld, $totaltime, $global_var;

  $dbinfo = current($array);
  next($array);
  $dbinfo = eval_list($dbinfo);

  $sql = current($array);
  next($array);
  $sql = eval_list($sql);

  $sqlsort = current($array);
  next($array);
  $sqlsort = eval_list($sqlsort);

  $elements_sql = array();
  while (list(, $val) = each($array)) {
    $tmp_l = eval_list($val);

    if (
      $tmp_l == 'LM' ||
      $tmp_l == 'LF' ||
      $tmp_l == 'LP' ||
      $tmp_l == 'LL' ||
      $tmp_l == 'LTB' ||
      $tmp_l == 'LTT' ||
      $tmp_l == 'LE' ||
      $tmp_l == 'L1' ||
      $tmp_l == 'LD'
    ) {
      $val = current($array);
      next($array);
      if (!isset($val))
        throw new TempleetError("Error in list_tree! You didn't specify a second argument");
      $elements_sql[$tmp_l] = $val;
    } else
      throw new TempleetError("Error in list_tree! You must use LM, LP, LF, LL, LE, L1, LTT, LTB  or LD as first argument");
  }

  $tmp = '';

  /* making SQL */
  $nestedlist++;
  $list_handle = list_metaconnect($dbinfo);

  if (!is_object($list_handle))
    switch ($list_handle) {
      case -1:
        $nestedlist--;
        return;

      case -2:
        if (isset($elements_sql['LE'])) {
          $list_gfld[$nestedlist]['error'] = $global_var->list_error;
          $tmp = eval_list($elements_sql['LE']);
          $nestedlist--;
          return $tmp;
        } else
          throw new TempleetError($global_var->list_error);
    }

  $time_start = getmicrotime();
  if (!($results =  $list_handle->query($sql))) {
    if ($global_var->list_error == '')
      $global_var->list_error = "Could not query: $sql";
    if (isset($elements_sql['LE'])) {
      $list_gfld[$nestedlist]['error'] = $global_var->list_error;
      $tmp .= eval_list($elements_sql['LE']);
      $nestedlist--;
      return $tmp;
    } else
      throw new TempleetError($global_var->list_error);
  }

  $time_end = getmicrotime();
  $time = $time_end - $time_start;
  $totaltime += $time;
  $global_var->templeet_total_queries++;
  $list_gfld[$nestedlist]['executedtime'] = $time;

  /*  $query_tmp = substr(strtolower(ltrim($sql)),0,6);

  if ($query_tmp != 'select')
    {
      $nestedlist--;
      throw new TempleetError("Only select allowed in list_tree!");
    }
*/
  $numrows = $list_handle->numrows($results);
  $list_gfld[$nestedlist]['numrows'] = $numrows;

  if ($list_gfld[$nestedlist]['numrows'] == '0') {
    if (isset($elements_sql['LD'])) {
      $nestedlist--;
      $tmp .= eval_list($elements_sql['LD']);
      $nestedlist++;
    }
    $list_handle->free_result($results);
  } elseif ($list_gfld[$nestedlist]['numrows'] == '1' && isset($elements_sql['L1'])) {
    $x = 1;
    $row = $list_handle->fetcharray($results);
    $sql_row[$nestedlist] = array();
    $sql_row[$nestedlist]['cur'] = $row;
    $list_gfld[$nestedlist]['counter'] = 1;
    $tmp .= eval_list($elements_sql['L1']);
    $list_handle->free_result($results);
  } else {
    $list_tree_sql = array();
    $list_tree_thread = array();
    $list_gfld[$nestedlist]['counter'] = 0;

    $x = 1;
    $list_last_row = array();
    while ($row = $list_handle->fetcharray($results)) {
      $id = current($row);
      next($row);
      $parent_id = current($row);
      next($row);
      $list_tree_sql[$id] = $row;
      $list_prev_last_row = $list_last_row;
      $list_last_row = $row;
      if ($x == 1)
        $list_first_row = $row;
      if ($x == 2)
        $list_next_first_row = $row;

      if (!isset($list_tree_thread[$parent_id]))
        $list_tree_thread[$parent_id] = array();

      if (!isset($smallest) || $parent_id < $smallest)
        $smallest = $parent_id;
      array_push($list_tree_thread[$parent_id], $id);

      $x++;
    }

    $list_handle->free_result($results);

    if ($sqlsort == 'T' || $sqlsort == 'TR' || $sqlsort == '') {

      if (isset($elements_sql['LF'])) {
        $sql_row[$nestedlist] = array();
        $sql_row[$nestedlist]['cur'] = $list_first_row;
        if (isset($list_next_first_row))
          $sql_row[$nestedlist]['next'] = $list_next_first_row;
        else
          $sql_row[$nestedlist]['next'] = NULL;

        $tmp .= eval_list($elements_sql['LF']);
      }

      $tmp .= list_tree_thread($smallest, $list_tree_thread, $list_tree_sql, 1, $elements_sql, $sqlsort);
      if (isset($elements_sql['LL'])) {
        $sql_row[$nestedlist] = array();
        $sql_row[$nestedlist]['cur'] = $list_last_row;
        $sql_row[$nestedlist]['prev'] = $list_prev_last_row;
        $tmp .= eval_list($elements_sql['LL']);
      }
    }
  }

  $nestedlist--;

  return $tmp;
}

function list_tree_thread($threadhead = 0, $thread = array(), $tree = array(), $depth = 1, $elements_sql = array(), $sqlsort = 'T')
{
  global $nestedlist, $sql_row, $list_gfld;

  $tmp = '';

  if (isset($thread[$threadhead]) && is_array($thread[$threadhead]) && (is_countable($thread[$threadhead]) ? count($thread[$threadhead]) : 0) > 0) {
    if ($sqlsort == 'T')
      sort($thread[$threadhead]);
    elseif ($sqlsort == 'TR')
      rsort($thread[$threadhead]);
    $x = 1;
    $tnumrows = is_countable($thread[$threadhead]) ? count($thread[$threadhead]) : 0;

    $sql_row[$nestedlist]['tprev'] = array();
    $prev = -1;
    $current = current($thread[$threadhead]);
    next($thread[$threadhead]);
    $sql_row[$nestedlist]['cur'] = $tree[$current];

    if (list(, $next) = each($thread[$threadhead]))
      $sql_row[$nestedlist]['tnext'] = $tree[$next];
    else
      $sql_row[$nestedlist]['tnext'] = array();

    while ($current) {
      $list_gfld[$nestedlist]['tnumrows'] = $tnumrows;
      $list_gfld[$nestedlist]['tdepth'] = $depth;
      $list_gfld[$nestedlist]['tcounter'] = $x;
      $list_gfld[$nestedlist]['counter']++;

      if (isset($thread[$current]) && is_array($thread[$current])) {
        if ($sqlsort == 'T')
          $tc = min($thread[$current]);
        elseif ($sqlsort == 'TR')
          $tc = max($thread[$current]);
        else
          $tc = $thread[$current][0];
        $sql_row[$nestedlist]['tchild'] = $child = $tree[$tc];
      } else
        $sql_row[$nestedlist]['tchild'] = $child = array();

      if ($x == 1 && isset($elements_sql['LTT']))
        $tmp .= eval_list($elements_sql['LTT']);

      if (isset($elements_sql['LM']))
        $tmp .= eval_list($elements_sql['LM']);

      $tmp .= list_tree_thread($current, $thread, $tree, ($depth + 1), $elements_sql, $sqlsort);

      $sql_row[$nestedlist]['tchild'] = $child;

      if (isset($tree[$prev]))
        $sql_row[$nestedlist]['tprev'] = $tree[$prev];
      else
        $sql_row[$nestedlist]['tprev'] = array();

      $sql_row[$nestedlist]['cur'] = $tree[$current];
      if (isset($tree[$next]))
        $sql_row[$nestedlist]['tnext'] = $tree[$next];
      else
        $sql_row[$nestedlist]['tnext'] = -1;

      $list_gfld[$nestedlist]['tnumrows'] = $tnumrows;
      $list_gfld[$nestedlist]['tdepth'] = $depth;
      $list_gfld[$nestedlist]['tcounter'] = $x;

      if (isset($elements_sql['LP']))
        $tmp .= eval_list($elements_sql['LP']);

      if ($x == $tnumrows && isset($elements_sql['LTB']))
        $tmp .= eval_list($elements_sql['LTB']);

      $prev = $current;
      $sql_row[$nestedlist]['tprev'] = $sql_row[$nestedlist]['cur'];
      $current = $next;
      $sql_row[$nestedlist]['cur'] = $sql_row[$nestedlist]['tnext'];
      $next = current($thread[$threadhead]);
      next($thread[$threadhead]);
      if (isset($next))
        $sql_row[$nestedlist]['tnext'] = $tree[$next];
      else
        $sql_row[$nestedlist]['tnext'] = array();

      $x++;
    }
  }
  return $tmp;
}

function list_tree_return()
{
  return array('list_tree');
}
