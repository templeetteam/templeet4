<?php
/*
 * This code has been developed by:
 *
 * Pascal Courtois
 *
 */

define("CUTHTML_MAXEXPR", 250);

function return_cuthtml($array)
{
  global $config;

  $val = current($array);
  next($array);
  $txt = trim(eval_list($val));

  $val = current($array);
  next($array);
  if (!empty($val))
    $max_length = eval_list($val);
  else
    $max_length = '';

  $val = current($array);
  next($array);
  if (isset($val))
    $window = eval_list($val);
  else
    $window = 0;

  $val = current($array);
  next($array);
  if (isset($val))
    $closing_text = eval_list($val);
  else
    $closing_text = '';

  $val = current($array);
  next($array);
  if (isset($val))
    $allowed_tags_tmp = eval_list($val);
  else
    $allowed_tags_tmp = '';

  $val = current($array);
  next($array);
  if (isset($val)) {
    $quotestyle = eval_list($val);
    if ($quotestyle === '')
      $quotestyle = ENT_COMPAT;
  } else
    $quotestyle = ENT_COMPAT;

  $tags = cuthtml_allowedtag($allowed_tags_tmp, $config['htmltags']);

  if ($window == '')
    $window = 0;

  $tag_stack = array();

  $utf8 = 0;
  $prev_encoding = "";
  if (function_exists("mb_internal_encoding")) {
    $prev_encoding = mb_internal_encoding();
    mb_internal_encoding("UTF-8");
    $coding = mb_detect_encoding($txt, array("UTF-8", "ISO-8859-1", "ASCII"));


    if ($coding == "UTF-8")
      $utf8 = 1;
  } elseif (preg_match(
    '%(?:
        [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
        |\xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
        |[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2} # straight 3-byte
        |\xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
        |\xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
        |[\xF1-\xF3][\x80-\xBF]{3}         # planes 4-15
        |\xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
        )+%xs',
    $txt
  )) {
    throw new TempleetError("cuthtml: the php mbstring module is mandatory when handling multibyte charset strings");
  }

  if ($utf8) {
    $sep = "[\\s\\pZ]";
    $nosep = "[^\\s\\pZ]";
    $letter = "[\\pL']";
    $utf8modifier = "u";
  } else {
    $sep = "\\s";
    $nosep = "\\S";
    $letter = "[\\w']";
    $utf8modifier = '';
  }

  $tag_expr = "(?:<[^>]*>)*";
  $txt = preg_replace(
    array(
      '/<!--.*?-->/',
      '/<(?!\/?[a-zA-Z])/'
    ),
    array(
      '',
      '&lt;'
    ),
    $txt
  );

  if ($max_length != '') {
    $res = '';
    $expr = "/^(${tag_expr}(?:(?:${sep}+|&#\d+;|&#x[\da-fA-F]+;|&[a-zA-Z]{1,8};|${nosep})${tag_expr}){" . CUTHTML_MAXEXPR . "})(.*)/${utf8modifier}s";
    while (
      $max_length > CUTHTML_MAXEXPR &&
      preg_match($expr, $txt, $countchar)
    ) {
      $res .= $countchar[1];
      $txt = $countchar[2];
      $max_length -= CUTHTML_MAXEXPR;
    }
    if ($max_length > CUTHTML_MAXEXPR)
      $max_length = CUTHTML_MAXEXPR;

    $length = "{0,$max_length}";
    $expr = "/^${tag_expr}((?:${sep}+|&#\d+;|&#x[\da-fA-F]+;|&[a-zA-Z]{1,8};|${nosep})${tag_expr})${length}(?:(?:&#\d+;|&#x[\da-fA-F]+;|&[a-zA-Z]{1,8};|${letter}){0,${window}})?/${utf8modifier}s";
    $match = preg_match($expr, $txt, $countchar);
    if (strlen($txt) > strlen($countchar[0]))
      $txt = $res . $countchar[0] . $closing_text;
    else
      $txt = $res . $countchar[0];
  }

  $ressplit = preg_split("/(<\/?[a-zA-Z][^>]*>)/", $txt, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

  $res = '';

  foreach ($ressplit as $value) {
    if ($value[0] == "<") {
      if (preg_match("/^<(\/?)(\w+)\s*((?:\w+(?:=(?:\"[^\"]*\"|\w+))?\s*)*)\/?>/", $value, $tagsplit)) {
        $tagname = strtolower($tagsplit[2]);
        if ($tagsplit[1] == '') {
          if (isset($tags[$tagname])) {
            $tag_out = "<" . $tagname;
            if (is_array($tags[$tagname])) {
              $params = preg_split("/\s*(\w+(?:=(?:\"[^\"]*\"|\w+))?)\s*/", $tagsplit[3], -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
              foreach ($params as $param) {
                preg_match("/^(\w+)(?:=(\"[^\"]*\"|\w+))?/", $param, $resparam);
                $paramname = strtolower($resparam[1]);
                if (isset($tags[$tagname][$paramname])) {
                  if (isset($resparam[2])) {
                    $paramvalue = $resparam[2];
                    if (preg_match("/^\"([^\"]*)\"$/", $paramvalue, $resparamvalue))
                      $paramvalue = $resparamvalue[1];
                    if ($paramname == "src" || $paramname == "lowsrc" || $paramname == "href") {
                      $decodevalue = preg_replace_callback(
                        '~&#x([0-9a-f]+);?~i',
                        fn($matches) => chr(hexdec($matches[1])),
                        $paramvalue
                      );

                      $decodevalue = preg_replace_callback(
                        '~&#0*([0-9]+);?~',
                        fn($matches) => chr($matches[1]),
                        $decodevalue
                      );
                      $decodevalue = html_entity_decode($decodevalue, ENT_QUOTES | ENT_HTML401);
                      $decodevalue = preg_replace('/[\s\xAD\000-\x0D]+/', '', $decodevalue);

                      if (preg_match("/^\s*(?:javascript|vbscript|mocha|livescript):/i", $decodevalue))
                        $paramvalue = '';
                    }
                    $tag_out .= " ${paramname}=\"" . $paramvalue . '"';
                  } else {
                    $tag_out .= " ${paramname}=\"${paramname}\"";
                  }
                }
              }

              if (isset($tags[$tagname]['/']))
                $tag_out .= " /";
              else
                $tag_stack[] = $tagname;
            } else {
              if ($tags[$tagname] == "/")
                $tag_out .= " /";
              else
                $tag_stack[] = $tagname;
            }
            $tag_out .= ">";
            $res .= $tag_out;
          }
        } else {
          if (count($tag_stack) != 0 && $tag_stack[count($tag_stack) - 1] == $tagname) {
            array_pop($tag_stack);
            $res .= "</" . $tagname . ">";
          }
        }
      } else {
        $res .= htmlspecialchars($value, $quotestyle, ($utf8 ? "UTF-8" : "ISO-8859-1"), FALSE);
      }
    } else {
      $res .= htmlspecialchars($value, $quotestyle, ($utf8 ? "UTF-8" : "ISO-8859-1"), FALSE);
      $tmp = htmlspecialchars($value, $quotestyle, ($utf8 ? "UTF-8" : "ISO-8859-1"), FALSE);
    }
  }
  while ($tag = array_pop($tag_stack)) {
    $res .= "</$tag>";
  }

  if ($prev_encoding) {
    mb_internal_encoding($prev_encoding);
  }

  return $res;
}


function cuthtml_allowedtag($allowed_tags_tmp, &$tags)
{
  if (is_array($allowed_tags_tmp))
    return $allowed_tags_tmp;

  if ($allowed_tags_tmp == '')
    return $tags;
  if ($allowed_tags_tmp == -1)
    return array();

  $tags_out = array();
  $allowed_tags = explode(',', $allowed_tags_tmp);

  while (list(, $val) = each($allowed_tags)) {
    $val = strtolower($val);
    if (array_key_exists($val, $tags))
      $tags_out[$val] = $tags[$val];
  }

  return $tags_out;
}

function cuthtml_return()
{
  return array('cuthtml');
}
