<?php
/*
 * This code has been developed by:
 *
 * Pascal COURTOIS
 *
 * This module contains functions related to regex.
 *
 */

function return_preg($expr)
{
  if (@preg_match(eval_list($expr[0]), eval_list($expr[1]), $out)) {
    if (isset($expr[2]))
      $value = eval_list($expr[2]);
    else
      $value = 1;

    if (is_numeric($value)) {
      if (isset($out[$value]))
        return $out[$value];
      else
        return TRUE;
    } elseif ($value != '') {
      global $global_var;
      $global_var->$value = $out;
    }
  }
}

function regex_callback($matches)
{
  global $regex_reg, $regex_replace_expr, $local_var, $nestedinclude;

  $reg = end($regex_reg);
  $replace_expr = end($regex_replace_expr);

  if (is_array($reg)) {
    $elt_reg = array_shift($reg);
    $elt_replace_expr = array_shift($replace_expr);
    while (isset($elt_reg) && !preg_match($elt_reg, $matches[0])) {
      $elt_reg = array_shift($reg);
      $elt_replace_expr = array_shift($replace_expr);
    }
    if (!isset($elt_reg))
      return '';
    $replace_expr = $elt_replace_expr;
  }

  $i = 0;
  while (isset($matches[$i])) {
    $local_var[$nestedinclude]->$i = $matches[$i];
    $i++;
  }
  while ($i < 10) {
    unset($local_var[$nestedinclude]->$i);
    $i++;
  }

  $txt = eval_list($replace_expr);

  return $txt;
}

function return_tpl_replace($expr)
{
  global $regex_reg, $regex_replace_expr;

  $reg = eval_list($expr[0]);
  if (is_array($reg)) {
    if (is_array($expr[1]))
      $regex_replace = $expr[1];
    elseif (is_object($expr[1]) && get_class($expr[1]) == "tf" && $expr[1]->f == "array") {
      if (is_array($expr[1]->p))
        $regex_replace = $expr[1]->p;
      else
        $regex_replace = array($expr[1]->p);
    } else
      throw new TempleetError("tpl_replace: Can't handle such replacement expression");

    if (count($reg) != count($regex_replace))
      throw new TempleetError("tpl_replace: arrays should have the same size");

    $regex_replace_expr[] = $regex_replace;
  } else
    $regex_replace_expr[] = $expr[1];

  $regex_reg[] = $reg;

  $txt = eval_list($expr[2]);

  if (isset($expr[3]))
    $limit = eval_list($expr[3]);
  else
    $limit = -1;

  $res = preg_replace_callback(end($regex_reg), "regex_callback", $txt, $limit);

  array_pop($regex_reg);
  array_pop($regex_replace_expr);
  return $res;
}

function regex_return()
{
  return array('tpl_replace', 'preg');
}
