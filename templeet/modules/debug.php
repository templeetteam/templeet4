<?php
/*
 * This code has been developed by:
 *
 * Pascal COURTOIS
 *
 * Templeet 4
 */

function return_print_r($expr)
{
  $val = eval_list($expr[0]);
  return print_r($val, true);
}

function return_php_info()
{
  ob_start();

  phpinfo(63);
  $val_phpinfo = ob_get_contents();
  ob_end_clean();

  $arrayres = array();
  preg_match('|<body>(.*)</body>|is', $val_phpinfo, $arrayres);

  $val_phpinfo = $arrayres[1];

  $val_phpinfo = preg_replace(array(
    '|<a href="http://www.php.net/.*?PHP Logo" /></a>|s',
    '|<table border="0" cellpadding="3" width="600">\s+<tr class="v">.*?Zend logo.*?PHP Credits</a></h1>|s',
    '|width="600"|s',
    '|class="."|s'
  ), array('', '', '', ''), $val_phpinfo);

  return $val_phpinfo;
}

function debug_return()
{
  return array('print_r', 'php_info');
}
