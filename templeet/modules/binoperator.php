<?php
/*
 * This module contains binary operators
 *
 */

function return_bwand($expr)
{
  $val = current($expr);
  next($expr);
  $tmp = eval_list($val);
  while (list(, $val) = each($expr))
    $tmp = $tmp & eval_list($val);
  return $tmp;
}

function return_bwor($expr)
{
  $val = current($expr);
  next($expr);
  $tmp = eval_list($val);
  while (list(, $val) = each($expr))
    $tmp = $tmp | eval_list($val);
  return $tmp;
}

function return_bwxor($expr)
{
  $val = current($expr);
  next($expr);
  $tmp = eval_list($val);
  while (list(, $val) = each($expr))
    $tmp = $tmp ^ eval_list($val);
  return $tmp;
}

function return_bwrshift($expr)
{
  $val = current($expr);
  next($expr);
  $tmp = eval_list($val);
  $val = current($expr);
  next($expr);
  return $tmp >> eval_list($val);
}

function return_bwlshift($expr)
{
  $val = current($expr);
  next($expr);
  $tmp = eval_list($val);
  $val = current($expr);
  next($expr);
  return $tmp << eval_list($val);
}

function return_bwnot($expr)
{
  $val = current($expr);
  next($expr);
  $x = eval_list($val);
  if (!is_int($x))
    templeet_exit();
  return ~eval_list($val);
}

function binoperator_return()
{
  return array('bwor', 'bwand', 'bwxor', 'bwnot', 'bwlshift', 'bwrshift');
}
