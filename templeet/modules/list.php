<?php
/* This code has been developed by:
 *
 * Fabien Penso
 * Pascal COURTOIS
 *
 */

// global vars for the ~list()

global $nestedlist;
$nestedlist = 0;

global $sql_row;
$sql_row = array();

global $list_gfld;
$list_gfld = array();

global $global_var;
if (!isset($global_var)) 
    $global_var = new stdClass();
$global_var->templeet_total_queries = 0;

function return_list($expr)
{
  global $nestedlist, $sql_row, $totaltime, $list_gfld, $global_var;

  $dbinfo = current($expr);
  next($expr);
  $dbinfo = eval_list($dbinfo);

  $sql = current($expr);
  next($expr);
  $sql = eval_list($sql);

  $elements_sql = array();
  while (list(, $val) = each($expr)) {
    $tmp_l = eval_list($val);

    if (
      $tmp_l == 'LM' ||
      $tmp_l == 'LR' ||
      $tmp_l == 'LF' ||
      $tmp_l == 'LL' ||
      $tmp_l == 'LN' ||
      $tmp_l == 'LS' ||
      $tmp_l == 'LE' ||
      $tmp_l == 'L1' ||
      $tmp_l == 'LD'
    ) {
      $val = current($expr);
      next($expr);
      if (!isset($val))
        throw new TempleetError('Error in list! You didn\'t specify a second argument');

      $elements_sql[$tmp_l] = $val;
    } else
      throw new TempleetError('Error in list! You must use LM, LR, LF, LL, LN, LS, LE, L1 or LD as first argument');
  }

  $tmp = '';

  /* making SQL */
  $nestedlist++;

  $list_handle = list_metaconnect($dbinfo);

  if (!is_object($list_handle))
    switch ($list_handle) {
      case -1:
        $nestedlist--;
        return;

      case -2:
        if (isset($elements_sql['LE'])) {
          $list_gfld[$nestedlist]['error'] = $global_var->list_error;
          try {
            $tmp = eval_list($elements_sql['LE']);
          } catch (ReturnValue $e) {
            $nestedlist--;
            throw $e;
          }

          $nestedlist--;
          return $tmp;
        } else
          throw new TempleetError($global_var->list_error);
    }

  $time_start = getmicrotime();
  if (!($results = $list_handle->query($sql))) {
    if ($global_var->list_error == '')
      $global_var->list_error = "Could not query: $sql";
    if (isset($elements_sql['LE'])) {
      $list_gfld[$nestedlist]['error'] = $global_var->list_error;
      try {
        $tmp = eval_list($elements_sql['LE']);
      } catch (ReturnValue $e) {
        $nestedlist--;
        throw $e;
      }

      $nestedlist--;
      return $tmp;
    } else
      throw new TempleetError($global_var->list_error);
  }
  $time_end = getmicrotime();
  $time = $time_end - $time_start;
  $totaltime += $time;
  $global_var->templeet_total_queries++;
  $list_gfld[$nestedlist]['executedtime'] = $time;

  if (isset($global_var->debugsql) && $global_var->debugsql != '') {
    if ($time > 0.1)
      $tempfile = "*** " . round($time, 2) . ": " . trim($sql) . ";\n";
    else
      $tempfile = round($time, 2) . ": " . trim($sql) . ";\n";

    if (file_put_contents($global_var->debugsql, $tempfile, FILE_APPEND) === FALSE)
      throw new TempleetError("Writing problem for " . $global_var->debugsql . "\n");
  }

  if (is_object($results)) {
    $list_gfld[$nestedlist]['numrows'] = $num_rows = $list_handle->numrows($results);

    if ($num_rows == '0' && isset($elements_sql['LD'])) {
      $nestedlist--;
      try {
        $tmp = eval_list($elements_sql['LD']);
      } catch (ReturnValue $e) {
        $nestedlist--;
        throw $e;
      }
      $nestedlist++;
    } elseif ($num_rows == '1' && isset($elements_sql['L1'])) {
      $x = 1;
      $row = $list_handle->fetcharray($results, $x);
      $sql_row[$nestedlist]['prev'] = array();
      $sql_row[$nestedlist]['cur'] = $row;
      $sql_row[$nestedlist]['next'] = array();
      $list_gfld[$nestedlist]['counter'] = 1;
      try {
        $tmp = eval_list($elements_sql['L1']);
      } catch (ReturnValue $e) {
        $nestedlist--;
        throw $e;
      }
    } else {
      $x = 1;
      $row_prev = array();
      $row = $list_handle->fetcharray($results, $x);
      $row_next = $list_handle->fetcharray($results, $x + 1);
      while ($row) {
        $sql_row[$nestedlist]['prev'] = $row_prev;
        $sql_row[$nestedlist]['cur'] = $row;
        $sql_row[$nestedlist]['next'] = $row_next;
        $list_gfld[$nestedlist]['counter'] = $x;

        if ($x == '1') {
          if (isset($elements_sql['LF']))
            try {
              $tmp .= eval_list($elements_sql['LF']);
            } catch (ReturnValue $e) {
              $nestedlist--;
              throw $e;
            }

          if (isset($elements_sql['LR'])) {
            try {
              $tmp .= eval_list($elements_sql['LR']);
            } catch (ReturnValue $e) {
              $nestedlist--;
              throw $e;
            }
            $x++;
            $row_prev = $row;
            $row = $row_next;
            $row_next = $list_handle->fetcharray($results, $x + 1);
            continue;
          }
        }

        if (isset($elements_sql['LS']) && $x > 1)
          try {
            $tmp .= eval_list($elements_sql['LS']);
          } catch (ReturnValue $e) {
            $nestedlist--;
            throw $e;
          }

        if ($x == $num_rows && isset($elements_sql['LN'])) {
          try {
            $tmp .= eval_list($elements_sql['LN']);
          } catch (ReturnValue $e) {
            $nestedlist--;
            throw $e;
          }
          break;
        }

        if (isset($elements_sql['LM']))
          try {
            $tmp .= eval_list($elements_sql['LM']);
          } catch (ReturnValue $e) {
            $nestedlist--;
            throw $e;
          }

        if ($x == $num_rows && isset($elements_sql['LL']))
          try {
            $tmp .= eval_list($elements_sql['LL']);
          } catch (ReturnValue $e) {
            $nestedlist--;
            throw $e;
          }

        $x++;
        /*offset and fetch */
        $row_prev = $row;
        $row = $row_next;
        $row_next = $list_handle->fetcharray($results, $x + 1);
      }
    }

    $list_handle->free_result($results);
  } else {
    if (isset($elements_sql['LD'])) {
      $list_gfld[$nestedlist]['numrows'] = $list_handle->numrows(0);
      try {
        $tmp = eval_list($elements_sql['LD']);
      } catch (ReturnValue $e) {
        $nestedlist--;
        throw $e;
      }
    }
  }
  $nestedlist--;
  return $tmp;
}

function return_fld($expr)
{
  global $nestedlist, $sql_row;
  $row = 'cur';
  $depth = $nestedlist;
  if (!list(, $val) = each($expr)) {
    if (!isset($sql_row[$depth]))
      throw new TempleetError("fld function not used in list context");
    return $sql_row[$depth];
  }
  $tmp_l = eval_list($val);
  if (list(, $new_depth) = each($expr)) {
    $new_depth = eval_list($new_depth);
    if ($new_depth <= 0)
      $new_depth = $depth + $new_depth;
    $depth = $new_depth;
    if (list(, $val) = each($expr)) {
      $row = eval_list($val);
    }
  }
  if (isset($sql_row[$depth][$row][$tmp_l]))
    return $sql_row[$depth][$row][$tmp_l];
  else
    return NULL;
}

function return_gfld($expr)
{
  global $nestedlist, $list_gfld;
  $tmp = '';
  $val = current($expr);
  next($expr);
  $depth = $nestedlist;
  $tmp_l = eval_list($val);
  if (list(, $new_depth) = each($expr)) {
    $new_depth = eval_list($new_depth);
    if ($new_depth <= 0)
      $new_depth = $depth + $new_depth;
    $depth = $new_depth;
  }
  if (isset($list_gfld[$depth][$tmp_l]))
    $tmp = $list_gfld[$depth][$tmp_l];
  return $tmp;
}

function return_allfld($array)
{
  global $nestedlist, $sql_row;
  return $sql_row[$nestedlist]['cur'];
}

function list_metaconnect($dbinfo)
{
  global $config, $global_var;

  if (isset($config['sqlconfig'][$dbinfo])) {
    $dbdata = &$config['sqlconfig'][$dbinfo];
  } elseif (isset($config['sqlconfig']['*'])) {
    $dbdata = &$config['sqlconfig']['*'];
    $dbinfo = '*';
  } else {
    throw new TempleetError("You didn't configure the database access!");
    return -1;
  }

  if (isset($dbdata['handle']))
    return $dbdata['handle'];

  if (!isset($dbdata['handle']) || !is_object($dbdata['handle'])) {
    $dbdata['handle'] = class_list::create($dbdata['type'], $dbdata);

    if (!isset($dbdata['handle'])) {
      throw new TempleetError("You don't have the module for " . $dbdata['type'] . "!\n");
      return -1;
    }
  }

  $global_var->list_errno = 0;
  $global_var->list_error = '';

  if (!$dbdata['handle']->connect()) {
    if ($global_var->list_error == '')
      $global_var->list_error = "Couldn't connect to the database\n";
    return -2;
  }

  return $dbdata['handle'];
}

function return_listtotaltime($expr)
{
  global $totaltime;
  return round($totaltime, 4);
}

function return_list_prev($expr)
{
  global $list_first;

  $first = current($expr);
  next($expr);
  $first = eval_list($first);

  $pagesize = current($expr);
  next($expr);
  $pagesize = eval_list($pagesize);

  $match = current($expr);
  next($expr);
  $nomatch = current($expr);
  next($expr);

  if ($first == 0)
    $res = eval_list($nomatch);
  else {
    $list_first = $first - $pagesize;
    if ($list_first < 0)
      $list_first = 0;
    $res = eval_list($match);
  }

  return $res;
}

function return_list_next($expr)
{
  global $list_first;

  $first = current($expr);
  next($expr);
  $first = eval_list($first);

  $pagesize = current($expr);
  next($expr);
  $pagesize = eval_list($pagesize);

  $nbrows = current($expr);
  next($expr);
  $nbrows = eval_list($nbrows);

  $match = current($expr);
  next($expr);
  $nomatch = current($expr);
  next($expr);

  if ($first + $pagesize >= $nbrows)
    $res = eval_list($nomatch);
  else {
    $list_first = $first + $pagesize;
    $res = eval_list($match);
  }

  return $res;
}

function return_getfirst($expr)
{
  global $list_first;

  return $list_first;
}

function return_list_lastid($expr)
{
  $dbinfo = current($expr);
  next($expr);
  $dbinfo = eval_list($dbinfo);

  $list_handle = list_metaconnect($dbinfo);

  if (!is_object($list_handle))
    switch ($list_handle) {
      case -1:
      case -2:
        return;
    }

  return $list_handle->lastid();
}

function return_list_escapestring($expr)
{
  if (!$expr) {
    return;
  }

  $dbinfo = null;
  if (is_array($expr)) {
    if (count($expr) > 1) {
      $dbinfo = current($expr);
      next($expr);
      $dbinfo = eval_list($dbinfo);
    }

    $string = current($expr);
    next($expr);
    $string = eval_list($string);
  } else {
    $string = $expr;
  }

  $list_handle = list_metaconnect($dbinfo);

  if (!is_object($list_handle))
    switch ($list_handle) {
      case -1:
      case -2:
        return;
    }

  return $list_handle->escapestring($string);
}

function return_dbfetcharray($expr)
{
  global $totaltime, $global_var;

  $dbinfo = current($expr);
  next($expr);
  $dbinfo = eval_list($dbinfo);

  $sql = current($expr);
  next($expr);
  $sql = eval_list($sql);

  $res = array();

  /* making SQL */

  $list_handle = list_metaconnect($dbinfo);

  if (!is_object($list_handle))
    return $list_handle;

  $time_start = getmicrotime();
  if (!($results = $list_handle->query($sql))) {
    if ($global_var->list_error == '')
      $global_var->list_error = "Could not query: $sql";
    return -3;
  }
  $time_end = getmicrotime();
  $time = $time_end - $time_start;
  $totaltime += $time;
  $global_var->templeet_total_queries++;

  if (isset($global_var->debugsql) && $global_var->debugsql != '') {
    if ($time > 0.1)
      $tempfile = "*** " . round($time, 2) . ": " . trim($sql) . ";\n";
    else
      $tempfile = round($time, 2) . ": " . trim($sql) . ";\n";

    if (file_put_contents($global_var->debugsql, $tempfile, FILE_APPEND) === FALSE)
      throw new TempleetError("Writing problem for " . $global_var->debugsql . "\n");
  }

  if (is_object($results)) {
    $x = 1;
    while ($row = $list_handle->fetcharray($results, $x++))
      $res[] = $row;
    $list_handle->free_result($results);
  }

  return $res;
}


/*
 * First parameter is the alias name of SQL connexion in Templeet
 * Second parameter is the table name
 * Returns a table with all columns names and type
 */

function return_list_querycolumns($expr)
{
  $dbinfo = current($expr);
  next($expr);
  $dbinfo = eval_list($dbinfo);

  $table_name = current($expr);
  next($expr);
  $table_name = eval_list($table_name);

  $list_handle = list_metaconnect($dbinfo);

  if (!is_object($list_handle))
    switch ($list_handle) {
      case -1:
      case -2:
        return;
    }
  return $list_handle->querycolumns($table_name);
}


function list_return()
{
  return array('list', 'fld', 'gfld', 'listtotaltime', 'list_prev', 'list_next', 'getfirst', 'list_lastid', 'dbfetcharray', 'list_querycolumns', 'list_escapestring');
}
