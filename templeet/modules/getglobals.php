<?php
/*
 * This code has been developed by:
 *
 * Fabien Penso
 * S�bastien Delahaye
 *
 * This module contains functions to get PHP predefined variables
 * such as the ones transmitted through forms, the server variables,
 * cookies, and environment variables.
 *
 * ~getget returns the GET variables
 * ~getpost returns the POST variables
 * ~getserver returns the SERVER variables
 * ~getenvironment returns the ENV variables
 * ~getcookie returns the COOKIE variables
 * ~getconf returns a config variable
 */

/*
 * This is to return the GET variables from forms
 */
function return_getget($expr)
{
  $val = current($expr);
  next($expr);
  if (!isset($val) || $val == '')
    return (get_magic_quotes_gpc() ? unmagicquote($_GET) : $_GET);
  $name = eval_list($val);
  return getget($name);
}

/*
 * This is to return the POST variables from forms
 */
function return_getpost($expr)
{
  $val = current($expr);
  next($expr);
  if (!isset($val) || $val == '')
    return (get_magic_quotes_gpc() ? unmagicquote($_POST) : $_POST);
  $name = eval_list($val);
  return getpost($name);
}

/*
 * This is to return the REQUEST variables
 */
function return_getrequest($expr)
{
  $val = current($expr);
  next($expr);
  if (!isset($val) || $val == '')
    return (get_magic_quotes_gpc() ? unmagicquote($_REQUEST) : $_REQUEST);
  $name = eval_list($val);
  if (isset($_REQUEST[$name]))
    return (get_magic_quotes_gpc() ? unmagicquote($_REQUEST[$name]) : $_REQUEST[$name]);
}

/*
 * This is to return the SERVER variables
 */
function return_getserver($expr)
{
  $val = current($expr);
  next($expr);
  if (!isset($val) || $val == '')
    return $_SERVER;
  $name = eval_list($val);
  return getserver($name);
}

/*
 * This is to return the ENVIRONMENT variables
 */
function return_getenvironment($expr)
{
  $val = current($expr);
  next($expr);
  if (!isset($val) || $val == '')
    return $_ENV;
  $name = eval_list($val);
  return getenvironment($name);
}

/*
 * This is to return the cookie variables
 */
function return_getcookie($expr)
{
  $val = current($expr);
  next($expr);
  if (!isset($val) || $val == '')
    return (get_magic_quotes_gpc() ? unmagicquote($_COOKIE) : $_COOKIE);
  $name = eval_list($val);
  return getcookie($name);
}

/*
 * This is to return the files variables
 */
function return_getfile($expr)
{
  $val = current($expr);
  next($expr);
  $name = eval_list($val);
  $val = current($expr);
  next($expr);
  $arg = eval_list($val);
  return getfile($name, $arg);
}

function getfile($name, $arg)
{
  if (isset($_FILES[$name]))
    return $_FILES[$name][$arg];
}


function getglobals_return()
{
  return array(
    'getget', 'getpost', 'getrequest', 'getserver',
    'getenvironnement', 'getcookie', 'getfile'
  );
}
