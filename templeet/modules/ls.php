<?php
/*
 * This code has been developed by:
 *
 * Pascal COURTOIS
 * Fabien Penso
 *
 * This module reproduces the UNIX shell command "ls".
 *
 * ~ls list files from a directory, and sort them.
 */

global $ls_nested, $ls_row;

$ls_nested = '0';
$ls_row = array();
function return_ls($expr)
{
  global $ls_nested, $ls_row;

  $val = current($expr);
  next($expr);
  $dir = eval_list($val);

  $val = current($expr);
  next($expr);
  $reg = eval_list($val);

  $val = current($expr);
  next($expr);
  $typefile = eval_list($val);

  $val = current($expr);
  next($expr);
  $order = eval_list($val);
  $val = current($expr);
  next($expr);
  $indice = intval(eval_list($val));//can be empty
  $val = current($expr);
  next($expr);
  $number = eval_list($val);

  $elements = array();
  while (list(, $val) = each($expr)) {
    $tmp_l = eval_list($val);

    if (
      $tmp_l == 'LM' ||
      $tmp_l == 'LR' ||
      $tmp_l == 'LF' ||
      $tmp_l == 'LL' ||
      $tmp_l == 'LN' ||
      $tmp_l == 'L1' ||
      $tmp_l == 'LD' ||
      $tmp_l == 'LS'
    ) {
      $val = current($expr);
      next($expr);
      if (!isset($val))
        return 'Error in ls!';
      $elements[$tmp_l] = $val;
    } else
      return 'Error in ls!';
  }

  if ($reg != '' && @preg_match($reg, '', $res) === FALSE)
    return "Error in regular expression in ls!";

  $ls_nested++;

  if (
    @file_exists($dir) === FALSE ||
    !$tmpdir = @opendir($dir)
  ) {
    $tmp = '';
    if (isset($elements['LD']))
      $tmp = eval_list($elements['LD']);
    $ls_nested--;
    return $tmp;
  }

  $dirlist = array();
  while (false !== ($file = readdir($tmpdir))) {
    if ($file == '.' || $file == '..')
      continue;

    if (($typefile == 'dir' || $typefile == 'd') && !is_dir($dir . '/' . $file))
      continue;

    if (
      is_dir($dir . '/' . $file) &&
      $typefile != 'dir' &&
      $typefile != '' &&
      $typefile != 'd'
    )
      continue;

    if ($reg != '' && !preg_match($reg, $file, $res))
      continue;

    $dotpos = strrpos($file, '.');
    $tmp_array = array(
      "rfile" => ($dotpos !== FALSE ? substr($file, 0, $dotpos) : $file),
      "file" => $file
    );

    $tmp_array2 = @stat($dir . '/' . $file);
    if (is_array($tmp_array2)) {
      while (list($key, $val) = each($tmp_array2)) {
        if (preg_match('/^[a-z]+$/', $key))
          $tmp_array[$key] = $val;
      }
    }

    if (isset($res) && is_array($res))
      $tmp_array['matches'] = $res;
    else
      $tmp_array['matches'] = NULL;

    $dirlist[] = $tmp_array;
  }
  closedir($tmpdir);

  switch ($order) {
    case 'M':
      usort($dirlist, fn($a, $b) => $a["mtime"] == $b["mtime"] ? strcmp($a["rfile"], $b["rfile"]) : ($a["mtime"] > $b["mtime"] ? 1 : -1));
      break;

    case 'MR':
      usort($dirlist, fn($a, $b) => $a["mtime"] == $b["mtime"] ? strcmp($a["rfile"], $b["rfile"]) : ($a["mtime"] < $b["mtime"] ? 1 : -1));
      break;

    case 'S':
      usort($dirlist, fn($a, $b) => strcmp($a["file"], $b["file"]));
      break;

    case 'SR':
      usort($dirlist, fn($a, $b) => -strcmp($a["file"], $b["file"]));
      break;

    case 'N':
      usort($dirlist, fn($a, $b) => (int) $a["file"] == (int) $b["file"] ? 0 : ((int) $a["file"] > (int) $b["file"] ? 1 : -1));
      break;

    case 'NR':
      usort($dirlist, fn($a, $b) => (int) $a["file"] == (int) $b["file"] ? 0 : ((int) $a["file"] < (int) $b["file"] ? 1 : -1));
      break;

    case 'RR':
      shuffle($dirlist);
      break;
  }

  $tmp = '';

  if (count($dirlist) == 0) {
    if (isset($elements['LD']))
      $tmp .= eval_list($elements['LD']);
  } else {
    $i = 0;
    /*
           * This is in case you asked for indice=2 and you only have one file
           */
    if ($indice > count($dirlist)) {
      if (isset($elements['LD']))
        $tmp .= eval_list($elements['LD']);
      $ls_nested--;
      return $tmp;
    }
    /* */
    while ($i < $indice) {
      each($dirlist);
      $i++;
    }

    $i = 1;
    $allcount = count($dirlist);
    if ($number == '')
      $number = $allcount - $indice;
    else {
      if ($number + $indice > $allcount)
        $number = $allcount - $indice;
    }

    while ($i <= $number && list($a, $b) = each($dirlist)) {
      $ls_row[$ls_nested] = $b;
      $ls_row[$ls_nested]['totalfile'] = $number;
      $ls_row[$ls_nested]['allfile'] = $allcount;
      $ls_row[$ls_nested]['counter'] = $i;
      if ($i == 1) {
        if ($i == $number && isset($elements['L1'])) {
          $tmp .= eval_list($elements['L1']);
          $ls_nested--;
          return $tmp;
        }
        if (isset($elements['LF']))
          $tmp .= eval_list($elements['LF']);
        if (isset($elements['LR'])) {
          $tmp .= eval_list($elements['LR']);
          $i++;
          continue;
        }
      }

      if (isset($elements['LS']) && $i > 1)
        $tmp .= eval_list($elements['LS']);

      if ($i == $number && isset($elements['LN'])) {
        $tmp .= eval_list($elements['LN']);
        break;
      }

      if (isset($elements['LM']))
        $tmp .= eval_list($elements['LM']);

      if ($i == $number && isset($elements['LL']))
        $tmp .= eval_list($elements['LL']);

      $i++;
    }
  }

  $ls_nested--;
  return $tmp;
}

function return_ls_fld($array)
{
  global $ls_nested, $ls_row, $ls_statfields;
  $val = current($array);
  next($array);
  $name = trim(eval_list($val));
  $depth = $ls_nested;
  $default = NULL;

  if (list(, $new_depth) = each($array)) {
    $new_depth = eval_list($new_depth);
    if ($new_depth <= 0) {
      $new_depth = $depth + $new_depth;
      $depth = $new_depth;
    }
    if (list(, $default) = each($array)) {
      $default = eval_list($default);
    }
  }

  if (!isset($ls_row[$depth]))
    return $default;

  if (isset($ls_row[$depth][$name]))
    return $ls_row[$depth][$name];
  else
    return $default;
}

function ls_return()
{
  return array('ls', 'ls_fld');
}
