<?php
/*
 * Pascal COURTOIS
 */

global $register_css_list, $register_beginscript_list, $register_endscript_list;
$register_css_list = $register_beginscript_list = $register_endscript_list = array();

function register_css($name, $path)
{
  global $register_css_list;
  $register_css_list[$name] = $path;
}

function pop_css()
{
  global $register_css_list;

  $out = '';
  foreach ($register_css_list as $key => $value) {
    $out .= '<link rel="stylesheet" type="text/css" href="' . $value . '" />' . "\n";
  }
  return $out;
}

function register_beginscript($name, $path)
{
  global $register_beginscript_list;
  $register_beginscript_list[$name] = $path;
}

function pop_beginscript()
{
  global $register_beginscript_list;

  $out = '';
  foreach ($register_beginscript_list as $key => $value) {
    $out .= '<script type="text/javascript" src="' . $value . '"></script>' . "\n";
  }
  return $out;
}

function register_endscript($name, $path)
{
  global $register_endscript_list;
  $register_endscript_list[$name] = $path;
}

function pop_endscript()
{
  global $register_endscript_list;

  $out = '';
  foreach ($register_endscript_list as $key => $value) {
    $out .= '<script type="text/javascript" src="' . $value . '"></script>' . "\n";
  }
  return $out;
}

function register_return()
{
  return array('register_css', 'pop_css', 'register_beginscript', 'pop_beginscript', 'register_endscript', 'pop_endscript');
}
