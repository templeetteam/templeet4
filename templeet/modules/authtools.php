<?php
/*
 * This module handles personnal informations
 *
 * This code has been developed by:
 *
 * Pascal Courtois
 *
 */

if (!class_exists("authedit")) {
  global $config;
  include_once($config['modulesdir'] . 'authedit.php');
}

function return_auth_swapfield($expr)
{
  if (!auth::$admin)
    return -107;

  $fieldname = current($expr);
  next($expr);
  $fieldname = eval_list($fieldname);

  $authfields = authedit::readfields();

  if (!isset($authfields[$fieldname]))
    return -121;

  $fieldsout = array();

  while (list($field, $value) = each($authfields)) {
    if ($fieldname == $field) {
      $field2 = key($authfields);
      $value2 = current($authfields);
      next($authfields);
      $fieldsout[$field2] = $value2;
      $fieldsout[$field] = $value;
    } else {
      $fieldsout[$field] = $value;
    }
  }

  auth::$config['infoperso'] = $fieldsout;
  authedit::writeconfigfile();

  return '';
}

function return_auth_modfield($expr)
{
  if (!auth::$admin)
    return -107;

  $fieldnameold = current($expr);
  next($expr);
  $fieldnameold = eval_list($fieldnameold);

  $fieldname = current($expr);
  next($expr);
  $fieldname = eval_list($fieldname);

  $desc = current($expr);
  next($expr);
  $desc = eval_list($desc);

  $authfields = authedit::readfields();

  if (
    !preg_match("/^[a-zA-Z_]\w*$/", $fieldname) ||
    !preg_match("/^[a-zA-Z_]\w*$/", $fieldnameold)
  )
    return -124;

  if (($fieldnameold != $fieldname && isset($authfields[$fieldname])) || empty($fieldname))
    return -121;

  if (
    ($desc["type"] == "sep" || $desc["type"] == "title") &&
    $authfields[$fieldnameold]["desc"]["type"] != "sep" &&
    $authfields[$fieldnameold]["desc"]["type"] != "title"
  ) {
    $res = auth::$handle->delfield(array($fieldnameold));
    if ($res)
      return $res;
  } elseif (
    ($desc["type"] != "sep" && $desc["type"] != "title") &&
    ($authfields[$fieldnameold]["desc"]["type"] == "sep" ||
      $authfields[$fieldnameold]["desc"]["type"] == "title")
  ) {
    $res = auth::$handle->addfield($fieldname, $desc);
    if ($res)
      return $res;
  } else {
    $res = auth::$handle->modfield($fieldnameold, $fieldname, $desc);
    if ($res)
      return $res;
  }

  if ($fieldnameold != $fieldname) {
    $fieldsout = array();
    foreach ($authfields as $field => $value) {
      if ($fieldnameold == $field) {
        $fieldsout[$fieldname] = $value;
        $fieldsout[$fieldname]["desc"] = $desc;
      } else {
        $fieldsout[$field] = $value;
      }
    }
    auth::$config['infoperso'] = $fieldsout;
  } else {
    $authfields[$fieldname]["desc"] = $desc;
    auth::$config['infoperso'] = $authfields;
  }
  authedit::writeconfigfile();

  return 0;
}

function return_auth_addfield($expr)
{
  if (!auth::$admin)
    return -107;

  $fieldname = current($expr);
  next($expr);
  $fieldname = eval_list($fieldname);

  $desc = current($expr);
  next($expr);
  $desc = eval_list($desc);

  $authfields = authedit::readfields();
  if (!preg_match("/^[a-zA-Z_]\w*$/", $fieldname))
    return -124;

  if (isset($authfields[$fieldname]) || empty($fieldname))
    return -121;

  if ($desc["type"] == "sep" || $desc["type"] == "title") {
    $authfields[$fieldname]["desc"]["type"] = $desc["type"];
    auth::$config['infoperso'] = $authfields;
    authedit::writeconfigfile();
    return 0;
  }

  $res = auth::$handle->addfield($fieldname, $desc);
  if ($res)
    return $res;

  $authfields[$fieldname]["desc"] = $desc;
  auth::$config['infoperso'] = $authfields;
  authedit::writeconfigfile();

  return 0;
}

function return_auth_delfield($expr)
{
  if (!auth::$admin)
    return -107;

  $fields = current($expr);
  next($expr);
  $fields = eval_list($fields);
  if (!is_array($fields))
    $fields = array($fields);

  $authfields = authedit::readfields();
  $delfield = FALSE;
  $okfields = array();
  foreach ($fields as $field) {
    if (isset($authfields[$field])) {
      if (
        isset($authfields[$field]["desc"]) &&
        $authfields[$field]["desc"]["type"] != "sep" &&
        $authfields[$field]["desc"]["type"] != "title"
      ) {
        $okfields[] = $field;
        $delfield = TRUE;
      }
    }
  }

  if ($delfield) {
    $res = auth::$handle->delfield($okfields);

    if ($res)
      return $res;
  }

  foreach ($fields as $field) {
    if (isset($authfields[$field])) {
      unset($authfields[$field]);
    }
  }

  auth::$config['infoperso'] = $authfields;
  authedit::writeconfigfile();

  return 0;
}

function authtools_getparam($prefix, $name, $param, $default)
{
  $res = "<table class=\"lo top\">";
  foreach ($param as $paramname => $info) {
    if (!is_array($info) || count($info) < 1)
      return "error in paramater description";

    $res .= "<tr><td class=\"lo top\">$paramname</td><td class=\"lo top\">";
    $default_value = $info[1];

    switch ($info[0]) {
      case "selectdiv":
        if (!is_array($info[2]))
          return "error in paramater description";

        $tmpres = '';
        $tmpdiv = '';
        $listname = '';
        if (isset($default[$paramname]) && isset($default[$paramname]['value']))
          $default_value = $default[$paramname]['value'];

        foreach ($info[2] as $selectname => $selectparam) {
          if ($listname == '')
            $listname = "'$selectname'";
          else
            $listname .= ",'$selectname'";

          if ($selectname == $default_value) {
            $selected = " selected=\"selected\"";
            $display = "inline";
          } else {
            $selected = '';
            $display = "none";
          }

          $tmpres .= "<option value=\"$selectname\"$selected>$selectname</option>\n";

          if (isset($default[$paramname]) && isset($default[$paramname]['value']) && isset($default[$paramname][$default[$paramname]['value']]))
            $tmp_default = $default[$paramname][$default[$paramname]['value']];
          else
            $tmp_default = array();
          $id = preg_replace("/\[(.*?)\]/", "_$1", "${prefix}${name}_${paramname}_$selectname");
          $tmpdiv .= "<div id=\"$id\" style=\"display:$display;\">\n" . authtools_getparam($prefix, "${name}[${paramname}][$selectname]", $selectparam, $tmp_default) . "</div>\n";
        }
        $id = preg_replace("/\[(.*?)\]/", "_$1", "${prefix}${name}_${paramname}_");

        $res .= "<select name=\"${prefix}${name}[${paramname}][value]\" onchange=\"selectDiv(this,'$id',new Array($listname))\">";
        $res .= $tmpres;
        $res .= "</select>";
        $res .= $tmpdiv;
        break;

      case "select":
        if (!is_array($info[2]))
          return "error in paramater description";

        $tmpres = '';
        if (isset($default[$paramname]) && is_string($default[$paramname]))
          $default_value = $default[$paramname];
        foreach ($info[2] as $selectname => $selectparam) {
          if ($selectname == $default_value)
            $selected = " selected=\"selected\"";
          else
            $selected = '';

          $tmpres .= "<option value=\"$selectname\"$selected>$selectparam</option>\n";
        }
        $res .= "<select name=\"${prefix}${name}[${paramname}]\">";
        $res .= $tmpres;
        $res .= "</select>";
        break;

      case "text":
        if (isset($default[$paramname]) && is_string($default[$paramname]))
          $default_value = $default[$paramname];
        $res .= "<input type=\"text\" name=\"${prefix}${name}[${paramname}]\" value=\"" . htmlentities($default_value) . "\" size=\"" . $info[2] . "\" />\n";
        break;

      case "pass":
        if (isset($default[$paramname]) && is_string($default[$paramname]))
          $default_value = $default[$paramname];
        $res .= "<input type=\"password\" name=\"${prefix}${name}[${paramname}]\" value=\"" . htmlentities($default_value) . "\" size=\"" . $info[2] . "\" />\n";
        break;

      default:
        $res .= $info[0];
    }
    $res .= "</td></tr>\n";
  }

  $res .= "</table>";
  return $res;
}

function return_auth_methodparam($expr)
{
  global $config;

  $method = current($expr);
  next($expr);
  $method = eval_list($method);

  $noparam = current($expr);
  next($expr);
  $noparam = eval_list($noparam);

  $tmp = auth::create($method);
  $param = $tmp->param();

  if (!is_array($param) || count($param) == 0)
    return $noparam;

  $default = current($expr);
  next($expr);
  if (isset($default))
    $default = eval_list($default);

  if (!isset($default) || $default == '') {
    if (isset(auth::$config['method']['param']))
      $default = auth::$config['method']['param'];
    else
      $default = array();
  }

  return authtools_getparam("method", "[$method]", $param, $default);
}

function return_authtools_getparam($expr)
{
  $prefix = current($expr);
  next($expr);
  $prefix = eval_list($prefix);

  $method = current($expr);
  next($expr);
  $method = eval_list($method);

  $param = current($expr);
  next($expr);
  $param = eval_list($param);

  $noparam = current($expr);
  next($expr);
  $noparam = eval_list($noparam);

  $default = current($expr);
  next($expr);
  $default = eval_list($default);

  if (!is_array($param) || count($param) == 0)
    return $noparam;

  return authtools_getparam($prefix, $method, $param, $default);
}

function auth_setmethod($method, $method_param)
{
  if (!auth::$admin)
    return -107;

  $tmp = auth::create($method);
  $res = $tmp->checkparam($method_param);
  if (!is_array($res)) {
    return $res;
  }
  auth::$config['method']['type'] = $method;
  auth::$config['method']['param'] = $res;
  authedit::writeconfigfile();

  return '';
}

function authtools_return()
{
  return array('auth_swapfield', 'auth_modfield', 'auth_addfield', 'auth_delfield', 'auth_methodparam', 'authtools_getparam', 'auth_setmethod');
}
