<?php
/*
 * This code has been developed by:
 *
 * Pascal COURTOIS
 * Fabien Penso
 *
 * Permits you to fetch RDF file to do whatever you want with it.
 *
 * Example:
 *
 * ~rdf('http://linuxfr.org/backend.rss',0,10,'LM','~rdf_fld('title')<br/>')
 *
 */

$nestedrdf = -1;
function return_rdf($array)
{
  global $nestedrdf, $rdf_array, $rdf_level, $rdf_item_row, $config;

  $val = current($array);
  next($array);
  $file = eval_list($val);
  $val = current($array);
  next($array);
  $indice = eval_list($val) + 1;
  $val = current($array);
  next($array);
  $number = eval_list($val) - 1;

  $elements = array();
  while (list(, $val) = each($array)) {
    $tmp_l = eval_list($val);

    if (
      $tmp_l == 'LM' ||
      $tmp_l == 'LF' ||
      $tmp_l == 'LL' ||
      $tmp_l == 'LE' ||
      $tmp_l == 'LD' ||
      $tmp_l == 'charset'
    ) {
      $val = current($array);
      next($array);
      if (!isset($val))
        return 'Error in rdf!';
      $elements[$tmp_l] = $val;
    } else
      return 'Error in rdf!';
  }
  $nestedrdf++;

  if (!isset($elements['charset']))
    $elements['charset'] = 'ISO-8859-1';
  else {
    $elements['charset'] = strtoupper($elements['charset']);
    if (
      $elements['charset'] != 'US-ASCII' &&
      $elements['charset'] != 'UTF-8' &&
      $elements['charset'] != 'ISO-8859-1'
    )
      return " rdf: error in charset ";
  }

  $rdfout = '';
  $res = rdf_xml2array($file, $elements['charset']);
  if ($res < 0) {
    if (isset($elements['LE']))
      $rdfout = eval_list($elements['LE']);
    else {
      if (!preg_match("|^https?://|", $file))
        $file = "data";
      switch ($res) {
        case -1:
          $error = error_get_last();
          return "can't open $file : " . $error['message'];
        case -2:
          return "XML error on $file";
      }
    }
  } else {
    if (isset($rdf_array[$nestedrdf]['rss']['channel']['item'])) {
      if (!isset($rdf_array[$nestedrdf]['rss']['channel']['item'][0])) {
        $tmp = $rdf_array[$nestedrdf]['rss']['channel']['item'];
        unset($rdf_array[$nestedrdf]['rss']['channel']['item']);
        $rdf_array[$nestedrdf]['rss']['channel']['item'][0] = $tmp;
      }
      $rdf_count_item = is_countable($rdf_array[$nestedrdf]['rss']['channel']['item']) ? count($rdf_array[$nestedrdf]['rss']['channel']['item']) : 0;
    } elseif (isset($rdf_array[$nestedrdf]['rdf:RDF']['item'])) {
      if (!isset($rdf_array[$nestedrdf]['rdf:RDF']['item'][0])) {
        $tmp = $rdf_array[$nestedrdf]['rdf:RDF']['item'];
        unset($rdf_array[$nestedrdf]['rdf:RDF']['item']);
        $rdf_array[$nestedrdf]['rdf:RDF']['item'][0] = $tmp;
      }
      $rdf_count_item = is_countable($rdf_array[$nestedrdf]['rdf:RDF']['item']) ? count($rdf_array[$nestedrdf]['rdf:RDF']['item']) : 0;
    } else
      $rdf_count_item = 0;

    if ($rdf_count_item == 0) {
      if (isset($elements['LF']))
        $rdfout .= eval_list($elements['LF']);
      if (isset($elements['LD']))
        $rdfout .= eval_list($elements['LD']);
      if (isset($elements['LL']))
        $rdfout .= eval_list($elements['LL']);
    } else {
      for ($i = $indice; $i <= ($indice + $number) && $i <= $rdf_count_item; $i++) {
        $rdf_item_row = $i;
        if ($i == $indice) {
          if (isset($elements['LF']))
            $rdfout .= eval_list($elements['LF']);
          if (isset($elements['LR'])) {
            $rdfout .= eval_list($elements['LR']);
            continue;
          }
        }

        if (isset($elements['LM']))
          $rdfout .= eval_list($elements['LM']);

        if ($i == $indice + $number || $i == $rdf_count_item) {
          if (isset($elements['LL']))
            $rdfout .= eval_list($elements['LL']);
        }
      }
    }
  }

  $nestedrdf--;
  return $rdfout;
}

function rdf_xml2array($file, $charset_output = "UTF-8")
{
  global $nestedrdf, $rdf_array, $rdf_level;

  $rdf_level[$nestedrdf] = 0;
  $rdf_array[$nestedrdf][$rdf_level[$nestedrdf]] = array();

  if (
    @is_file($file) ||
    substr($file, 0, 7) == 'http://' ||
    substr($file, 0, 8) == 'https://'
  ) {
    if (!($fp = @fopen($file, "r")))
      return -1;
    else {
      $data = '';
      while ($tmp = fread($fp, 4096))
        $data .= $tmp;

      fclose($fp);
    }
  } else
    $data = $file;

  $data = preg_replace('~.*(<\?xml.*?\?>.*<(?:rss|rdf).*<\/(?:rss|rdf.*?)>).*~s', "$1", $data);

  $data = preg_replace('/<(\/?)(i|b)>/', '&lt;\1\2&gt;', $data);

  if (preg_match('~<?xml.*?encoding="([^\"]*?)".*?\?>~', $data, $res))
    $charset_input = $res[1];
  else
    $charset_input = "UTF-8";

  $charset_input = strtoupper($charset_input);
  if ($charset_input != 'UTF-8')
    $charset_input = 'ISO-8859-1';

  $rdf_array[$nestedrdf]['!charset_input'] = $charset_input;
  $rdf_array[$nestedrdf]['!charset_output'] = $charset_output;

  $xml_parser = xml_parser_create($charset_input);
  xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, false); /* mandatory for xml 1.0 compliance */
  xml_set_element_handler($xml_parser, "rdfstartElement", "rdfendElement");
  xml_set_character_data_handler($xml_parser, "rdfdataElement");

  if (!xml_parse($xml_parser, $data, TRUE)) {
    xml_parser_free($xml_parser);

    if ($charset_input != 'ISO-8859-1')
      $charset_input = 'ISO-8859-1';
    elseif ($charset_input != 'UTF-8')
      $charset_input = 'UTF-8';
    else
      return -2;

    $rdf_level[$nestedrdf] = 0;
    $rdf_array[$nestedrdf][$rdf_level[$nestedrdf]] = array();
    $rdf_array[$nestedrdf]['!charset_input'] = $charset_input;

    $xml_parser = xml_parser_create($charset_input);
    xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, false); /* mandatory for xml 1.0 compliance */
    xml_set_element_handler($xml_parser, "rdfstartElement", "rdfendElement");
    xml_set_character_data_handler($xml_parser, "rdfdataElement");

    if (!xml_parse($xml_parser, $data, TRUE)) {
      xml_parser_free($xml_parser);
      return -2;
    }
  }
  xml_parser_free($xml_parser);

  $rdf_array[$nestedrdf] = $rdf_array[$nestedrdf][0];
  $rdf_array[$nestedrdf]['!encoding_input'] = $charset_input;
  $rdf_array[$nestedrdf]['!charset_output'] = $charset_output;

  return 0;
}

function return_rdf_fld($array)
{
  global $nestedrdf, $rdf_array, $rdf_item_row;

  $val = current($array);
  next($array);
  $name = eval_list($val);

  if ($name == '' && isset($rdf_array[$nestedrdf]))
    return $rdf_array[$nestedrdf];

  if ($name == "channeltitle") {
    if (isset($rdf_array[$nestedrdf]['rss']['channel']['title']))
      return $rdf_array[$nestedrdf]['rss']['channel']['title'];
    return '';
  }
  if ($name == "channellink") {
    if (isset($rdf_array[$nestedrdf]['rss']['channel']['link']))
      return $rdf_array[$nestedrdf]['rss']['channel']['link'];
    return '';
  }
  if ($name == "channeldescription") {
    if (isset($rdf_array[$nestedrdf]['rss']['channel']['description']))
      return $rdf_array[$nestedrdf]['rss']['channel']['description'];
    return '';
  }
  if ($name == "channellanguage") {
    if (isset($rdf_array[$nestedrdf]['rss']['channel']['language']))
      return $rdf_array[$nestedrdf]['rss']['channel']['language'];
    return '';
  }


  if (isset($rdf_array[$nestedrdf]['rss']['channel']['item'][$rdf_item_row - 1][$name]))
    return $rdf_array[$nestedrdf]['rss']['channel']['item'][$rdf_item_row - 1][$name];
  if (isset($rdf_array[$nestedrdf]['rdf:RDF']['item'][$rdf_item_row - 1][$name]))
    return $rdf_array[$nestedrdf]['rdf:RDF']['item'][$rdf_item_row - 1][$name];
}

function rdfstartElement($parser, $name, $attrs)
{
  global $nestedrdf, $rdf_array, $rdf_level;

  $rdf_level[$nestedrdf]++;
  $rdf_array[$nestedrdf][$rdf_level[$nestedrdf]] = array();
}

function rdfendElement($parser, $name)
{
  global $nestedrdf, $rdf_array, $rdf_level;

  if (isset($rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name])) {

    if (
      is_array($rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name]) &&
      isset($rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name][0])
    )
      $rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name][] = $rdf_array[$nestedrdf][$rdf_level[$nestedrdf]];
    else {
      $tmp = $rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name];
      $rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name] = array();
      $rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name][] = $tmp;
      $rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name][] = $rdf_array[$nestedrdf][$rdf_level[$nestedrdf]];
    }
  } else
    $rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name] = $rdf_array[$nestedrdf][$rdf_level[$nestedrdf]];

  if (is_array($rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name]) && (is_countable($rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name]) ? count($rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name]) : 0) == 0)
    $rdf_array[$nestedrdf][$rdf_level[$nestedrdf] - 1][$name] = '';

  unset($rdf_array[$nestedrdf][$rdf_level[$nestedrdf]]);

  $rdf_level[$nestedrdf]--;
}

function rdfdataElement($parser, $data, $attrs = '')
{
  global $nestedrdf, $rdf_array, $rdf_level;

  if (trim($data) == '')
    return;
  if ($rdf_array[$nestedrdf]['!charset_input'] == 'UTF-8') {
    if ($rdf_array[$nestedrdf]['!charset_output'] != 'UTF-8')
      $data = utf8_decode(preg_replace('/\xe2\x80\x99/', "'", $data));
  } else {
    if ($rdf_array[$nestedrdf]['!charset_output'] == 'UTF-8')
      $data = utf8_encode($data);
  }

  if (isset($rdf_array[$nestedrdf][$rdf_level[$nestedrdf]])) {
    if (is_array($rdf_array[$nestedrdf][$rdf_level[$nestedrdf]]))
      if ((is_countable($rdf_array[$nestedrdf][$rdf_level[$nestedrdf]]) ? count($rdf_array[$nestedrdf][$rdf_level[$nestedrdf]]) : 0) == 0)
        $rdf_array[$nestedrdf][$rdf_level[$nestedrdf]] = $data;
      else
        $rdf_array[$nestedrdf][$rdf_level[$nestedrdf]][] = $data;
    else
      $rdf_array[$nestedrdf][$rdf_level[$nestedrdf]] .= $data;
  } else {
    $rdf_array[$nestedrdf][$rdf_level[$nestedrdf]] = $data;
  }
}

function rdf_return()
{
  return array('rdf', 'rdf_fld');
}
