<?php
/*
 * This code has been developed by:
 *
 * Fabien Penso
 * Pascal COURTOIS
 *
 * This module contains the ~lines function.
 *
 * ~lines returns the chosen lines from a text.
 *  - the first argument is the text
 *  - the second is line number where the quote begins
 *  - the third (optional) is the line number where the quote ends.
 *    If it isn't set, the text is quoted till the end.
 */

$nestedlines = '0';
$lines_array = array();
function return_lines($array)
{
  global $nestedlines, $lines_array;

  $val = current($array);
  next($array);
  $content = eval_list($val);

  $myarray = preg_split("/\r?\n/", rtrim($content));

  $val = current($array);
  next($array);
  $indice = eval_list($val);
  $val = current($array);
  next($array);
  $number = eval_list($val);

  if ($number == '' || $number > (is_countable($myarray) ? count($myarray) : 0))
    $number = is_countable($myarray) ? count($myarray) : 0;

  if ($number < 0)
    return '';

  if ($indice == '')
    return is_countable($myarray) ? count($myarray) : 0;

  $elements = array();
  while (list(, $val) = each($array)) {
    $tmp_l = eval_list($val);
    if (
      $tmp_l == 'LM' ||
      $tmp_l == 'LD' ||
      $tmp_l == 'LN' ||
      $tmp_l == 'LR' ||
      $tmp_l == 'LF' ||
      $tmp_l == 'LL' ||
      $tmp_l == 'LS' ||
      $tmp_l == 'L1'
    ) {
      $val = current($array);
      next($array);
      if (!isset($val))
        throw new TempleetError('Error in lines! You didn\'t specify a second argument');

      $elements[$tmp_l] = $val;
    } else
      throw new TempleetError('Error in list! You must use LM, LR, LF, LL, LN, LS, L1 or LD as first argument');
  }

  $txt = '';
  $nestedlines++;

  if (trim($content) == '') {
    if (isset($elements['LD']))
      $txt .= eval_list($elements['LD']);
    $nestedlines--;
    return $txt;
  }

  if (isset($elements['L1']) && (is_countable($myarray) ? count($myarray) : 0) == 1) {
    $lines_array[$nestedlines]['line'] = $myarray[0];
    $lines_array[$nestedlines]['counter'] = 1;
    $txt .= eval_list($elements['L1']);
    $nestedlines--;
    return $txt;
  }

  for ($i = $indice; $i < ($indice + $number); $i++) {
    if (!isset($myarray[($i - 1)]))
      break;

    $lines_array[$nestedlines]['line'] = $myarray[($i - 1)];
    $lines_array[$nestedlines]['counter'] = $i;

    if ($i == $indice) {
      if (isset($elements['LF']))
        $txt .= eval_list($elements['LF']);
      if (isset($elements['LR'])) {
        $txt .= eval_list($elements['LR']);
        continue;
      }
    }

    if (($i == ($indice + $number - 1) || !isset($myarray[$i]))
      && isset($elements['LN'])
    ) {
      $txt .= eval_list($elements['LN']);
      break;
    }

    if (isset($elements['LM']))
      $txt .= eval_list($elements['LM']);

    if ((!isset($myarray[$i]) || ($i == ($indice + $number - 1))) && isset($elements['LL']))
      $txt .= eval_list($elements['LL']);

    if ($i != ($indice + $number - 1) && isset($elements['LS']))
      $txt .= eval_list($elements['LS']);
  }
  $nestedlines--;
  return $txt;
}

function return_lines_fld($array)
{
  global $nestedlines, $lines_array;

  $val = current($array);
  next($array);
  $name = eval_list($val);

  if (isset($lines_array[$nestedlines][$name]))
    return $lines_array[$nestedlines][$name];
}

function lines_return()
{
  return array('lines', 'lines_fld');
}
