<?php
/* This code has been developed by:
 *
 * Pascal COURTOIS
 *
 * This modules offers a MySQL backend.
 *
 */

class class_list_mysql
{

  private $db_link, $host, $login, $password, $database, $charset;

  function __construct($dbdata)
  {
    global $global_var;

    $global_var->list_errno = 0;
    $global_var->list_error = '';

    if (isset($dbdata['host'])) $this->host = $dbdata['host'];
    if (isset($dbdata['login'])) $this->login = $dbdata['login'];
    if (isset($dbdata['password'])) $this->password = $dbdata['password'];
    if (isset($dbdata['database'])) $this->database = $dbdata['database'];
    if (isset($dbdata['charset'])) $this->charset = $dbdata['charset'];
  }

  private function checktools()
  {
    if (!function_exists('class_tools_mysql::querycolumns')) {
      include_once('tools_mysql.php');
      $tools = new class_tools_mysql;
    }
  }

  public function query($sql)
  {

    if (!($results = mysqli_query($this->db_link, $sql))) {
      global $global_var, $config;

      $global_var->list_error = mysqli_error($this->db_link) . "\n";
      if ($config['verboseerror'])
        $global_var->list_error .= "query: $sql\n";
      $global_var->list_errno = mysqli_errno($this->db_link);
      return FALSE;
    }
    return $results;
  }

  public function numrows($results)
  {
    if (gettype($results) == 'object')
      $rows = mysqli_num_rows($results);
    else
      $rows = mysqli_affected_rows($this->db_link);

    return $rows;
  }

  public function fetcharray($results)
  {
    if ($result = mysqli_fetch_array($results, MYSQLI_ASSOC)) {
      return $result;
    } else {
      return FALSE;
    }
  }

  public function free_result($results)
  {
    mysqli_free_result($results);
  }

  public function connect()
  {
    global $global_var;

    if (!is_object($this->db_link)) {

      if (!function_exists('mysqli_connect')) {
        $global_var->list_error = "You don't have MySQLi support in your PHP!\n";
        return FALSE;
      }

      if (!$this->db_link = @mysqli_connect($this->host, $this->login, $this->password)) {
        $global_var->list_error = 'Cannot connect to MySQL host: ' . $this->host . ' login: ' . $this->login . "\n";
        return FALSE;
      }
    }

    if (!@mysqli_select_db($this->db_link, $this->database)) {
      $global_var->list_error = 'Cannot select MySQL database: ' . $this->database . ' host: ' . $this->host . ' login: ' . $this->login . '  ' . mysqli_error($this->db_link) . "\n";
      return FALSE;
    }

    if (isset($this->charset) && (!mysqli_query($this->db_link, "SET NAMES '" . $this->charset . "'") || !mysqli_query($this->db_link, "SET CHARACTER SET '" . $this->charset . "'"))) {

      $global_var->list_error = 'Cannot set charset for MySQL database: ' . $this->database . ' host: ' . $this->host . ' login: ' . $this->login . "\n";
      return FALSE;
    }

    return TRUE;
  }

  public function lastid()
  {
    return mysqli_insert_id($this->db_link);
  }

  public function escapestring($string)
  {
    return mysqli_real_escape_string($this->db_link, $string);
  }

  private function setselectoptions($selectoptions)
  {
    $tmp = '';
    while (list($key, $value) = each($selectoptions)) {
      switch ($value) {
        case 'DISTINCT':
        case 'DISTINCTROW':
          $tmp .= 'DISTINCT ';
          break;
      }
    }
    return $tmp;
  }

  private function eval2param($operator, $cond)
  {
    if (!isset($cond[1]) || !isset($cond[2]))
      return NULL;

    $p1 = $this->evalcond($cond[1]);
    $p2 = $this->evalcond($cond[2]);

    if (is_null($p1) || is_null($p2))
      return NULL;

    switch ($operator) {
      case "==":
        return "($p1 = $p2)";

      default:
        return "($p1 $operator $p2)";
    }
  }

  private function evalcond($cond)
  {
    if (!is_array($cond)) {
      if (is_numeric($cond))
        return $cond;
      if (!is_string($cond))
        return NULL;
      if (preg_match('/^priv:(.*)/', $cond, $res)) {
        $priv = $res[1];
        return 'SUBSTRING_INDEX(SUBSTRING_INDEX(priv,"(CREATElogin;",-1),")",1)';
      }

      if (preg_match('/^field:(.*)/', $cond, $res)) {
        return $res[1];
      }
      return '"' . mysqli_real_escape_string($this->db_link, $cond) . '"';
    }

    switch ($cond[0]) {
      case '<':
      case '>':
      case '>=':
      case '<=':
      case '==':
      case '!=':
      case '&&':
      case '||':
      case 'LIKE':
        return $this->eval2param($cond[0], $cond);

      case 'ISNOTNULL':
        return '( ' . $this->evalcond($cond[1]) . ' IS NOT NULL ) ';

      case 'ISNULL':
        return '( ' . $this->evalcond($cond[1]) . ' IS NULL ) ';


      default:
        return 'ERROR';
    }
  }


  private function query_where($where)
  {
    if (!isset($where))
      return '';

    if (is_string($where)) {
      if ($where == '')
        return '';
      else
        return " WHERE $where ";
    }

    if (is_array($where)) {
      return ' WHERE ' . $this->evalcond($where) . ' ';
    }

    die("mysql : query_where: strange parameter " . gettype($where) . " :" . $where);
  }

  public function select($selectoptions, $fields, $tablename, $where, $group, $having, $order, $limit)
  {
    $query = "SELECT ";
    if (is_string($selectoptions))
      $query .= $this->setselectoptions(array($selectoptions));
    elseif (is_array($selectoptions))
      $query .= $this->setselectoptions($selectoptions);

    if (is_array($fields)) {
      $tmp = '';
      while (list($key, $value) = each($fields)) {
        if ($tmp != '')
          $tmp .= ",";
        if (is_int($key))
          $tmp .= $value;
        else
          $tmp .= "$key AS $value";
      }
      $query .= $tmp . ' ';
    } elseif (is_string($fields)) {
      $query .= $fields . ' ';
    }

    if (is_array($tablename)) {
      $tmp = '';
      while (list($key, $value) = each($tablename)) {
        if ($tmp != '')
          $tmp .= ",";
        if (is_int($key))
          $tmp .= $value;
        else
          $tmp .= "$key AS $value";
      }
      $query .= "FROM $tmp ";
    } else {
      $query .= "FROM $tablename ";
    }

    $query .= $this->query_where($where);

    if (is_string($group) && $group != '') {
      $query .= "GROUP BY $group ";
    } elseif (is_array($group)) {
      $tmp = '';
      /* TODO
        while(list($key,$value)=each($tablename))
          {
            if ($tmp!='')
              $tmp.=",";
            if (is_int($key))
                $tmp.=$value;
              else
                $tmp.="$key AS $value";
          }
*/
      $query .= "GROUP BY $tmp ";
    }

    if (is_string($having) && $having != '') {
      $query .= "HAVING $having ";
    } elseif (is_array($having)) {
      $tmp = '';
      /* TODO
        while(list($key,$value)=each($tablename))
          {
            if ($tmp!='')
              $tmp.=",";
            if (is_int($key))
                $tmp.=$value;
              else
                $tmp.="$key AS $value";
          }
*/
      $query .= "HAVING $tmp ";
    }

    if (is_string($order) && $order != '') {
      $query .= "ORDER BY $order ";
    } elseif (is_array($order)) {
      $tmp = '';
      /* TODO
        while(list($key,$value)=each($tablename))
          {
            if ($tmp!='')
              $tmp.=",";
            if (is_int($key))
                $tmp.=$value;
              else
                $tmp.="$key AS $value";
          }
*/
      $query .= "ORDER BY $tmp ";
    }

    if (is_array($limit) && isset($limit[0]) && isset($limit[1])) {
      $query .= "LIMIT $limit[0],$limit[1]";
    }

    return $query;
  }

  public function insert($structure, $tablename, $fields)
  {
    $query = "INSERT INTO $tablename SET ";
    $first = 1;

    foreach ($fields as $key => $value) {
      if ($first)
        $first = 0;
      else
        $query .= ",";

      $query .= "$key=";
      if (
        isset($structure['fields'][$key]['type']) &&
        ($structure['fields'][$key]['type'] == "int"  ||
          $structure['fields'][$key]['type'] == "serial")
      )
        $query .= $value;
      else
        $query .= '"' . mysqli_real_escape_string($this->db_link, $value) . '"';
    }

    return $query;
  }

  public function update($structure, $tablename, $where, $fields)
  {
    $query = "UPDATE $tablename SET ";
    $first = 1;

    foreach ($fields as $key => $value) {
      if ($first)
        $first = 0;
      else
        $query .= ',';

      $query .= "$key=";

      if (
        isset($structure['fields'][$key]['type']) &&
        ($structure['fields'][$key]['type'] == 'int'  ||
          $structure['fields'][$key]['type'] == 'double'  ||
          $structure['fields'][$key]['type'] == 'uid' ||
          $structure['fields'][$key]['type'] == 'serial')
      ) {
        if ($value == '')
          $query .= 'NULL';
        else
          $query .= $value;
      } else {
        if ($structure['fields'][$key]['type'] == 'date' && $value == '')
          $query .= 'NULL';
        else
          $query .= '"' . mysqli_real_escape_string($this->db_link, $value) . '"';
      }
    }

    $query .= $this->query_where($where);
    return $query;
  }

  public function delete($tablename, $where)
  {
    $query = "DELETE FROM $tablename ";

    $query .= $this->query_where($where);

    return $query;
  }

  public function count()
  {
  }

  /* tools */
  public function querycolumns($tablename, $mode = 0)
  {
    $this->checktools();
    return class_tools_mysql::querycolumns($this, $tablename, $mode);
  }

  public function addcolumns($tablename, $structure)
  {
    $this->checktools();
    return class_tools_mysql::addcolumns($this, $tablename, $structure);
  }

  public function createtable($tablename, $structure)
  {
    $this->checktools();
    return class_tools_mysql::createtable($this, $tablename, $structure);
  }

  public function modfield($tablename, $fieldnameold, $fieldnamenew, $desc)
  {
    $this->checktools();
    return class_tools_mysql::modfield($this, $tablename, $fieldnameold, $fieldnamenew, $desc);
  }

  public function addfield($tablename, $fieldname, $desc)
  {
    $this->checktools();
    return class_tools_mysql::addfield($this, $tablename, $fieldname, $desc);
  }

  public function delfield($tablename, $arrayfieldname)
  {
    $this->checktools();
    return class_tools_mysql::delfield($this, $tablename, $arrayfieldname);
  }

  public function param()
  {
    $this->checktools();
    return class_tools_mysql::param();
  }
};
