<?php
/* This code has been developed by:
 *
 * Pascal COURTOIS
 *
 * This modules offers a postgresql backend.
 *
 */

class class_list_postgresql
{

  private $db_link, $host, $port, $login, $password, $database, $charset;

  function __construct($dbdata)
  {
    global $global_var;

    $global_var->list_errno = 0;
    $global_var->list_error = '';

    if (isset($dbdata['host'])) $this->host = $dbdata['host'];
    if (isset($dbdata['port'])) $this->port = $dbdata['port'];
    if (isset($dbdata['login'])) $this->login = $dbdata['login'];
    if (isset($dbdata['password'])) $this->password = $dbdata['password'];
    if (isset($dbdata['database'])) $this->database = $dbdata['database'];
    if (isset($dbdata['charset'])) $this->charset = $dbdata['charset'];
  }

  private function checktools()
  {
    if (!function_exists("class_tools_postgresql::querycolumns")) {
      include_once("tools_postgresql.php");
      $tools = new class_tools_postgresql;
    }
  }

  public function query($sql)
  {

    if (!@pg_send_query($this->db_link, $sql)) {
      global $global_var, $config;

      $global_var->list_error = pg_last_error() . "\n";
      if ($config['verboseerror'])
        $global_var->list_error .= "query: $sql\n";
      $global_var->list_errno = -1000;
      return FALSE;
    }

    $results = pg_get_result($this->db_link);
    $status = pg_result_status($results);
    if ($status != PGSQL_COMMAND_OK && $status != PGSQL_TUPLES_OK) {
      global $global_var;

      $global_var->list_error = pg_last_error() . "\n";
      $global_var->list_errno = pg_result_error_field($results, PGSQL_DIAG_SQLSTATE);

      return FALSE;
    }
    return $results;
  }

  public function numrows($results)
  {
    $rows = pg_affected_rows($results);

    return $rows;
  }

  public function fetcharray($results)
  {
    return pg_fetch_assoc($results);
  }

  public function free_result($results)
  {
    pg_free_result($results);
  }

  public function connect()
  {
    global $global_var;

    if (!is_resource($this->db_link)) {
      if (!function_exists('pg_connect')) {
        $global_var->list_error = "You don't have postgresql support in your PHP!\n";
        return FALSE;
      }

      $connectstring = "host='" . addcslashes($this->host, "\'") . "'" .
        (isset($this->port) ? ' port=' . addcslashes($this->port, "\'") . "'" : '') .
        " dbname='" . addcslashes($this->database, "\'") . "'" .
        " user='" . addcslashes($this->login, "\'") . "'";
      if (!$this->db_link = @pg_connect($connectstring . " password='" . addcslashes($this->password, "\'") . "'")) {
        $global_var->list_error = "Cannot connect to postgresql host: " . $this->host . " user: " . $this->login . "\n" . $connectstring . " password='XXXXXXXXX'";
        return FALSE;
      }

      if (isset($this->charset) && (!pg_query($this->db_link, "SET NAMES '" . $this->charset . "'"))) {
        $global_var->list_error = "Cannot set charset for postgresql database: " . $this->database . " host: " . $this->host . " user: " . $this->login . "\n";
        return FALSE;
      }
    }

    return TRUE;
  }

  public function lastid()
  {
    $rows = pg_fetch_row(
      pg_query($this->db_link, 'SELECT lastval();')
    );
    return $rows[0];
  }

  private function setselectoptions($selectoptions)
  {
    $tmp = '';
    while (list($key, $value) = each($selectoptions)) {
      switch ($value) {
        case 'DISTINCT':
        case 'DISTINCTROW':
          $tmp .= 'DISTINCT ';
          break;
      }
    }
    return $tmp;
  }

  private function eval2param($operator, $cond)
  {
    if (!isset($cond[1]) || !isset($cond[2]))
      return NULL;

    $p1 = $this->evalcond($cond[1]);
    $p2 = $this->evalcond($cond[2]);

    if (is_null($p1) || is_null($p2))
      return NULL;

    switch ($operator) {
      case '==':
        return "($p1 = $p2)";

      default:
        return "($p1 $operator $p2)";
    }
  }
  
  private function evalcond($cond)
  {
    if (!is_array($cond)) {
      if (is_numeric($cond))
        return $cond;
      if (!is_string($cond))
        return NULL;
      if (preg_match('/^priv:(.*)/', $cond, $res)) {
        $priv = $res[1];
        return 'SUBSTRING_INDEX(SUBSTRING_INDEX(priv,"(CREATEUSER;",-1),")",1)';
      }

      if (preg_match('/^field:(.*)/', $cond, $res)) {
        return $res[1];
      }
      return "'" . pg_escape_string($this->db_link, $cond) . "'";
    }

    switch ($cond[0]) {
      case '<':
      case '>':
      case '>=':
      case '<=':
      case '==':
      case '!=':
      case 'LIKE':
        return $this->eval2param($cond[0], $cond);
      case '&&':
        return $this->eval2param('AND', $cond);
      case '||':
        return $this->eval2param('OR', $cond);

      case 'ISNOTNULL':
        return "( " . $this->evalcond($cond[1]) . " IS NOT NULL ) ";

      case 'ISNULL':
        return "( " . $this->evalcond($cond[1]) . " IS NULL ) ";


      default:
        return 'ERROR';
    }
  }

  private function query_where($where)
  {
    if (!isset($where))
      return '';

    if (is_string($where)) {
      if ($where == '')
        return '';
      else
        return " WHERE $where ";
    }

    if (is_array($where)) {
      return ' WHERE ' . $this->evalcond($where) . ' ';
    }

    die('postgresql : query_where: strange parameter ' . gettype($where) . ' :' . $where);
  }

  public function select($selectoptions, $fields, $tablename, $where, $group, $having, $order, $limit)
  {
    $query = 'SELECT ';
    if (is_string($selectoptions))
      $query .= $this->setselectoptions(array($selectoptions));
    elseif (is_array($selectoptions))
      $query .= $this->setselectoptions($selectoptions);

    if (is_array($fields)) {
      $tmp = '';
      while (list($key, $value) = each($fields)) {
        if ($tmp != '')
          $tmp .= ',';
        if (is_int($key))
          $tmp .= $value;
        else
          $tmp .= "$key AS $value";
      }
      $query .= $tmp . ' ';
    } elseif (is_string($fields)) {
      $query .= $fields . ' ';
    }

    if (is_array($tablename)) {
      $tmp = '';
      while (list($key, $value) = each($tablename)) {
        if ($tmp != '')
          $tmp .= ",";
        if (is_int($key))
          $tmp .= $value;
        else
          $tmp .= "$key AS $value";
      }
      $query .= "FROM $tmp ";
    } else {
      $query .= "FROM $tablename ";
    }

    $query .= $this->query_where($where);

    if (is_string($group) && $group != '') {
      $query .= "GROUP BY $group ";
    } elseif (is_array($group)) {
      $tmp = '';
      /* TODO
        while(list($key,$value)=each($tablename))
          {
            if ($tmp!='')
              $tmp.=",";
            if (is_int($key))
                $tmp.=$value;
              else
                $tmp.="$key AS $value";
          }
*/
      $query .= "GROUP BY $tmp ";
    }

    if (is_string($having) && $having != '') {
      $query .= "HAVING $having ";
    } elseif (is_array($having)) {
      $tmp = '';
      /* TODO
        while(list($key,$value)=each($tablename))
          {
            if ($tmp!='')
              $tmp.=",";
            if (is_int($key))
                $tmp.=$value;
              else
                $tmp.="$key AS $value";
          }
*/
      $query .= "HAVING $tmp ";
    }

    if (is_string($order) && $order != '') {
      $query .= "ORDER BY $order ";
    } elseif (is_array($order)) {
      $tmp = '';
      /* TODO
        while(list($key,$value)=each($tablename))
          {
            if ($tmp!='')
              $tmp.=",";
            if (is_int($key))
                $tmp.=$value;
              else
                $tmp.="$key AS $value";
          }
*/
      $query .= "ORDER BY $tmp ";
    }

    if (is_array($limit) && isset($limit[0]) && isset($limit[1])) {
      $query .= "LIMIT $limit[1] OFFSET $limit[0]";
    }

    return $query;
  }

  function convertvalue($structure, $field, $value)
  {
    if (isset($structure['fields'][$field]['type'])) {
      switch ($structure['fields'][$field]['type']) {
        case 'date':
          if ($value == '')
            return "NULL";
          return "'" . pg_escape_string($this->db_link, substr($value, 0, 4) . "-" . substr($value, 4, 2) . "-" . substr($value, 6, 2)) . "'";
          break;

        case 'datetime':
          if ($value == '')
            return "NULL";
          return "'" . pg_escape_string($this->db_link, substr($value, 0, 4) . "-" . substr($value, 4, 2) . "-" . substr($value, 6, 2) .
            " " . substr($value, 8, 2) . ":" . substr($value, 10, 2) . ":" . substr($value, 12, 2)) . "'";
          break;

        case 'int':
        case 'uid':
        case 'serial':
        case 'double':
          return $value;
          break;

        default:
          return "'" . pg_escape_string($this->db_link, $value) . "'";
      }
    } else
      return "'" . pg_escape_string($this->db_link, $value) . "'";
  }

  public function insert($structure, $tablename, $fields)
  {
    $query = "INSERT INTO $tablename ";
    $first = 1;
    $columns = '';
    $values = '';
    foreach ($fields as $key => $value) {
      if ($first)
        $first = 0;
      else {
        $columns .= ",";
        $values .= ",";
      }

      $columns .= $key;
      $values .= $this->convertvalue($structure, $key, $value);
    }
    $query .= "($columns) VALUES ($values)";

    return $query;
  }

  public function update($structure, $tablename, $where, $fields)
  {
    $query = "UPDATE $tablename SET ";
    $first = 1;

    foreach ($fields as $key => $value) {
      if ($first)
        $first = 0;
      else
        $query .= ",";

      $query .= "$key=" . $this->convertvalue($structure, $key, $value);
    }

    $query .= $this->query_where($where);
    return $query;
  }

  public function delete($tablename, $where)
  {
    $query = "DELETE FROM $tablename ";

    $query .= $this->query_where($where);

    return $query;
  }

  public function count()
  {
  }

  /* tools */
  public function querycolumns($tablename, $mode = 0)
  {
    $this->checktools();
    return class_tools_postgresql::querycolumns($this, $tablename, $mode);
  }

  public function addcolumns($tablename, $structure)
  {
    $this->checktools();
    return class_tools_postgresql::addcolumns($this, $tablename, $structure);
  }

  public function createtable($tablename, $structure)
  {
    $this->checktools();
    return class_tools_postgresql::createtable($this, $tablename, $structure);
  }

  public function modfield($tablename, $fieldnameold, $fieldnamenew, $desc)
  {
    $this->checktools();
    return class_tools_postgresql::modfield($this, $tablename, $fieldnameold, $fieldnamenew, $desc);
  }

  public function addfield($tablename, $fieldname, $desc)
  {
    $this->checktools();
    return class_tools_postgresql::addfield($this, $tablename, $fieldname, $desc);
  }

  public function delfield($tablename, $arrayfieldname)
  {
    $this->checktools();
    return class_tools_postgresql::delfield($this, $tablename, $arrayfieldname);
  }
  public function param()
  {
    $this->checktools();
    return class_tools_postgresql::param();
  }
};
