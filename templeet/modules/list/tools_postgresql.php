<?php
/* This code has been developed by:
 *
 * Pascal COURTOIS
 *
 * This modules offers tools for postgresql backend.
 *
 */

class class_tools_postgresql
{
  static function querycolumns($pgsql_obj, $tablename, $mode = 0)
  {
    $results = $pgsql_obj->query("
              SELECT attname as name, t.typname as type,attnotnull as notnull,
                (SELECT substring(pg_catalog.pg_get_expr(d.adbin, d.adrelid) for 128)
                 FROM pg_catalog.pg_attrdef d
                 WHERE d.adrelid = a.attrelid AND d.adnum = a.attnum AND a.atthasdef) as default,
                 CASE a.atttypmod
                   WHEN -1 THEN -1
                   ELSE a.atttypmod-4
                 END as len
              FROM pg_class, pg_attribute a, pg_type t
              WHERE relname='$tablename' and pg_class.oid=attrelid and attnum>0 and not attisdropped and a.atttypid = t.oid ;
             ");

    if (!$results)
      return FALSE;

    $nbrows = pg_num_rows($results);

    $columns = array();
    $i = 0;
    while ($i < $nbrows) {
      $row = $pgsql_obj->fetcharray($results);
      $null = $row['notnull'] == 'f';
      $key = '';
      switch ($row['type']) {
        case 'int4':
          if (preg_match('/^nextval/', $row['default']))
            $type = 'serial';
          else
            $type = 'int';
          break;
        case 'float8':
          $type = "double";
          break;
        case 'timestamp':
          $type = 'datetime';
          break;
        case 'varchar':
        case 'text':
        case 'date':
        case 'bool':
          $type = $row['type'];
          break;

        default:
          $type = "unknown";
      }
      if ($mode == 0)
        $columns[] = array('name' => $row['name'], 'type' => $type);
      else
        $columns[$row['name']] = array('type' => $type, 'size' => $row['len'], 'null' => $null, 'key' => $key);

      $i++;
    }
    return $columns;
  }

  static function addcolumns($pgsql_obj, $tablename, $structure)
  {
    $query = '';
    while (list($key, $value) = each($structure['fields'])) {
      if ($query != '')
        $query .= ',';

      $tmp = '';
      switch ($structure['fields'][$key]['type']) {
        case 'uid':
        case 'serial':
          $tmp = "$key SERIAL";
          break;
        case 'int':
          $tmp = "$key INT";
          break;
        case 'date':
          $tmp = "$key DATE";
          break;
        case 'datetime':
          $tmp = "$key TIMESTAMP";
          break;
        case 'varchar':
          $tmp = "$key VARCHAR(" . $structure['fields'][$key]['size'] . ")";
          break;
        case 'text':
          $tmp = "$key text";
          break;
      }
      if (isset($structure['fields'][$key]['notnull']) && $structure['fields'][$key]['notnull'])
        $tmp .= ' NOT NULL';
      if (isset($structure['fields'][$key]['default'])) {
        if ($structure['fields'][$key]['default'] === 'NULL') {
          $tmp .= ' DEFAULT NULL';
        } elseif ($structure['fields'][$key]['type'] == 'int')
          $tmp .= ' DEFAULT ' . $structure['fields'][$key]['default'];
        else
          $tmp .= " DEFAULT '" . $structure['fields'][$key]['default'] . "'";
      }

      $query .= "ADD COLUMN $tmp";
    }

    if (isset($structure['key']) && is_array($structure['key'])) {
      while (list($key, $value) = each($structure['key'])) {
        if ($query != '')
          $query .= ',';

        $unique = 0;
        $indexname = '';

        if (is_array($value)) {
          if (isset($value['fields'])) {
            $fields = $value['fields'];
            if (isset($value['options'])) {
              if (is_array($value['options'])) {
                if (isset($value['options']['unique']))
                  $unique = 1;
                if (isset($value['options']['indexname']))
                  $indexname = $value['options']['indexname'];
              }
            }
          } else {
            if ($value[0] == 'unique')
              $unique = 1;

            $fields = array($value[1]);
          }
        } else {
          $fields = array($value);
        }

        if ($unique)
          $tmp = 'ADD UNIQUE ';
        else
          $tmp = 'ADD INDEX ';

        if ($indexname != '')
          $tmp .= $indexname . ' ';

        $tmp .= '(' . implode(',', $fields) . ')';

        $query .= $tmp;
      }
    }

    $query = "ALTER TABLE $tablename $query";

    $results = $pgsql_obj->query($query);

    if (!$results)
      return FALSE;

    return TRUE;
  }

  static function createtable($pgsql_obj, $tablename, $structure)
  {

    $query = 'BEGIN';
    $results = $pgsql_obj->query($query);
    if (!$results)
      return $results;

    $query = '';
    while (list($key, $value) = each($structure['fields'])) {
      if ($query != '')
        $query .= ',';

      $tmp = '';
      switch ($structure['fields'][$key]['type']) {
        case 'uid':
        case 'serial':
          $tmp = "$key SERIAL PRIMARY KEY";
          break;
        case 'int':
          $tmp = "$key INT";
          break;
        case 'date':
          $tmp = "$key DATE";
          break;
        case 'datetime':
          $tmp = "$key TIMESTAMP";
          break;
        case 'varchar':
          $tmp = "$key VARCHAR(" . $structure['fields'][$key]['size'] . ")";
          break;
        case 'text':
          $tmp = "$key text";
          break;
      }
      if (isset($structure['fields'][$key]['notnull']) && $structure['fields'][$key]['notnull'])
        $tmp .= " NOT NULL";
      if (isset($structure['fields'][$key]['default'])) {
        if ($structure['fields'][$key]['default'] === 'NULL') {
          $tmp .= " DEFAULT NULL";
        } elseif ($structure['fields'][$key]['type'] == 'int')
          $tmp .= " DEFAULT " . $structure['fields'][$key]['default'];
        else
          $tmp .= " DEFAULT '" . $structure['fields'][$key]['default'] . "'";
      }

      $query .= $tmp;
    }

    $query = "CREATE TABLE $tablename (" . $query . ") WITH OIDS";

    $results = $pgsql_obj->query($query);
    if (!$results)
      return $results;

    while (list($key, $value) = each($structure['key'])) {
      $unique = 0;
      $indexname = '';
      if (is_array($value)) {
        if (isset($value['fields'])) {
          $fields = $value['fields'];
          if (isset($value['options'])) {
            if (is_array($value['options'])) {
              if (isset($value['options']['unique']))
                $unique = 1;
              if (isset($value['options']['indexname']))
                $indexname = $value['options']['indexname'];
            }
          }
        } else {
          $fields = $value;
          if ($value[0] == 'unique') {
            $unique = 1;
            array_shift($fields);
          }
        }
      } else {
        $fields = array($value);
      }
      $query = "CREATE ";
      if ($unique)
        $query .= 'UNIQUE ';

      if ($indexname == '')
        $indexname = implode('_', $fields);

      $query .= 'INDEX ' . $indexname . ' ON $tablename ' . '(' . implode(',', $fields) . ')';

      $results = $pgsql_obj->query($query);
      if (!$results)
        return $results;
    }

    $query = 'COMMIT';
    $results = $pgsql_obj->query($query);
    if (!$results)
      return $results;

    return $results;
  }

  private function check_array_param($param)
  {
    return
      isset($param['host']) &&
      isset($param['login']) &&
      isset($param['password']) &&
      isset($param['database']) &&
      isset($param['type']);
  }

  static function param()
  {
    global $config;
    $method = return_auth_getmethod();


    $host = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['host']) ? auth::$config['methodparam']['host'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['auth']) && isset($config['sqlconfig']['auth']['host']) ? $config['sqlconfig']['auth']['host'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['*']) && isset($config['sqlconfig']['*']['host']) ? $config['sqlconfig']['*']['host'] : "localhost")));

    $database = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['database']) ? auth::$config['methodparam']['database'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['auth']) && isset($config['sqlconfig']['auth']['database']) ? $config['sqlconfig']['auth']['database'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['*']) && isset($config['sqlconfig']['*']['database']) ? $config['sqlconfig']['*']['database'] : "templeetauth")));

    $login = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['login']) ? auth::$config['methodparam']['login'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['auth']) && isset($config['sqlconfig']['auth']['login']) ? $config['sqlconfig']['auth']['login'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['*']) && isset($config['sqlconfig']['*']['login']) ? $config['sqlconfig']['*']['login'] : '')));

    $password = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['password']) ? auth::$config['methodparam']['password'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['auth']) && isset($config['sqlconfig']['auth']['password']) ? $config['sqlconfig']['auth']['password'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['*']) && isset($config['sqlconfig']['*']['password']) ? $config['sqlconfig']['*']['password'] : '')));

    $charset = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['charset']) ? auth::$config['methodparam']['charset'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['auth']) && isset($config['sqlconfig']['auth']['charset']) ? $config['sqlconfig']['auth']['charset'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['*']) && isset($config['sqlconfig']['*']['charset']) ? $config['sqlconfig']['*']['charset'] : "UTF-8")));

    return array(
      "host" => array('text', $host, 40),
      "database" => array('text', $database, 40),
      "login" => array('text', $login, 40),
      "password" => array('pass', $password, 40),
      "charset" => array(
        'select', $charset, array(
          'UTF-8' => 'UTF-8',
          'ISO8859-1' => 'ISO8859-1'
        )
      )
    );
  }

  static function columndef($desc)
  {
    if (!is_array($desc) || !isset($desc['type']))
      return -12;

    switch ($desc['type']) {
      case 'text':
        if (!isset($desc[1]) || !preg_match('/^\d+$/', $desc[1]) || $desc[1] > 1000)
          return -12;
        return 'varchar(' . $desc[1] . ')';

      case 'date':
        return 'date';
        break;

      case 'timestamp':
        return 'datetime';
        break;

      case 'textarea':
        return 'text';
        break;

      case 'int':
        return 'int';
        break;

      case 'double':
        return 'double precision';
        break;

      case 'checkbox':
        return 'bool';
        break;


      default:
        return -12;
    }
  }

  static function modfield($pgsql_obj, $tablename, $fieldnameold, $fieldnamenew, $desc)
  {
    $columndef = class_tools_postgresql::columndef($desc);
    if (!is_string($columndef))
      return $columndef;

    if ($fieldnameold != $fieldnamenew) {
      $query = "alter table $tablename rename $fieldnameold to $fieldnamenew;";
      $results = $pgsql_obj->query($query);
      if ($results === FALSE)
        return -1000;
    }

    $query = "alter table $tablename alter column $fieldnamenew TYPE $columndef;";
    $results = $pgsql_obj->query($query);
    if ($results === FALSE) {
      $query = "alter table $tablename drop column $fieldnamenew ;";
      $results = $pgsql_obj->query($query);
      if ($results === FALSE)
        return -1000;

      $columndef = class_tools_postgresql::columndef($desc);
      if (!is_string($columndef))
        return $columndef;

      $query = "alter table $tablename add column $fieldnamenew $columndef;";
      $results = $pgsql_obj->query($query);
      if ($results === FALSE)
        return -1000;

      return 0;
    }
    return 0;
  }


  static function addfield($pgsql_obj, $tablename, $fieldname, $desc)
  {
    $columndef = class_tools_postgresql::columndef($desc);
    if (!is_string($columndef))
      return $columndef;

    $query = "alter table $tablename add column $fieldname $columndef;";
    $results = $pgsql_obj->query($query);
    if ($results === FALSE)
      return -1000;

    return 0;
  }

  static function delfield($pgsql_obj, $tablename, $arrayfieldname)
  {
    $listfields = '';
    foreach ($arrayfieldname as $fieldname) {
      if ($listfields != '')
        $listfields .= ",";
      $listfields .= " drop column $fieldname";
    }
    $query = "alter table $tablename $listfields;";
    $results = $pgsql_obj->query($query);
    if ($results === FALSE)
      return -1000;

    return 0;
  }
}
function return_tools_postgresql_param($array)
{
  return class_tools_postgresql::param();
}

function tools_postgresql_return()
{
  return array('tools_postgresql_param');
}
