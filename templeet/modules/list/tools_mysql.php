<?php
/* This code has been developed by:
 *
 * Pascal COURTOIS
 *
 * This modules offers tools for MySQL backend.
 *
 */

class class_tools_mysql
{
  static function querycolumns($mysql_obj, $tablename, $mode = 0)
  {
    $results = $mysql_obj->query("show columns from $tablename;");

    if (!$results)
      return FALSE;

    $nbrows = mysqli_num_rows($results);

    $columns = array();

    $i = 0;
    while ($i < $nbrows) {
      $row = $mysql_obj->fetcharray($results);
      if (preg_match('/^int\((\d+)\)/', $row['Type'], $res)) {
        if ($row['Extra'] == "auto_increment")
          $type = "serial";
        else
          $type = 'int';
        $size = $res[1];
      } elseif (preg_match('/^varchar\((\d+)\)/', $row['Type'], $res)) {
        $type = 'varchar';
        $size = $res[1];
      } elseif (preg_match('/^(?:text|blob)/', $row['Type'])) {
        $type = 'text';
        $size = NULL;
      } elseif ('tinyint(1)' == $row['Type']) {
        $type = 'bool';
        $size = NULL;
      } else {
        $type = $row['Type'];
        $size = NULL;
      }

      $null = ($row['Null'] == 'YES');
      $key = $row['Key'];

      if ($mode == 0)
        $columns[] = array('name' => $row["Field"], 'type' => $type, 'size' => $size);
      else
        $columns[$row["Field"]] = array('type' => $type, 'size' => $size, 'null' => $null, 'key' => $key);
      $i++;
    }
    return $columns;
  }

  static function addcolumns($mysql_obj, $tablename, $structure)
  {
    $query = '';
    while (list($key, $value) = each($structure['fields'])) {
      if ($query != '')
        $query .= ',';

      $tmp = '';
      switch ($structure['fields'][$key]['type']) {
        case 'uid':
        case 'serial':
          $tmp = "$key INT NOT NULL AUTO_INCREMENT";
          break;
        case 'int':
          $tmp = "$key INT";
          break;
        case 'date':
          $tmp = "$key DATE";
          break;
        case 'datetime':
          $tmp = "$key DATETIME";
          break;
        case 'varchar':
          $tmp = "$key VARCHAR(" . $structure['fields'][$key]['size'] . ")";
          break;
        case 'text':
          $tmp = "$key text";
          break;
      }
      if (isset($structure['fields'][$key]['notnull']) && $structure['fields'][$key]['notnull'])
        $tmp .= ' NOT NULL';
      if (isset($structure['fields'][$key]['default'])) {
        if ($structure['fields'][$key]['default'] === 'NULL') {
          $tmp .= " DEFAULT NULL";
        } elseif ($structure['fields'][$key]['type'] == 'int')
          $tmp .= " DEFAULT " . $structure['fields'][$key]['default'];
        else
          $tmp .= " DEFAULT '" . $structure['fields'][$key]['default'] . "'";
      }

      $query .= "ADD COLUMN $tmp\n";
    }

    if (isset($structure['key']) && is_array($structure['key'])) {
      while (list($key, $value) = each($structure['key'])) {
        if ($query != '')
          $query .= ',';

        $unique = 0;
        $indexname = '';

        if (is_array($value)) {
          if (isset($value['fields'])) {
            $fields = $value['fields'];
            if (isset($value['options'])) {
              if (is_array($value['options'])) {
                if (isset($value['options']['unique']))
                  $unique = 1;
                if (isset($value['options']['indexname']))
                  $indexname = $value['options']['indexname'];
              }
            }
          } else {
            if ($value[0] == 'unique')
              $unique = 1;

            $fields = array($value[1]);
          }
        } else {
          $fields = array($value);
        }

        if ($unique)
          $tmp = 'ADD UNIQUE ';
        else
          $tmp = 'ADD INDEX ';

        if ($indexname != '')
          $tmp .= $indexname . ' ';

        $tmp .= '(' . implode(',', $fields) . ')';

        $query .= $tmp . "\n";
      }
    }

    $query = "ALTER TABLE $tablename $query";
    $results = $mysql_obj->query($query);

    if (!$results)
      return FALSE;

    return TRUE;
  }

  static function createtable($mysql_obj, $tablename, $structure)
  {
    $query = '';
    while (list($key, $value) = each($structure['fields'])) {
      if ($query != '')
        $query .= ',';

      $tmp = '';
      switch ($structure['fields'][$key]['type']) {
        case 'uid':
        case 'serial':
          $tmp = "$key INT NOT NULL AUTO_INCREMENT";
          break;
        case 'int':
          $tmp = "$key INT";
          break;
        case 'date':
          $tmp = "$key DATE";
          break;
        case 'datetime':
          $tmp = "$key DATETIME";
          break;
        case 'varchar':
          $tmp = "$key VARCHAR(" . $structure['fields'][$key]['size'] . ")";
          break;
        case 'text':
          $tmp = "$key text";
          break;
      }
      if (isset($structure['fields'][$key]['notnull']) && $structure['fields'][$key]['notnull'])
        $tmp .= " NOT NULL";
      if (isset($structure['fields'][$key]['default'])) {
        if ($structure['fields'][$key]['default'] === 'NULL') {
          $tmp .= " DEFAULT NULL";
        } elseif ($structure['fields'][$key]['type'] == 'int')
          $tmp .= " DEFAULT " . $structure['fields'][$key]['default'];
        else
          $tmp .= " DEFAULT '" . $structure['fields'][$key]['default'] . "'";
      }

      $query .= $tmp . "\n";
    }

    while (list($key, $value) = each($structure['key'])) {
      if ($query != '')
        $query .= ',';

      $unique = 0;
      $indexname = '';

      if (is_array($value)) {
        if (isset($value['fields'])) {
          $fields = $value['fields'];
          if (isset($value['options'])) {
            if (is_array($value['options'])) {
              if (isset($value['options']['unique']))
                $unique = 1;
              if (isset($value['options']['indexname']))
                $indexname = $value['options']['indexname'];
            }
          }
        } else {
          $fields = $value;
          if ($value[0] == 'unique') {
            $unique = 1;
            array_shift($fields);
          }
        }
      } else {
        $fields = array($value);
      }

      if ($unique)
        $tmp = 'UNIQUE ';
      else
        $tmp = 'INDEX ';

      if ($indexname != '')
        $tmp .= $indexname . ' ';

      $tmp .= '(' . implode(',', $fields) . ')';

      $query .= $tmp . "\n";
    }

    $query = "CREATE TABLE $tablename (" . $query . ")";;
    if (isset($structure['charset']))
      $query .= " DEFAULT CHARSET=" . $structure['charset'];

    $results = $mysql_obj->query($query);
    return $results;
  }

  private function check_array_param($param)
  {
    return
      isset($param['host']) &&
      isset($param['login']) &&
      isset($param['password']) &&
      isset($param['database']) &&
      isset($param['type']);
  }

  static function param()
  {
    global $config;

    $host = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['host']) ? auth::$config['methodparam']['host'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['auth']) && isset($config['sqlconfig']['auth']['host']) ? $config['sqlconfig']['auth']['host'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['*']) && isset($config['sqlconfig']['*']['host']) ? $config['sqlconfig']['*']['host'] : "localhost")));

    $database = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['database']) ? auth::$config['methodparam']['database'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['auth']) && isset($config['sqlconfig']['auth']['database']) ? $config['sqlconfig']['auth']['database'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['*']) && isset($config['sqlconfig']['*']['database']) ? $config['sqlconfig']['*']['database'] : "templeetauth")));

    $login = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['login']) ? auth::$config['methodparam']['login'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['auth']) && isset($config['sqlconfig']['auth']['login']) ? $config['sqlconfig']['auth']['login'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['*']) && isset($config['sqlconfig']['*']['login']) ? $config['sqlconfig']['*']['login'] : '')));

    $password = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['password']) ? auth::$config['methodparam']['password'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['auth']) && isset($config['sqlconfig']['auth']['password']) ? $config['sqlconfig']['auth']['password'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['*']) && isset($config['sqlconfig']['*']['password']) ? $config['sqlconfig']['*']['password'] : '')));

    $charset = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['charset']) ? auth::$config['methodparam']['charset'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['auth']) && isset($config['sqlconfig']['auth']['charset']) ? $config['sqlconfig']['auth']['charset'] : (isset($config['sqlconfig']) && isset($config['sqlconfig']['*']) && isset($config['sqlconfig']['*']['charset']) ? $config['sqlconfig']['*']['charset'] : "UTF-8")));

    return array(
      "host" => array('text', $host, 40),
      "database" => array('text', $database, 40),
      "login" => array('text', $login, 40),
      "password" => array('pass', $password, 40),
      "charset" => array(
        'select', $charset, array(
          'UTF8' => 'UTF-8',
          'latin1' => 'ISO8859-1'
        )
      )
    );
  }

  static function columndef($desc)
  {
    if (!is_array($desc) || !isset($desc["type"]))
      return -12;

    switch ($desc["type"]) {
      case 'text':
        if (!isset($desc[1]) || !preg_match("/^\d+$/", $desc[1]) || $desc[1] > 1000)
          return -12;
        return "varchar(" . $desc[1] . ")";

      case 'date':
        return 'date';
        break;

      case 'datetime':
        return 'datetime';
        break;

      case 'textarea':
        return 'text';
        break;

      case 'int':
        return 'int';
        break;

      case 'double':
        return 'double';
        break;

      case 'checkbox':
        return 'bool';
        break;


      default:
        return -12;
    }
  }

  static function modfield($mysql_obj, $tablename, $fieldnameold, $fieldnamenew, $desc)
  {
    $columndef = class_tools_mysql::columndef($desc);
    if (!is_string($columndef))
      return $columndef;

    $query = "alter table $tablename change column $fieldnameold $fieldnamenew $columndef;";
    $results = $mysql_obj->query($query);
    if (!$results) {
      if (mysqli_errno($mysql_obj->db_link) == 1265) {
        $query = "alter table $tablename drop column $fieldnameold, add column $fieldnamenew $columndef;";
        $results = $mysql_obj->query($query);
        if (!$results)
          return -1000;
      }
    }
    return 0;
  }

  static function addfield($mysql_obj, $tablename, $fieldname, $desc)
  {
    $columndef = class_tools_mysql::columndef($desc);
    if (!is_string($columndef))
      return $columndef;

    $query = "alter table $tablename add column `$fieldname` $columndef;";
    $results = $mysql_obj->query($query);
    if (!$results)
      return -1000;
    return 0;
  }

  static function delfield($mysql_obj, $tablename, $arrayfieldname)
  {
    $listfields = '';
    foreach ($arrayfieldname as $fieldname) {
      if ($listfields != '')
        $listfields .= ",";
      $listfields .= " drop column `$fieldname`";
    }
    $query = "alter table $tablename $listfields;";
    $results = $mysql_obj->query($query);
    if (!$results)
      return -1000;
    return 0;
  }
}

function return_tools_mysql_param($array)
{
  return class_tools_mysql::param();
}

function tools_mysql_return()
{
  return array('tools_mysql_param');
}
