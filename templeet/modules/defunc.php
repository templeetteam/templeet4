<?php
/* This code has been developed by:
 *
 * Pascal COURTOIS
 * Fabien Penso
 *
 */

function return_defunc($expr)
{
  global $defunc_list;
  $name_tmp = strtolower(eval_list($expr[0]));
  $defunc_list[$name_tmp] = $expr[1];
}


function return_sparam($expr)
{
  global $nesteddefunc, $defunc_argu;

  if (!isset($expr[0]))
    return $defunc_argu[$nesteddefunc];

  $argu_tmp = eval_list($expr[0]);
  //  debug_print_r("argu_tmp:",$argu_tmp,"==",$nesteddefunc,"==",$defunc_argu,"==\n");
  if (!isset($defunc_argu[$nesteddefunc][$argu_tmp]))
    return NULL;
  else
    return $defunc_argu[$nesteddefunc][$argu_tmp];
}

function return_sparam_num($array)
{
  global $nesteddefunc, $defunc_argu;
  if (!isset($defunc_argu[$nesteddefunc]))
    return NULL;
  else
    return is_countable($defunc_argu[$nesteddefunc]) ? count($defunc_argu[$nesteddefunc]) : 0;
}

function return_mapparam($array)
{
  global $nesteddefunc, $defunc_argu;
  $numarg = 1;
  while (list(, $val) = each($array)) {
    if (!isset($defunc_argu[$nesteddefunc][$numarg]))
      return_setref(array($val, NULL));
    else
      return_setref(array($val, $defunc_argu[$nesteddefunc][$numarg]));
    $numarg++;
  }
  return '';
}

function return_function_exists($array)
{
  global $defunc_list, $config;
  $val = current($array);
  next($array);
  $val = strtolower(eval_list($val));
  return (isset($defunc_list[$val])
    || isset($config['function2module'][$val]) || function_exists($val));
}


function return_anonfunc($array)
{
  $val = current($array);
  next($array);

  return $val;
}

function return_evalanonfunc($expr)
{
  global $defunc_argu, $nesteddefunc, $func_var;

  $val = current($expr);
  next($expr);

  $i = 0;
  $param = array();
  foreach ($expr as $tmp) {
    if ($i != 0)
      $param[$i] = eval_list($tmp);
    $i++;
  }

  $nesteddefunc++;
  $defunc_argu[$nesteddefunc] = $param;
  $func_var[$nesteddefunc] = new vars();
  $tmp = eval_list(eval_list($val));
  unset($func_var[$nesteddefunc]);
  unset($defunc_argu[$nesteddefunc]);
  $nesteddefunc--;

  return $tmp;
}

function defunc_return()
{
  return array('defunc', 'sparam', 'sparam_num', 'mapparam', 'function_exists', 'anonfunc', 'evalanonfunc');
}
