<?php

function ffa_addkey($filename, $key)
{
  global $config;

  if (substr($filename, -4) == '.php')
    $filename = substr($filename, 0, -4);

  if (ffa_getlock($filename))
    return -100;

  $handle = @fopen("$filename.index.php", "r+b");
  if (!$handle) {
    $handle = @fopen("$filename.index.php", "wb");
    if (!$handle)
      return ffa_returnreleaselock($filename, -1);

    $index = array(1 => array(1, $key, $key));

    $handle2 = @fopen("$filename.1.php", "wb");
    if (!$handle2)
      return ffa_returnreleaselock($filename, -1);

    fwrite($handle2, "<?php\n\000\n" . serialize(array($key => 1)) . "\n?>");
    fclose($handle2);
  } else {
    $text = fread($handle, 1000000);
    rewind($handle);
    $index = unserialize(substr($text, 8));

    $min = 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz';
    $minfile = 0;
    $max = ' ';
    $maxfile = 0;

    $indexfile = '';

    while (list($key2, $val) = each($index)) {
      // print "key:$key2)\n";
      // print_r($val);

      if (strcmp($val[1], $key) < 0 && strcmp($val[2], $key) > 0) {
        $indexfile = $key2;
        break;
      }

      if (strcmp($val[1], $min) < 0) {
        $min = $val[1];
        $minfile = $key2;
      }

      if (strcmp($val[2], $max) > 0) {
        $max = $val[2];
        $maxfile = $key2;
      }
    }

    // print "indexfile:$indexfile)\n";
    if ($indexfile == '') {
      if (strcmp($min, $key) > 0)
        $indexfile = $minfile;
      else
        $indexfile = $maxfile;
    }

    $handle2 = @fopen("$filename.$indexfile.php", "r+b");
    if (!$handle2)
      return ffa_returnreleaselock($filename, -1);

    $text = fread($handle2, 1000000);
    rewind($handle2);

    $array = unserialize(substr($text, 8));

    $array[$key] = 1;

    ksort($array);

    fwrite($handle2, "<?php\n\000\n" . serialize($array) . "\n?>");

    fclose($handle2);

    $index[$indexfile][0]++;
    if (strcmp($key, $index[$indexfile][1]) < 0)
      $index[$indexfile][1] = $key;
    elseif (strcmp($key, $index[$indexfile][2]) > 0)
      $index[$indexfile][2] = $key;
  }

  fwrite($handle, "<?php\n\000\n" . serialize($index) . "\n?>");
  fclose($handle);

  ffa_releaselock($filename);

  return 0;
}

function ffa_writefile($filename, $array)
{
  global $config;

  if (substr($filename, -4) == '.php')
    $filename = substr($filename, 0, -4);

  if (ffa_getlock($filename))
    return -100;

  $handle = @fopen("$filename.php", "wb");
  if (!$handle)
    return ffa_returnreleaselock($filename, -1);

  fwrite($handle, "<?php\n\000\n" . serialize($array) . "\n?>");
  fclose($handle);

  ffa_releaselock($filename);

  return 0;
}

function return_ffa_writefile($expr)
{

  $filename = current($expr);
  next($expr);
  $filename = eval_list($filename);

  $value = current($expr);
  next($expr);
  $value = eval_list($value);

  return ffa_writefile($filename, $value);
}

function return_ffa_replace($expr)
{

  $filename = current($expr);
  next($expr);
  $filename = eval_list($filename);

  $value = current($expr);
  next($expr);
  $value = eval_list($value);

  return ffa_replace($filename, $value);
}

function ffa_readfile($filename)
{

  if (substr($filename, -4) == '.php')
    $filename = substr($filename, 0, -4);

  if (ffa_getlock($filename))
    return -100;

  $handle = @fopen("$filename.php", "rb");
  if (!$handle)
    return ffa_returnreleaselock($filename, -1);

  $fs = filesize("$filename.php");
  if ($fs > 0)
    $text = fread($handle, $fs);
  fclose($handle);

  ffa_releaselock($filename);

  if ($fs == 0)
    return 0;

  return @unserialize(substr($text, 8));
}

function return_ffa_readfile($expr)
{
  $filename = current($expr);
  next($expr);
  $filename = eval_list($filename);

  return ffa_readfile($filename);
}

function ffa_mergevalue($filename, $array)
{

  if (substr($filename, -4) == '.php')
    $filename = substr($filename, 0, -4);

  if (ffa_getlock($filename))
    return -100;

  $handle = @fopen("$filename.php", "r+b");
  if (!$handle) {
    $handle = @fopen("$filename.php", "wb");
    if (!$handle)
      return ffa_returnreleaselock($filename, -1);

    $res = $array;
  } else {
    $text = fread($handle, filesize("$filename.php"));
    rewind($handle);
    $res = array_merge_recursive(unserialize(substr($text, 8)), $array);
  }

  fwrite($handle, "<?php\n\000\n" . serialize($res) . "\n?>");

  fclose($handle);
  ffa_releaselock($filename);

  return $res;
}

function ffa_replace($filename, $array)
{

  if (substr($filename, -4) == '.php')
    $filename = substr($filename, 0, -4);

  if (ffa_getlock($filename))
    return -100;

  $handle = @fopen("$filename.php", "r+b");
  if (!$handle) {
    $handle = @fopen("$filename.php", "wb");
    if (!$handle)
      return ffa_returnreleaselock($filename, -1);

    $res = $array;
  } else {
    $text = fread($handle, filesize("$filename.php"));
    rewind($handle);
    $res = array_merge(unserialize(substr($text, 8)), $array);
  }

  fwrite($handle, "<?php\n\000\n" . serialize($res) . "\n?>");

  fclose($handle);
  ffa_releaselock($filename);

  return $res;
}

function ffa_adduniquekey($filename, $key, $value)
{

  if (substr($filename, -4) == '.php')
    $filename = substr($filename, 0, -4);

  if (ffa_getlock($filename))
    return -100;

  $handle = @fopen("$filename.php", "r+b");
  if (!$handle) {
    $handle = @fopen("$filename.php", "wb");
    if (!$handle)
      return ffa_returnreleaselock($filename, -1);

    $res = array();
    $res[$key] = $value;
  } else {
    $text = fread($handle, filesize("$filename.php"));
    rewind($handle);
    $res = unserialize(substr($text, 8));
    if (isset($res[$key])) {
      fclose($handle);
      return ffa_returnreleaselock($filename, -2);
    }
    $res[$key] = $value;
  }

  fwrite($handle, "<?php\n\000\n" . serialize($res) . "\n?>");

  fclose($handle);
  ffa_releaselock($filename);

  return $res;
}

function ffa_setkey($filename, $key, $value)
{

  if (substr($filename, -4) == '.php')
    $filename = substr($filename, 0, -4);

  if (ffa_getlock($filename))
    return -100;

  $handle = @fopen("$filename.php", "r+b");
  if (!$handle) {
    $handle = @fopen("$filename.php", "wb");
    if (!$handle)
      return ffa_returnreleaselock($filename, -1);

    $res = array();
    $res[$key] = $value;
  } else {
    $text = fread($handle, filesize("$filename.php"));
    rewind($handle);
    $res = unserialize(substr($text, 8));
    $res[$key] = $value;
  }

  fwrite($handle, "<?php\n\000\n" . serialize($res) . "\n?>");
  ftruncate($handle, ftell($handle));

  fclose($handle);
  ffa_releaselock($filename);

  return $res;
}

function ffa_delkey($filename, $key)
{

  if (substr($filename, -4) == '.php')
    $filename = substr($filename, 0, -4);

  if (ffa_getlock($filename))
    return -100;

  $handle = @fopen("$filename.php", "r+b");
  if (!$handle)
    return ffa_returnreleaselock($filename, -1);

  $text = fread($handle, filesize("$filename.php"));
  rewind($handle);
  $res = unserialize(substr($text, 8));
  if (!isset($res[$key])) {
    fclose($handle);
    return ffa_returnreleaselock($filename, -2);
  }

  unset($res[$key]);

  fwrite($handle, "<?php\n\000\n" . serialize($res) . "\n?>");
  ftruncate($handle, ftell($handle));

  fclose($handle);
  ffa_releaselock($filename);

  return $res;
}

function ffa_getlock($filename)
{
  global $config;

  clearstatcache();

  if (!file_exists("$filename.free") && !file_exists("$filename.lock")) {
    if (!$fd = @fopen("$filename.free", 'w'))
      return -1;

    fclose($fd);
  }

  clearstatcache();
  $stat = @stat("$filename.lock");
  if (is_array($stat) && $stat[9] + 8 < time()) {
    templeet_rename("$filename.lock", "$filename.free");
  }

  $maxtime = time() + 10;
  $ok = 0;
  while ($maxtime > time() && !$ok) {
    $res = templeet_rename("$filename.free", "$filename.lock");

    if ($res)
      $ok = 1;
    else {
      if ($config['windows'])
        sleep(1);
      else
        usleep(20000);
    }
  }

  if (!$ok)
    return -2;

  if (!$fd = @fopen("$filename.lock", 'w'))
    return -3;

  fclose($fd);

  return 0;
}

function ffa_releaselock($filename)
{
  templeet_rename("$filename.lock", "$filename.free");
}

function ffa_returnreleaselock($filename, $code)
{
  ffa_releaselock($filename);
  return $code;
}

function return_ffa_readkey($expr)
{
  global $ffa_record;

  $filename = current($expr);
  next($expr);
  $filename = eval_list($filename);

  $key = current($expr);
  next($expr);
  $key = eval_list($key);

  $res = ffa_readfile($filename);
  if (is_array($res)) {
    if (isset($res[$key]))
      $ffa_record = $res[$key];
    else
      unset($ffa_record);

    return returncode('');
  }

  return returncode($res);
}

function return_ffa_getkey($expr)
{
  global $ffa_record;

  $key = current($expr);
  next($expr);
  if (!isset($key))
    return $ffa_record;

  $key = eval_list($key);

  if ($key === '')
    return $ffa_record;

  if (isset($ffa_record[$key]))
    return $ffa_record[$key];

  return '';
}

function return_ffa_delkey($expr)
{
  $filename = current($expr);
  next($expr);
  $filename = eval_list($filename);

  $key = current($expr);
  next($expr);
  $key = eval_list($key);

  $res = ffa_delkey($filename, $key);
  if (is_array($res))
    return returncode(0);

  return returncode($res);
}

function return_ffa_add($expr)
{
  $filename = current($expr);
  next($expr);
  $filename = eval_list($filename);

  $key = current($expr);
  next($expr);
  $key = eval_list($key);

  $mode = current($expr);
  next($expr);
  $mode = eval_list($mode);

  switch ($mode) {
    case 0:
      $value = current($expr);
      next($expr);
      $value = eval_list($value);
      break;

    case 1:
      $value = array();
      while (list(, $val) = each($expr))
        $value[] = eval_list($val);
      break;

    case 2:
      $value = array();
      while (list(, $key2) = each($expr)) {
        $key2 = eval_list($key2);
        if (!list(, $val) = each($expr))
          return returncode(-3);

        $val = eval_list($val);
        $value[$key2] = $val;
      }
      break;
  }

  $res = ffa_adduniquekey($filename, $key, $value);

  if (!is_array($res))
    return returncode($res);

  return returncode('');
}

function return_ffa_filearray_replace($expr)
{
  $filename = current($expr);
  next($expr);
  $filename = eval_list($filename);

  $tab = current($expr);
  next($expr);
  $tab = eval_list($tab);

  $res = ffa_replace($filename, $tab);
  if (!is_array($res))
    return returncode($res);

  return returncode('');
}

function fieldfileaccess_return()
{
  return array(
    'ffa_add', 'ffa_setkey', 'ffa_delkey', 'ffa_readkey', 'ffa_getkey',
    'ffa_filearray_replace', 'ffa_writefile', 'ffa_readfile', 'ffa_replace'
  );
}
