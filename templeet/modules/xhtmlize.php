<?php
/*
 * This code has been developed by:
 *
 * Mike Hommey
 * Fabien Penso
 *
 * This module contains functions to format and check html.
 *
 * ~xhtmlize transforms some text in XHTML with paragraphs and linebreaks.
 * ~strictize converts HTML tags in others so that it conforms to HTML
 *  strictness.
 */

global $strict_tags, $strict_pattern, $authorized_tags;
$strict_tags = array(
  'u' => array('span', 'style="text-decoration: underline"'),
  's' => array('span', 'style="text-decoration: line-through"')
);
$strict_pattern = 's|u|/s|/u';
$authorized_tags = 'b|s|u|i|tt|/b|/s|/u|/i|/tt';

function return_xhtmlize($array)
{
  $val = current($array);
  next($array);
  $tmp = eval_list($val);
  $tmp = str_replace("\r\n", "\n", $tmp);
  $list = 0;
  $result = '';
  $paragraph = 0;
  $lines = explode("\n", $tmp);
  while (list(, $tmp) = each($lines)) {
    if (isset($tmp[0]) && $tmp[0] == '-') {
      if ($paragraph) {
        $paragraph = 0;
        $result .= "</p>\n";
      }
      if (!$list) {
        $list = 1;
        $result .= "<ul>\n";
      }
      $result .= '<li>';
      $tmp[0] = ' ';
    } elseif ($list) {
      $list = 0;
      $result .= "</ul>\n";
    } elseif ($paragraph && ($tmp == '')) {
      $paragraph = 0;
      $result .= "</p>\n";
    } elseif ($paragraph)
      $result .= "<br />\n";
    if (!$paragraph && !$list && ($tmp != '')) {
      $paragraph = 1;
      $result .= '<p>';
    }
    $result .= trim($tmp);
    if ($list) {
      $result .= "</li>\n";
    }
  }
  if ($list)
    $result .= "</ul>\n";
  if ($paragraph)
    $result .= "</p>\n";

  return $result;
}

function return_strictize($array)
{
  global $strict_tags, $strict_pattern;
  $val = current($array);
  next($array);
  $val = eval_list($val);
  $tmp = '<(' . $strict_pattern . ')>(.*)$';
  $coin = '';
  while(preg_match('/' . preg_quote($tmp, '/') . '/i',$val,$plop)) {
    $val = substr($val,0,-strlen($plop[0]));
    $tag = strtolower($plop[1]);
    if ($tag[0] == '/') {
      $tag = substr($tag, 1);
      $val .= '</' . $strict_tags[$tag][0] . '>';
    } else {
      $val .= '<' . $strict_tags[$tag][0] . ' ' . $strict_tags[$tag][1] . '>';
    }
    $coin .= $val;
    $val = $plop[2];
  }

  return $coin . $val;
}


function xhtmlize_return()
{
  return array('xhtmlize', 'strictize');
}
