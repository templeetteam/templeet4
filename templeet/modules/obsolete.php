<?php
/*
 * This code has been developed by:
 *
 * Pascal Courtois
 * Antony GIBBS
 * Philippe Poelvoorde
 *
 */

function obsolete_return() {
  return array('get', 'set', 'getl', 'setl', 'gets', 'sets', 'mapparaml', 'call', 'mapfilenamevar');
}

function return_mapfilenamevar($expr)
{
  global $global_var;

  $i = 0;
  while (list(, $var) = each($expr)) {
    $i++;
    if (isset($global_var->filenamevars[$i]))
      $val = $global_var->filenamevars[$i];
    else
      $val = NULL;
    return_set(array($var,$val));
  }
}

Function return_call($array) {
        global $defunc_argu,$nesteddefunc,$defunc_list;
        $val = current($array);
        next($array);
        $name_tmp = eval_list($val);
        $i=1;
        $param = array();
        while(list(,$val) = each($array)) {
                $param[$i] = eval_list($val);
                $i++;
        }

        $nesteddefunc++;
        $defunc_argu[$nesteddefunc] = $param;
        if (isset($defunc_list[$name_tmp]))
                $tmp = eval_list($defunc_list[$name_tmp]);
        else
                $tmp = '';
        $nesteddefunc--;
        return $tmp;
}

Function return_sets($expr) {
  global $global_var;
  @session_start();

  while( list(,$name) = each ($expr) ) 
  {
    $name = eval_list($name);
    $value = current($expr);
    next($expr);
    $value = eval_list($value);
  
    if (strpos($name, '[') === false)
      $_SESSION[$name] = $value;
    else
    {
      $name = obsolete_get_set_name_fix($name);
      $r = return_eval(Array('~rem('.$name.' = ~unserialize(\''.addcslashes(serialize($value),'\'').'\'))'));
    }
  }
}

Function return_gets($expr) {
  global $global_var;
  @session_start();

  $name = current($expr);
  next($expr);
  $name = eval_list($name);
  $r = null;

  if (strpos($name, '[') === false)
    {
      if (isset($_SESSION[$name]))
          $r = $_SESSION[$name];
      // else
      //   $r = null;
    }
  else
    {
      $name = obsolete_get_set_name_fix($name);
      $r = unserialize(return_eval(Array('~serialize('.$name.')')));
    }
  return $r;
}

Function return_mapparaml($array)
{
  global $nesteddefunc,$defunc_argu;
  $numarg=1;
  while(list(,$val) = each($array))
    {
      $argu_tmp = eval_list($val);

      if (!isset($defunc_argu[$nesteddefunc][$numarg])) 
          return_setl(array($argu_tmp,NULL));
        else 
          return_setl(array($argu_tmp,$defunc_argu[$nesteddefunc][$numarg]));
      $numarg++;
    }
  return "";  
}

Function return_setl($expr) {
  global $global_var;

  while( list(,$name) = each ($expr) ) 
  {
    $name = 'L'.md5($global_var->actual_template) . '_' . eval_list($name);

    $value = current($expr);
    next($expr);
    $value = eval_list($value);
  
    if (strpos($name, '[') === false)
      $global_var->{$name} = $value;
    else
    {
      $name = obsolete_get_set_name_fix($name);
      $r = return_eval(Array('~rem('.$name.' = ~unserialize(\''.addcslashes(serialize($value),'\'').'\'))'));
    }
  }
}

Function return_getl($expr) {
  global $global_var;
  $name = current($expr);
  next($expr);
  $name = 'L'.md5($global_var->actual_template) . '_' . eval_list($name);

  $r = null;

  if (strpos($name, '[') === false)
    {
      if (property_exists($global_var, $name))
          $r = $global_var->{$name};
      // else
      //   $r = null;
    }
  else
    {
      $name = obsolete_get_set_name_fix($name);
      $r = unserialize(return_eval(Array('~serialize('.$name.')')));
    }
  return $r;
}

Function return_set($expr) {
  global $global_var;
  
  while( list(,$name) = each ($expr) )
  {
    $name = eval_list($name);

    $value = current($expr);
    next($expr);
    $value = eval_list($value);

    if (strpos($name, '[') === false)
      $global_var->{$name} = $value;
    else
    {
      $name = obsolete_get_set_name_fix($name);
#      $r = return_eval(Array('~rem('.$name.' = unserialize(\"".addcslashes(serialize($value),'\"").'\'))'));
      $r = return_eval(Array('~rem('.$name.' = ~unserialize(\''.addcslashes(serialize($value),'\'').'\'))'));

    }
  }
}

function return_get($expr) {
  global $global_var;
  $name = current($expr);
  next($expr);
  $name = eval_list($name);

  $r = null;

  if (strpos($name, '[') === false)
    {
      if (property_exists($global_var, $name))
          $r = $global_var->{$name};
      // else
      //   $r = null;
    }
  else
    {
      $name = obsolete_get_set_name_fix($name);
      $r = unserialize(return_eval(Array('~serialize('.$name.')')));
    }

  return $r;
}

function obsolete_get_set_name_fix($name) {
/*
 * ~get takes a string 'example[array[key]][key2]' without any quotes to array key value
 * wrap key values after escaping double quotes
 *
 */

  $name = addcslashes($name,'"');
  $name = str_replace('[', '["', $name);
  $name = str_replace(']', '"]', $name);

  // to accept ~set('foo[]', 'bar')
#  $name = str_replace('['']', '[]', $name);
  $name = str_replace('[""]', '[]', $name);


  return $name;
}
