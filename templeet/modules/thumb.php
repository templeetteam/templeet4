<?php
/*
 * This code has been developed by:
 *
 * Pascal COURTOIS
 * Pascal TERJAN
 * Fabien PENSO
 * Philippe POELVOORDE
 *
 * This module contains functions to modify images.
 *
 * ~thumb creates a thumbnail from a GIF, JPEG, WEBP or PNG picture.
 */
global $global_var;
if(!isset($global_var))
    $global_var = new stdClass();
$global_var->image_quality = 80;
$global_var->image_compress = 9;

function return_thumb($array)
{
  global $global_var, $config;

  $val = current($array);
  next($array);
  $source = eval_list($val);
  $val = current($array);
  next($array);
  $cible = eval_list($val);
  $val = current($array);
  next($array);
  $max_x = eval_list($val);
  $val = current($array);
  next($array);
  $max_y = eval_list($val);
  //Enlarge if image is smaller than thumbnail
  $val = current($array);
  next($array);
  $enlarge = eval_list($val);
  //Point of origin (orig_x, orig_y) for a cropped thumbnail (default (0,0))
  $val = current($array);
  next($array);
  $orig_x = eval_list($val);
  $val = current($array);
  next($array);
  $orig_y = eval_list($val);
  //dimension of the rectangle copied from the original image (default all)
  $val = current($array);
  next($array);
  $orig_w = eval_list($val);
  $val = current($array);
  next($array);
  $orig_h = eval_list($val);

  if (!$config['windows'] && isset($global_var->thumbuse) && $global_var->thumbuse == 'convert') {
    if (isset($global_var->convertopt))
      $cvtopt = $global_var->convertopt;
    else
      $cvtopt = '';

    if (!$enlarge)
      $im_enl = ">";
    if ($orig_x && $orig_y)
      $im_orig = '+' . $orig_x . '+' . $orig_y;
    if ($orig_w && $orig_h)
      $im_rect = $orig_w . 'x' . $orig_h;
    if ((isset($im_orig) && $im_orig) || (isset($im_rect) && $im_rect)) {
      $im_crop = '-crop ' . $im_rect . $im_orig;
      exec("convert '$source' $im_crop $cvtopt '$cible'");
      exec("convert '$cible' -geometry '$max_x" . "x$max_y$im_enl' $cvtopt '$cible'");
    } else {
      exec("convert '$source' -geometry '$max_x" . "x$max_y$im_enl' $cvtopt '$cible'");
    }

    return 0;
  }

  $s = @getimagesize($source);
  $t = $s[2];
  switch ($t) {
    case IMAGETYPE_GIF:
      if (!function_exists('imagecreatefromgif'))
        return -3;
      $im = @imagecreatefromgif($source);
      break;
    case IMAGETYPE_JPEG:
      $im = @imagecreatefromjpeg($source);
      break;
    case IMAGETYPE_PNG:
      $im = @imagecreatefrompng($source);
      break;
    case IMAGETYPE_WEBP:
      if (!function_exists('imagecreatefromwebp'))
        return -4;
      $im = @imagecreatefromwebp($source);
  }
  if (!isset($im) || !$im) {
    return -1;
  }
  include_once("image.php");
  image_resize($im, $max_x, $max_y, $enlarge, $orig_x, $orig_y, $orig_w, $orig_h);
  $tmp = strrpos($cible, '.');

  $extension = substr($cible, ++$tmp);
  if (!strcasecmp('png', $extension)) {
    image_checkcompress();
    $res = @imagepng($im, $cible, $global_var->image_compress);
  } elseif (!strcasecmp('jpg', $extension) || !strcasecmp('jpeg', $extension)) {
    image_checkquality();
    $res = @imagejpeg($im, $cible, $global_var->image_quality);
  } elseif (!strcasecmp('gif', $extension)) {
    $res = @imagegif($im, $cible);
  } elseif (!strcasecmp('webp', $extension)) {
    image_checkquality();
    $res = @imagewebp($im, $cible, $global_var->image_quality);
  } 

  if (!$res)
    return -2;

  return 0;
}

function thumb_return()
{
  return array('thumb');
}
