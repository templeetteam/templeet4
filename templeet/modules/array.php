<?php
/*
 * This code has been developed by:
 *
 * Fabien PENSO
 * Pascal COURTOIS
 *
 */

$nestedarray = 0;
$array_array = array();
function return_array_list($array)
{
  global $nestedarray, $array_array, $global_var;
  $val = current($array);
  next($array);
  $array_tmp = eval_list($val);

  $val = current($array);
  next($array);
  $indice = eval_list($val);
  $val = current($array);
  next($array);
  $number = eval_list($val);

  if (isset($global_var->array_first)) {
    if ($global_var->array_first)
      $array_first = 1;
    else
      $array_first = 0;
  } else
    $array_first = 1;

  $elements = array();
  while (list(, $val) = each($array)) {
    $tmp_l = eval_list($val);
    if (
      $tmp_l == 'LM' ||
      $tmp_l == 'LR' ||
      $tmp_l == 'LN' ||
      $tmp_l == 'LD' ||
      $tmp_l == 'LS' ||
      $tmp_l == 'LE' ||
      $tmp_l == 'LF' ||
      $tmp_l == 'LL'
    ) {
      $val = current($array);
      next($array);
      if (!isset($val))
        return 'Error in array_list parameters!';

      $elements[$tmp_l] = $val;
    } else
      return 'Error in array_list parameters!';
  }

  $txt = '';

  $nestedarray++;
  $array_array[$nestedarray] = array();
  if (is_object($array_tmp)) {
    $tmp = array();
    foreach ($array_tmp as $key => $value)
      $tmp[$key] = $value;
    $array_tmp = $tmp;
  }

  if (is_null($array_tmp))
    $array_tmp = array();

  if (!is_array($array_tmp)) {
    if (!isset($elements['LE']))
      throw new TempleetError("array_list: parameter not an array or object\n");

    $txt = eval_list($elements['LE']);
    $nestedarray--;
    return $txt;
  }

  reset($array_tmp);
  $count = count($array_tmp);

  if ($number == '')
    $number = $count;

  if ($count == 0 && isset($elements['LD'])) {
    $nestedarray--;
    $txt .= eval_list($elements['LD']);
  } else {
    for ($i = $array_first; $i < $indice; $i++)
      each($array_tmp);

    for ($i = $indice; $i < ($indice + $number); $i++) {
      if (isset($array_array[$nestedarray]['key'])) {
        $array_array[$nestedarray]['keyprev'] = $array_array[$nestedarray]['key'];
        $array_array[$nestedarray]['currentprev'] = $array_array[$nestedarray]['current'];
      }

      if (!list($a, $b) = each($array_tmp))
        break;

      $array_array[$nestedarray]['key'] = $a;
      $array_array[$nestedarray]['current'] = $b;
      $array_array[$nestedarray]['count'] = $i;
      $array_array[$nestedarray]['counter'] = $i;
      $array_array[$nestedarray]['countall'] = $count;
      $array_array[$nestedarray]['keynext'] = key($array_tmp);
      $array_array[$nestedarray]['currentnext'] = current($array_tmp);

      $nolm = FALSE;
      if ($i == $indice) {
        if (isset($elements['LF']))
          $txt .= eval_list($elements['LF']);
        if (isset($elements['LR'])) {
          $txt .= eval_list($elements['LR']);
          $nolm = TRUE;
        }
      }

      if (isset($elements['LS']) && $i > $indice)
        $txt .= eval_list($elements['LS']);

      if (($i == $count || $i == ($indice + $number - 1)) && isset($elements['LN']) && !$nolm) {
        $txt .= eval_list($elements['LN']);
        $nolm = TRUE;
      }

      if (!$nolm && isset($elements['LM']))
        $txt .= eval_list($elements['LM']);

      if ($i == $count || $i == ($indice + $number - 1)) {
        if (isset($elements['LL']))
          $txt .= eval_list($elements['LL']);
      }
    }
    $nestedarray--;
  }

  return $txt;
}

function return_array_fld($array)
{
  global $nestedarray, $array_array;
  $val = current($array);
  next($array);
  $name = eval_list($val);

  if (isset($array_array[$nestedarray][$name]))
    return $array_array[$nestedarray][$name];
}

function array_return()
{
  return array('array_list', 'array_fld');
}
