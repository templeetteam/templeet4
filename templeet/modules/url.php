<?php
/*
 * This module offers url management
 *
 * ~url constructs an url with variables given or from original url
 * ~relative_base returns a relative link
 *
 */


function return_relative_base($expr)
{
  global $relative_base;

  return $relative_base;
}

function return_relative_templeet($expr)
{
  global $relative_base, $config;

  if (
    isset($config['error404used']) &&
    $config['error404used'] == 1
  ) {
    if ($relative_base == '')
      return "./";
    return $relative_base;
  }

  return return_relative_templeet_script($expr);
}

function return_absolute_templeet($expr)
{
  global $config;

  if ((isset($config['error404used']) && $config['error404used']) ||
    (isset($config['fallbackused']) && $config['fallbackused'])
  )
    return $config['dir_installed'];

  if ($config['pathinfoaccepted'] == 0)
    return $config['dir_installed'] . 'templeet.php?';
  else
    return $config['dir_installed'] . 'templeet.php/';
}

function return_imagepath($expr)
{
  global $config;

  if (
    isset($config['error404used']) &&
    $config['error404used'] == 1
  )
    return return_relative_base($expr);

  if (
    isset($config['templatedirprotected']) &&
    $config['templatedirprotected'] == 1
  )
    return return_relative_templeet_script($expr);

  if ($config['templatedir'][0] == '/' || $config['templatedir'][0] == '.')
    return $config['templatedir'];
  else
    return return_relative_base($expr) . $config['templatedir'];
}

function return_absolute_imagepath($expr)
{
  global $config;

  if (
    isset($config['error404used']) &&
    $config['error404used'] == 1
  )
    return $config['dir_installed'];

  if (
    isset($config['templatedirprotected']) &&
    $config['templatedirprotected'] == 1
  )
    return return_absolute_templeet_script($expr);

  if ($config['templatedir'][0] == '/' || $config['templatedir'][0] == '.')
    return $config['templatedir'];
  else
    return return_relative_base($expr) . $config['templatedir'];
}

function return_relative_templeet_script($expr)
{
  global $relative_base, $config;

  if ($config['fallbackused'])
    return return_relative_base($expr);

  $scriptname = preg_replace("|.*\/|", '', $config['dir_installed'] . 'templeet.php');
  $tmp = preg_replace("|/$scriptname\?|", "/", $relative_base) . $scriptname;

  if ($config['pathinfoaccepted'] == 0)
    return $tmp . '?';
  else
    return $tmp . '/';
}

function return_absolute_templeet_script($expr)
{
  global $relative_base, $config;

  if ($config['fallbackused'])
    return $config['dir_installed'];

  $tmp = $config['dir_installed'] . 'templeet.php';

  if ($config['pathinfoaccepted'] == 0)
    return $tmp . '?';
  else
    return $tmp . '/';
}

function url_return()
{
  return array(
    'relative_base', 'relative_templeet', 'imagepath',
    'absolute_imagepath', 'relative_templeet_script', 'absolute_templeet_script', 'absolute_templeet'
  );
}
