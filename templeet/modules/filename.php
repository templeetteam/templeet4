<?php
/*
 * This module introduces filename variables extraction and management
 *
 * ~get_filename returns the file name without extension from the file path
 * ~get_filenamevar returns the nth variable passed in the url
 * ~map_filenamevar assigns variables passed in url to global variables
 *
 */

function return_get_filename($expr)
{
  global $filename;
  return $filename;
}

function return_get_extension($expr)
{
  global $extension;
  return $extension;
}

function return_get_filenameext($expr)
{
  global $filename, $extension;
  return $filename . "." . $extension;
}

function return_get_dirname($expr)
{
  global $dirname;

  if (!isset($expr[0]))
    return $dirname;

  $num = eval_list($expr[0]);
  if ($num != '') {
    $array_tmp = explode('/', $dirname);
    if ($num > 0)
      $num = count($array_tmp) - $num - 1;
    else
      $num = -$num - 1;

    if (isset($array_tmp[$num]))
      return $array_tmp[$num];
    return;
  } else
    return $dirname;
}

function return_get_filenamevar($expr)
{
  global $global_var;

  if (!isset($expr[0]))
    return implode(',', $global_var->filenamevars);
  $num = eval_list($expr[0]);

  if ($num >= 0) {
    if (isset($global_var->filenamevars[$num]))
      return $global_var->filenamevars[$num];
    else
      return;
  } else {
    $num = (is_countable($global_var->filenamevars) ? count($global_var->filenamevars) : 0) + $num;
    if (isset($global_var->filenamevars[$num]))
      return $global_var->filenamevars[$num];
    else
      return;
  }
}

function return_map_filenamevar($expr)
{
  global $global_var;

  $i = 0;
  while (list(, $var) = each($expr)) {
    $i++;
    if (isset($global_var->filenamevars[$i]))
      $val = $global_var->filenamevars[$i];
    else
      $val = NULL;
    return_setref(array($var, $val));
  }
}

function filename_return()
{
  return array(
    'get_filename', 'get_extension', 'get_filenameext', 'get_filenamevar',
    'map_filenamevar', 'get_dirname'
  );
}
