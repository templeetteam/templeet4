<?php
/*
 * This code has been developed by:
 *
 * Fabien Penso
 * Pascal COURTOIS
 *
 */


function return_configurator($array)
{
  global $config;

  if (!auth::$admin)
    return -1;

  $serverconf = 'templeet/serverconf.php';

  $handle = opendir($config['modulesdir']);
  $functions = array();
  if (!$fd = fopen($serverconf, "rb"))
    return "Can't open $serverconf";

  $txt = fread($fd, filesize($serverconf));
  fclose($fd);
  $res = array();
  while ($file = readdir($handle)) {
    if (is_file($config['modulesdir'] . $file))
      $res[$file] = configurator_testfile($file, $functions);

    if (is_dir($config['modulesdir'] . $file) && $file != '.' && $file != '..') {
      $handlesub = opendir($config['modulesdir'] . $file);
      while ($subfile = readdir($handlesub)) {
        if (is_file($config['modulesdir'] . $file . '/' . $subfile))
          $res[$file . '/' . $subfile] = configurator_testfile($file . '/' . $subfile, $functions);
      }
      closedir($handlesub);
    }
  }
  closedir($handle);

  $tmp = '';
  $i = 1;
  while (list($a, $b) = each($functions)) {
    $tmp .= "\t\t'$a'=>'$b'";
    if ($i < count($functions))
      $tmp .= ",\n";
    $i++;
  }
  $txt = preg_replace(
    '/\$config\[\'function2module\'\] = array\((.*?)\)\;/smi',
    "\$config['function2module'] = array(\n$tmp);",
    $txt
  );

  $txt = preg_replace('/\r\n/m', "\n", $txt);
  $txt = preg_replace('/\?>.*/m', '?>', $txt);

  if (!$fp = @fopen($serverconf, 'w+')) {
    return -1;
  }

  fwrite($fp, $txt);
  fclose($fp);

  return $res;
}

function configurator_testfile($file, &$functions)
{
  global $config;

  $tmp = strrpos($file, '.');
  if (substr($file, ($tmp + 1)) != 'php')
    return 1;

  $rfile = substr($file, 0, $tmp);
  include_once($config['modulesdir'] . $file);
  $p = preg_replace('/.*\//', '', $rfile) . '_return';
  if (!function_exists($p))
    return 2;

  $functions_tmp = $p();

  while (list(, $b) = each($functions_tmp))
    $functions[strtolower($b)] = $rfile;

  return 3;
}

function return_html2templatearray($array)
{
  global $config;
  each($array);

  $tmp1 = current($array);
  next($array);
  $tmp1 = eval_list($tmp1);
  $tmp2 = current($array);
  next($array);
  $tmp2 = eval_list($tmp2);
  $tmp3 = current($array);
  next($array);
  $tmp3 = eval_list($tmp3);
  if (
    gettype($tmp1) != 'array' || (gettype($tmp2) != 'array' && $tmp3 != 'delete') ||
    (is_countable($tmp1) ? count($tmp1) : 0) < 1
  )
    return -1;

  if ($tmp3 == 'delete') {
    while (list(, $b) = each($tmp1))
      unset($config['html2template_array'][$b]);
  } else {
    reset($tmp1);
    $tmpt = array();
    while (list($a, $b) = each($tmp1)) {
      if (!isset($config['html2template_array'][$b]))
        $tmpt[$b] = $tmp2[$a];
    }

    if ($tmp3 == 'begin')
      $config['html2template_array'] = $tmpt + $config['html2template_array'];
    else
      $config['html2template_array'] = $config['html2template_array'] + $tmpt;
  }

  $new_array = '';
  reset($config['html2template_array']);
  $i = 0;
  while (list($a, $b) = each($config['html2template_array'])) {
    if ($i != 0)
      $new_array .= ",\n";
    $new_array .= "'" . str_replace("'", "\\'", $a) . "' => '" .
      str_replace("'", "\\'", $b) . "'";
    $i++;
  }

  $new_array = "\$config['html2template_array'] = array(\n$new_array);";

  if (!$fd = @fopen('templeet/config.php', 'rb'))
    return -1;
  $output = '';
  while ($tmp = fread($fd, 40960))
    $output .= $tmp;

  fclose($fd);

  $newfile = preg_replace(
    '/\$config\[\'html2template_array\'\] = array\(.*?\);/s',
    str_replace('\\', '\\\\', $new_array),
    $output
  );
  @copy('templeet/config.php', 'templeet/config_old.php');
  include_once('templeet/modules/filesystem.php');
  return_writefile(array('', 'templeet/config.php', $newfile));

  return;
}

function configurator_return()
{
  return array('configurator', 'html2templatearray');
}
