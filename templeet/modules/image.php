<?php
/*
 * This code has been developed by:
 *
 * Pascal COURTOIS
 * Pascal TERJAN
 * Fabien PENSO
 * Philippe POELVOORDE
 *
 * This module contains functions to modify images.
 *
 */

$config['reffunc']["image_resize"] = "&";

function image_resize(&$im, $max_x, $max_y, $enlarge, $orig_x = 0, $orig_y = 0, $orig_w = 0, $orig_h = 0)
{
  if ($max_x != 0 && $max_y != 0) {
    $ox = imagesx($im) - $orig_x;
    $sx = $x = $orig_w ? $orig_w : $ox;
    $oy = imagesy($im) - $orig_y;
    $sy = $y = $orig_h ? $orig_h : $oy;

    if ($x > $max_x || $enlarge) {
      $y = (int)floor($y * ($max_x / $x));
      $x = $max_x;
    }

    if ($y > $max_y) {
      $x = (int)floor($x * ($max_y / $y));
      $y = $max_y;
    }

    if ($ox != $x || $oy != $y) {
      if (function_exists('imagecreatetruecolor') && @imagecreatetruecolor(1, 1)) {
        $tmp = imagecreatetruecolor($x, $y);
        imagegammacorrect($im, 2.2, 1.0);
        if (!imagecopyresampled($tmp, $im, 0, 0, $orig_x, $orig_y, $x, $y, $sx, $sy))
          return FALSE;
        imagegammacorrect($tmp, 1.0, 2.2);
      } else {
        $tmp = imagecreate($x, $y);
        if (!imagecopyresized($tmp, $im, 0, 0, $orig_x, $orig_y, $x, $y, $sx, $sy))
          return FALSE;
      }

      imagedestroy($im);
      $im = $tmp;
    }
  }
  return TRUE;
}

function return_image2stringpng($array)
{
  global $global_var;

  $val = current($array);
  next($array);
  $im = eval_list($val);

  ob_start();
  image_checkcompress();

  imagepng($im, NULL, $global_var->image_compress);
  $contents = ob_get_contents();

  ob_end_clean();

  return $contents;
}

function return_image2stringgif($array)
{
  global $global_var;

  $val = current($array);
  next($array);
  $im = eval_list($val);

  ob_start();

  imagegif($im);
  $contents = ob_get_contents();

  ob_end_clean();

  return $contents;
}

function return_image2stringjpg($array)
{
  global $global_var;

  $val = current($array);
  next($array);
  $im = eval_list($val);


  ob_start();
  image_checkquality();

  imagejpeg($im, NULL, $global_var->image_quality);
  $contents = ob_get_contents();

  ob_end_clean();

  return $contents;
}

function return_image2stringwebp($array)
{
  global $global_var;
  $val = current($array);
  next($array);
  $im = eval_list($val);
  ob_start();
  image_checkquality();
  imagewebp($im, NULL, $global_var->image_quality);
  $contents = ob_get_contents();
  ob_end_clean();
  return $contents;
}



function image_checkquality()
{
  global $global_var;

  if (
    !isset($global_var->image_quality) ||
    !is_numeric($global_var->image_quality) ||
    $global_var->image_quality < 10 ||
    $global_var->image_quality > 100
  ) {
    $global_var->image_quality = 80;
  }
}

function image_checkcompress()
{
  global $global_var;

  if (
    !isset($global_var->image_compress) ||
    !is_numeric($global_var->image_compress) ||
    $global_var->image_compress < 0 ||
    $global_var->image_compress > 9
  ) {
    $global_var->image_compress = 9;
  }
}

function image_return()
{
  return array(
    'image_resize',
    'image2stringpng', 'image2stringgif', 'image2stringjpg', 'image2stringwebp'
  );
}
