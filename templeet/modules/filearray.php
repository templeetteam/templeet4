<?php


// filename,first,nb,LM ... LF ...
function return_filearray_list($array)
{
  global $filearray_key, $filearray_val, $filearray_nbrows;

  $filename = current($array);
  next($array);
  $filename = eval_list($filename);

  include_once('templeet/modules/fieldfileaccess.php');

  $first = current($array);
  next($array);
  $first = eval_list($first);

  $nb = current($array);
  next($array);
  $nb = eval_list($nb);

  if ($nb <= 0)
    $nb = 9999999;

  $elements = array();

  while (list(, $val) = each($array)) {
    $tmp_l = eval_list($val);

    if (
      $tmp_l == 'LM' ||
      $tmp_l == 'LF' ||
      $tmp_l == 'LL' ||
      $tmp_l == 'L1' ||
      $tmp_l == 'LD' ||
      $tmp_l == 'LN'
    ) {
      $val = current($array);
      next($array);
      if (!isset($val))
        throw new TempleetError('Error in list! You didn\'t specify a second argument');
      $elements[$tmp_l] = $val;
    } else
      throw new TempleetError('Error in list! You must use LM, LF, LL, L1, LD or LN as first argument');
  }

  $filearray = ffa_readfile($filename);

  $tmp = '';
  $filearray_nbrows = is_countable($filearray) ? count($filearray) : 0;

  $row = 0;
  if (!is_array($filearray)) {
    if (isset($elements['LE']))
      $tmp .= eval_list($elements['LE']);
    else
          if (isset($elements['LD']))
      $tmp .= eval_list($elements['LD']);
    else
      return '';
  } elseif ($filearray_nbrows == 0) {
    if (isset($elements['LD']))
      $tmp .= eval_list($elements['LD']);
    else
      return '';
  } elseif ($filearray_nbrows == 1 && isset($elements['L1'])) {
    $filearray_key = key($filearray);
    $filearray_val = current($filearray);
    next($filearray);
    $tmp .= eval_list($elements['L1']);
  } else {
    while (list($filearray_key, $filearray_val) = each($filearray)) {
      if ($row == 0) {
        if (isset($elements['LF']))
          $tmp .= eval_list($elements['LF']);
      }

      if (isset($elements['LM']))
        $tmp .= eval_list($elements['LM']);

      if ($row == $filearray_nbrows - 1 && isset($elements['LL']))
        $tmp .= eval_list($elements['LL']);

      $row++;
    }
  }

  return $tmp;
}

function return_filearray_fld($array)
{
  global $filearray_key, $filearray_val, $filearray_nbrows;

  $val = current($array);
  next($array);

  $val = eval_list($val);

  if ($val == '') {
    if (isset($filearray_val))
      return $filearray_val;
    return '';
  }

  if ($val == '@uid')
    return $filearray_key;

  if ($val == '@count')
    return $filearray_nbrows;

  if (isset($filearray_val[$val]))
    return $filearray_val[$val];

  return '';
}

function return_filearray_replace($array)
{
  $filename = current($array);
  next($array);
  $filename = eval_list($filename);

  $key = current($array);
  next($array);
  $key = eval_list($key);

  include_once('templeet/modules/fieldfileaccess.php');

  $elements = array();

  while (list(, $val) = each($array)) {
    $key2 = eval_list($val);

    $val = current($array);
    next($array);
    if (!isset($val))
      throw new TempleetError('Error in list! You didn\'t specify a second argument');
    $elements[$key2] = eval_list($val);
  }

  if ($key == '')
    $key = 1;
  $res = ffa_replace($filename, array($key => $elements));

  if (!is_array($res))
    return $res;

  return '';
}

function return_filearray_delete($array)
{
  $filename = current($array);
  next($array);
  $filename = eval_list($filename);

  $key = current($array);
  next($array);
  $key = eval_list($key);

  include_once('templeet/modules/fieldfileaccess.php');

  $res = ffa_readfile($filename);

  if (!is_array($res))
    return $res;

  unset($res[$key]);

  $res = ffa_writefile($filename, $res);

  if (!is_array($res) && $res != 0)
    return $res;

  return '';
}

function return_filearray_addkey($array)
{
  global $config;

  $filename = current($array);
  next($array);
  $filename = eval_list($filename);

  $key = current($array);
  next($array);
  $key = eval_list($key);

  $value = current($array);
  next($array);
  $value = eval_list($value);

  if (substr($filename, -4) == '.php')
    $filename = substr($filename, 0, -4);

  $handle = @fopen("$filename.php", "r+b");
  if (!$handle) {
    $handle = @fopen("$filename.php", "wb");
    if (!$handle)
      return ffa_returnreleaselock($filename, -1);
    $res = array();
    $res[$key] = $value;
  } else {
    $text = fread($handle, filesize("$filename.php"));
    rewind($handle);
    $res = unserialize(substr($text, 8));
    if (isset($res[$key])) {
      fclose($handle);
      return ffa_returnreleaselock($filename, -2);
    }

    $res[$key] = $value;
  }

  $res = filearray_renum($res);

  fwrite($handle, "<?php\n\000\n" . serialize($res) . "\n?>");

  fclose($handle);

  return 0;
}

function filearray_renum($array)
{
  ksort($array, SORT_NUMERIC);

  $arrayres = array();
  $num = 1;
  while (list(, $val) = each($array)) {
    $arrayres[$num++] = $val;
  }

  return $arrayres;
}

function filearray_return()
{
  return array('filearray_list', 'filearray_fld', 'filearray_replace', 'filearray_delete', 'filearray_addkey');
}
