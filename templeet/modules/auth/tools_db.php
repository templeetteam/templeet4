<?php

class tools_auth_db
{

  static function param()
  {
    global $config;

    $dh = opendir($config['modulesdir'] . 'list');
    $select = array();

    $type = '';
    while (($file = readdir($dh)) !== false) {
      if (preg_match('/^list_(.*)\.php$/', $file, $arrayres)) {
        include_once($config['modulesdir'] . "list/$file");
        $classname = "class_list_" . $arrayres[1];
        $tmp = new $classname(array());
        $select[$arrayres[1]] = $tmp->param();
        $type = $arrayres[1];
      }
    }
    closedir($dh);

    $type = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['db']) && isset(auth::$config['methodparam']['db']['type']) ?
      auth::$config['methodparam']['db']['type'] : $type);

    $default_tablename = (isset(auth::$config['methodparam']) && isset(auth::$config['methodparam']['db']) && isset(auth::$config['methodparam']['db']['tablename']) ?
      auth::$config['methodparam']['db']['tablename'] : "templeetauth");

    $res = array(
      "dbtype" => array(
        'selectdiv', $type, $select
      ),
      "tablename" => array('text', $default_tablename, 40)
    );

    return $res;
  }

  static function checkparam($param, $structure)
  {
    global $config;

    $type = $param['dbtype']['value'];

    $creationtime = strftime("%Y%m%d%H%M");

    foreach ($param['dbtype'] as $key => $value) {
      if ($key != $type && $key != "value")
        unset($param['dbtype'][$key]);
    }

    $link = class_list::create($type, $param['dbtype'][$type]);
    global $global_var;
    if (!$link->connect()) {
      return $global_var->list_error;
    }
    $columns = $link->querycolumns($param['tablename'], 1);

    if ($columns) {
      $notfound = array();
      $wrongtype = array();
      $wrongsize = array();
      foreach ($structure['fields'] as $fieldname => $def) {
        if (!isset($columns[$fieldname]))
          $notfound[] = $fieldname;
        else {
          if ($columns[$fieldname]['type'] != $def['type'])
            $wrongtype[] = $fieldname;

          if ($def['type'] == "varchar" && $def['size'] > $columns[$fieldname]['size'])
            $wrongsize[] = $fieldname;
        }
      }

      if (count($notfound) != 0 || count($wrongtype) != 0 || count($wrongsize) != 0) {
        if (count($wrongtype) == 0 && count($wrongsize) == 0) {
          foreach ($notfound as $fieldname) {
            $addfield['fields'][$fieldname] = $structure['fields'][$fieldname];
          }
          if (!$link->addcolumns($param['tablename'], $addfield)) {
            $error = "field not found:" . implode(",", $notfound) . "\n";
            return $error;
          }
        } else {
          $error = '';
          if (count($notfound) != 0)
            $error .= "field not found:" . implode(",", $notfound) . ";\n";
          if (count($wrongtype) != 0)
            $error .= "wrong type:" . implode(",", $wrongtype) . ";\n";
          if (count($wrongsize) != 0)
            $error .= "wrong size:" . implode(",", $wrongsize) . ";\n";

          return $error;
        }
      }
    } else {
      $results = $link->createtable($param["tablename"], $structure);
      if (!$results)
        return "can't create " . $param["tablename"] . " table";
    }

    return $param;
  }

  static function readfields($db_obj, $structure)
  {
    global $config;

    $columns = $db_obj->list_handle->querycolumns(auth::$config['method']['param']['tablename'], 1);
    $fields = array();
    foreach ($columns as $fieldname => $fielddesc) {
      if (!isset($structure['fields'][$fieldname])) {
        switch ($fielddesc['type']) {
          case 'varchar':
            $fields[$fieldname]['type'] = 'text';
            $fields[$fieldname][1] = $fielddesc['size'];
            break;

          case 'text':
          case 'blob':
            $fields[$fieldname]['type'] = 'textarea';
            break;

          case 'int':
          case 'double':
          case 'date':
          case 'datetime':
            $fields[$fieldname]['type'] = $fielddesc['type'];
            break;

          case 'bool':
            $fields[$fieldname]['type'] = 'checkbox';
            break;
        }
      }
    }

    return $fields;
  }

  private function checkfieldname($fieldname)
  {
    return preg_match("/^\w{1,25}$/", $fieldname);
  }

  static function modfield($db_obj, $fieldnameold, $fieldnamenew, $desc)
  {
    if (!(new tools_auth_db())->checkfieldname($fieldnameold) || !(new tools_auth_db())->checkfieldname($fieldnamenew))
      return -120;

    $res = $db_obj->list_handle->modfield(auth::$config['method']['param']['tablename'], $fieldnameold, $fieldnamenew, $desc);
    if ($res)
      return -1000;
    return 0;
  }

  static function addfield($db_obj, $fieldname, $desc)
  {
    if (!(new tools_auth_db())->checkfieldname($fieldname))
      return -120;
    $res = $db_obj->list_handle->addfield(auth::$config['method']['param']['tablename'], $fieldname, $desc);
    if ($res)
      return -1000;
    return 0;
  }

  static function delfield($db_obj, $arrayfieldname)
  {
    while (list(, $field) = each($arrayfieldname)) {
      if (!(new tools_auth_db())->checkfieldname($field))
        return -120;
    }

    $res = $db_obj->list_handle->delfield(auth::$config['method']['param']['tablename'], $arrayfieldname);
    if ($res)
      return -1000;
    return 0;
  }
}
