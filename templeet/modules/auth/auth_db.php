<?php
/*
 * database layer for authentication
 *
 * This code has been developed by:
 *
 * Pascal Courtois
 *
 */


class class_auth_db
{
  public string $error = '';

  private array $db_structure;

  function __construct()
  {
    $this->db_structure = array(
      "fields" => array(
        "uid" => array('type' => 'serial'),
        "login" => array('type' => 'varchar', 'size' => 80, 'notnull' => 1, 'default' => ''),
        "pass" => array('type' => 'varchar', 'size' => 40, 'default' => 'NULL'),
        "priv" => array('type' => 'text'),
        "creation" => array('type' => 'datetime'),
        "valid" => array('type' => 'int'),
        "validkey" => array('type' => 'varchar', 'size' => 40),
        "email" => array('type' => 'varchar', 'size' => 80),
        "ipaddr" => array('type' => 'varchar', 'size' => 45),
        "nickname" => array('type' => 'varchar', 'size' => 80, 'notnull' => 1, 'default' => ''),
        "userspace" => array('type' => 'int', 'notnull' => 1, 'default' => 0),
        "adminspace" => array('type' => 'int', 'notnull' => 1, 'default' => 1),
        "lostpasswordtime" => array('type' => 'text')
      ),
      "key" => array(
        0 => array('unique', "uid"),
        1 => array('unique', "userspace", "login"),
        2 => array('unique', "userspace", "nickname"),
        3 => array('unique', "adminspace", "login"),
        4 => array('unique', "adminspace", "nickname"),
        5 => "userspace",
        6 => "adminspace"
      ),
      "charset" => "UTF8"
    );
  }

  /* array : key=uid  , value= array of info for uid */
  private array $cache_info = array();
  /* key="userspace|login" value=uid */
  private array $cache_user_uid = array();

  /* list variables*/
  private $list_rows, $list_nb_rows;

  public $list_handle;

  private function connect()
  {
    if (isset($this->list_handle)) return TRUE;

    if (!isset($this->list_handle))
      $this->list_handle = class_list::create(auth::$config['method']['param']['dbtype']['value'], auth::$config['method']['param']['dbtype'][auth::$config['method']['param']['dbtype']['value']]);

    return $this->list_handle->connect();
  }

  function getinfo($user, $fields = '')
  {
    if (is_int($user) || ctype_digit($user)) {
      if (isset($this->cache_info[$user]))
        return $this->cache_info[$user];

      if (!$this->connect())
        return -1000;

      $query = $this->list_handle->select(
        '',
        array("*"),
        auth::$config['method']['param']['tablename'],
        array('==', 'field:uid', $user),
        '',
        '',
        '',
        ''
      );
      $results = $this->list_handle->query($query);
    } else {
      if (!is_array($user) || !isset($user[0]) || !(is_int($user[0]) || ctype_digit($user[0])) || !isset($user[1]))
        return -1000;

      if (!$this->connect())
        return -1000;

      $query = $this->list_handle->select(
        '',
        array("*"),
        auth::$config['method']['param']['tablename'],
        array(
          '&&',
          array('==', 'field:login', strtolower($user[1])),
          array('==', 'field:userspace', $user[0])
        ),
        '',
        '',
        '',
        ''
      );

      $results = $this->list_handle->query($query);
    }

    if (!$results)
      return -1000;

    $row = $this->list_handle->fetcharray($results);

    if ($row === FALSE)
      return -1;

    $uid = $row['uid'];
    $this->cache_info[$uid] = $row;
    $this->cache_user_uid[$row['login']] = $uid;

    $array_perms = preg_split('/\((.*?)\)/', $row['priv'], -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

    $allpriv = array();
    while (list(, $key) = each($array_perms)) {
      list($areafile, $priv) = explode(';', $key);
      $allpriv[$areafile] = $priv;
    }

    $this->cache_info[$uid]['priv'] = $allpriv;
    $profile = array();
    $fields = $this->readfields();
    foreach ($row as $name => $value) {
      if (isset($fields[$name])) {
        if ($fields[$name]['type'] == "date" || $fields[$name]['type'] == "datetime") {
          $profile[$name] = str_replace("-", '', $value);
        } else
          $profile[$name] = $value;
        unset($this->cache_info[$uid][$name]);
      }
    }
    $this->cache_info[$uid]['profile'] = $profile;

    if ($uid == 0) {
      global $config;
      $info['email'] = $config['emailadmin'];
    }

    return $this->cache_info[$uid];
  }

  /* $user is either an uid or a login */
  function checkauth($userspace, $user, $pass)
  {
    global $auth_admin, $auth_priv;

    $info = $this->getinfo(array($userspace, $user));

    if (!is_array($info))
      return $info;

    if ($info['valid'] <= 0)
      return -2;

    if (sha1($info['uid'] . ":$pass") != $info['pass'])
      return -10;

    $auth_admin = ($info['uid'] == 0 || !empty($info['priv']["ADMIN"]));
    $auth_priv = $info['priv'];

    return $info;
  }

  function userspace()
  {

    if (!$this->connect())
      return -1000;

    $query = $this->list_handle->select(
      '',
      explode(",", "login,userspace"),
      auth::$config['method']['param']['tablename'],
      array('==', 'field:userspace', 'field:uid'),
      '',
      '',
      '',
      ''
    );
    $results = $this->list_handle->query($query);

    if (!$results)
      return -1000;

    $userspaces = array();
    while ($row = $this->list_handle->fetcharray($results))
      $userspaces[$row["userspace"]] = $row['login'];

    $this->list_handle->free_result($results);

    return $userspaces;
  }

  function setpriv($setpriv)
  {
    if (!$this->connect())
      return -1000;

    $res = array();
    while (list($uid,) = each($setpriv)) {
      $query = $this->list_handle->select(
        '',
        explode(",", "priv,valid,userspace"),
        auth::$config['method']['param']['tablename'],
        array('==', 'field:uid', $uid),
        '',
        '',
        '',
        ''
      );

      $results = $this->list_handle->query($query);

      if ($results !== FALSE) {
        $row = $this->list_handle->fetcharray($results);
        if ($row !== FALSE) {
          if (auth::$admin || auth::$uid == $row['userspace']) {
            $perms = $row['priv'];

            $fields = array();
            $email = '';
            if (isset($setpriv[$uid]['email'])) {
              if ($email = '')
                $fields['email'] = "NULL";
              else
                $fields['email'] = $setpriv[$uid]['email'];
            }

            if (isset($setpriv[$uid]['valid'])) {
              $fields['valid'] = $setpriv[$uid]['valid'];
              if ($row['valid'] != 0 && $setpriv[$uid]['valid'] == 0)
                $fields['validkey'] = auth_mkrdstr(12);
              else
                                if ($setpriv[$uid]['valid'] < 0)
                $fields['validkey'] = '';
            }

            if (isset($setpriv[$uid]['priv'])) {
              while (list($area, $val) = each($setpriv[$uid]['priv'])) {
                if ($val != 0) {
                  if (preg_match("/\($area;\d+\)/", $perms))
                    $perms = preg_replace("/\($area;\d+\)/", "($area;$val)", $perms);
                  else
                    $perms .= "($area;$val)";
                } else
                  $perms = preg_replace("/\($area;\d+\)/", '', $perms);
              }
            }

            $fields['priv'] = $perms;

            $query = $this->list_handle->update(
              $this->db_structure,
              auth::$config['method']['param']['tablename'],
              array('==', 'field:uid', $uid),
              $fields
            );
            $results = $this->list_handle->query($query);

            if ($results == 1)
              $res[$uid] = 0;

            unset($this->cache_info[$uid]);
          } else
            $res[$uid] = -107;
        } else
          $res[$uid] = -1;
      } else {
        $res[$uid] = -1;
      }
    }

    return $res;
  }

  function newuser($userspace, $user, $password, $email, $validaccount, $validkey, $setpriv, $profile)
  {
    if (!$this->connect())
      return -1000;

    $priv = '';
    while (list($key, $val) = each($setpriv)) {
      $priv .= "($key;$val)";
    }

    $creationtime = strftime("%Y%m%d%H%M%S");
    $ipaddr = $_SERVER['REMOTE_ADDR'];

    $adminspace = $userspace > 0 ? 0 : 1;

    $sha1 = sha1($user . $creationtime . $ipaddr . $email);
    $query = $this->list_handle->insert(
      $this->db_structure,
      auth::$config['method']['param']['tablename'],
      array(
        'login' => strtolower($user),
        'pass' => '',
        'priv' => $priv,
        'creation' => $creationtime,
        'valid' => $validaccount,
        'validkey' => $sha1,
        'email' => $email,
        'ipaddr' => $ipaddr,
        'nickname' => $user,
        'userspace' => $userspace,
        'adminspace' => $adminspace,
        'lostpasswordtime' => ''
      )
    );

    $results = $this->list_handle->query($query);

    if ($results === FALSE)
      return -3;

    $query = $this->list_handle->select(
      '',
      explode(",", "uid,login"),
      auth::$config['method']['param']['tablename'],
      array('==', 'field:validkey', $sha1),
      '',
      '',
      '',
      ''
    );
    $results = $this->list_handle->query($query);

    $row = $this->list_handle->fetcharray($results);
    $uid = $row['uid'];

    if ($userspace < 0) {
      $query = $this->list_handle->update(
        $this->db_structure,
        auth::$config['method']['param']['tablename'],
        array('==', 'field:uid', $uid),
        array(
          "login" => strtolower($user),
          "nickname" => $user,
          "userspace" => $uid,
          "validkey" => $validkey,
          "pass" => sha1("$uid:$password")
        )
      );

      $results = $this->list_handle->query($query);
    } else {
      $query = $this->list_handle->update(
        $this->db_structure,
        auth::$config['method']['param']['tablename'],
        array('==', 'field:uid', $uid),
        array(
          "pass" => sha1("$uid:$password"),
          "validkey" => $validkey
        )
      );
      $results = $this->list_handle->query($query);
    }

    $this->updateprofile($uid, $profile);
    return $uid;
  }

  function deluser($uid)
  {
    if (!$this->connect())
      return -1000;

    $query = $this->list_handle->delete(
      auth::$config['method']['param']['tablename'],
      array('==', 'field:uid', $uid)
    ) or die("Query failed (1)");
    $results = $this->list_handle->query($query);
    unset($this->cache_info[$uid]);

    return 0;
  }

  function passwd($uid, $oldpassword, $newpassword)
  {

    if (!$this->connect())
      return -1000;

    $query = $this->list_handle->select(
      '',
      explode(",", "pass,userspace"),
      auth::$config['method']['param']['tablename'],
      array('==', 'field:uid', $uid),
      '',
      '',
      '',
      ''
    );
    $results = $this->list_handle->query($query);

    $row = $this->list_handle->fetcharray($results);

    if ($row === FALSE)
      return -1;

    $nocheck = auth::$admin || (auth::$userspace_admin && $row["userspace"] == auth::$userspace);

    if (!$nocheck && sha1($uid . ":" . $oldpassword) != $row["pass"])
      return -5;

    $query = $this->list_handle->update(
      $this->db_structure,
      auth::$config['method']['param']['tablename'],
      array('==', 'field:uid', $uid),
      array(
        "pass" => sha1("$uid:$newpassword")
      )
    );

    $results = $this->list_handle->query($query);

    return 0;
  }

  function loginnick($uid, $login, $nickname)
  {
    if (!$this->connect())
      return -1000;

    $info = $this->getinfo($uid);

    $userspace = $info['userspace'];

    $arrayupdate = array();

    if ($login != '') {
      $arrayupdate["login"] = $login;
    }

    if ($nickname != '') {
      $arrayupdate["nickname"] = $nickname;
    }

    $query = $this->list_handle->update(
      $this->db_structure,
      auth::$config['method']['param']['tablename'],
      array('==', 'field:uid', $uid),
      $arrayupdate
    );

    $results = $this->list_handle->query($query);

    if (!$results)
      return -3;
    unset($this->cache_info[$uid]);

    return 0;
  }

  function validaccount($uid, $key, $pass, $validvalue)
  {
    if (!$this->connect())
      return -1000;

    $info = $this->getinfo($uid);
    if (!is_array($info))
      return -105;

    if ($key == '' || $key != $info['validkey'])
      return -6;

    if ($pass == '') {
      $query = $this->list_handle->update(
        $this->db_structure,
        auth::$config['method']['param']['tablename'],
        array('==', 'field:uid', $uid),
        array(
          'valid' => $validvalue,
          'validkey' => ''
        )
      ) or die("Query failed (2)");

      $results = $this->list_handle->query($query);
      if (!$results)
        return -1000;
    } else {
      if ($info['valid'] > 0) {
        $query = $this->list_handle->update(
          $this->db_structure,
          auth::$config['method']['param']['tablename'],
          array('==', 'field:uid', $uid),
          array(
            'valid' => $validvalue,
            'validkey' => '',
            "pass" => sha1("$uid:$pass")
          )
        ) or die("Query failed (3)");
        $results = $this->list_handle->query($query);
        if (!$results)
          return -1000;
      }
    }

    unset($this->cache_info[$uid]);

    return $info;
  }

  function setemail($uid, $email)
  {
    if (!$this->connect())
      return -1000;

    $query = $this->list_handle->update(
      $this->db_structure,
      auth::$config['method']['param']['tablename'],
      array('==', 'field:uid', $uid),
      array(
        'email' => $email
      )
    ) or die("Query failed (4)");
    $results = $this->list_handle->query($query);
    unset($this->cache_info[$uid]);

    return 0;
  }

  function setkeypass($userspace, $user, $key, $emailcheck = 1)
  {
    if (!$this->connect())
      return -1000;

    $query = $this->list_handle->select(
      '',
      explode(",", "uid,valid,email,lostpasswordtime"),
      auth::$config['method']['param']['tablename'],
      array(
        '&&',
        array('==', 'field:login', $user),
        array('==', 'field:userspace', $userspace)
      ),
      '',
      '',
      '',
      ''
    );
    $results = $this->list_handle->query($query);

    $row = $this->list_handle->fetcharray($results);
    if ($row === FALSE)
      return -1;

    if ($row['email'] == '' && $emailcheck)
      return -7;

    if ($row['valid'] <= 0)
      return -2;

    $lostpass = preg_split("/;/", $row["lostpasswordtime"], NULL, PREG_SPLIT_NO_EMPTY);

    global $config;
    $limit = time() - $config['lostpass_time'];
    foreach ($lostpass as $index => $val) {
      if ($val < $limit)
        unset($lostpass[$index]);
    }

    if ((is_countable($lostpass) ? count($lostpass) : 0) >= $config['lostpass_retry'])
      return -125;

    $lostpass = implode(';', $lostpass) . ';' . time();
    $query = $this->list_handle->update(
      $this->db_structure,
      auth::$config['method']['param']['tablename'],
      array('==', 'field:uid', $row['uid']),
      array(
        'lostpasswordtime' => $lostpass,
        'validkey' => $key
      )
    ) or die("Query failed (5)");

    $results = $this->list_handle->query($query);
    return array($row['uid'], $row['email']);
  }

  function listuser($array)
  {
    $userspace = current($array);
    next($array);
    $userspace = eval_list($userspace);

    $searchpattern = current($array);
    next($array);
    $searchpattern = eval_list($searchpattern);
    if (is_array($searchpattern)) {
      if (isset($searchpattern['cond']))
        $cond = $searchpattern['cond'];
      else
        $cond = NULL;

      if (isset($searchpattern['regexp']))
        $searchpattern = $searchpattern['regexp'];
      else
        $searchpattern = '';
    } else
      $cond = NULL;

    $first = current($array);
    next($array);
    $first = eval_list($first);

    $nb = current($array);
    next($array);
    $nb = eval_list($nb);

    if (!is_numeric($first) || ($nb != '' && !is_numeric($nb)))
      return -12;

    if (!preg_match("/^[" . auth::$userpattern . "\*\?]*$/", $searchpattern)) {
      return -8;
    }
    $elements = array();

    while (list(, $val) = each($array)) {
      $tmp_l = eval_list($val);

      if (
        $tmp_l == 'LM' ||
        $tmp_l == 'LF' ||
        $tmp_l == 'LL' ||
        $tmp_l == 'L1' ||
        $tmp_l == 'LD' ||
        $tmp_l == 'LN'
      ) {
        $val = current($array);
        next($array);
        if (!isset($val))
          throw new TempleetError('Error in list! You didn\'t specify a second argument');
        $elements[$tmp_l] = $val;
      } else
        throw new TempleetError('Error in list! You must use LM, LF, LL, L1, LD or LN as first argument');
    }

    if ($searchpattern != '') {
      $searchpattern = preg_replace(array("/\*/", "/\?/"), array("%", "_"), $searchpattern);
    }

    if (!$this->connect())
      return -1000;

    $basecond = array(
      '&&',
      array('!=', 'field:login', 'admin'),
      array('!=', 'field:uid', auth::$uid)
    );

    if (!auth::$admin) {
      if (auth::$uid < 0) {
        $basecond = array(
          '&&',
          $basecond,
          array('==', 'field:userspace', $userspace)
        );
      } else {
        $basecond = array(
          '&&',
          $basecond,
          array('==', 'field:userspace', auth::$userspace)
        );
      }
    }

    if ($cond != NULL) {
      if ($searchpattern != '') {
        $cond = array(
          '&&',
          $cond,
          array(
            '&&',
            array('LIKE', 'field:login', $searchpattern),
            $basecond
          )
        );
      } else {
        $cond = array(
          '&&',
          $cond,
          $basecond
        );
      }
    } else {
      if ($searchpattern != '') {
        $cond = array(
          '&&',
          array('LIKE', 'field:login', $searchpattern),
          $basecond
        );
      } else {
        $cond = $basecond;
      }
    }

    $query = $this->list_handle->select(
      '',
      array("count(*)" => "count"),
      auth::$config['method']['param']['tablename'],
      $cond,
      '',
      '',
      '',
      ''
    );

    $results = $this->list_handle->query($query);

    if (!$results)
      return -1000;


    $this->list_rows = $this->list_handle->fetcharray($results);
    $this->list_nb_rows = $this->list_rows["count"];

    if ($nb == '') $nb = $this->list_nb_rows;

    $query = $this->list_handle->select(
      '',
      explode(",", "uid,login,email,ipaddr,creation,nickname,valid,userspace"),
      auth::$config['method']['param']['tablename'],
      $cond,
      '',
      '',
      "uid desc",
      array($first, $nb)
    );
    $results = $this->list_handle->query($query);
    if (!$results) {
      unset($this->list_nb_rows);
      return -1000;
    }

    $tmp = '';
    $this->list_nb_rows = $this->list_handle->numrows($results);

    $row = 0;
    if ($this->list_nb_rows == 0 && isset($elements['LD']))
      $tmp .= eval_list($elements['LD']);

    elseif ($this->list_nb_rows == 1 && isset($elements['L1'])) {
      $this->list_rows = $this->list_handle->fetcharray($results);
      $tmp .= eval_list($elements['L1']);
    } else {
      while ($this->list_rows = $this->list_handle->fetcharray($results)) {
        if ($row == 0) {
          if (isset($elements['LF']))
            $tmp .= eval_list($elements['LF']);
        }

        if (isset($elements['LM']))
          $tmp .= eval_list($elements['LM']);

        if ($row == $this->list_nb_rows - 1 && isset($elements['LL']))
          $tmp .= eval_list($elements['LL']);

        $row++;
      }
    }
    $this->list_handle->free_result($results);
    unset($this->list_nb_rows);

    return $tmp;
  }

  function listusernbrows()
  {
    if (!isset($this->list_nb_rows))
      throw new TempleetError('~auth_nbrows() not used in ~auth_listuser() context');

    return $this->list_nb_rows;
  }

  function listuserfld($field)
  {
    if (!isset($this->list_nb_rows))
      throw new TempleetError('~auth_listuserfld() not used in ~auth_listuser() context');

    if (
      $field == 'uid' ||
      $field == 'login' ||
      $field == 'userspace' ||
      $field == 'email' ||
      $field == 'ipaddr' ||
      $field == 'creation' ||
      $field == 'nickname'
    ) return $this->list_rows[$field];

    if ($field == 'profile') return unserialize(stripslashes($this->list_rows['profile']));

    return '';
  }

  function getprofile($uid)
  {
    $info = $this->getinfo($uid);
    if (isset($info['profile']))
      return $info['profile'];

    return array();
  }

  function updateprofile($uid, $profile)
  {
    if (!isset($profile) || (is_array($profile) && (is_countable($profile == 0) ? count($profile == 0) : 0)))
      return '';

    if (!$this->connect())
      return -1000;

    $structure = $this->db_structure;

    $fields = $this->readfields();
    $structure["fields"] = array_merge($structure["fields"], $fields);
    $query = $this->list_handle->update(
      $structure,
      auth::$config['method']['param']['tablename'],
      array('==', 'field:uid', $uid),
      $profile
    );

    $results = $this->list_handle->query($query);
    if ($results != 1)
      return -1000;

    unset($this->cache_info[$uid]);

    return 0;
  }

  private function checktools()
  {
    if (!function_exists("tools_auth_db::param")) {
      include_once("tools_db.php");
    }
  }

  function param()
  {
    $this->checktools();
    return tools_auth_db::param();
  }

  function checkparam($param)
  {
    $this->checktools();
    return tools_auth_db::checkparam($param, $this->db_structure);
  }

  function readfields()
  {
    if (!$this->connect())
      return -1000;

    $this->checktools();
    return tools_auth_db::readfields($this, $this->db_structure);
  }

  function modfield($fieldnameold, $fieldname, $desc)
  {
    if (!$this->connect())
      return -1000;

    $this->checktools();
    return tools_auth_db::modfield($this, $fieldnameold, $fieldname, $desc);
  }

  function addfield($fieldname, $fieldtype)
  {
    if (!$this->connect())
      return -1000;

    $this->checktools();
    $res = tools_auth_db::addfield($this, $fieldname, $fieldtype);
    return $res;
  }

  function delfield($arrayfieldname)
  {
    if (!$this->connect())
      return -1000;

    $this->checktools();
    return tools_auth_db::delfield($this, $arrayfieldname);
  }
} // class_auth_db
