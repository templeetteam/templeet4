<?php
/*
 * File layer for authentication
 *
 * This code has been developed by:
 *
 * Pascal Courtois
 *
 */

define("auth_file_users", "templeet/auth/users");
define("auth_file_login", "templeet/auth/login");
define("auth_file_info", "templeet/auth/info");

class class_auth_file
{
  public string $error = '';

  private $cache_logins; /* $cache_logins[login]=uid */
  private ?array $cache_users = null;  /* $cache_users[uid]=array( ... ) */

  /* used by ~auth_listuser() */
  private ?array $listuser_row = null;
  private $listuser_user;
  private ?int $listuser_nbrows = null;

  private $profile_self;
  private $profile_other;
  private $profile_other_uid;

  private function read_login_file()
  {
    if (!isset($this->cache_logins)) {
      include_once("templeet/modules/fieldfileaccess.php");
      $this->cache_logins = ffa_readfile(auth_file_login);
    }
  }

  private function read_user_file()
  {
    if (!isset($this->cache_users)) {
      include_once("templeet/modules/fieldfileaccess.php");
      $this->cache_users = ffa_readfile(auth_file_users);
    }
  }

  private function checkcond($cond, $uid)
  {
    $this->listuser_user = $this->cache_users[$uid];

    return $this->evalcond($cond);
  }

  private function evalcond($cond)
  {
    if (!is_array($cond)) {
      if (is_numeric($cond))
        return $cond;
      if (!is_string($cond))
        return NULL;
      if (preg_match('/^priv:(.*)/', $cond, $res)) {
        $priv = $res[1];
        return ($this->listuser_user['priv']['$priv'] ?? 0);
      }

      if (preg_match('/^field:(.*)/', $cond, $res)) {
        $field = $res[1];
        return ($this->listuser_user[$field] ?? '');
      }
      return '';
    }

    switch ($cond[0]) {
      case '<':
        return $this->evalcond($cond[1]) <  $this->evalcond($cond[2]);
      case '>':
        return $this->evalcond($cond[1]) >  $this->evalcond($cond[2]);
      case '>=':
        return $this->evalcond($cond[1]) >=  $this->evalcond($cond[2]);
      case '<=':
        return $this->evalcond($cond[1]) <=  $this->evalcond($cond[2]);
      case '==':
        return $this->evalcond($cond[1]) ==  $this->evalcond($cond[2]);
      case '!=':
        return $this->evalcond($cond[1]) !=  $this->evalcond($cond[2]);
      case '&&':
        return $this->evalcond($cond[1]) &&  $this->evalcond($cond[2]);
      case '||':
        return $this->evalcond($cond[1]) ||  $this->evalcond($cond[2]);
      default:
        return "ERROR";
    }
  }

  function getinfo($user, $fields = '')
  {
    if (!is_int($user) && ctype_digit($user))
      $user = (int)$user;

    if (!is_int($user)) {
      $this->read_login_file();

      if (!isset($this->cache_logins[$user[0] . "|" . $user[1]]))
        return -105;
      $user = $this->cache_logins[$user[0] . "|" . $user[1]];
    }

    $this->read_user_file();

    if (!isset($this->cache_users[$user]))
      return -105;

    $info = $this->cache_users[$user];
    if (!isset($info['priv']) || !is_array($info['priv']))
      $info['priv'] = array();

    $profile = ffa_readfile("templeet/auth/info/$user");
    if (!is_array($profile))
      $profile = array();

    $info['profile'] = $profile;

    if ($user == 0) {
      global $config;
      $info['email'] = $config['emailadmin'];
    }

    return $info;
  }


  private function canonic_user($user)
  {
    return strtolower($user);
  }

  function checkauth($userspace, $user, $pass)
  {
    $info = $this->getinfo(array($userspace, $user));
    if (!is_array($info))
      return $info;

    if ($info['valid'] <= 0)
      return -2;

    if (sha1($info['uid'] . ":$pass") != $info['pass'])
      return -10;

    auth::$admin = ($info['uid'] == 0 || !empty($info['priv']["ADMIN"]));
    auth::$userspace_admin = auth::$admin || $info['uid'] == $info['userspace'];
    auth::$priv = $info['priv'];

    return $info;
  }

  function listuser($array)
  {
    $userspace = current($array);
    next($array);
    $userspace = eval_list($userspace);

    $searchpattern = current($array);
    next($array);
    $searchpattern = eval_list($searchpattern);
    if (is_array($searchpattern)) {
      if (isset($searchpattern['cond']))
        $cond = $searchpattern['cond'];
      else
        $cond = NULL;

      if (isset($searchpattern['regexp']))
        $searchpattern = $searchpattern['regexp'];
      else
        $searchpattern = '';
    } else {
      $cond = NULL;
    }

    $first = current($array);
    next($array);
    $first = eval_list($first);

    $nb = current($array);
    next($array);
    $nb = eval_list($nb);

    if (!preg_match("/^[" . auth::$userpattern . "\*\?]*$/", $searchpattern))
      return -8;

    $elements = array();

    while (list(, $val) = each($array)) {
      $tmp_l = eval_list($val);
      if (
        $tmp_l == 'LM' ||
        $tmp_l == 'LF' ||
        $tmp_l == 'LL' ||
        $tmp_l == 'L1' ||
        $tmp_l == 'LN' ||
        $tmp_l == 'LD'
      ) {
        $val = current($array);
        next($array);
        if (!isset($val)) {
          return -200;
        }
        $elements[$tmp_l] = $val;
      } else {
        return -200;
      }
    }

    $this->read_login_file();

    if (!$searchpattern == '') {
      $searchpattern = preg_replace(array("/\./", "/\*/", "/\?/"), array("\\.", ".*", "."), $searchpattern);
    }

    $basecond = array(
      '&&',
      array('!=', 'field:login', 'admin'),
      array('!=', 'field:uid', auth::$uid)
    );
    if (!auth::$admin) {
      if (auth::$uid < 0) {
        $basecond = array(
          '&&',
          $basecond,
          array('==', 'field:userspace', $userspace)
        );
      } else {
        $basecond = array(
          '&&',
          $basecond,
          array('==', 'field:userspace', auth::$userspace)
        );
      }
    }

    if ($cond != NULL) {
      if ($searchpattern != '') {
        $cond = array(
          '&&',
          $cond,
          array(
            '&&',
            array('LIKE', 'field:login', $searchpattern),
            $basecond
          )
        );
      } else {
        $cond = array(
          '&&',
          $cond,
          $basecond
        );
      }
    } else {
      if ($searchpattern != '') {
        $cond = array(
          '&&',
          array('LIKE', 'field:login', $searchpattern),
          $basecond
        );
      } else {
        $cond = $basecond;
      }
    }

    $this->read_user_file();
    $arrayres = array();
    while (list($key, $uid) = each($this->cache_logins)) {
      if ($this->checkcond($cond, $uid))
        $arrayres[] = array($uid, $key);
    }

    $this->listuser_nbrows = count($arrayres);

    $res = '';

    if ($this->listuser_nbrows == 0 && isset($elements['LD'])) {
      $res .= eval_list($elements['LD']);
    } elseif ($this->listuser_nbrows == 1 && isset($elements['L1'])) {
      $res .= eval_list($elements['L1']);
    } else {
      $row = $this->listuser_nbrows - $first - 1;
      $i = 0;

      if ($nb == '') $nb = $this->listuser_nbrows;

      while ($i < $nb && $row >= 0) {
        $this->listuser_row = $arrayres[$row];
        if ($i == 0) {
          if (isset($elements['LF']))
            $res .= eval_list($elements['LF']);
        }

        if (isset($elements['LM']))
          $res .= eval_list($elements['LM']);

        if (($i == $nb - 1 || $row == 0) && isset($elements['LL']))
          $res .= eval_list($elements['LL']);

        $row--;
        $i++;
      }
    }
    return $res;
  }

  function listusernbrows()
  {
    if (!isset($this->listuser_row))
      throw new TempleetError('~auth_nbrows() not used in ~auth_listuser() context');

    return $this->listuser_nbrows;
  }

  function listuserfld($field = NULL)
  {
    if (!isset($this->listuser_row))
      throw new TempleetError('~auth_listuserfld() not used in ~auth_listuser() context');

    $info = $this->getinfo($this->listuser_row[0]);

    if (!isset($field))
      return $info;

    if (isset($info[$field]))
      return $info[$field];

    return '';
  }

  function newuser($userspace, $user, $password, $email, $validaccount, $validkey, $setpriv, $info)
  {
    $this->read_user_file();
    $this->read_login_file();

    $escuser = $this->canonic_user($user);
    do {
      $uid = mt_rand(1, PHP_INT_MAX);
    } while (isset($this->cache_users[$uid]));

    if ($userspace < 0) {
      $userspace = $uid;
    } else {
      if (isset($this->cache_logins[$userspace . "|" . $escuser]))
        return -3;

      reset($this->cache_users);
      while (list($key, $value) = each($this->cache_users)) {
        if (
          (

            ($value['login'] == $escuser) ||
            ($value['nickname'] == $user)) &&
          $value['userspace'] == $userspace
        ) {
          return -3;
        }
      }
    }


    $uservalue = array();

    $priv = array();
    while (list($key, $val) = each($setpriv))
      $priv[$key] = $val;


    $uservalue['uid'] = $uid;
    $uservalue['login'] = $escuser;
    $uservalue['valid'] = $validaccount;
    $uservalue['email'] = $email;
    $uservalue['ipaddr'] = $_SERVER['REMOTE_ADDR'];
    $uservalue['creation'] = strftime("%Y%m%d%H%M%S");
    $uservalue['pass'] = sha1($uid . ":$password");
    $uservalue['key'] = $validkey;
    $uservalue['priv'] = $priv;
    $uservalue['nickname'] = $user;
    $uservalue['userspace'] = $userspace;
    $uservalue['lostpasswordtime'] = array();

    $this->cache_users[$uid] = $uservalue;

    $res = ffa_adduniquekey(auth_file_login, $userspace . "|" . $escuser, $uid);
    if ($res < 0)
      return -3; // race condition adding same user

    $this->cache_logins = $res;

    ffa_adduniquekey(auth_file_users, $uid, $uservalue);
    createdir("templeet/auth/info");
    if (is_array($info) && count($info) != 0) {
      include_once("templeet/modules/fieldfileaccess.php");
      ffa_writefile("templeet/auth/info/$uid", $info);
    }

    return $uid;
  }

  function userspace()
  {
    $this->read_user_file();
    $userspaces = array();

    foreach ($this->cache_users as $user) {
      if ($user['userspace'] == $user['uid'])
        $userspaces[$user['userspace']] = $user['login'];
    }

    return $userspaces;
  }

  function setpriv($setpriv)
  {
    $this->read_user_file();

    while (list($uid,) = each($setpriv)) {
      if (isset($this->cache_users[$uid])) {
        if (isset($setpriv[$uid]['email']))
          $this->cache_users[$uid]['email'] = $setpriv[$uid]['email'];

        if (isset($setpriv[$uid]['valid'])) {
          $this->cache_users[$uid]['valid'] = $setpriv[$uid]['valid'];
          if ($this->cache_users[$uid]['valid'] != 0 && $setpriv[$uid]['valid'] == 0)
            $this->cache_users[$uid]['key'] = auth_mkrdstr(12);
          else
                    if ($setpriv[$uid]['valid'] < 0)
            $this->cache_users[$uid]['key'] = '';
        }

        if (isset($setpriv[$uid]['priv'])) {
          while (list($area, $val) = each($setpriv[$uid]['priv'])) {
            if ($val != 0)
              $this->cache_users[$uid]['priv'][$area] = $val;
            else
              unset($this->cache_users[$uid]['priv'][$area]);
          }
        }
      }

      ffa_setkey(auth_file_users, $uid, $this->cache_users[$uid]);
    }

    return 0;
  }

  function deluser($uid)
  {
    $uid = (int)$uid;
    $this->read_user_file();
    if (!isset($this->cache_users[$uid]))
      return -105;

    if (!auth::$admin && !(auth::$userspace_admin && auth::$userspace == $this->cache_users[$uid]['userspace']))
      return -107;

    ffa_delkey(auth_file_login, $this->cache_users[$uid]['userspace'] . "|" . $this->cache_users[$uid]['login']);
    unset($this->cache_login[$this->cache_users[$uid]['login']]);
    ffa_delkey(auth_file_users, $uid);
    unset($this->cache_users[$uid]);

    @unlink("templeet/auth/info/$uid.php");
    @unlink("templeet/auth/info/$uid.free");

    return 0;
  }

  function passwd($uid, $oldpassword, $newpassword)
  {
    $this->read_user_file();
    if (!isset($this->cache_users[$uid]))
      return -105;

    if (
      !auth::$admin &&
      !(auth::$userspace_admin && auth::$userspace == $this->cache_users[$uid]['userspace']) &&
      sha1("$uid:$oldpassword") != $this->cache_users[$uid]['pass']
    )
      return -5;

    $this->cache_users[$uid]['pass'] = sha1("$uid:$newpassword");
    ffa_setkey(auth_file_users, $uid, $this->cache_users[$uid]);

    return 0;
  }

  function loginnick($uid, $login, $nickname)
  {
    $uid = (int)$uid;
    $this->read_user_file();
    if (!isset($this->cache_users[$uid]))
      return -105;

    $this->read_login_file();
    $login = $this->canonic_user($login);

    reset($this->cache_users);
    while (list($key, $value) = each($this->cache_users)) {
      if (
        (
          ($login != '' && $value['login'] == $login) ||
          ($nickname != '' && $value['nickname'] == $nickname)) &&
        $value['userspace'] == $this->cache_users[$uid]['userspace'] &&
        $key != $uid
      ) {
        return -3;
      }
    }

    if ($login != '') {
      ffa_delkey(auth_file_login, $this->cache_users[$uid]['userspace'] . "|" . $this->cache_users[$uid]['login']);
      $this->cache_users[$uid]['login'] = $login;
      ffa_adduniquekey(auth_file_login, $this->cache_users[$uid]['userspace'] . "|" . $login, $uid);
    }

    if ($nickname != '')
      $this->cache_users[$uid]['nickname'] = $nickname;

    ffa_setkey(auth_file_users, $uid, $this->cache_users[$uid]);

    unset($this->cache_users);

    return 0;
  }

  function setkeypass($userspace, $login, $key, $emailcheck = 1)
  {
    $this->read_user_file();
    $this->read_login_file();

    $login = $this->canonic_user($login);

    if (!isset($this->cache_logins[$userspace . "|" . $login]))
      return -105;

    $uid = $this->cache_logins[$userspace . "|" . $login];

    if (!isset($this->cache_users[$uid]))
      return -105;

    if ($this->cache_users[$uid]['email'] == '' && $emailcheck)
      return -7;

    if (isset($this->cache_users[$uid]['lostpasswordtime']) && is_array($this->cache_users[$uid]['lostpasswordtime'])) {
      global $config;
      $limit = time() - $config['lostpass_time'];
      foreach ($this->cache_users[$uid]['lostpasswordtime'] as $key => $val) {
        if ($key < $limit)
          unset($this->cache_users[$uid]['lostpasswordtime'][$key]);
      }
      if ((is_countable($this->cache_users[$uid]['lostpasswordtime']) ? count($this->cache_users[$uid]['lostpasswordtime']) : 0) >= $config['lostpass_retry'])
        return -125;
      $this->cache_users[$uid]['lostpasswordtime'][time()] = 1;
    } else {
      $this->cache_users[$uid]['lostpasswordtime'] = array();
    }

    if ($this->cache_users[$uid]['valid'] > 0)
      $this->cache_users[$uid]['key'] = $key;
    else
      return -2;

    ffa_setkey(auth_file_users, $uid, $this->cache_users[$uid]);

    return array($uid, $this->cache_users[$uid]['email']);
  }

  function readfields()
  {
    $info = ffa_readfile(auth_file_info);
    if (!is_array($info)) {
      $info = array();
      $res = ffa_writefile(auth_file_info, $info);
    }
    return $info;
  }

  function columndef($desc)
  {
    if (!is_array($desc) || !isset($desc["type"]))
      return -12;

    switch ($desc["type"]) {
      case 'text':
        if (!isset($desc[1]) || !preg_match("/^\d+$/", $desc[1]) || $desc[1] > 1000)
          return -12;
        return array("type" => "text", "size" => $desc[1]);

      case "date":
        return array("type" => "date");
        break;

      case "textarea":
        return array("type" => "textarea");
        break;

      case "int":
        return array("type" => "int");
        break;

      case "double":
        return array("type" => "double");
        break;

      case "checkbox":
        return array("type" => "checkbox");
        break;

      default:
        return -12;
    }
  }

  function modfield($fieldnameold, $fieldnamenew, $fieldtype)
  {
    include_once("templeet/modules/fieldfileaccess.php");
    $info = ffa_readfile(auth_file_info);

    $desc = (new class_auth_file())->columndef($fieldtype);
    if (!is_array($desc))
      return $desc;

    if (!isset($info[$fieldnameold]))
      return -12;

    unset($info[$fieldnameold]);
    $info[$fieldnamenew] = $desc;
    return ffa_writefile(auth_file_info, $info);
  }

  function addfield($fieldname, $fieldtype)
  {
    include_once("templeet/modules/fieldfileaccess.php");

    $desc = (new class_auth_file())->columndef($fieldtype);
    if (!is_array($desc))
      return $desc;
    $res = ffa_setkey(auth_file_info, $fieldname, $desc);
    if (!is_array($res))
      return $res;

    return 0;
  }

  function delfield($arrayfieldname)
  {
    include_once("templeet/modules/fieldfileaccess.php");
    $info = ffa_readfile(auth_file_info);
    foreach ($arrayfieldname as $fieldname) {
      if (isset($info[$fieldname]))
        unset($info[$fieldname]);
    }

    return ffa_writefile(auth_file_info, $info);
  }

  private function readprofile($uid)
  {
    $priv = ffa_readfile("templeet/auth/info/$uid");
    if (!is_array($priv))
      $priv = array();

    return $priv;
  }

  function getprofile($uid)
  {
    if ($uid == auth::$uid) {
      if (!isset($profile_self))
        $profile_self = $this->readprofile($uid);

      return $profile_self;
    } else {
      if (isset($profile_other_uid) && $profile_other_uid == $uid)
        return $profile_other;

      $profile_other = $this->readprofile($uid);
      $profile_other_uid = $uid;
      return $profile_other;
    }
  }

  function updateprofile($uid, $var)
  {
    include_once("templeet/modules/fieldfileaccess.php");

    return ffa_writefile("templeet/auth/info/$uid", $var);
  }

  function validaccount($uid, $key, $pass, $validvalue)
  {
    $info = $this->getinfo($uid);
    if (!is_array($info))
      return $info;

    if ($key == '' || $key != $info['key'])
      return -6;

    if ($pass == '')
      $this->cache_users[$uid]['valid'] = $validvalue;
    else {
      if ($this->cache_users[$uid]['valid'] > 0)
        $this->cache_users[$uid]['pass'] = sha1("$uid:$pass");
    }

    $this->cache_users[$uid]['key'] = '';

    ffa_setkey(auth_file_users, $uid, $this->cache_users[$uid]);
    return $this->cache_users[$uid];
  }

  function setemail($uid, $email)
  {
    $info = $this->getinfo($uid);
    if (!is_array($info))
      return $info;

    $this->cache_users[$uid]['email'] = $email;

    ffa_setkey(auth_file_users, $uid, $this->cache_users[$uid]);
    return 0;
  }

  function param()
  {
    return array();
  }

  function checkparam($param)
  {
    return array();
  }
} // class_auth_file
