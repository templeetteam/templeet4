<?php
/*
 * This module handles generic authentication
 *
 * This code has been developed by:
 *
 * Pascal Courtois
 *
 */

if (!class_exists("auth")) {
  global $config;
  include_once($config['modulesdir'] . 'auth.php');
}

class authedit
{
  static int $MAX_LENGTH_USER = 80;
  static int $MAX_LENGTH_PASS = 40;

  static ?array $area_name = null;
  static ?array $area_keys = null;

  // variables used in auth_list
  static $list_value; // array of values for current line in auth_list
  static $list_row;   // row number in auth_list
  static $list_uidarea; // uid of areas to be listed
  static $list_uid;   // uid of user listed

  // variables used in auth_listarea
  static $areaname;
  static $arealabel;

  static bool $fields_checked = FALSE;

  static function checkuser($user)
  {
    return preg_match("/^[" . auth::$userpattern . "]{1," . authedit::$MAX_LENGTH_USER . "}$/", $user);
  }

  static function checkpass($password)
  {
    return preg_match("/^[" . auth::$passpattern . "]{1," . authedit::$MAX_LENGTH_PASS . "}$/", $password);
  }

  static function checkareaname($areaname)
  {
    return preg_match("/^[A-Za-z][\w\-]*$/", $areaname);
  }

  static function checkarealabel($arealabel)
  {
    return preg_match("/^[\w \(\)\-]*$/", $arealabel);
  }

  static function set_area($uid = 0)
  {
    if (isset(authedit::$area_keys[$uid]) && is_array(authedit::$area_keys[$uid]))
      return;

    $areas = ffa_readfile("templeet/auth/area/$uid");

    if (!is_array($areas))
      $areas = array();

    if ($uid == 0)
      $areaowner = '';
    else
      $areaowner = "\{$uid}";

    authedit::$area_keys[$uid] = array();
    authedit::$area_name[$uid] = array();

    while (list($key, $val) = each($areas)) {
      if ($key == 'ADMIN' && $uid == 0) {
        if (auth::$admin) {
          authedit::$area_keys[$uid][] = $key;
          if ($val == '') {
            authedit::$area_name[$uid][] = $key;
          } else {
            authedit::$area_name[$uid][] = $val;
          }
        }
      } else {
        if ($val == '')
          $label = $key;
        else
          $label = $val;

        if (
          auth::$admin || ($uid == auth::$uid) ||
          (isset(auth::$priv[$areaowner . "_ED_" . $key]) && auth::$priv[$areaowner . "_ED_" . $key] > 0) ||
          (isset(auth::$priv[$areaowner . "_DEL_" . $key]) && auth::$priv[$areaowner . "_DEL_" . $key] > 0)
        ) {
          authedit::$area_keys[$uid][] = $key;
          authedit::$area_name[$uid][] = $val;
        }

        if (auth::$admin || ($uid == auth::$uid) || (isset(auth::$priv[$areaowner . "_DEL_" . $key]) && auth::$priv[$areaowner . "_DEL_" . $key] > 0)) {
          authedit::$area_keys[$uid][] = "_ED_" . $key;
          authedit::$area_name[$uid][] = "edit<br />" . $val;
        }

        if (auth::$admin || ($uid == auth::$uid)) {
          authedit::$area_keys[$uid][] = "_DEL_" . $key;
          authedit::$area_name[$uid][] = "delegate<br />" . $val;
        }
      }
    }
  }

  static function writeconfigfile()
  {
    global $config;
    if (is_array(auth::$config) && isset(auth::$config['key']) && strlen(auth::$config['key']) == AUTH_ENTROPY) {
      return ffa_writefile($config['authconfigfile'], auth::$config);
    } else
      throw new TempleetError('Error in authedit::writeconfigfile(). Bad auth::config');
  }

  static function checkfieldname($name)
  {
    return preg_match('/^[a-zA-Z]\w{24}$/', $name) && !preg_match("/!$name!/", "!uid!login!valid!email!ipaddr!pass!key!priv!creation!nickname!userspace!");
  }

  static function checkfieldpriv($field)
  {

    return TRUE; // temporary fix for unfinished function

    if (auth::$userspace_admin)
      return TRUE;

    if (!isset(auth::$config['field'][$field]))
      return FALSE;

    // public field
    if (!is_array(auth::$config['field'][$field]))
      return TRUE;

    if (!isset(auth::$config['field'][$field]['priv']) || !is_array(auth::$config['field'][$field]['priv']))
      return FALSE;

    reset(auth::$config['field'][$field]['priv']);
    $area = key(auth::$config['field'][$field]['priv']);
    $value = current(auth::$config['field'][$field]['priv']);
    next(auth::$config['field'][$field]['priv']);

    return auth::getpriv($area) >= $value;
  }

  static function readfields($type = 0)
  {
    include_authmodule();

    if (!isset(auth::$config['infoperso']))
      auth::$config['infoperso'] = array();

    if (!authedit::$fields_checked) {
      $authlayer_fields = auth::$handle->readfields();
      $fields_modified = FALSE;
      foreach ($authlayer_fields as $fieldname => $value) {
        if (!isset(auth::$config['infoperso'][$fieldname])) {
          auth::$config['infoperso'][$fieldname]['desc'] = $value;
          $fields_modified = TRUE;
        }
      }

      $authfields = auth::$config['infoperso'];
      foreach ($authfields as $fieldname => $value) {
        if (
          auth::$config['infoperso'][$fieldname]['desc']['type'] != "sep" &&
          auth::$config['infoperso'][$fieldname]['desc']['type'] != "title" &&
          !isset($authlayer_fields[$fieldname])
        ) {
          unset(auth::$config['infoperso'][$fieldname]);
          $fields_modified = TRUE;
        }
      }

      if ($fields_modified)
        authedit::writeconfigfile();

      authedit::$fields_checked = TRUE;
    }
    return auth::$config['infoperso'];
  }

  static function makeheaders($headers_array)
  {
    if (!is_array($headers_array))
      return '';

    $headers = '';
    foreach ($headers_array as $name => $value) {
      if ($name != "additional_parameters")
        $headers .= "$name: $value\r\n";
    }

    if (!isset($headers_array['Date']))
      $headers .= 'Date: ' . date('r') . "\r\n";

    global $config;
    if ($config['emailadmin'] != '') {
      if (!isset($headers_array['From']))
        $headers .= 'From: ' . $config['emailadmin'] . "\r\n";
      if (!isset($headers_array['Reply-To']))
        $headers .= 'Reply-To: ' . $config['emailadmin'] . "\r\n";
      if (!isset($headers_array['X-Sender']))
        $headers .= 'X-Sender: ' . $config['emailadmin'] . "\r\n";
    }
    return $headers;
  }
} // class authedit

function return_auth_listuser($expr)
{
  $res = auth_listuserwrap($expr);
  if ($res < 0) {
    each($expr);
    each($expr);
    each($expr);
    each($expr);
    each($expr);
    each($expr);
    $elements = array();
    while (list(, $val) = each($expr)) {
      $tmp_l = eval_list($val);
      if (
        $tmp_l == 'LM' ||
        $tmp_l == 'LF' ||
        $tmp_l == 'LL' ||
        $tmp_l == 'L1' ||
        $tmp_l == 'LN' ||
        $tmp_l == 'LD'
      ) {
        $val = current($expr);
        next($expr);
        if (!isset($val))
          throw new TempleetError("Error in list! You didn't specify a second argument");
        $elements[$tmp_l] = $val;
      } else
        throw new TempleetError('Error in list! You must use LM, LF, LL, L1, LD or LN as first argument');
    }

    if (isset($elements['LN']))
      return eval_list($elements['LN']);

    return '';
  }
  return $res;
}

function auth_listuserwrap($expr)
{
  include_authmodule();

  $res = auth::$handle->listuser($expr);
  return $res;
}

function return_auth_nbrows($expr)
{
  if (!isset(auth::$handle))
    throw new TempleetError('~auth_nbrows() not used in ~auth_listuser() context');

  $res = auth::$handle->listusernbrows();
  return $res;
}

function return_auth_listuserfld($expr)
{
  if (!isset(auth::$handle))
    throw new TempleetError('~auth_listuserfld() not used in ~auth_listuser() context');

  $field = current($expr);
  next($expr);
  if (isset($field)) {
    $field = eval_list($field);
    if (authedit::checkfieldpriv($field)) {
      $res = auth::$handle->listuserfld($field);
      return $res;
    }
  } else {
    $tmp = auth::$handle->listuserfld();
    $res = array();
    foreach ($tmp as $field => $value) {
      if (authedit::checkfieldpriv($field))
        $res[$field] = $value;
    }
    return $res;
  }

  return NULL;
}

function return_auth_newuser($expr)
{
  global $global_var, $config;

  $validmode = return_auth_validmode();
  // createuserspace<0 => create a new userspace
  // createuserspace>=0 create user in that userspace
  // createuserspace=='' in current userspace

  $createuserspace = current($expr);
  next($expr);
  $createuserspace = eval_list($createuserspace);
  if ($createuserspace === '') {
    if (!auth::$userspace_admin)
      return -107;

    $userspace = auth::$userspace;
  } elseif ((int)$createuserspace < 0) {
    if (!auth::$admin && auth::getpriv('CREATEUSERSPACE') < $config['createuserspacepriv'])
      return -107;
    $userspace = -1;
  } elseif (is_int($createuserspace) || ctype_digit($createuserspace)) {
    if (!auth::$admin && (!auth::$userspace_admin || (int)$createuserspace != auth::$userspace)) {
      if (auth::getpriv('CREATEUSER') < $config['createuserspriv'] && auth::getpriv('{$createuserspace}CREATEUSER') < $config['createuserspriv'])
        return -107;
    }
    $userspace = (int)$createuserspace;
  } else
    return -12;

  $user = current($expr);
  next($expr);
  $user = eval_list($user);
  if (strtolower($user) == "admin")
    return -3;

  $password = current($expr);
  next($expr);
  $password = eval_list($password);

  $profile = current($expr);
  next($expr);
  if (isset($profile)) {
    $profile = eval_list($profile);
    if (isset($profile)) {
      if (is_string($profile) && isset($global_var->$profile))
        $profile = $global_var->$profile;
      elseif (!is_array($profile))
        $profile = NULL;
    } else
      $profile = NULL;
  }

  $email = current($expr);
  next($expr);
  if (return_auth_emailislogin())
    $email = $user;
  else
    $email = eval_list($email);

  $subject = current($expr);
  next($expr);
  $message = current($expr);
  next($expr);
  $headers = current($expr);
  next($expr);
  $force = current($expr);
  next($expr);
  $force = eval_list($force);

  if (!authedit::checkuser($user) || !authedit::checkpass($password)) {
    return -101;
  }

  if ($config['createuserspriv'] > 0 && !auth::$admin && (!auth::$userspace_admin || auth::$userspace != $userspace))
    return -107;

  $defaultpriv = array();

  if (list(, $area) = each($expr)) {
    $area = eval_list($area);
    if (is_array($area)) {
      $priv_param = $area;
    } else {
      $priv_param = array();
      $ok = 1;
      while ($ok) {
        $val = current($expr);
        next($expr);
        if (!isset($val))
          throw new TempleetError('Error in newuser! You didn\'t specify a second argument');

        $priv_param[$area] = $val;
        $area = current($expr);
        next($expr);
        $ok = isset($area);
      }
    }

    $areas = array();
    foreach ($priv_param as $area => $val) {
      if (!preg_match('/^({(\d*)})?((?:_DEL_|_ED_)?)(.*)/', $area, $res))
        return -115;

      if ($res[1] == "{}" && auth::$userspace >= 0)
        $uidarea = auth::$userspace;
      else {
        $uidarea = $res[2];
        if ($uidarea == '')
          $uidarea = 0;
      }
      $area_ext = $res[3];
      $area_base = $res[4];

      if (!isset($areas[$uidarea]))
        $areas[$uidarea] = ffa_readfile("templeet/auth/area/$uidarea");

      if (!isset($areas[$uidarea][$area_base]))
        return -115;

      $val = eval_list($val);

      if ($uidarea == 0)
        $tmp_userspace = '';
      else
        $tmp_userspace = "{" . $uidarea . "}";

      if (
        auth::$admin || auth::$uid == $uidarea ||
        ($area_ext == '' && isset(auth::$priv[$tmp_userspace . "_ED_" . $area_base]) && auth::$priv[$tmp_userspace . "_ED_" . $area_base] >= $val) ||
        (($area_ext == '' || $area_ext == "_ED_") && isset(auth::$priv[$tmp_userspace . "_DEL_" . $area_base]) && auth::$priv[$tmp_userspace . "_DEL_" . $area_base] > 0)
      ) {
        $defaultpriv["$tmp_userspace$area_ext$area_base"] = $val;
      } else {
        return -107;
      }
    }
  }

  switch ($validmode) {
    case 'YES':
      $validaccount = 1;
      $validkey = '';
      break;

    case 'CONFADMIN':
    case 'CONFIRM':
      if ($force)
        $validaccount = 1;
      else
        $validaccount = 0;
      $validkey = auth_mkrdstr(30);

      if (!preg_match('/^\s*<?([\w\-\.\+]+@(?:[\w\-]+\.)*[\w\-]+)>?\s*$/', $email, $arrayres))
        return -113;

      $email = $arrayres[1];

      $global_var->auth_validkey = $validkey;

      break;

    default:
      if ($force)
        $validaccount = 1;
      else
        $validaccount = -1;
      $validkey = '';
      break;
  }

  include_authmodule();

  $res = auth::$handle->newuser($userspace, $user, $password, $email, $validaccount, $validkey, $defaultpriv, $profile);
  $tmpsubject = eval_list($subject);
  $tmpmessage = eval_list($message);

  if ((($force && preg_match('/^\s*<?([\w\-\.\+]+@(?:[\w\-]+\.)*[\w\-]+)>?\s*$/', $email)) || $validmode == 'CONFIRM' || $validmode == 'CONFADMIN') &&
    $res >= 0 && $tmpsubject != '' && $tmpmessage != ''
  ) {
    $global_var->newuser_uid = $res;
    $subject = eval_list($subject);
    $message = eval_list($message);

    $additional_parameters = NULL;
    $headers = eval_list($headers);
    if (is_array($headers) && isset($headers['additional_parameters']))
      $additional_parameters = $headers['additional_parameters'];
    $headers = authedit::makeheaders($headers);

    if (!@mail($email, auth_encode_suject($subject), $message, $headers, $additional_parameters)) {
      seterrormessage();
      return -102;
    }
  }

  if (!$force && $validmode == 'NO' && $res >= 0 && $tmpsubject != '' && $tmpmessage != '') {
    $global_var->newuser_uid = $res;
    $subject = eval_list($subject);
    $message = eval_list($message);

    $additional_parameters = NULL;
    $headers = eval_list($headers);
    if (is_array($headers) && isset($headers['additional_parameters']))
      $additional_parameters = $headers['additional_parameters'];
    $headers = authedit::makeheaders($headers);

    if ($userspace <= 0) {
      global $config;
      $email = $config['emailadmin'];
    } else {
      $infouserspace = auth_getinfo($userspace);
      if (!is_array($infouserspace))
        return -126;

      $email = $infouserspace['email'];
    }

    if ($email != '' && !@mail($email, auth_encode_suject($subject), $message, $headers, $additional_parameters)) {
      seterrormessage();
      return -102;
    }
  }

  return $res;
}

function return_auth_validmode()
{
  global $config;

  auth_getconfig();

  if (isset(auth::$config['account']['valid']) && preg_match('/^(?:YES|NO|CONFIRM|CONFADMIN)$/', auth::$config['account']['valid']))
    return auth::$config['account']['valid'];
  else
    return 'NO';
}

function return_auth_list($expr)
{
  $elements = array();

  if (auth::$uid < 0)
    return -103;

  $tmp = '';

  $val = current($expr);
  next($expr);
  authedit::$list_uidarea = eval_list($val);

  $val = current($expr);
  next($expr);
  authedit::$list_uid = eval_list($val);

  if (authedit::$list_uid == '') {
    authedit::$list_uid = -1;
  } else
    authedit::$list_uid = (int)authedit::$list_uid;

  while (list(, $val) = each($expr)) {
    $tmp_l = eval_list($val);

    if (
      $tmp_l == 'LM' ||
      $tmp_l == 'LF' ||
      $tmp_l == 'LFO' ||
      $tmp_l == 'LR' ||
      $tmp_l == 'LL' ||
      $tmp_l == 'LLO' ||
      $tmp_l == 'LN' ||
      $tmp_l == 'L1' ||
      $tmp_l == 'LD' ||
      $tmp_l == 'LFA' ||
      $tmp_l == 'LLA'
    ) {
      $val = current($expr);
      next($expr);
      if (!isset($val))
        throw new TempleetError('Error in list! You didn\'t specify a second argument');
      $elements[$tmp_l] = $val;
    } else
      throw new TempleetError('Error in list! You must use LM, LF, LFO, LFA, LL, LLO, LLA,  L1 or LD as first argument');
  }

  if (!auth::$admin) {
    if (authedit::$list_uidarea != 0 && authedit::$list_uidarea != auth::$userspace) {
      if (isset($elements['LD']))
        return eval_list($elements['LD']);
      else
        return '';
    }
  }

  authedit::set_area(authedit::$list_uidarea);

  if (authedit::$list_uid >= 0) {
    authedit::$list_value = auth_getinfo(authedit::$list_uid);
    if (!auth::$admin && authedit::$list_value["userspace"] != auth::$userspace)
      authedit::$list_value = array();
  }

  authedit::$list_row = 0;
  $num_rows = is_countable(authedit::$area_keys[authedit::$list_uidarea]) ? count(authedit::$area_keys[authedit::$list_uidarea]) : 0;

  if ($num_rows == 0 && isset($elements['LD']))
    $tmp .= eval_list($elements['LD']);
  elseif ($num_rows == 1 && isset($elements['L1']))
    $tmp .= eval_list($elements['L1']);
  else {
    if (isset($elements['LFA']))
      $tmp .= eval_list($elements['LFA']);
    while (authedit::$list_row < $num_rows) {
      if (authedit::$list_row == 0) {
        if (isset($elements['LFO']))
          $tmp .= eval_list($elements['LFO']);
        else {
          if (isset($elements['LF']))
            $tmp .= eval_list($elements['LF']);
          if (isset($elements['LM']))
            $tmp .= eval_list($elements['LM']);
        }
      } elseif (authedit::$list_row == $num_rows - 1) {
        if (isset($elements['LLO']))
          $tmp .= eval_list($elements['LLO']);
        else {
          if (isset($elements['LM']))
            $tmp .= eval_list($elements['LM']);
          if (isset($elements['LL']))
            $tmp .= eval_list($elements['LL']);
        }
      } else
            if (isset($elements['LM']))
        $tmp .= eval_list($elements['LM']);

      authedit::$list_row++;
    }
    if (isset($elements['LLA']))
      $tmp .= eval_list($elements['LLA']);
  }

  return $tmp;
}

function return_auth_listfld($expr)
{
  $val = current($expr);
  next($expr);

  $field = eval_list($val);

  $display = current($expr);
  next($expr);
  if (isset($display))
    $display = eval_list($display);
  else
    $display = 0;

  if ($field == 'name') {
    return authedit::$area_name[authedit::$list_uidarea][authedit::$list_row];
  }

  if (isset(authedit::$area_keys[authedit::$list_uidarea]) && isset(authedit::$area_keys[authedit::$list_uidarea][authedit::$list_row]))
    $area = authedit::$area_keys[authedit::$list_uidarea][authedit::$list_row];
  else
    $area = '';

  if ($field == 'area')
    return $area;

  if (authedit::$list_uidarea == 0)
    $userarea = '';
  else
    $userarea = '{' . authedit::$list_uidarea . '}';

  if (isset(authedit::$list_value['priv'][$userarea . $area]))
    $priv = authedit::$list_value['priv'][$userarea . $area];
  else
    $priv = 0;

  if ($field == 'val')
    return $priv;

  if ($field == 'select') {
    if (($area == 'ADMIN' && authedit::$list_uidarea == 0) || substr($area, 0, 5) == "_DEL_") {
      if ($priv == 0)
        $tmp = "<input type=\"hidden\" name=\"_userinfo[" . authedit::$list_uid . "][areavalue][$userarea$area]\" value=\"off\" /><input type=\"checkbox\" name=\"_userinfo[" . authedit::$list_uid . "][areavalue][$userarea$area]\" value=\"on\" />";
      else
        $tmp = "<input type=\"hidden\" name=\"_userinfo[" . authedit::$list_uid . "][areavalue][$userarea$area]\" value=\"off\" /><input type=\"checkbox\" name=\"_userinfo[" . authedit::$list_uid . "][areavalue][$userarea$area]\" value=\"on\" checked />";
    } else {
      $tmp = '';

      $max = 10;

      if (!auth::$admin && authedit::$list_uidarea != auth::$uid && $area[0] != '_') {
        if (
          isset(auth::$priv[$userarea . "_ED_" . $area]) &&
          (!isset(auth::$priv[$userarea . "_DEL_" . $area]) || auth::$priv[$userarea . "_DEL_" . $area] == 0)
        )
          $max = auth::$priv[$userarea . "_ED_" . $area];
      }

      $i = 0;
      while ($i <= $max) {
        if ($i == $priv)
          $tmp .= "<option value=\"$i\" selected=\"selected\">$i</option>\n";
        else
          $tmp .= "<option value=\"$i\">$i</option>\n";
        $i++;
      }
      $tmp = "<select name=\"_userinfo[" . authedit::$list_uid . "][areavalue][$userarea$area]\">\n$tmp</select>";
    }
    return $tmp;
  }

  if ($field == 'valid') {
    if (!auth::$userspace_admin && !auth::getpriv('CREATEUSER'))
      return '';

    if (!$display)
      return authedit::$list_value['valid'];

    if (list(, $tmp) = each($expr)) $choice1 = eval_list($tmp);
    else $choice1 = "No, Email OK";
    if (list(, $tmp) = each($expr)) $choice2 = eval_list($tmp);
    else $choice2 = "No";
    if (list(, $tmp) = each($expr)) $choice3 = eval_list($tmp);
    else $choice3 = "Conf";
    if (list(, $tmp) = each($expr)) $choice4 = eval_list($tmp);
    else $choice4 = "Yes";

    if ($display == 2) {
      if (authedit::$list_value['valid'] == -2)
        return $choice1;
      if (authedit::$list_value['valid'] < 0)
        return $choice2;
      if (authedit::$list_value['valid'] == 0)
        return $choice3;
      return $choice4;
    }

    $tmp = "<select name=\"_userinfo[" . authedit::$list_value['uid'] . "][valid]\"><option value=\"-2\" ";
    if (authedit::$list_value['valid'] == -2)
      $tmp .= "selected=\"selected\"";
    $tmp .= ">$choice1</option><option value=\"-1\" ";
    if (authedit::$list_value['valid'] == -1)
      $tmp .= "selected=\"selected\"";
    $tmp .= ">$choice2</option><option value=\"0\" ";
    if (authedit::$list_value['valid'] == 0)
      $tmp .= "selected=\"selected\"";
    $tmp .= ">$choice3</option><option value=\"1\" ";
    if (authedit::$list_value['valid'] > 0)
      $tmp .= "selected=\"selected\"";
    $tmp .= ">$choice4</option></select>";
    return $tmp;
  }

  if ($field == "uid") {
    return authedit::$list_value[$field];
  }

  if (
    $field == "login" ||
    $field == "nickname"
  ) {
    $value = authedit::$list_value[$field];
    if (preg_match("/.*\|(.*)/", $value, $res)) {
      return $res[1];
    } else {
      return $value;
    }
  }

  if ($field == "userspace") {
    if (preg_match("/(.*)\|.*/", authedit::$list_value['login'], $res)) {
      return $res[1];
    } else {
      return 0;
    }
  }

  if (auth::$admin || auth::$userspace_admin || (getpriv('CREATEUSER') > 0)) {
    if ($field == 'email') {
      $email = authedit::$list_value['email'];

      if (!$display)
        return $email;

      return "<input type=\"text\" name=\"_userinfo[" . authedit::$list_value['uid'] . "][email]\" size=\"40\" value=\"$email\" />";
    }
    if ($field == 'ipaddr')
      return authedit::$list_value['ipaddr'];
    if ($field == 'creation')
      return authedit::$list_value['creation'];
  }

  return '';
}

function return_auth_listarea($expr)
{
  if (auth::$uid < 0)
    return -103;

  $uid = current($expr);
  next($expr);
  $uid = eval_list($uid);

  if (!auth::$admin)
    $uid = auth::$uid;
  else {
    if ($uid < 0)
      $uid = 0;
  }

  $elements = array();

  $tmp = '';
  while (list(, $val) = each($expr)) {
    $tmp_l = eval_list($val);

    if (
      $tmp_l == 'LM' ||
      $tmp_l == 'LF' ||
      $tmp_l == 'LL' ||
      $tmp_l == 'L1' ||
      $tmp_l == 'LD'
    ) {
      $val = current($expr);
      next($expr);
      if (!isset($val))
        throw new TempleetError('Error in list! You didn\'t specify a second argument');
      $elements[$tmp_l] = $val;
    } else
      throw new TempleetError('Error in list! You must use LM, LF, LL, L1 or LD as first argument');
  }

  $areas = ffa_readfile("templeet/auth/area/$uid");
  if (!is_array($areas))
    $areas = array();

  $num_rows = count($areas);
  $row = 0;

  if ($num_rows == 0 && isset($elements['LD']))
    $tmp .= eval_list($elements['LD']);
  elseif ($num_rows == 1 && isset($elements['L1'])) {
    authedit::$areaname = key($areas);
    authedit::$arealabel = current($areas);
    next($areas);
    $tmp .= eval_list($elements['L1']);
  } else {
    while (list(authedit::$areaname, authedit::$arealabel) = each($areas)) {
      if ($row == 0) {
        if (isset($elements['LF']))
          $tmp .= eval_list($elements['LF']);
      }

      if (isset($elements['LM']))
        $tmp .= eval_list($elements['LM']);

      if ($row == $num_rows - 1 && isset($elements['LL'])) {
        $tmp .= eval_list($elements['LL']);
      }

      $row++;
    }
  }

  return $tmp;
}

function return_auth_listareafld($expr)
{
  $fld = current($expr);
  next($expr);
  $fld = eval_list($fld);

  if ($fld == 'name') return authedit::$areaname;
  if ($fld == 'label') return authedit::$arealabel;

  return '';
}

function return_auth_newarea($expr)
{
  if (!auth::$userspace_admin)
    return -107;

  $userspace = current($expr);
  next($expr);
  $userspace = eval_list($userspace);

  if (!auth::$admin) {
    $userspace = auth::$userspace;
  } else {
    if ($userspace < 0)
      $userspace = 0;
  }

  $newarea = current($expr);
  next($expr);
  $newarea = eval_list($newarea);

  $newarealabel = current($expr);
  next($expr);
  $newarealabel = eval_list($newarealabel);

  if (!authedit::checkareaname($newarea))
    return -108;

  if (!authedit::checkarealabel($newarealabel))
    return -109;

  $res = ffa_adduniquekey("templeet/auth/area/$userspace", $newarea, $newarealabel);

  if (is_array($res))
    return 0;

  if ($res == -2)
    return -110;
  else
    return -123;
}

function return_auth_delarea($expr)
{
  if (!auth::$userspace_admin)
    return -107;

  $userspace = current($expr);
  next($expr);
  $userspace = eval_list($userspace);

  if (!auth::$admin) {
    $userspace = auth::$userspace;
  } else {
    if ($userspace < 0)
      $userspace = 0;
  }

  $area = current($expr);
  next($expr);
  $area = eval_list($area);

  if (!authedit::checkareaname($area))
    return -108;

  if ($area == 'ADMIN' || $area == 'CREATEUSER')
    return -111;

  $res = ffa_delkey("templeet/auth/area/$userspace", $area);

  if (is_array($res))
    return 0;

  return $res;
}

function return_auth_setarea($expr)
{
  if (!auth::$userspace_admin)
    return -107;

  $userspace = current($expr);
  next($expr);
  $userspace = eval_list($userspace);

  if (!auth::$admin) {
    $userspace = auth::$userspace;
  } else {
    if ($userspace < 0)
      $userspace = 0;
  }

  $area = current($expr);
  next($expr);
  $area = eval_list($area);

  $label = current($expr);
  next($expr);
  $label = eval_list($label);

  if (!authedit::checkareaname($area))
    return -108;

  if (!authedit::checkarealabel($label))
    return -109;

  $res = ffa_setkey("templeet/auth/area/$userspace", $area, $label);
  if (is_array($res))
    return 0;

  if ($res == -2)
    return -110;
  else
    return -123;
}

function return_auth_setpriv($expr)
{
  global $config;

  $subject = current($expr);
  next($expr);
  $subject = eval_list($subject);

  $message = current($expr);
  next($expr);
  $message = eval_list($message);

  $headers = current($expr);
  next($expr);
  $headers = eval_list($headers);

  authedit::set_area();

  $userinfo = current($expr);
  next($expr);
  $userinfo = eval_list($userinfo);

  if (!is_array($userinfo))
    return -12;

  $num_rows = is_countable(authedit::$area_keys) ? count(authedit::$area_keys) : 0;
  $setpriv = array();

  $sendmail = array();

  $validmode = return_auth_validmode();

  $res = array();

  reset($userinfo);
  while (list($uid, $uid_info) = each($userinfo)) {
    $arraypriv = array();

    if (isset($uid_info['areavalue'])) {
      foreach ($uid_info['areavalue'] as $area => $value) {
        preg_match('/^(?:{([^}]+)})?(.*)/', $area, $resmatch);
        if ($resmatch[1] == '')
          $resmatch[1] = 0;
        if ($area == 'ADMIN' || substr($resmatch[2], 0, 5) == "_DEL_") {
          if ((auth::$admin || auth::$uid == $resmatch[1]) && $value == "on")
            $priv = 1;
          else
            $priv = 0;
        } else {
          $priv = $value;
          if (!is_numeric($priv) || $priv < 0 || $priv > 10)
            $res[] = array($uid, -112);
          if ($resmatch[2][0] != "_" && !isset($uid_info['areavalue']["_DEL_" . $resmatch[2]])) {
            if ($resmatch[1] != 0)
              $arraypriv["{" . $resmatch[1] . "}_DEL_" . $resmatch[2]] = array($resmatch[1], "_DEL_" . $resmatch[2], 0);
            else
              $arraypriv["_DEL_" . $resmatch[2]] = array($resmatch[1], "_DEL_" . $resmatch[2], 0);
          }
        }

        $arraypriv[$area] = array($resmatch[1], $resmatch[2], $priv);
      }
    }

    foreach ($arraypriv as $area => $value) {
      if ($value[0] != 0)
        $tmp = "{" . $value[0] . "}";
      else
        $tmp = '';

      if (
        auth::$admin || auth::$uid == $value[0] ||
        ($value[1][0] != "_" &&
          (
            (isset($auth_priv[$tmp . "_ED_" . $value[1]]) && $value[2] <= $auth_priv[$tmp . "_ED_" . $value[1]]) ||
            (isset($auth_priv[$tmp . "_DEL_" . $value[1]]) && $auth_priv[$tmp . "_DEL_" . $value[1]] > 0))) ||
        (substr($value[1], 0, 4) == "_ED_" && isset($auth_priv[$tmp . "_DEL_" . substr($value[1], 4)]) &&
          $auth_priv[$tmp . "_DEL_" . substr($value[1], 4)])
      )
        $setpriv[$uid]['priv'][$area] = $value[2];
    }

    if (isset($uid_info['valid']) && isset($uid_info['prevvalid']) && auth::$userspace_admin) {
      if ($uid_info['prevvalid'] != $uid_info['valid'])
        $setpriv[$uid]['valid'] = $uid_info['valid'];

      if ($uid_info['valid'] == 1 && $uid_info['prevvalid'] == -2  && $validmode == 'CONFADMIN') {
        if (isset($uid_info['email']) && preg_match('/^\s*<?([\w\-\.\+]+@(?:[\w\-]+\.)*[\w\-]+)>?\s*$/', $uid_info['email']))
          $sendmail[$uid] = $uid_info['email'];
        else {
          $tmp_user = auth_getinfo($uid);
          $sendmail[$uid] = $tmp_user['email'];
        }
      }
    }

    if (auth::$userspace_admin) {
      if (isset($uid_info['email'])) {
        $setpriv[$uid]['email'] = $uid_info['email'];

        if ($setpriv[$uid]['email'] != '')
          if (preg_match('/^\s*<?([\w\-\.\+]+@(?:[\w\-]+\.)*[\w\-]+)>?\s*$/', $setpriv[$uid]['email'], $arrayres))
            $setpriv[$uid]['email'] = $arrayres[1];
          else {
            unset($setpriv[$uid]['email']);
            unset($sendmail[$uid]);
            $res[] = array($uid, -113);
          }
      }
    }

    if (isset($setpriv[$uid]['valid']) && !is_numeric($setpriv[$uid]['valid']))
      $res[] = array($uid, -114);
  }

  include_authmodule();

  $res2 = auth::$handle->setpriv($setpriv);
  if ($res2 != 0)
    return $res2;

  if (count($sendmail) != 0) {
    $additional_parameters = NULL;
    if (is_array($headers) && isset($headers['additional_parameters']))
      $additional_parameters = $headers['additional_parameters'];
    $headers = authedit::makeheaders($headers);

    foreach ($sendmail as $uid => $email) {
      if (!@mail($email, auth_encode_suject($subject), $message, $headers, $additional_parameters))
        $res[] = array($uid, -102);
    }
  }

  if (count($res) == 0)
    return 0;

  return $res;
}

function return_auth_deluser($expr)
{
  $uid = current($expr);
  next($expr);
  $uid = eval_list($uid);

  if ($uid < 0)
    return -105;

  if ($uid == 0 || !auth::$userspace_admin)
    return -107;

  include_authmodule();
  $res = auth::$handle->deluser($uid);

  return $res;
}

function return_auth_passwd($expr)
{
  global $config;

  $uid = current($expr);
  next($expr);
  $uid = eval_list($uid);

  $oldpassword = current($expr);
  next($expr);
  $oldpassword = eval_list($oldpassword);

  $newpassword = current($expr);
  next($expr);
  $newpassword = eval_list($newpassword);

  if (auth::$uid < 0) {
    if ($uid == 0 && $oldpassword == 'admin') {
      auth::$uid = 0;
      $authmethod = 'file';
    } else
      return -105;
  } else {
    if ($newpassword == '')
      return -106;

    if ($uid != auth::$uid && !auth::$admin && auth::$uid != auth::$userspace)
      return -107;

    $authmethod = return_auth_getmethod();
  }

  include_authmodule();

  $res = auth::$handle->passwd($uid, $oldpassword, $newpassword);

  if (($authmethod != 'file') && ($uid == 0)) {
    include_once($config['modulesdir'] . "auth/auth_file.php");
    if (!isset(auth::$filehandle))
      auth::$filehandle = new class_auth_file;

    $res = auth::$filehandle->passwd($uid, $oldpassword, $newpassword);
  }

  if ($res == 0 && $uid == 0 && $oldpassword == 'admin') {
    global $global_var;

    $secretkey = auth_getsecretkey();

    auth::$priv['ADMIN'] = 1;

    auth::$admin = 1;
    $global_var->auth_user = 'admin';
    $global_var->auth_uid = 0;
    $global_var->auth_nickname = 'admin';

    auth::$uid = 0;
    auth::$userspace = 0;

    auth_setcookie(0, "admin", 0, 0, $secretkey);
  }

  return $res;
}

function return_auth_loginnick($expr)
{
  $uid = current($expr);
  next($expr);
  $uid = eval_list($uid);

  $login = current($expr);
  next($expr);
  $login = eval_list($login);

  $nickname = current($expr);
  next($expr);
  $nickname = eval_list($nickname);

  if (!authedit::checkuser($login) || !authedit::checkuser($nickname))
    return -101;

  if (!auth::$userspace_admin)
    return -107;

  include_authmodule();

  $res = auth::$handle->loginnick($uid, $login, $nickname);

  return $res;
}

function return_auth_setprivuser($expr)
{
  if (auth::$uid < 0)
    return -103;

  $uid = current($expr);
  next($expr);
  $uid = (int)eval_list($uid);

  $reqpriv = current($expr);
  next($expr);
  $reqpriv = eval_list($reqpriv);

  authedit::set_area();

  $setpriv = array();

  $arraypriv = array();

  foreach ($reqpriv as $area => $value) {
    if (!isset(authedit::$area_keys[$area]))
      return -115;

    preg_match('/^(?:{([^}]+)})?(.*)/', $area, $res);
    if ($res[1] == '')
      $res[1] = 0;
    if ($area == 'ADMIN' || substr($res[2], 0, 5) == "_DEL_") {
      if ($value > 0)
        $priv = 1;
      else
        $priv = 0;
    } else {
      $priv = $value;
      if (!is_numeric($priv) || $priv < 0 || $priv > 10)
        return -112;
    }

    $arraypriv[$area] = array($res[1], $res[2], $priv);
  }

  foreach ($arraypriv as $area => $value) {
    if ($value[0] != 0)
      $tmp = "{" . $value[0] . "}";
    else
      $tmp = '';

    if (
      auth::$admin ||
      (auth::$userspace_admin && auth::$userspace == $value[0]) ||
      ($value[1][0] != "_" &&
        (
          (isset(auth::$priv[$tmp . "_ED_" . $value[1]]) && $value[2] <= auth::$priv[$tmp . "_ED_" . $value[1]]) ||
          (isset(auth::$priv[$tmp . "_DEL_" . $value[1]]) && auth::$priv[$tmp . "_DEL_" . $value[1]] > 0))) ||
      (substr($value[1], 0, 4) == "_ED_" && isset(auth::$priv[$tmp . "_DEL_" . substr($value[1], 4)]) &&
        auth::$priv[$tmp . "_DEL_" . substr($value[1], 4)])
    )
      $setpriv[$uid]['priv'][$area] = $value[2];
  }

  include_authmodule();

  $res = auth::$handle->setpriv($setpriv);

  return $res;
}

function return_auth_setkeypass($expr)
{
  global $global_var;

  $userspace = current($expr);
  next($expr);
  $userspace = eval_list($userspace);

  if (!is_int($userspace) && !ctype_digit($userspace))
    return -12;

  $userspace = (int)$userspace;

  $login = current($expr);
  next($expr);
  $login = eval_list($login);

  $subject = current($expr);
  next($expr);

  $message = current($expr);
  next($expr);
  $headers = current($expr);
  next($expr);

  $validkey = auth_mkrdstr(30);

  $global_var->auth_validkey = $validkey;


  if (isset($headers))
    $headers = eval_list($headers);
  else
    $headers = array();

  include_authmodule();

  $res = auth::$handle->setkeypass($userspace, $login, $validkey);
  if (!is_numeric($res)) {
    $additional_parameters = NULL;
    if (is_array($headers) && isset($headers['additional_parameters']))
      $additional_parameters = $headers['additional_parameters'];
    $headers = authedit::makeheaders($headers);

    global $global_var;

    $global_var->user_uid = $res[0];

    $subject = eval_list($subject);
    $message = eval_list($message);

    if (!@mail($res[1], auth_encode_suject($subject), $message, $headers, $additional_parameters)) {
      seterrormessage();
      return -102;
    }

    return 0;
  }

  return $res;
}

function return_auth_newmail($expr)
{
  global $global_var;

  if (auth::$uid < 0)
    return -103;

  if (return_auth_emailislogin())
    return -127;

  $email = current($expr);
  next($expr);
  $email = eval_list($email);

  $subject = current($expr);
  next($expr);

  $message = current($expr);
  next($expr);

  $headers = current($expr);
  next($expr);
  if (isset($headers))
    $headers = eval_list($headers);
  else
    $headers = array();

  $global_var->auth_email = rawurlencode($email);

  $secretkey = auth_getsecretkey();

  $validkey = hash('sha256', $secretkey . "#" . $email . "#" . auth::$uid, FALSE);
  $global_var->auth_validkey = $validkey;

  $subject = eval_list($subject);
  $message = eval_list($message);

  $additional_parameters = NULL;
  if (is_array($headers) && isset($headers['additional_parameters']))
    $additional_parameters = $headers['additional_parameters'];
  $headers = authedit::makeheaders($headers);
  if (!@mail($email, auth_encode_suject($subject), $message, $headers, $additional_parameters)) {
    seterrormessage();
    return -102;
  }
  return 0;
}

function return_auth_setnewmail($expr)
{
  if (auth::$uid < 0)
    return -103;

  $uid = current($expr);
  next($expr);
  $uid = eval_list($uid);

  $email = current($expr);
  next($expr);
  $email = eval_list($email);

  $key = current($expr);
  next($expr);
  $key = eval_list($key);

  $secretkey = auth_getsecretkey();

  $email = rawurldecode($email);
  if (hash('sha256', $secretkey . "#" . $email . "#" . $uid, FALSE) != $key || $uid != auth::$uid)
    return -6;

  $res = auth::$handle->setemail(auth::$uid, $email);

  return $res;
}

function return_auth_setconfvalid($expr)
{
  global $config;

  if (!isset(auth::$admin) || !auth::$admin)
    return -107;

  $valid = current($expr);
  next($expr);
  $valid = eval_list($valid);

  if ($valid != 'YES' && $valid != 'NO' && $valid != 'CONFIRM' && $valid != 'CONFADMIN')
    return -104;

  if ($valid == 'CONFADMIN' && $config['emailadmin'] == '')
    return -116;

  auth::$config['account']['valid'] = $valid;

  authedit::writeconfigfile();

  return 0;
}

function return_auth_validaccount($expr)
{
  global $config;

  $uid = current($expr);
  next($expr);
  $uid = eval_list($uid);

  if (!is_int($uid) && !ctype_digit($uid))
    return -12;

  $uid = (int)$uid;

  $key = current($expr);
  next($expr);
  $key = eval_list($key);

  $pass = current($expr);
  next($expr);
  $pass = eval_list($pass);

  include_authmodule();

  $validaccount = return_auth_validmode();
  if ($validaccount == "CONFADMIN" && $pass == '') {
    $validvalue = -2;
    $res = auth::$handle->validaccount($uid, $key, $pass, $validvalue);
    if (is_array($res) && $config['emailadmin'] != '') {
      $subject = current($expr);
      next($expr);
      $subject = eval_list($subject);

      $message = current($expr);
      next($expr);
      $message = eval_list($message);

      $headers = current($expr);
      next($expr);
      if (isset($headers))
        $headers = eval_list($headers);
      else
        $headers = array();

      $additional_parameters = NULL;
      if (is_array($headers) && isset($headers['additional_parameters']))
        $additional_parameters = $headers['additional_parameters'];
      $headers = authedit::makeheaders($headers);

      $infouserspace = auth_getinfo($res['userspace']);
      if (!is_array($infouserspace))
        return -126;

      if (!@mail($infouserspace['email'], auth_encode_suject($subject), $message, $headers, $additional_parameters)) {
        seterrormessage();
        return -102;
      }
      return 0;
    } else
      return $res;
  } else {
    $validvalue = 1;
    $res = auth::$handle->validaccount($uid, $key, $pass, $validvalue);

    if (is_array($res)) {
      global $global_var;

      auth::$uid = $uid;
      auth::$admin = ($res['uid'] == 0 || !empty($res['priv']["ADMIN"]));
      auth::$userspace_admin = auth::$admin || $res['uid'] == $res['userspace'];
      auth::$priv = $res['priv'];

      $global_var->auth_userspace = $res['userspace'];
      $global_var->auth_user = $res['login'];
      $global_var->auth_uid = $uid;
      $global_var->auth_email = $res['email'];
      $global_var->auth_nickname = $res['nickname'];

      auth_setcookie($res["userspace"], $res["login"], $uid, 0, auth_getsecretkey());

      return 0;
    } else
      return $res;
  }
}


function return_auth_updateprofile($expr)
{
  include_authmodule();

  $uid = current($expr);
  next($expr);
  $uid = eval_list($uid);

  $var = current($expr);
  next($expr);
  $var = eval_list($var);

  if (auth::$uid < 0)
    return -103;

  if ($uid == '')
    $uid = auth::$uid;

  if ((auth::$uid != $uid) && !auth::$admin) {
    $userinfo = auth_getinfo(auth::$uid);
    if (!auth::$userspace_admin || auth::$userspace != $userinfo['userspace'])
      return -107;
  }

  $profile_in = auth::$handle->getprofile($uid);
  $fields = authedit::readfields();
  $profile = array();
  foreach ($fields as $name => $value) {
    if (isset($var[$name]))
      $profile[$name] = $var[$name];
    else {
      if (isset($profile_in[$name]))
        $profile[$name] = $profile_in[$name];
      else
        $profile[$name] = '';
    }
    switch ($value["desc"]["type"]) {
      case "int":
        $profile[$name] = (int)$profile[$name];
      case "double":
        $profile[$name] = (float)$profile[$name];
    }
  }
  $res = auth::$handle->updateprofile($uid, $profile);

  if (auth::$uid == $uid) {
    auth::$profile = $profile;
  }

  return $res;
}

function auth_encode_suject($subject)
{
  if (function_exists("mb_internal_encoding")) {
    mb_internal_encoding("UTF-8");
    $coding = mb_detect_encoding($subject, array("ASCII", "UTF-8", "ISO-8859-1"));
  } else
    $coding = "ISO-8859-1";

  if ($coding != "ASCII")
    $subject = '=?' . $coding . '?B?' . base64_encode($subject) . '?=';

  return $subject;
}

function return_auth_readfields($expr)
{
  return authedit::readfields();
}

function authedit_return()
{
  return array(
    'auth_listfld', 'auth_list', 'auth_setpriv', 'auth_setprivuser', 'auth_newuser', 'auth_deluser',
    'auth_passwd', 'auth_newarea', 'auth_listuser',
    'auth_listuserfld', 'auth_nbrows', 'auth_listarea', 'auth_listareafld', 'auth_setarea',
    'auth_delarea', 'auth_setkeypass', 'auth_newmail', 'auth_setnewmail',
    'auth_validaccount', 'auth_validmode',
    'auth_setconfvalid', 'auth_updateprofile', 'auth_loginnick',
    'auth_readfields'
  );
}
