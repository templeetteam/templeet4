<?php
/*
 * Pascal COURTOIS
 */

function return_session_start()
{
  global $global_var;
  session_start();
  $global_var->_SESSION = &$_SESSION;
}

function session_return()
{
  return array('session_start');
}
