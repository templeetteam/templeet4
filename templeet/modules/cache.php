<?php
/*
 * This module offers caching functions
 *
 * ~includewithcache: same as include, but with caching enabled
 * ~uncache: remove a file from the cache
 */

function cachelast_index($array)
{
    return array_key_last($array);
}

function return_includewithcache($expr)
{
  global $config, $inccache_current_expiretime,
    $nestedinclude, $include_row, $includedir, $local_var, $global_var;

  $template = current($expr);
  next($expr);
  $template = eval_list($template);

  if ($nestedinclude > MAX_INCLUDE)
    throw new TempleetError("You called $template too much, must have a loop!\n");

  $i = 1;
  $include_row[$nestedinclude + 1] = array();
  while (list(, $val) = each($expr)) {
    $include_row[$nestedinclude + 1][$i] = eval_list($val);
    $i++;
  }

  $templatepath = pre_includepath($template);
  $nestedinclude++;

  if ($config['useincludecache']) {
    if ($i > 1) {
      $tmp = implode('�', $include_row[$nestedinclude]);
      $md5 = md5($tmp);
      $dirtmp = $config['includecachedir'] . $templatepath . '/' . substr($md5, 0, 2) . '/';
      $includecachefilename = $dirtmp . $md5;
    } else {
      $dirtmp = $config['includecachedir'] . $templatepath;
      $includecachefilename = $dirtmp . substr($templatepath, strrpos($templatepath, '/'));
    }

    if (@file_exists($includecachefilename)) {
      $template_modified = stat($templatepath);
      $includecache_modified = stat($includecachefilename);
      if (($template_modified[9] <= $includecache_modified[9]) &&
        ($includecache_modified[9] > time())
      ) {
        $txt = file_get_contents($includecachefilename);
        if ($txt === FALSE)
          throw new TempleetError("Can't open $includecachefilename!");
        $nestedinclude--;
        return $txt;
      }
    }
  }

  $local_var[$nestedinclude] = new vars();
  $include_dir_backup = $includedir;

  if ($config['includecache_expiretime_inherit'])
    $inccache_current_expiretime[$nestedinclude] = $inccache_current_expiretime[cachelast_index($inccache_current_expiretime)];
  else
    $inccache_current_expiretime[$nestedinclude] = $config['includecache_expiretime'];

  $txt = parseform($templatepath);
  if ($config['useincludecache']) {
    if (!@file_exists($includecachefilename))
      createdir($dirtmp);
    $value = '.' . rand();
    if (file_put_contents($includecachefilename . $value, $txt) === FALSE)
      throw new TempleetError("Can't open $includecachefilename$value for writing!\n");
    templeet_rename($includecachefilename . $value, $includecachefilename);
    touch($includecachefilename, time() + $inccache_current_expiretime[$nestedinclude]);
  }

  unset($local_var[$nestedinclude]);
  unset($inccache_current_expiretime[$nestedinclude]);
  $includedir = $include_dir_backup;
  $global_var->includedir = $includedir;

  $nestedinclude--;

  return $txt;
}

function cache_search_file($dir, $filemask, $difffile)
{
  global $config;
  if ($dh = opendir("./" . $config["pagecachedir"] . $dir)) {
    while (($file = readdir($dh)) !== false) {
      if (preg_match($filemask, $file) && $file != $difffile) {
        closedir($dh);
        return true;
      }
    }
    closedir($dh);
  }
  return false;
}

function cache_redo($file)
{
  global $config;

  $templeet = $config['dir_installed'] . "templeet.php";
  $filetxt = <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
</head>
<body>
<form name="redo" method="post" action="$templeet">
<input type="hidden" name="_redo" value="$file">
</form>
<script language="javascript">
document.redo.submit();
</script>
</body>
EOF;
  file_put_contents($config['pagecachedir'] . "./" . $file . ".html", $filetxt);
}

function cache_uncache_recursive($file)
{
  global $config;

  if ($config['pagecachedir'] == '') {
    if (is_dir("./" . $config['shadowdir'] . $file)) {
      if (preg_match("|^\.\/+" . $config['templatecachedir'] . "|", $file))
        return;
      $dir = opendir("./" . $config['shadowdir'] . $file);
      while ($filename = readdir($dir))
        if ($filename != '.' && $filename != '..')
          cache_uncache_recursive($file . '/' . $filename);
      closedir($dir);
      @rmdir("./" . $config['pagecachedir'] . $file);
      @rmdir("./" . $config['shadowdir'] . $file);
    } else {
      if (@file_exists("./" . $config['shadowdir'] . $file)) {
        if ((preg_match("/^((?:.*\/)?)([^\.]+\.(?:\w{3,4}|js)\.)(\w{2})$/", $file, $res) &&
            cache_search_file($res[1], "/^" . $res[2] . "\w{2}(?:\.html)?\$/", $res[2] . $res[3])) ||
          (preg_match("/^((?:.*\/)?)([^\.]+\.)(\w{2})(\.(?:\w{3,4}|js))$/", $file, $res) &&
            cache_search_file($res[1], "/^" . $res[2] . "\w{2}" . $res[4] . "(?:\.html)?\$/", $res[2] . $res[3] . $res[4]))
        ) {
          cache_redo($file);
          @unlink("./" . $config['pagecachedir'] . $file);
        } else {
          @unlink("./" . $config['pagecachedir'] . $file);
          @unlink("./" . $config['shadowdir'] . $file);
        }
      }
    }
  } else {
    if (is_dir("./" . $config['pagecachedir'] . $file)) {
      $dir = opendir("./" . $config['pagecachedir'] . $file);
      while ($filename = readdir($dir))
        if ($filename != '.' && $filename != '..')
          cache_uncache_recursive($file . '/' . $filename);
      closedir($dir);
      @rmdir("./" . $config['pagecachedir'] . $file);
    } else
      @unlink("./" . $config['pagecachedir'] . $file);
  }
}

function return_uncache($array)
{
  global $config;

  if (
    (compacturl($config['pagecachedir']) == '' ||
      compacturl($config['pagecachedir']) == "/")
    &&
    (compacturl($config['shadowdir']) == '' ||
      compacturl($config['shadowdir']) == '/')
  )
    throw new TempleetError("uncache: pagecachedir and shadowdir are both empty.");

  while (list(, $file) = each($array)) {
    $file = eval_list($file);
    cache_uncache_recursive($file);
  }
  return;
}

function cache_uncachemaskshadow($dir1, $dir2, $dirmask, $filemask)
{
  global $config;

  $handle = @opendir("./" . $config['shadowdir'] . "$dir1$dir2");
  if ($handle === FALSE)
    return;

  while ($a = readdir($handle)) {
    if ($a != '.' && $a != '..') {
      if (is_dir("./" . $config['shadowdir'] . "$dir1$dir2/$a")) {
        if (preg_match($dirmask, "$dir2/$a"))
          cache_uncachemaskshadow($dir1, "$dir2/$a", $dirmask, $filemask);
      } elseif (is_file("./" . $config['shadowdir'] . "$dir1$dir2/$a")) {
        if (preg_match($filemask, $a)) {
          @unlink("./$dir1$dir2/$a");
          @unlink("./" . $config['shadowdir'] . "$dir1$dir2/$a");
        }
      }
    }
  }
  closedir($handle);

  return;
}

function cache_uncachemasknoshadow($dir1, $dir2, $dirmask, $filemask)
{
  global $config;

  $handle = opendir("./" . $config['pagecachedir'] . "$dir1$dir2");
  while ($a = readdir($handle)) {
    if ($a != '.' && $a != '..') {
      if (is_dir("./" . $config['pagecachedir'] . "$dir1$dir2/$a")) {
        if (preg_match($dirmask, "$dir2/$a"))
          cache_uncachemasknoshadow($dir1, "$dir2/$a", $dirmask, $filemask);
      } elseif (is_file("./" . $config['pagecachedir'] . "$dir1$dir2/$a")) {
        if (preg_match($filemask, $a))
          @unlink("./" . $config['pagecachedir'] . "$dir1$dir2/$a");
      }
    }
  }
  closedir($handle);
}

function return_uncachemask($array)
{
  global $config;

  $dir = current($array);
  next($array);
  $dir = eval_list($dir);

  $dirmask = current($array);
  next($array);
  $dirmask = eval_list($dirmask);


  if ($dirmask == '')
    $dirmask = '/.*/';

  $filemask = current($array);
  next($array);
  $filemask = eval_list($filemask);

  if ($filemask == '')
    $filemask = '/.*/';

  if ($config['pagecachedir'] == '' && $config['shadowdir'] != '')
    cache_uncachemaskshadow($dir, '', $dirmask, $filemask);
  else
    cache_uncachemasknoshadow($dir, '', $dirmask, $filemask);
}

function cache_uncache_include_recursive($file)
{
  if (is_dir($file)) {
    $dir = opendir($file);
    while ($filename = readdir($dir))
      if ($filename != '.' && $filename != '..')
        cache_uncache_include_recursive($file . '/' . $filename);
    closedir($dir);
    @rmdir($file);
  } else
    @unlink($file);
}

function return_uncache_include($array)
{
  global $config, $current_dir, $nestedinclude;
  if ($config['useincludecache']) {
    $template = current($array);
    next($array);
    $template = eval_list($template);

    if ($template[0] == '/') {
      global $config;

      $templatepath = substr($config['templatedir'], 0, -1) . $template;

      $templatedir = substr($templatepath, 0, strrpos($templatepath, '/'));
      if ($templatedir != '')
        $templatedir .= '/';
    } else {
      $templatedir = substr($template, 0, strrpos($template, '/'));
      if ($templatedir != '')
        $templatedir .= '/';

      $templatepath = $current_dir[$nestedinclude] . $template;
    }

    $i = 1;
    // include_row is local, it's _normal_
    $include_row[$nestedinclude] = array();
    while (list(, $val) = each($array)) {
      $include_row[$nestedinclude][$i] = eval_list($val);
      $i++;
    }

    if ($i > 1) {
      $tmp = implode('�', $include_row[$nestedinclude]);
      $md5 = md5($tmp);
      $dirtmp = $config['includecachedir'] . $templatepath . '/' . substr($md5, 0, 2) . '/';
      $includecachefilename = $dirtmp . $md5;
    } else
      $includecachefilename = $config['includecachedir'] . $templatepath;

    $includecachefilename = compactdir($includecachefilename);
    if (@file_exists($includecachefilename))
      cache_uncache_include_recursive($includecachefilename);
  }
  return;
}

function return_set_includeexpiretime($array)
{
  global $inccache_current_expiretime;

  $val = current($array);
  next($array);
  $inccache_current_expiretime[cachelast_index($inccache_current_expiretime)] = eval_list($val);
}

function return_set_expiretime($array)
{
  global $cache_current_expiretime;

  $val = current($array);
  next($array);
  $cache_current_expiretime = eval_list($val);
}

function return_force_cache($expr)
{
  global $cache;
  $cache = 1;
}

function cache_return()
{
  return array(
    'includewithcache', 'uncache', 'uncachemask', 'set_includeexpiretime',
    'set_expiretime', 'uncache_include', 'force_cache'
  );
}
