<?php
/*
* COURTOIS Pascal
* lang module
* handles language functions
*
*/

class Messages
{
  public static array $list = array();
}

function return_setmessages($expr)
{
  Messages::$list[] = $expr[0];
  return;
}

class SkinMessages
{
  public static array $list = array();
}

function return_setskinmessages($expr)
{
  $lang = eval_list($expr[0]);
  SkinMessages::$list[$lang] = eval_list($expr[1]);
  return;
}

function return_d($expr)
{
  global $nesteddefunc, $defunc_argu;

  $param = array();
  $i = 1;
  while (list(, $val) = each($expr)) {
    $param[$i++] = eval_list($val);
  }

  if (!isset($param[1]))
    return "no messageid to display ";

  $nesteddefunc++;
  $defunc_argu[$nesteddefunc] = $param;

  foreach (Messages::$list as $value) {
    $res = eval_list($value);
    if ($res != '') {
      $nesteddefunc--;
      return $res;
    }
  }

  $nesteddefunc--;
  return "unknown message to display: " . $param[1] . ' ';
}

function return_dskin($expr)
{
  global $nesteddefunc, $defunc_argu, $cache, $config;
  $name = current($expr);
  next($expr);
  $name = eval_list($name);

  if (!isset($name) || !is_string($name))
    return "no messageid to display ";

  $param = current($expr);
  next($expr);
  if (isset($param))
    $param = eval_list($param);


  if ((is_countable(SkinMessages::$list) ? count(SkinMessages::$list) : 0) == 1) {
    reset(SkinMessages::$list);
    $lang = key(SkinMessages::$list);
    $messages = current(SkinMessages::$list);
    next(SkinMessages::$list);
    if (!isset($messages[$name]))
      return "unknown message to display: " . $name . ' ';

    $res = $messages[$name];
  }

  if ($cache) {
    $paramstring = '';
    if (isset($param) && is_array($param)) {
      while (list($key, $val) = each($param)) {
        if ($paramstring != '')
          $paramstring .= ",";
        $paramstring .= " \"" . addslashes($key) . "\" : \"" . addslashes($val) . '"';
      }
      $paramstring = ",{" . $paramstring . "}";
    }

    return "<script type=\"text/javascript\">\n//<![CDATA[\ndskin(\"$name\"$paramstring);\n//]]>\n</script>";
  } else {
    $lang = bestlang(array_keys(SkinMessages::$list), $config["default_language"]);
    $messages = SkinMessages::$list[$lang];
    if (!isset($messages[$name]))
      return "unknown message to display: " . $name . ' ';

    $res = $messages[$name];
  }

  if (isset($param) && is_array($param)) {
    while (list($key, $val) = each($param)) {
      $res = preg_replace("/" . addslashes($key) . "/", $val, $res);
    }
  }

  return $res;
}

function return_chooselang($expr)
{
  $preferedlang = current($expr);
  next($expr);
  $preferedlang = eval_list($preferedlang);

  $lang = current($expr);
  next($expr);
  $lang = eval_list($lang);

  if (!is_array($lang))
    $lang = array();

  $key = key($lang);
  $val = current($lang);
  next($lang);
  if (is_int($key) && is_string($val) && strlen($val) == 2)
    $lang = array_flip($lang);

  if (isset($lang[$preferedlang]))
    return $preferedlang;

  $default = current($expr);
  next($expr);
  if (isset($default))
    $default = eval_list($default);

  return bestlang($lang, $default);
}

function return_bestlang($expr)
{
  $lang = current($expr);
  next($expr);
  $lang = eval_list($lang);

  $default = current($expr);
  next($expr);
  if (isset($default))
    $default = eval_list($default);

  return bestlang($lang, $default);
}

function bestlang($lang, $default = NULL)
{
  if (!is_array($lang))
    $lang = array();

  $key = key($lang);
  $val = current($lang);
  next($lang);
  if (is_int($key) && is_string($val) && strlen($val) == 2)
    $lang = array_flip($lang);

  if (!isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
    $accepted = '';
  else
    $accepted = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

  $accepted = explode(',', $accepted);
  reset($accepted);
  while (list(, $key) = each($accepted)) {
    list($lg) = explode(";", $key);
    $lg = substr($lg, 0, 2);
    if (isset($lang[$lg]))
      return $lg;
  }

  if (!isset($default) || !isset($lang[$default])) {
      return array_key_first($lang);
  }

  return $default;
}

function return_includelang($expr)
{
  global $nestedinclude, $include_row, $current_dir, $global_var, $includedir, $lang_requested, $cache, $local_var;

  global $actual_template;

  $val = current($expr);
  next($expr);
  $template = eval_list($val);

  $val = current($expr);
  next($expr);
  if (isset($val))
    $default = eval_list($val);

  if ($nestedinclude > MAX_INCLUDE)
    throw new TempleetError("Too many nested includes, you must have a loop!\n");

  $i = 1;
  $include_row[$nestedinclude + 1] = array();
  while (list(, $val) = each($expr)) {
    $include_row[$nestedinclude + 1][$i] = eval_list($val);
    $i++;
  }

  $basedir = $current_dir[$nestedinclude] . "lang";
  $dir = @opendir($basedir);
  if (!$dir)
    throw new TempleetError("includelang can't open " . $basedir . "\n");

  $langs_avail = array();
  while ($filetmp = readdir($dir)) {
    if ($filetmp == '.' || $filetmp == '..') continue;

    if (is_file($basedir . "/" . $filetmp) && preg_match("|^${template}\.(..)\.tmpl$|", $filetmp, $match)) {
      $langs_avail[$match[1]] = 1;
    }
  }
  closedir($dir);
  $global_var->all_lang = $langs_avail;

  if (isset($lang_requested) && isset($langs_avail[$lang_requested]))
    $includelang = $lang_requested;
  else {
    if (!isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
      $accepted = '';
    else
      $accepted = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

    $accepted = explode(',', $accepted);

    reset($accepted);
    while (!isset($includelang) && list(, $key) = each($accepted)) {
      list($lg) = explode(";", $key);
      $lg = substr($lg, 0, 2);
      if (isset($langs_avail[$lg]))
        $includelang = $lg;
    }

    if (!isset($includelang) && isset($default) && isset($langs_avail[$default]))
      $includelang = $default;

    if (!isset($includelang) && isset($langs_avail["en"]))
      $includelang = "en";

    if (!isset($includelang))
      $includelang = key($langs_avail);

    if (!isset($includelang))
      throw new TempleetError("No language file available for $template in $basedir\n");

    if ($cache) {
      global $dirname, $filename, $extension, $config;
      $cache = 0;
      $file = $config['dir_installed'] . $dirname . $filename . "." . $extension . "." . $includelang;
      throw new TempleetRedirect($file);
    }
  }

  $templatepath = pre_includepath("lang/" . $template . ".${includelang}.tmpl");
  $nestedinclude++;
  $include_dir_backup = $includedir;
  $global_var->includelang = $includelang;

  $local_var[$nestedinclude] = new vars();

  $res = parseform($templatepath);

  unset($local_var[$nestedinclude]);

  $includedir = $include_dir_backup;
  $global_var->includedir = $includedir;

  $nestedinclude--;

  return $res;
}

function return_includeskinlang($expr)
{
  global $nestedinclude, $include_row, $current_dir, $template_lines, $global_var, $includedir, $lang_requested, $cache;

  if ($nestedinclude > MAX_INCLUDE)
    throw new TempleetError("Too many nested includes, you must have a loop!\n");

  $basedir = $current_dir[$nestedinclude] . "lang";

  $dir = @opendir($basedir);
  if (!$dir)
    throw new TempleetError("includeskinlang can't open " . $current_dir[$nestedinclude] . "lang\n");

  $langs_avail = array();
  while ($filetmp = readdir($dir)) {
    if ($filetmp == '.' || $filetmp == '..') continue;

    if (is_file($basedir . "/" . $filetmp) && preg_match("|^skin\.(..)\.tmpl$|", $filetmp, $match)) {
      $langs_avail[$match[1]] = 1;
    }
  }

  closedir($dir);

  if (isset($langs_avail[$lang_requested]))
    $langs_avail = array($lang_requested => 1);

  $template_lines_save = $template_lines;

  foreach (array_keys($langs_avail) as $lang) {
    $template_lines = 1;

    $templatepath = pre_includepath("lang/skin.${lang}.tmpl");
    $nestedinclude++;
    $include_dir_backup = $includedir;

    $res = parseform($templatepath);

    $includedir = $include_dir_backup;
    $global_var->includedir = $includedir;

    $nestedinclude--;
  }

  $template_lines = $template_lines_save;

  if ($cache) {
    $res = "<script type=\"text/javascript\">\n//<![CDATA[\nvar skinlang= {\n";
    $firstlang = TRUE;
    foreach (SkinMessages::$list as $lang => $messages) {
      if (!$firstlang)
        $res .= ",\n";
      else
        $firstlang = FALSE;
      $res .= "\"$lang\" : {\n";
      $firstmessage = TRUE;
      foreach ($messages as $code => $text) {
        if (!$firstmessage)
          $res .= ",\n";
        else
          $firstmessage = FALSE;

        $res .= "\"$code\" : \"$text\"";
      }

      $res .= "}";
    }
    $res .= "};\n//]]>\n</script>";

    return $res;
  }
  return '';
}

function lang_return()
{
  return array('setmessages', 'setskinmessages', 'd', 'dskin', 'bestlang', 'chooselang', 'includelang', 'includeskinlang');
}
