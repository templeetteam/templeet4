<?php
/*
 * This module offers ip handling
 *
 * Code taken from daCode 1.X
 *
 * ~getip() returns the real ip
 */

function return_getip($array)
{

  $ip = getserver('REMOTE_ADDR');
  $ip_tmp = array();
  $ip_tmp = explode(",", getserver('HTTP_X_FORWARDED_FOR'));

  while ($cur = current($ip_tmp)) {
    $cur = trim($cur);
    /*
     * Known proxies we can trust
     */
    if (
      substr($cur, 0, 8) == "192.168." ||
      substr($cur, 0, 3) == "10." ||
      substr($cur, 0, 4) == "127." ||
      $cur == "unknown"
    ) {
      next($ip_tmp);
      continue;
    }
    // We remove 172.16/12
    if (substr($cur, 0, 4) == "172." && $cur[6] == ".") {
      $sub_tmp = substr($cur, 4, 6);
      if ($sub_tmp <= 31 && $sub_tmp > 16) {
        next($ip_tmp);
        continue;
      }
    }

    if ($cur != '') {
      $ip = $cur;
    }
    next($ip_tmp);
  }

  return $ip;
}

function ip_return()
{
  return array('getip');
}
