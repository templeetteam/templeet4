<?php

/**
 * Credit: https://stackoverflow.com/questions/46492621/how-to-resolve-this-deprecated-function-each-php
 * Adds the depreciated each() function back into 7.2
 */
if (!function_exists('each')) {
  function each($arr)
  {
    $key = key($arr);
    $result = ($key === null) ? false : [$key, current($arr), 'key' => $key, 'value' => current($arr)];
    next($arr);
    return $result;
  }
}
