<?php
/*
 * This code has been developed by:
 *
 * Fabien Penso
 * Pascal Courtois
 *
 */

require('templeet/polyfill-php-obsolete.php');
require('templeet/serverconf.php');
require('templeet/config.php');

if (is_int($config['umask']))
  umask($config['umask']);

if (function_exists('date_default_timezone_set'))
  date_default_timezone_set($config['timezone']);

$cache = $config['usepagecache'];

// We include the debug functions if $config['debugMode'] is set
if (isset($config['debugMode']) && $config['debugMode'])
  include('debug.php');

// global variables for defunc
$nesteddefunc = 0;
$defunc_list = array();
$defunc_argu = array();
$func_var[0] = new vars();
$local_var[0] = new vars();

// Default status is 200
$http_status = 200;
// special case of error with status 200
$evalerror = 0;
// current cache expire time
$cache_current_expiretime = $config['cache_expiretime'];
$inccache_current_expiretime[0] = $config['includecache_expiretime'];

$statusstring_array = array(
  200 => '200 OK',
  301 => '301 Moved Permanently',
  302 => '302 Redirect',
  304 => '304 Not Modified',
  401 => '401 Unauthorized',
  403 => '403 Forbidden',
  404 => '404 Not Found',
  405 => '405 Method Not Allowed',
  406 => '406 Not Acceptable',
  410 => '410 Gone',
  500 => '500 Internal server error'
);

$auth_uid = -1;
setcookie('browser_lang', getserver('HTTP_ACCEPT_LANGUAGE'), 0, $config['dir_installed']);


function getfileinfo()
{
  /* set:
  $global_var['path']
  $global_var->template
  $global_var['templatedir']
  $global_var['lang']
  $global_var['filenamevars']
  $path
  $extension
  $relative_base

  returns:
     location (http status 302) or ''

*/
  global $global_var, $path, $extension, $extension_low, $relative_base, $config,
    $dirname, $templ_parentdir, $filecache, $filename, $lang_before, $lang_requested, $cache, $fetch_eval;

  $fetch_eval = 0;

  //BEGIN-GETPATH
  $pathfromquerystring = 0;
  $pathinfo = 0;
  if (isset($_SERVER['QUERY_STRING']))
    $querystring = $_SERVER['QUERY_STRING'];
  else
    $querystring = '';

  if (getpost('_redo') != '')
    $path = getpost('_redo');
  else
  if (isset($_SERVER['REDIRECT_STATUS']) && preg_match('/^40[34]$/', $_SERVER['REDIRECT_STATUS'])) { /* err404 */
    preg_match('/^' . preg_quote($config['dir_installed'], '/') . '\/?([^\?]*?)(?:\?(.*))?$/', $_SERVER['REQUEST_URI'], $res);
    $path = $res[1];
    if (isset($res[2])) $querystring = $res[2];
  } else
  if (isset($_SERVER['PATH_INFO']) && preg_match('/^\/([^\?]*?)$/', $_SERVER['PATH_INFO'], $res_PATH_INFO)) { /* pathinfo */
    $path = $res_PATH_INFO[1];
    $pathinfo = 1;
  } else
  if (!empty($_SERVER['REQUEST_URI']) && !preg_match('/^' . preg_quote($config['dir_installed'], '/') . '\/?(?:templeet|index)\.php/', $_SERVER['REQUEST_URI'])) {
    $regex_request_uri = '/^' . preg_quote($config['dir_installed'], '/') . '\/?([^\?]*)(\?|$)/';
    preg_match($regex_request_uri, $_SERVER['REQUEST_URI'], $res);
    $path = $res[1];
  } else
  if (isset($_SERVER['REQUEST_URI']) && preg_match('/^' . preg_quote($config['dir_installed'], '/') . '\/?(?:templeet|index)\.php$/', $_SERVER['REQUEST_URI'])) { /* templeet */
    $path = '';
  } else
  if (isset($_SERVER['QUERY_STRING']) && preg_match('/^(?:file=)?([^&=]*)((?:&.*)?)$/', $_SERVER['QUERY_STRING'], $res_QUERY_STRING)) { /* querystring */
    $path = $res_QUERY_STRING[1];
    $querystring = preg_replace('/^&/', '', $res_QUERY_STRING[2]);

    $pathfromquerystring = 1;
  } else {
    $path = '';
  }

  // if ($path=="index.php") $path='';

  //END-GETPATH

  if ($querystring != '') {
    parse_str($querystring, $GET_404);
    $_GET = array_merge($_GET, $GET_404);
  }

  $path = preg_replace('/^\/+/', '', compacturl(rawurldecode($path)));
  $global_var->path = $path;
  $lang_requested = '';
  $global_var->template = '';

  if (getserver('HTTP_ACCEPT_LANGUAGE') != '')
    $global_var->accept_language = explode(',', getserver('HTTP_ACCEPT_LANGUAGE'));
  else
    $global_var->accept_language = array();

  if (
    $pathinfo && $config['error404used'] && $config['usepagecache'] &&
    (!isset($_POST) || count($_POST) == 0) &&
    (!isset($_GET) || count($_GET) == 0)
  )
    return $config['dir_installed'] . $path;

  if ((isset($_POST) && count($_POST) != 0) || (isset($_GET) && count($_GET) != 0))
    $cache = 0;

  $l = strlen($path);
  if ($l != 0 && $path[$l - 1] != '/' && template_isdir($path)) {
    if ($pathinfo)
      return $config['dir_installed'] . 'templeet.php/' . $path . '/';
    return $config['dir_installed'] . $path . '/';
  }

  $lang_before = 0;
  $global_var->all_lang = array();

  if (parsehtml2template())
    return $config['dir_installed'] . $path . '/';

  if (!$pathfromquerystring)
    $relative_base = preg_replace('/[^\/]+/', '..', $dirname);
  else
    $relative_base = '';

  if ($pathinfo)
    $relative_base .= '../';

  if ($config['force_lang_after_ext'])
    $lang_before = 0;

  return '';
  /*getfileinfo */
}

function setfilecache()
{

  // This function is called twice: once before reading cache if it exists,
  // once AFTER evaluation but before writing cache

  global $filecache, $dirname, $filename, $lang_before, $lang_requested, $global_var, $extension;

  $filecache = $dirname . $filename;
  $tmp = $global_var->filenamevars;
  array_shift($tmp);
  $vars = implode(',', $tmp);
  if ($vars != '')
    $filecache .= ',' . $vars;
  if ($lang_before && $lang_requested != '')
    $filecache .= '.' . $lang_requested;
  if ($extension != '')
    $filecache .= '.' . $extension;
  if (!$lang_before && $lang_requested != '')
    $filecache .= '.' . $lang_requested;
}

function parsehtml2template()
{

  global $path, $config, $global_var, $dirname, $extension, $extension_low, $templ_parentdir, $lang_requested;

  $global_var->template = '';

  $tmp = strrpos($path, '/');
  if (!$tmp) {
    $tmp = 0;
    $dirname = '';
    splitfile($path);
  } else {
    $dirname = substr($path, 0, $tmp + 1);
    splitfile(substr($path, $tmp + 1));
  }

  if (($extension == "tmpl") || preg_match("|^(?:[^/]+/)*\.ht|", $path))
    http_error(403);

  if (($extension=="php"))
	  http_error(404);

  reset($config['html2template_array']);
  while (list($reg, $temp) = each($config['html2template_array'])) {
    if (preg_match($reg, $path)) {
      $tmp = preg_replace($reg, $temp, $path);
      if (@template_fileexists($tmp)) {
        $global_var->template = $config['templatedir'] . $tmp;
        break;
      }
    }
  }

  if (!isset($global_var->template) || $global_var->template == '') {
    global $indexfile;

    $l = strlen($path);
    $indexfile = ($l == 0 || $path[$l - 1] == '/');
    $templ_parentdir = 0;
    $global_var->template = resolve_recursive_template($dirname);
    $global_var->lang = $lang_requested;
    if ($global_var->template != '')
      $global_var->template = $config['templatedir'] . $global_var->template;
    elseif ($global_var->template == '' && !$indexfile && $extension == '') {
      $indexfile = 1;
      if (resolve_recursive_template($dirname) != '')
        return 1;

      global $evalerror;
      $evalerror = 1;
      $global_var->errorLog = "Not found";
      http_error(404);
    } else {
      global $evalerror;
      $evalerror = 1;
      $global_var->errorLog = "Not found";
      http_error(404);
    }
  }

  if ($global_var->template != '') {
    if (isset($config['extensions'][$extension]))
      $global_var->HTTP_Content_type = $config['extensions'][$extension];
    elseif (isset($config['extensions'][$extension_low]))
      $global_var->HTTP_Content_type = $config['extensions'][$extension_low];
  }

  if (isset($global_var->lang) && $global_var->lang != '') {
    $global_var->dotlang = "." . $global_var->lang;
    if (isset($config["locales"])) {
      if (isset($config["locales"][$global_var->lang]))
        setlocale(LC_ALL, $config["locales"][$global_var->lang]);
      elseif (isset($config['default_language']) && isset($config["locales"][$config['default_language']]))
        setlocale(LC_ALL, $config["locales"][$config['default_language']]);
    }
  } else
    $global_var->dotlang = '';

  return 0;
}

function resolve_recursive_template($dir)
{
  global $config, $templ_parentdir, $indexfile, $filename;

  if ($templ_parentdir)
    $recurs = '#';
  else
    $recurs = '';

  if ($indexfile) {
    reset($config['indexfile']);
    while (list(, $index) = each($config['indexfile'])) {
      splitfile($index);
      if ($res = test_template($dir, $recurs)) return $res;
    }
  } else
	    if ($res = test_template($dir, $recurs)) return $res;

  if ($templ_parentdir) {
    if ($dir != '') {
      $newdir = dirname($dir) . '/';
      if ($newdir == './')
        $newdir = '';
      return resolve_recursive_template($newdir);
    }
  } else {
    $templ_parentdir = 1;
    return resolve_recursive_template($dir);
  }
  return '';
}

function escreg($reg)
{
  return addcslashes($reg, '/.()*?+');
}

function test_template($dir, $recurs)
{
  global $global_var, $config, $filename, $extension, $extension_low, $lang_before, $lang_requested;

  if ($extension) {
    if ($recurs != '')
      $recurspat = '#';
    else
      $recurspat = '#?';

    if ($lang_requested == '') {
      reset($global_var->accept_language);
      while (list(, $lang_tmp) = each($global_var->accept_language)) {
        $lang_tmp = substr(trim($lang_tmp), 0, 2);

        if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $extension . '.' . $lang_tmp)) {
          $lang_requested = $lang_tmp;
          findlanguages($dir, "^$recurspat" . escreg("$filename.$extension.") . '(..)$');
          return $filename_tmpl;
        }

        if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $lang_tmp . '.' . $extension)) {
          $lang_requested = $lang_tmp;
          $lang_before = 1;
          findlanguages($dir, "^$recurspat" . escreg("$filename.") . "(..)\.$extension$");
          return $filename_tmpl;
        }

        if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $extension_low . '.' . $lang_tmp)) {
          $lang_requested = $lang_tmp;
          findlanguages($dir, "^$recurspat" . escreg("$filename.$extension_low.") . '(..)$');
          return $filename_tmpl;
        }

        if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $lang_tmp . '.' . $extension_low)) {
          $lang_requested = $lang_tmp;
          $lang_before = 1;
          findlanguages($dir, "^$recurspat" . escreg("$filename.") . "(..)\.$extension_low$");
          return $filename_tmpl;
        }
      }

      if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $extension . '.' . $config['default_language'])) {
        $lang_requested = $config['default_language'];
        findlanguages($dir, "^$recurspat" . escreg("$filename.$extension.") . '(..)$');
        return $filename_tmpl;
      }

      if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $config['default_language'] . '.' . $extension)) {
        $lang_requested = $config['default_language'];
        $lang_before = 1;
        findlanguages($dir, "^$recurspat" . escreg("$filename.") . "(..)\.$extension$");
        return $filename_tmpl;
      }

      if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $extension_low . '.' . $config['default_language'])) {
        $lang_requested = $config['default_language'];
        findlanguages($dir, "^$recurspat" . escreg("$filename.$extension_low.") . '(..)$');
        return $filename_tmpl;
      }

      if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $config['default_language'] . '.' . $extension_low)) {
        $lang_requested = $config['default_language'];
        $lang_before = 1;
        findlanguages($dir, "^$recurspat" . escreg("$filename.") . "(..)\.$extension_low$");
        return $filename_tmpl;
      }
    } else {
      if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $extension . '.' . $lang_requested)) {
        findlanguages($dir, "^$recurspat" . escreg("$filename.$extension.") . '(..)$');
        return $filename_tmpl;
      }

      if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $lang_requested . '.' . $extension)) {
        $lang_before = 1;
        findlanguages($dir, "^$recurspat" . escreg("$filename.") . "(..)\.$extension$");
        return $filename_tmpl;
      }

      if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $extension_low . '.' . $lang_requested)) {
        findlanguages($dir, "^$recurspat" . escreg("$filename.$extension_low.") . '(..)$');
        return $filename_tmpl;
      }

      if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $lang_requested . '.' . $extension_low)) {
        $lang_before = 1;
        findlanguages($dir, "^$recurspat" . escreg("$filename.") . "(..)\.$extension_low$");
        return $filename_tmpl;
      }
    }

    if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $extension)) {
      return $filename_tmpl;
    }

    if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename . '.' . $extension_low)) {
      return $filename_tmpl;
    }


    if (@template_fileexists($filename_tmpl = $dir . $recurs . "\$${extension}.tmpl"))
      return $filename_tmpl;

    if (@template_fileexists($filename_tmpl = $dir . $recurs . "\$${extension_low}.tmpl"))
      return $filename_tmpl;
  } else {
    if (@template_fileexists($filename_tmpl = $dir . $recurs . $filename)) {
      return $filename_tmpl;
    }

    if ($recurs != '' && @template_fileexists($filename_tmpl = $dir . $recurs . '#' . $filename)) {
      return $filename_tmpl;
    }
  }

  if (@template_fileexists($filename_tmpl = $dir . $recurs . "\$\$.tmpl"))
    return $filename_tmpl;

  return '';
}

function compacturl($dir)
{

  if ($dir == '') return $dir;
  $tmp = explode('/', $dir);
  $realdir = array();

  $i = $k = 0;
  if ($tmp[$i] == '')
    $realdir[$k++] = $tmp[$i++];

  for (; $i < count($tmp); $i++) {
    $tok = $tmp[$i];

    if ($tok == '..') {
      if ($k == 0 || $realdir[$k - 1] == '..')
        $realdir[$k++] = $tok;
      unset($realdir[--$k]);
    } elseif ($tok != '' && $tok != '.')
      $realdir[$k++] = $tok;
  }
  if ($tok == '') $realdir[$k] = $tok;
  if ($k == 1 && $realdir[0] == '') $realdir[1] = '';

  return join('/', $realdir);
}

function removedirinstalled($dir)
{
  global $config;

  if (isset($config['dir_installed']) && $config['dir_installed'] != '') {
    if (!strncmp(
      $config['dir_installed'],
      $dir,
      strlen($config['dir_installed'])
    )) {
      $dir = substr($dir, strlen($config['dir_installed']));
    }
  }

  return $dir;
}


function createdir($dir = '', $mode = -1)
{

  if ($mode < 0)
    return @mkdir($dir, 0777, TRUE);
  else
    return @mkdir($dir, $mode, TRUE);
}

function parsetemplate($template)
{
  global $path, $http_status, $config,
    $statusstring_array,
    $current_dir, $nestedinclude, $cache, $filecache;

  setfilecache();

  if (!$template)
    http_error(404);

  // We display the cache if it exists!
  if ($cache) {
    if (@file_exists($config['pagecachedir'] . $filecache)) {
      $template_modified = stat($template);
      $cache_modified = stat($config['pagecachedir'] . $filecache);
      if (($template_modified['mtime'] <= $cache_modified['ctime']) &&
        ($cache_modified['mtime'] > time())
      ) {
        if ($config['uselastmodified']) {
          if (getserver('HTTP_IF_MODIFIED_SINCE')) {
            $time = strtotime(getserver('HTTP_IF_MODIFIED_SINCE'));
            if ($time >= $cache_modified['ctime']) {
              sendhttpstatus(304);
              templeet_exit();
            }
          }
          header('Last-Modified: ' .
            gmdate('D, d M Y H:i:s', $cache_modified['ctime']) . ' GMT');
        }

        if (!$fd = @fopen($config['pagecachedir'] . $filecache, 'rb'))
          throw new TempleetError("Can't open " . $config['pagecachedir'] . "$filecache for inclusion of cache!\n");

        $filesize = filesize($config['pagecachedir'] . $filecache);
        if ($filesize == 0)
          $txt = '';
        else {
          $txt = fread($fd, $filesize);
          fclose($fd);
        }

        header('Expires: ' . gmdate('D, d M Y H:i:s', $cache_modified['mtime']) . ' GMT');

        $maxage = $cache_modified['mtime'] - time();
        if ($maxage < 0)
          $maxage = 0;
        header('Cache-Control: max-age=' . $maxage);

        sendhttpstatus($http_status);

        return $txt;
      }
    }
  }

  if (!function_exists('parseform'))
    include('templeet/core.php');

  $template = compactdir($template);
  // global variables for the includes
  $nestedinclude = 0;
  $include_row = array();

  $current_dir = array(substr($template, 0, strrpos($template, '/')));
  if ($current_dir[0] != '') $current_dir[0] .= '/';

  try {
    $txt = parseform($template);

    sendhttpstatus($http_status);
  } catch (TempleetError $e) {
    global $global_var, $evalerror, $actual_template;

    if ($evalerror) {
      debug_tbacktrace();
      return "error evaluating error file<br />\n" .
        "first error was:" . $global_var->errorLog . "<br />\n" .
        "second error was:" . $e->getMessage();
    }

    $global_var->errorLog = $e->getMessage();

    $evalerror = 1;
    sethttpstatus(500);
    $global_var->HTTP_Content_type = "text/html";

    $global_var->error_template = $actual_template;
    $global_var->errorLine = $e->line;
    if (file_exists('template/error/eval.html'))
      return parsetemplate('template/error/eval.html');
    else
      return $global_var->errorLog;
  }

  return $txt;
}


function splitfile($file)
{
  global $path, $filename, $global_var, $extension, $extension_low, $lang_requested;

  if (preg_match('/(.*\.min)\.js$/', $file, $res)) {
    $file = $res[1];
    $extension = 'js';
    $lang_requested = '';
  } else if (preg_match('/(.*)\.(\w{3,4}|rt|js)\.(\w{2})$/', $file, $res)) {
    $file = $res[1];
    $extension = $res[2];
    $lang_requested = $res[3];
  } else if (preg_match('/(.*)\.(\w{2})\.(\w{3,4}|rt|js)$/', $file, $res)) {
    $file = $res[1];
    $lang_requested = $res[2];
    $extension = $res[3];
  } else if (preg_match('/(.*)\.(\w{3,4}|rt|js)$/', $file, $res)) {
    $file = $res[1];
    $extension = $res[2];
    $lang_requested = '';
  } else if (preg_match('/(.*)\.(\w{2})$/', $file, $res)) {
    $file = $res[1];
    $lang_requested = $res[2];
  } else {
    $extension = '';
    $lang_requested = '';
  }

  $extension_low = strtolower($extension);

  $global_var->filenamevars = explode(',', $file);
  $filename = $global_var->filenamevars[0];
}

function sethttpstatus($status)
{
  global $http_status;

  $http_status = $status;
}

function sendhttpstatus($http_status)
{
  global $config, $statusstring_array;

  if (isset($statusstring_array[$http_status]))
    $statusstring = $statusstring_array[$http_status];
  else
    $statusstring = $http_status . " no status string for status " . $http_status;

  if ($config['cgi_header'])
    header('Status: ' . $statusstring);
  else
    header($statusstring, true, $http_status);
}

function http_error($code)
{
  global $http_status, $config, $path, $cache, $global_var;

  $http_status = $code;
  $cache = 0;
  $global_var->HTTP_Content_type = 'text/html';

  if (
    isset($config['errortemplate_array'][$code]) &&
    file_exists($config['errortemplate_array'][$code])
  )
    $tmp = parsetemplate($config['errortemplate_array'][$code]);
  else
    $tmp = "http_error: error template not found, code: $code ";

  setheaders($tmp);
  echo $tmp;

  templeet_exit();
}

function unmagicquote($var)
{
  if (!is_array($var))
    return stripslashes($var);
  else {
    $res = array();
    while (list($a, $b) = each($var))
      $res[$a] = unmagicquote($b);
    return $res;
  }
}

function getget($name, $default = '')
{
  $output = '';

  if (isset($_GET[$name]))
    $output = $_GET[$name];
  else
    $output = $default;

  if (get_magic_quotes_gpc())
    $output = unmagicquote($output);

  return $output;
}

function getpost($name, $default = '')
{
  $output = '';

  if (isset($_POST[$name]))
    $output = $_POST[$name];
  else
    $output = $default;

  if (get_magic_quotes_gpc())
    $output = unmagicquote($output);

  return $output;
}

function getrequest($name, $default = '')
{
  $output = '';

  if (isset($_REQUEST[$name]))
    $output = $_REQUEST[$name];
  else
    $output = $default;

  if (get_magic_quotes_gpc())
    $output = unmagicquote($output);

  return $output;
}

function getserver($name)
{
  if (isset($_SERVER[$name]))
    return $_SERVER[$name];
}


function getenvironment($name)
{
  if (isset($_ENV[$name]))
    return $_ENV[$name];
}

function getcookie($name)
{
  if (isset($_COOKIE[$name]))
    return $_COOKIE[$name];
}

function templeet_chmod($filename, $mode)
{
  if (function_exists('chmod'))
    @chmod($filename, $mode);

  return TRUE;
}

function templeet_rename($oldname, $newname)
{
  global $config;
  if ($config['windows'])
    @unlink($newname);

  return @rename($oldname, $newname);
}
