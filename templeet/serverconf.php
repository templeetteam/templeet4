<?php
/*
 * Configuration file for Templeet
 *
 */

$config['snapshotid'] = '201606171145';

/*
	 * This file is initialised by the installer
	 * DO NOT MODIFY IT
	 */

/*
	 * Note: if you use databases please modify the $config['sqlconfig'] array
	 *	 in config.php
	 */
$config['sqlconfig'] = array(
  '*' => array('type' => 'mysql', 'host' => 'localhost', 'database' => 'database', 'login' => 'user', 'password' => 'pass', 'charset' => 'UTF8')
);

// Set to 1 when errordocument 404 is used
$config['error404used'] = 0;

// Set to 1 when FallbackResource is used
$config['fallbackused'] = 1;

// Don't forget the last '/'!
$config['pagecachedir'] = 'templeet/cache/pages/';

// Don't forget the last '/'!
$config['lockdir'] = 'templeet/cache/lock/';

// Use 1 to activate cache!
$config['usepagecache'] = 1;

// Use 1 to expire!
$config['usepageexpire'] = 1;

// expire cache dir
$config['expiredir'] = 'templeet/cache/expire/';

// max exec expire
$config['maxexecexpire'] = 1.5;

// max cycle expire
$config['maxcycleexpire'] = 30;

// expire password
$config['expirepassword'] = "set expire password here 1296118918";

// Shadow cache dir
$config['shadowdir'] = 'templeet/cache/shadow/';

// Don't forget the last '/'!
$config['includecachedir'] = 'templeet/cache/includes/';

// Use 1 to activate include cache!
$config['useincludecache'] = 1;

// Pages cache expiration time. Default is one hour.
$config['cache_expiretime'] = 3600;

// Includes cache expiration time. Default is one hour.
$config['includecache_expiretime'] = 3600;
$config['includecache_expiretime_inherit'] = 1;

// Don't forget the last / !
$config['templatecachedir'] = 'templeet/cache/templates/';

// Use 1 to activate template cache!
$config['usetemplatecache'] = 1;

// Dir for modules - Don't forget the last '/'!
$config['modulesdir'] = 'templeet/modules/';

// Is templatedir protected with a deny all in .htaccess or
// apache config ?
$config['templatedirprotected'] = 1;

// Associate the url which is called with the template you want to use.
// Use preg()
// See http://www.php.net/manual/en/ref.pcre.php for informations
//
// This will allow you to have url like index.php?/dir1/dir2/ to be using
// template/dir1.tmpl as template:
// '/^([^\/]+)\/.+\.html$/'=>'\1.tmpl'
//
// This will allow you to have url like index.php?/dir1/dir2/ to be using
// template/dir1/dir2/index.tmpl as template (be careful with dir like
// '../':
// '/^(.+)\.html$/'=>'\1.tmpl'

$config['html2template_array'] = array(
  //			  '/^([^\/]+)\.html$/'=>'\1.tmpl'
);

$config['errortemplate_array'] = array(
	404 => 'template/error/404.html',
	403 => 'template/error/403.html'
	);

// regexp matching function names which makes a page built with a template
// not cacheable
$config['nocachefunc'] = "/^(?:dont_cache|getauth)$/";

// Templeet has a multi-language support similar to multiviews in Apache,
// do set the default language in case the client doesn't give any.
$config['default_language'] = 'en';

// base url for links
// Don't forget to edit $config['site_url'] with your own URI!
$config['site_url'] = 'http://192.168.33.10';
// auth cookie domain
$config['auth_domain'] = '';

// protocol used
$config['protocol'] = 'http:';

// Uncomment to generate absolute links
// $config['base_path'] = '/templeet4/templeet.php?';

$config['pathinfoaccepted'] = 1;

// directory where templeet is installed in the web treepath
$config['dir_installed'] = '/templeet4/';

// Use 1 to set Last-Modified header when using cache to save bandwidth
// Will work only if cache is activated.
$config['uselastmodified'] = 1;

// CGI - Set it to 1 if you get Internal Server Error (500)
$config['cgi_header'] = 1;

// server default charset
$config['servercharset'] = '';

// Debug mode. Set it to 1 if you want to use the debug functions
$config['debugMode'] = 0;

// Detect the f*cking os
$config['windows'] = 0;

// admin address
$config['emailadmin'] = '';

// force lang after extension in page cache
$config['force_lang_after_ext'] = 1;

// Error reporting. Set it to 0 if you want parsing errors to never
// be displayed.
$config['errorReporting'] = 1;

// Location where you did put your template. Don't forget the last '/' !
// This is used only in case you don't specify a template into the array.
$config['templatedir'] = 'template/';

// Location where you did put your personnal dictionaries for spellchecking.
// Don't forget the last '/' !
$config['spell_dict_dir'] = 'template/';

// default file for directory
$config['indexfile'] = array('index.html');

// umask
$config['umask'] = 022;

// timezone
$config['timezone'] = "UTC";

// default authentication form when getauth param 3 missing
$config['default_auth_form'] = "auth/authform.html";

// default no privilege page when getauth param 4 missing
$config['default_no_priv'] = "auth/nopriv.html";

// mime types
$config['extensions'] = array(
  'html' => 'text/html',
  'smi' => 'application/smil',
  'txt' => 'text/plain',
  'svg' => 'image/svg+xml',
  'css' => 'text/css',
  'htm' => 'text/html',
  'rt' => 'text/plain',
  'rss' => 'text/xml',
  'xml' => 'text/xml',
  'xhtml' => 'application/xml+xhtml',
  'js' => 'text/javascript',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',
  'png' => 'image/png',
  'webp' => 'image/webp',
  'gif' => 'image/gif'
);

// This is used for cuthtml
//
$config['htmltags'] = array(
  'b'  => '',
  'i'  => '',
  'br' => '/',
  'p'  => '',
  'img' => array('src' => '', '/' => '', 'alt' => ''),
  'u'  => '',
  'em'  => '',
  'tt'  => '',
  'strong'  => '',
  'blockquote' => '',
  's' => '',
  'pre' => '',
  'ul' => '',
  'li' => '',
  'abbr' => array('title' => '', 'lang' => ''),
  'sub' => '',
  'sup' => '',
  'a' => array('href' => '')
);

$config['reffunc'] = array(
  'preg_match' => "12&",
  'preg_match_all' => "12&",
  'preg_filter' => "1234&",
  'preg_replace' => "1234&",
  'exec' => "1&&",
  'array_pop' => "&",
  'array_push' => "&",
  'array_shift' => "&",
  'array_unshift' => "&",
  'array_splice' => "&",
  'array_multisort' => '&&&&&&&&&&&&&&&&&&&&&&&&&&&&',
  'arsort' => "&",
  'asort' => "&",
  'sort' => "&",
  'ksort' => "&",
  'krsort' => "&",
  'rsort' => "&",
  'shuffle' => "&",
  'natcasesort' => "&",
  'natsort' => "&",
  'next' => "&",
  'prev' => "&",
  'reset' => "&",
  'each' => "&",
  'end' => "&",
  'fsockopen' => "12&&5"
);

// Don't touch!
$config['function2module'] = array(
  'getip' => 'ip',
  'ls' => 'ls',
  'ls_fld' => 'ls',
  'rdf' => 'rdf',
  'rdf_fld' => 'rdf',
  'relative_base' => 'url',
  'relative_templeet' => 'url',
  'imagepath' => 'url',
  'absolute_imagepath' => 'url',
  'relative_templeet_script' => 'url',
  'absolute_templeet_script' => 'url',
  'absolute_templeet' => 'url',
  'getauth' => 'auth',
  'auth_signout' => 'auth',
  'getpriv' => 'auth',
  'auth_getmethod' => 'auth',
  'getprofile' => 'auth',
  'auth_editpriv' => 'auth',
  'setauth' => 'auth',
  'auth_getbyuid' => 'auth',
  'auth_userspace' => 'auth',
  'auth_emailislogin' => 'auth',
  'timestamp2day' => 'time',
  'timestamp2hhmmss' => 'time',
  'timestamp2month' => 'time',
  'timestamp2year' => 'time',
  'format_timestamp' => 'time',
  'format_unixtimestamp' => 'time',
  'timestamp2hhmm' => 'time',
  'now' => 'time',
  'passthru' => 'exec',
  'list' => 'list',
  'fld' => 'list',
  'gfld' => 'list',
  'listtotaltime' => 'list',
  'list_prev' => 'list',
  'list_next' => 'list',
  'getfirst' => 'list',
  'list_lastid' => 'list',
  'dbfetcharray' => 'list',
  'list_querycolumns' => 'list',
  'list_escapestring' => 'list',
  'makeselect' => 'html',
  'makeselectnum' => 'html',
  'makehidden' => 'html',
  'setmessages' => 'lang',
  'setskinmessages' => 'lang',
  'd' => 'lang',
  'dskin' => 'lang',
  'bestlang' => 'lang',
  'chooselang' => 'lang',
  'includelang' => 'lang',
  'includeskinlang' => 'lang',
  'array_list' => 'array',
  'array_fld' => 'array',
  'lines' => 'lines',
  'lines_fld' => 'lines',
  'print_r' => 'debug',
  'php_info' => 'debug',
  'thumb' => 'thumb',
  'includewithcache' => 'cache',
  'uncache' => 'cache',
  'uncachemask' => 'cache',
  'set_includeexpiretime' => 'cache',
  'set_expiretime' => 'cache',
  'uncache_include' => 'cache',
  'force_cache' => 'cache',
  'tpl_replace' => 'regex',
  'preg' => 'regex',
  'image_resize' => 'image',
  'image2stringpng' => 'image',
  'image2stringgif' => 'image',
  'image2stringjpg' => 'image',
  'defunc' => 'defunc',
  'sparam' => 'defunc',
  'sparam_num' => 'defunc',
  'mapparam' => 'defunc',
  'function_exists' => 'defunc',
  'anonfunc' => 'defunc',
  'evalanonfunc' => 'defunc',
  'ls_tree' => 'ls_tree',
  'ls_tree_fld' => 'ls_tree',
  'tpl_strftime' => 'locales',
  'cuthtml' => 'cuthtml',
  'session_start' => 'session',
  'xhtmlize' => 'xhtmlize',
  'strictize' => 'xhtmlize',
  'auth_listfld' => 'authedit',
  'auth_list' => 'authedit',
  'auth_setpriv' => 'authedit',
  'auth_setprivuser' => 'authedit',
  'auth_newuser' => 'authedit',
  'auth_deluser' => 'authedit',
  'auth_passwd' => 'authedit',
  'auth_newarea' => 'authedit',
  'auth_listuser' => 'authedit',
  'auth_listuserfld' => 'authedit',
  'auth_nbrows' => 'authedit',
  'auth_listarea' => 'authedit',
  'auth_listareafld' => 'authedit',
  'auth_setarea' => 'authedit',
  'auth_delarea' => 'authedit',
  'auth_setkeypass' => 'authedit',
  'auth_newmail' => 'authedit',
  'auth_setnewmail' => 'authedit',
  'auth_validaccount' => 'authedit',
  'auth_validmode' => 'authedit',
  'auth_setconfvalid' => 'authedit',
  'auth_updateprofile' => 'authedit',
  'auth_loginnick' => 'authedit',
  'auth_readfields' => 'authedit',
  'get_filename' => 'filename',
  'get_extension' => 'filename',
  'get_filenameext' => 'filename',
  'get_filenamevar' => 'filename',
  'map_filenamevar' => 'filename',
  'get_dirname' => 'filename',
  'register_css' => 'register',
  'pop_css' => 'register',
  'register_beginscript' => 'register',
  'pop_beginscript' => 'register',
  'register_endscript' => 'register',
  'pop_endscript' => 'register',
  'filearray_list' => 'filearray',
  'filearray_fld' => 'filearray',
  'filearray_replace' => 'filearray',
  'filearray_delete' => 'filearray',
  'filearray_addkey' => 'filearray',
  'auth_swapfield' => 'authtools',
  'auth_modfield' => 'authtools',
  'auth_addfield' => 'authtools',
  'auth_delfield' => 'authtools',
  'auth_methodparam' => 'authtools',
  'authtools_getparam' => 'authtools',
  'auth_setmethod' => 'authtools',
  'list_tree' => 'list_tree',
  'sendfile' => 'filesystem',
  'writefile' => 'filesystem',
  'deletemask' => 'filesystem',
  'deletepath' => 'filesystem',
  'copy' => 'filesystem',
  'getwebfile' => 'filesystem',
  'arraysplit' => 'arraysplit',
  'getget' => 'getglobals',
  'getpost' => 'getglobals',
  'getrequest' => 'getglobals',
  'getserver' => 'getglobals',
  'getenvironnement' => 'getglobals',
  'getcookie' => 'getglobals',
  'getfile' => 'getglobals',
  'bwor' => 'binoperator',
  'bwand' => 'binoperator',
  'bwxor' => 'binoperator',
  'bwnot' => 'binoperator',
  'bwlshift' => 'binoperator',
  'bwrshift' => 'binoperator',
  'ffa_add' => 'fieldfileaccess',
  'ffa_setkey' => 'fieldfileaccess',
  'ffa_delkey' => 'fieldfileaccess',
  'ffa_readkey' => 'fieldfileaccess',
  'ffa_getkey' => 'fieldfileaccess',
  'ffa_filearray_replace' => 'fieldfileaccess',
  'ffa_writefile' => 'fieldfileaccess',
  'ffa_readfile' => 'fieldfileaccess',
  'ffa_replace' => 'fieldfileaccess',
  'tools_mysql_param' => 'list/tools_mysql',
  'configurator' => 'lib/configurator',
  'html2templatearray' => 'lib/configurator',
  'tools_postgresql_param' => 'list/tools_postgresql',
  'get'=>'obsolete',
  'set'=>'obsolete',
  'getl'=>'obsolete',
  'setl'=>'obsolete',
  'gets'=>'obsolete',
  'sets'=>'obsolete',
  'call'=>'obsolete',
  'mapparaml'=>'obsolete'
);

$config["locales"] = array(
  'en' => 'en_US.UTF-8',
);
