<?php

function getmicrotime($time = 0)
{
  if (!$time) {
    $time = microtime();
  }
  list($usec, $sec) = explode(" ", $time);
  return ((float)$usec + (float)$sec);
}

if (!isset($_POST["key"])) {
  print "error|badkey";
  exit();
}

$start = getmicrotime();
chdir("..");
$registry = @unserialize(substr(file_get_contents("templeet/registry.php"), 8));
if (
  !is_array($registry) ||
  !isset($registry["removepackage"]) ||
  !isset($registry["removepackage"]["key"])
) {
  print "error|badregistry";
  exit();
}


if ($_POST["key"] != $registry["removepackage"]["key"]) {
  print "error|badkey";
  exit();
}

$package = $registry["removepackage"]["package"];

if (
  !isset($registry['dists'][$package]) ||
  !isset($registry['dists'][$package]["files"])
) {
  print "error|badregistry";
  exit();
}

if (!empty($_POST["cleanup"])) {
  unset($registry['dists'][$package]);
  unset($registry['removepackage']);
  file_put_contents("templeet/registry.php", "<?php\n\000\n" . serialize($registry) . "\n?>");

  print "ok|";
  exit();
}

$unlinked = $_POST["unlinked"];

$maxexec = ini_get("max_execution_time");
if ($maxexec < 1)
  $maxexec = 0.5;
if ($maxexec > 2)
  $maxexec = 2;

$maxfiles = 10000;

$i = 0;
$trace = '';
$checkmodules = FALSE;
while (getmicrotime() - $start < $maxexec && $i < $maxfiles && list($filename, $md5) = each($registry['dists'][$package]["files"])) {
  if (@file_exists($filename)) {
    if (preg_match("%^./templeet/modules/%", $filename))
      $checkmodules = TRUE;

    $tmp = @file_get_contents($filename);
    if ($tmp === FALSE)
      $tmp = '';
    if (md5($tmp) != $md5) {
      @file_put_contents($filename . ".old", $tmp);
      $trace .= "<br />\n$filename => $filename.old";
    } else {
      $trace .= "<br />\n$filename " . $unlinked;
    }
    @unlink($filename);

    if (preg_match("%^./template/.+?/.+%", $filename))
      @rmdir(dirname($filename));
  }
  unset($registry['dists'][$package]["files"][$filename]);

  $i++;
}

$end = (count($registry['dists'][$package]["files"]) == 0 ? "1" : "0");

file_put_contents("templeet/registry.php", "<?php\n\000\n" . serialize($registry) . "\n?>");


print "ok|$end|" . ($checkmodules ? "1" : "0") . "|" . $trace;
