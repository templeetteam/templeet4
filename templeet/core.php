<?php
/*
 * This code has been developed by:
 *
 * Pascal Courtois
 *
 */

define('MAX_INCLUDE', 100);

class tf    // f(p[0],p[1],...)
{
  public $f;
  public $p;
  function __construct($function, $parameters, $line = NULL)
  {
    $this->f = $function;
    $this->p = $parameters;
    if (!empty($line))
      $this->l = $line;
  }
};

class sc  // c::p
{
  public $c;
  public $p;
  function __construct($class, $property)
  {
    $this->c = $class;
    $this->p = $property;
  }
};

class ob  // o[0]->o[1]->o[2]
{
  public $o;
  function __construct($objects)
  {
    $this->o = $objects;
  }
};

class vr // v[p[0]][p[1]]...
{
  public $v;
  public $p;
  function __construct($variable)
  {
    $this->v = $variable;
  }
};

class TempleetWriteCacheError extends Exception
{
  public $message;
};

function parseform($template)
{
  global $contents, $template_length, $config,
    $nestedinclude, $templatecache, $time_start,
    $global_var, $template_nocache, $path, $extension, $extension_low,
    $cache_current_expiretime, $cache, $lockfile, $filecache, $dirname,
    $filename, $lang_before, $lang_requested, $includedir, $actual_template;

  $templatecacheread = 0;
  $templateistext = 1;

  $tmp = substr($template, strpos($template, '/') + 1);
  if (strrpos($tmp, '/'))
    $includedir = compacturl(substr($tmp, 0, strrpos($tmp, '/') + 1));
  else
    $includedir = '';

  $global_var->includedir = $includedir;

  if ($nestedinclude == 0)
    setfilecache();

  if ($config['usetemplatecache']) {
    if (!@file_exists($config['templatecachedir'] . $template) || !is_file($config['templatecachedir'] . $template)) {
      $dir = $config['templatecachedir'] .
        substr($template, 0, strrpos($template, '/'));
      createdir($dir);
    } else {
      $template_modified = @stat($template);
      $templatecache_modified = stat($config['templatecachedir'] . $template);
      if ($template_modified[9] <= $templatecache_modified[9] && is_array($template_modified)) {
        if (
          !isset($templatecache[$template]) ||
          !$templatecache[$template]['template']
        ) {
          if (!$fd = @fopen($config['templatecachedir'] . $template, 'rb')) {
            throw new TempleetError("Can't open " . $config['templatecachedir'] . "$template!\n");
          } else {

            $filesize = filesize($config['templatecachedir'] . $template);
            if ($filesize != 0) {
              $tree = fread($fd, $filesize);
              fclose($fd);
              $templatecache[$template] = unserialize($tree);
              //debug_print_r("readtemplatecache:",$templatecache[$template]);
            }
          }
        }
	//From the cache or already in-mem, but no need to write tmpl again
        if (isset($templatecache[$template]['template']))
          $templatecacheread = 1;
      }
    }
  }

  if (
    !isset($templatecache[$template]['template']) ||
    !$templatecache[$template]['template']
  ) {
    if (!file_exists($template) || !$fp = @fopen($template, 'rb'))
      throw new TempleetError("Can't open ${template} \nCWD:" . getcwd() . "\n");

    $filesize = filesize($template);
    if ($filesize == 0)
      $contents = '';
    else {
      $contents = fread($fp, $filesize);
      fclose($fp);
    }

    if ($config['windows']) {
      if (isset($config['extensions'][$extension]) && preg_match('|text/|', $config['extensions'][$extension]))
        $contents = str_replace("\r", '', $contents);
    }

    $template_length = strlen($contents);

    global $http_status, $evalerror;
    if ($nestedinclude == 0 && $http_status == 200 && $evalerror == 0) {
      $templateistext = 0;
      if ($extension == 'tmpl')
        $cache = 0;
      elseif (
        (isset($config['extensions'][$extension]) &&
          (preg_match('|text/|', $config['extensions'][$extension]) ||
            $config['extensions'][$extension] == "application/smil" ||
            $config['extensions'][$extension] == "image/svg+xml")) ||
        preg_match('/^(?:\xef\xbb\xbf)?\s*~[\w\-]{1,40}\(/', $contents)
      )
        $templateistext = 1;
    }

    if ($nestedinclude != 0 || $templateistext) {
      if (!function_exists('fetch_template'))
        include('templeet/fetch.php');

      $actual_template_backup = $actual_template;

      $global_var->actual_template = $actual_template = $template;

      if ($templateistext && strlen($contents) >= 3 && $contents[0] == chr(239) && $contents[1] == chr(187) && $contents[2] == chr(191)) {
        /* utf8 with BOM */
        $contents = substr($contents, 3);
        $template_length -= 3;
        $global_var->actual_charset = "utf-8";
      }
      $temp = fetch_template();

      if (isset($debug_template) && $debug_template) {
        header("Content-type: text/plain");
        debug_print_r("template:", $temp);
        templeet_exit();
      }
      $global_var->actual_template = $actual_template = $actual_template_backup;

      $templatecache[$template] = array(
        'nocache' => $template_nocache,
        'template' => $temp
      );
    }
  }

  if ($config['usetemplatecache'] && $templatecacheread == 0 && ($nestedinclude != 0 || $templateistext)) {
    $value = '.' . rand();
    if (!$fp = @fopen($config['templatecachedir'] . $template . $value, 'wb')) {
      throw new TempleetError("Can't open " . $config['templatecachedir'] . "$template$value for writing!\n");
    } else {
      $tmp = serialize($templatecache[$template]);
      $tmpsize = strlen($tmp);
      if (@fwrite($fp, $tmp) == $tmpsize) {
        fclose($fp);
        if ($config['windows'])
          @unlink($config['templatecachedir'] . $template);

        @rename($config['templatecachedir'] . $template . $value, $config['templatecachedir'] . $template);
      } else {
        fclose($fp);
        @unlink($config['templatecachedir'] . $template . $value);
      }
    }
  }

  if ($nestedinclude == 0 && $cache && $templatecache[$template]['nocache'] == 0) {

    /*
     * We wait until the lock ends, another process is running...
     */

    $lockfile = $config['lockdir'] . crc32($filecache);
    createdir($config['lockdir']);

    $max_exec_time = ini_get("max_execution_time") - 2;
    if ($max_exec_time < 1) {
      $max_exec_time = 1;
    }
    do {
      clearstatcache();
      $fdCache = @fopen($config['pagecachedir'] . $filecache, 'rb');
      if ($fdCache !== FALSE) {
        $fdShadow = @fopen($config['shadowdir'] . $filecache, 'rb');
        if ($fdShadow !== FALSE) {
          $cache_modified = fstat($fdShadow);

          if ($cache_modified['mtime'] <= time()) { // cache expired
            fclose($fdShadow);
            @unlink($config['pagecachedir'] . $filecache);
          } else {
            $txt = fread($fdCache, filesize($config['pagecachedir'] . $filecache));
            fclose($fdCache);

            if ($config['uselastmodified']) {
              if (getserver('HTTP_IF_MODIFIED_SINCE')) {
                $time = strtotime(getserver('HTTP_IF_MODIFIED_SINCE'));
                sendhttpstatus(304);
                templeet_exit();
              }

              header('Last-Modified: ' .
                gmdate('D, d M Y H:i:s', $cache_modified['ctime']) . ' GMT');
            }

            header('Expires: ' .
              gmdate('D, d M Y H:i:s', $cache_modified['mtime']) . ' GMT');

            $maxage = $cache_modified['mtime'] - time();
            if ($maxage < 0)
              $maxage = 0;
            header('Cache-Control: max-age=' . $maxage);

            sethttpstatus(200);

            return $txt;
          }
        }
      }

      if (getmicrotime() - $time_start > $max_exec_time) {
        // timeout
        header("Location: " . $config['dir_installed'] . $path);
        sendhttpstatus(302);
        if ($config['fallbackused']) // workaround for Apache FallbackResource bug
        {
          header("Content-Length: 1");
          print ' ';
        }
        templeet_exit();
      }

      $fplock = @fopen($lockfile, 'r');
      if ($fplock !== FALSE) {
        $lockInfo  = fstat($fplock);
        fclose($fplock);
        if ($lockInfo[9] + $max_exec_time < time()) {
          @unlink($lockfile);
        }
      }

      $fplock = @fopen($lockfile, 'x');

      if ($fplock === FALSE) {
        usleep(500000);
      }
    } while ($fplock === FALSE);
  }

  if ($nestedinclude != 0 || $templateistext) {
    try {
      if ($template != 'template/error/syntax.html') {
        $actual_template_backup = $actual_template;
        $global_var->actual_template = $actual_template = $template;

        $txt = eval_list($templatecache[$template]['template']);

        $global_var->actual_template = $actual_template = $actual_template_backup;
      } else
        $txt = eval_list($templatecache[$template]['template']);
    } catch (TempleetError $e) {
      if (isset($fplock) && $fplock) {
        fclose($fplock);
        @unlink($lockfile);
      }

      throw $e;
    }
  } else {
    $txt = $contents;
  }

  if ($nestedinclude == 0) {
    setfilecache();

    @unlink($filecache . ".html");
  }

  if ($nestedinclude == 0 && $config['servercharset'] != '' && $templateistext && function_exists("iconv")) {
    $charset = '';
    if (isset($global_var->HTTP_charset))
      $charset = $global_var->HTTP_charset;
    elseif (preg_match('|<meta\s+http-equiv="Content-Type"\s+content="text/html;\s+charset=(.*?)"\s+/>|', $txt, $res))
      $charset = $res[1];

    if ($charset != '' && strtolower($charset) != strtolower($config['servercharset'])) {
      $txt = preg_replace(
        '|(<meta\s+http-equiv="Content-Type"\s+content="text/html;\s+charset=).*?("\s+/>)|',
        "$1" . $config['servercharset'] . "$2",
        $txt
      );
      $txt = @iconv($charset, $config['servercharset'] . "//TRANSLIT", $txt);

      if (isset($global_var->HTTP_charset))
        $global_var->HTTP_charset = $config['servercharset'];
    }
  }

  if ($nestedinclude == 0 && is_dir("templeet/output/")) {
    if ($dh = opendir("templeet/output/")) {
      $wrappers = array();
      while (($file = readdir($dh)) !== false) {
        if (preg_match("/\.php$/", $file))
          $wrappers[] = $file;
      }
      closedir($dh);
      sort($wrappers);

      foreach ($wrappers as $wrapper) {
        include("templeet/output/" . $wrapper);
      }
    }
  }

  if ($nestedinclude == 0 && $cache) {
    try {
      $tmp_value = '.' . rand();
      $expire = time() + $cache_current_expiretime;

      header('Expires: ' .
        gmdate('D, d M Y H:i:s', $expire) . ' GMT');
      header('Cache-Control: max-age=' . $cache_current_expiretime);

      $i = 0;
      do {
        createdir($config['pagecachedir'] . substr($path, 0, strrpos($filecache, '/')));
        $fp = @fopen($config['pagecachedir'] . $filecache . $tmp_value, 'wb');
        $i++;
      } while ($i < 5 && !$fp);
      if (!$fp)
        throw new TempleetWriteCacheError("Open error for page cache writing:" . $config['pagecachedir'] . "$filecache$tmp_value\n");


      if (@fwrite($fp, $txt) === FALSE) {
        @fclose($fp);
        @unlink($config['pagecachedir'] . $filecache . $tmp_value);
        throw new TempleetWriteCacheError("Write error for " . $config['pagecachedir'] . "$filecache$tmp_value\n");
      } else {
        @fclose($fp);

        if ($config['windows'])   // race condition on windows :-((((
          @unlink($config['pagecachedir'] . $filecache);

        @rename(
          $config['pagecachedir'] . $filecache . $tmp_value,
          $config['pagecachedir'] . $filecache
        );

        $stat_filecache = @stat($config['pagecachedir'] . $filecache);
        chmod($config['pagecachedir'] . $filecache, 0);
        chmod($config['pagecachedir'] . $filecache, $stat_filecache["mode"]);
      }

      if ($config['uselastmodified'])
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');

      createdir($config['shadowdir'] . substr($filecache, 0, strrpos($filecache, '/')));
      $fp = @fopen($config['shadowdir'] . $filecache, 'wb');
      @fclose($fp);
      @touch($config['shadowdir'] . $filecache, $expire);

      if (isset($config['usepageexpire']) && !empty($config['usepageexpire'])) {
        $hexpire = sprintf("%08x", $expire);
        $dir = substr($hexpire, 0, 2) . "/" . substr($hexpire, 2, 2) . "/" . substr($hexpire, 4, 2) . "/" . substr($hexpire, 6, 2);

        createdir($config['expiredir'] . $dir);
        file_put_contents($config['expiredir'] . $dir . "/" . random(20) . ".txt", $filecache);
      }

      global $lang_before, $dirname, $filename;

      if (
        $lang_requested != '' && ($config['force_lang_after_ext'] || !$lang_before) &&
        is_array($global_var->all_lang) && (is_countable($global_var->all_lang) ? count($global_var->all_lang) : 0) > 1  && $config['pagecachedir'] == ''
      ) {
        $tmplang = $global_var->all_lang;
        $key = key($tmplang);
        $val = current($tmplang);
        next($tmplang);
        if (is_int($key) && is_string($val) && strlen($val) == 2)
          $tmplang = array_flip($tmplang);

        reset($tmplang);
        while (list($lang,) = each($tmplang)) {
          if ($lang != $lang_requested) {
            $file = $dirname . $filename . "." . $extension . "." . $lang;
            if (!@file_exists($file)) {
              $templeet = $config['dir_installed'] . "templeet.php";
              $filetxt = <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
</head>
<body>
<form name="redo" method="post" action="$templeet">
<input type="hidden" name="_redo" value="$file">
</form>
<script language="javascript">
document.redo.submit();
</script>
</body>
EOF;

              if ($fp = @fopen($config['pagecachedir'] . $file . '.html', 'wb')) {
                @fwrite($fp, $filetxt);
                @fclose($fp);

                $fp = @fopen($config['shadowdir'] . $file . '.html', 'wb');
                @fclose($fp);
              }
            }
          }
        }
      }
    } catch (TempleetWriteCacheError $e) {
      error_log($e->message);
    }
  }

  if (isset($fplock) && $fplock) {
    fclose($fplock);
    @unlink($lockfile);
  }

  return $txt;
}

function pre_includepath($template)
{
  global $current_dir, $nestedinclude;

  if ($template[0] == '/') {
    global $config;

    $templatepath = substr($config['templatedir'], 0, -1) . $template;

    $templatedir = substr($templatepath, 0, strrpos($templatepath, '/'));
    if ($templatedir != '')
      $templatedir .= '/';

    $current_dir[$nestedinclude + 1] = $templatedir;
  } else {
    $templatedir = substr($template, 0, strrpos($template, '/'));
    if ($templatedir != '')
      $templatedir .= '/';

    $current_dir[$nestedinclude + 1] = $current_dir[$nestedinclude] . $templatedir;
    $templatepath = $current_dir[$nestedinclude] . $template;
  }

  return compactdir($templatepath);
}

function eval_list($expr)
{
  if (!is_object($expr))
    return $expr;

  switch (get_class($expr)) {
    case 'tf':
      global $defunc_list;
      $p = strtolower($expr->f);

      if (isset($defunc_list[$p])) {
        global $defunc_argu, $nesteddefunc, $func_var;
        $i = 1;
        $param = array();
        if (is_array($expr->p)) {
          foreach ($expr->p as $val) {
            $param[$i] = eval_list($val);
            $i++;
          }
        } elseif (isset($expr->p))
          $param[$i] = eval_list($expr->p);

        $nesteddefunc++;
        $defunc_argu[$nesteddefunc] = $param;

        $func_var[$nesteddefunc] = new vars();

        try {
          $tmp = eval_list($defunc_list[$p]);
        } catch (ReturnValue $e) {
          $tmp = $e->value;
        }
        unset($func_var[$nesteddefunc]);
        unset($defunc_argu[$nesteddefunc]);
        $nesteddefunc--;
        return $tmp;
      } elseif (function_exists('return_' . $p)) {
        $p = 'return_' . $p;
        try {
          if (is_array($expr->p))
            $res = $p($expr->p);
          else
            $res = $p(array($expr->p));
        } catch (TempleetError $e) {
          if (!isset($e->line) && isset($expr->l))
            $e->line = $expr->l;
          throw $e;
        }
        return $res;
      } else {
        global $config;
        if (
          isset($config['function2module'][$p]) &&
          @file_exists($config['modulesdir'] .
            $config['function2module'][$p] . '.php') &&
          !function_exists($config['function2module'][$p] . "_return")
        ) {
          include($config['modulesdir'] .
            $config['function2module'][$p] . '.php');
          return eval_list($expr);
        } elseif (function_exists($p)) {
          if (isset($config['reffunc'][$p])) {
            if (is_array($expr->p))
              $paramin = $expr->p;
            else
              $paramin = array($expr->p);
            $param = array();
            $paramout = array();
            $i = 0;

            foreach ($paramin as $val) {
              if (isset($config['reffunc'][$p][$i]) && $config['reffunc'][$p][$i] == "&") {
                $paramout[$i] = eval_list($val);
                $param[] = &$paramout[$i];
              } else
                $param[] = eval_list($val);
              $i++;
            }

            $res = call_user_func_array($p, $param);
            $i = 0;
            foreach ($paramin as $val) {
              if (isset($config['reffunc'][$p][$i]) && $config['reffunc'][$p][$i] == "&" && is_object($val))
                return_setref(array($val, $paramout[$i]));
              $i++;
            }
            return $res;
          } else {
            $param = array();
            if (is_array($expr->p))
              foreach ($expr->p as $val) {
                $param[] = eval_list($val);
              }
            else
              $param[0] = eval_list($expr->p);

            try {
              $res = call_user_func_array($p, $param);
            } catch (TempleetError $e) {
              if (!isset($e->line) && isset($expr->l))
                $e->line = $expr->l;
              throw $e;
            }

            return $res;
          }
        } else {
          if (is_string($expr->f)) {
            $tmpexpr = new vr($expr->f);
            $val = eval_list($tmpexpr);
            if (
              is_string($val) &&
              ($val = strtolower($val)) &&
              (isset($defunc_list[$val]) ||
                isset($config['function2module'][$val]) ||
                function_exists($val))
            ) {
              $tmptf = new tf($val, $expr->p, $expr->l ?? NULL);
              return eval_list($tmptf);
            } else {
              if (isset($config['function2module'][$p]) && function_exists($config['function2module'][$p] . "_return"))
                throw new TempleetError("Couldn't find function $p in module " . $config['function2module'][$p]);
              throw new TempleetError("The function $p doesn't exist!\n", $expr->l ?? NULL);
            }
          } else
            throw new TempleetError("The function $p doesn't exist!\n", $expr->l ?? NULL);
        }
      }


    case 'vr':
      $var = &getcontextvar($expr);
      $tmp = getvrvalue($var, $expr);
      return $tmp;

    case 'ob':
      $var = &getcontextvar($expr);
      return getobvalue($var, $expr);

    case 'sc':
      $var = getscvalue($expr);
      return $var;

    default:
      return $expr;
  }
}

function return_return($expr)
{
  $val = current($expr);
  next($expr);
  throw new ReturnValue(eval_list($val));
}

function gettfvalue(&$var, $object)
{
  if (!is_callable(array($var, $object->f)))
    throw new TempleetError("Method " . $object->f . " doesn't exist!\n", $object->l ?? NULL);

  $param = array();
  if (is_array($object->p)) {
    foreach ($object->p as $val)
      $param[] = eval_list($val);
  } else
    $param[] = eval_list($object->p);

  $res = call_user_func_array(array($var, $object->f), $param);
  return $res;
}

function getvrvalue(&$var, $object)
{
  $name = $object->v;
  if ($name[0] == "#" || $name[0] == '$')
    $name = substr($name, 1);
  if (!isset($var->$name))
    return NULL;

  $var = &$var->$name;
  $p = $object->p;
  if (is_array($p)) {
    while (count($p)) {
      $elt = eval_list(array_shift($p));
      if (!is_array($var) || !isset($var[$elt]))
        return NULL;
      $var = &$var[$elt];
    }
  }
  return $var;
}

function getobvalue($var, $object)
{
  foreach ($object->o as $elt) {
    if (is_string($elt)) {
      $name = $elt;
      if ($name[0] == "#" || $name[0] == '$')
        $name = substr($name, 1);

      if (!isset($var->$name))
        return;

      $var = $var->$name;
    } else {
      if (!is_object($elt))
        throw new TempleetError("getobvalue: elt not an object:<pre>" . print_r($elt, TRUE) . "</pre>\n");

      switch (get_class($elt)) {
        case "vr":
          $var = getvrvalue($var, $elt);
          break;

        case "tf":
          $var = gettfvalue($var, $elt);
          break;

        case "sc":
          $var = getscvalue($elt);
          break;

        default:
          throw new TempleetError("getobvalue: unknown class:<pre>" . print_r($elt, TRUE) . "</pre>\n");
      }
    }
  }

  return $var;
}

function getscvalue($object)
{
  if (is_string($object->p) || (is_object($object) && get_class($object->p) == "vr")) {
    global $global_var;

    $var = $global_var;
    $val = &getref($var, $object, TRUE);
    return $val;
  }

  switch (get_class($object->p)) {
    case "tf":
      if (!is_callable(array($object->c, $object->p->f)))
        throw new TempleetError("The function " . $object->c . "::" . $object->p->f . " doesn't exist!\n");
      if (is_array($object->p->p)) {
        $param = array();
        foreach ($object->p->p as $val) {
          $param[] = eval_list($val);
        }
      } else
        $param = array(eval_list($object->p->p));

      return call_user_func_array(array($object->c, $object->p->f), $param);
      break;

    default:
      throw new TempleetError("getscvalue: unknown object class: " . get_class($object->p) . "!\n");
  }
}

function &getcontextvar($object)
{
  if (!is_object($object))
    throw new TempleetError("Only variables can be set (getcontextvar)\n");

  if (get_class($object) == "sc") {
    global $global_var;
    return $global_var;
  }
  if (get_class($object) == "ob") {
    if (is_string($object->o[0]))
      $name = $object->o[0];
    else
      return getcontextvar($object->o[0]);
  } elseif (get_class($object) == "vr") {
    $name = $object->v;
  } else {
    throw new TempleetError("Only variables can be set\n");
  }

  switch ($name[0]) {
    case '#':
      global $nesteddefunc, $func_var;
      return $func_var[$nesteddefunc];

    case '$':
      global $nestedinclude, $local_var;
      return $local_var[$nestedinclude];

    default:
      global $global_var;
      return $global_var;
  }
}


function return_include($expr)
{
  global $nestedinclude, $include_row, $global_var, $includedir, $local_var;

  $val = current($expr);
  next($expr);
  $template = eval_list($val);

  if ($nestedinclude > MAX_INCLUDE) {
    throw new TempleetError("Too many nested includes, you must have a loop!\n");
  }

  $i = 1;
  $include_row[$nestedinclude + 1] = array();
  while (list(, $val) = each($expr)) {
    $include_row[$nestedinclude + 1][$i] = eval_list($val);
    $i++;
  }
  $templatepath = pre_includepath($template);
  $nestedinclude++;
  $include_dir_backup = $includedir;
  $local_var[$nestedinclude] = new vars();
  $res = parseform($templatepath);
  unset($local_var[$nestedinclude]);
  $includedir = $include_dir_backup;
  $global_var->includedir = $includedir;

  $nestedinclude--;

  return $res;
}

function return_includeonce($expr)
{
  global $nestedinclude, $global_var, $local_var, $templatecache, $includedir;

  $val = current($expr);
  next($expr);
  $template = eval_list($val);

  if ($nestedinclude > MAX_INCLUDE) {
    throw new TempleetError("Too many nested includes, you must have a loop!\n");
  }

  $templatepath = pre_includepath($template);

  if (!isset($templatecache[$templatepath])) {
    $include_dir_backup = $includedir;
    $nestedinclude++;
    $local_var[$nestedinclude] = new vars();
    $res = parseform($templatepath);
    unset($local_var[$nestedinclude]);
    $nestedinclude--;
    $includedir = $include_dir_backup;
    $global_var->includedir = $includedir;
  } else
    $res = '';

  return $res;
}

function return_eval($expr)
{
  global $contents, $template_length, $fetch_eval;

  $val = current($expr);
  next($expr);
  $contents = eval_list($val);
  $template_length = strlen($contents);

  $fetch_eval = 1;
  if (!function_exists('fetch_template')) {
    include('templeet/fetch.php');
  }

  $res = eval_list(fetch_template());
  $fetch_eval = 0;

  return $res;
}

function return_eval_php($expr)
{
  return eval(eval_list($expr[0]));
}

function return_parseparam($expr)
{
  global $include_row, $nestedinclude;

  if (!isset($expr[0]))
    return $include_row[$nestedinclude];

  $num = eval_list($expr[0]);

  if (!isset($include_row[$nestedinclude][$num]))
    return '';
  else
    return $include_row[$nestedinclude][$num];
}

function return_map_parseparam($expr)
{
  global $global_var, $include_row, $nestedinclude;

  $numarg = 0;
  while (list(, $var) = each($expr)) {
    $numarg++;
    if (isset($include_row[$nestedinclude][$numarg]))
      $val = $include_row[$nestedinclude][$numarg];
    else
      $val = NULL;
    return_setref(array($var, $val));
  }
}

function string_errorhandler($a, $b, $c, $d)
{
  throw new ErrorException($b, 0, $a, $c, $d);
}

function return_string($expr)
{
  $res = '';
  while (list(, $val) = each($expr)) {
    $tmp = eval_list($val);
    if (!is_string($tmp)) {
      set_error_handler('string_errorhandler', E_ALL);
      try {
        $tmp = (string)$tmp;
      } catch (ErrorException $e) {
        $tmp = $e->getMessage();
      }
      restore_error_handler();
    }
    $res .= $tmp;
  }

  return $res;
}

function return_block($expr)
{
  $noblockvalue = array(
    'setref' => 1,
    'setdot' => 1,
    'setmult' => 1,
    'setdiv' => 1,
    'setmod' => 1,
    'setminus' => 1,
    'setplus' => 1,
    'setbinand' => 1,
    'setbinxor' => 1,
    'setbinor' => 1
  );

  $tmp = '';
  while (list(, $val) = each($expr)) {
    $tmp = eval_list($val);
  }

  $last = end($expr);
  if (is_object($last) && get_class($last) == "tf" && isset($noblockvalue[$last->f])) {
    $tmp = '';
  }

  return $tmp;
}

function return_integer($expr)
{
  $val = current($expr);
  next($expr);
  return (int) eval_list($val);
}

function return_double($expr)
{
  $val = current($expr);
  next($expr);
  return (float) eval_list($val);
}

function return_castarray($expr)
{
  $val = current($expr);
  next($expr);
  return (array) eval_list($val);
}

function return_caststring($expr)
{
  $val = current($expr);
  next($expr);
  return (string) eval_list($val);
}

function return_if($expr)
{
  if (eval_list($expr[0]))
    return (isset($expr[1]) ? eval_list($expr[1]) : NULL);
  elseif (isset($expr[2]))
    return eval_list($expr[2]);
}

function return_switch($expr)
{
  while (list(, $cond) = each($expr)) {
    if (list(, $val) = each($expr)) {
      if (eval_list($cond))
        return eval_list($val);
    } else
      return eval_list($cond);
  }
  return '';
}

function return_while($expr)
{
  $val = current($expr);
  next($expr);
  $val2 = current($expr);
  next($expr);
  if (empty($val2))
    return '';

  $tmp = '';
  try {
    while (eval_list($val)) {
      $tmp .= eval_list($val2);
    }
  } catch (BreakException $e) {
  }

  return $tmp;
}

function return_foreach($expr)
{
  $val = current($expr);
  next($expr);
  $val = eval_list($val);
  $tmp = '';
  $idval = current($expr);
  next($expr);

  try {
    switch (is_countable($expr) ? count($expr) : 0) {
      case 3:
        $expr = current($expr);
        next($expr);

        if (!is_array($val) && !is_object($val))
          return;
        foreach ($val as $value) {
          return_setref(array($idval, $value));
          $tmp .= eval_list($expr);
        }

        break;
      case 4:
        if (!is_array($val) && !is_object($val))
          return;

        $idkey = current($expr);
        next($expr);
        $expr = current($expr);
        next($expr);

        foreach ($val as $key => $value) {
          return_setref(array($idkey, $key));
          return_setref(array($idval, $value));
          $reseval = eval_list($expr);
          if (is_null($reseval))
            $reseval = '';
          if (!is_string($reseval)) {
            throw new TempleetError("not a string in foreach:" . print_r($reseval, TRUE) . "=type:" . gettype($reseval) . "==" . $key . "==" . $value . "==" . print_r($val, TRUE) . "\n", $expr->l);
          }

          $tmp .= $reseval;
        }

        break;
      default:
        throw new TempleetError("wrong number of parameters in foreach!\n", $expr->l);
    }
  } catch (BreakException $e) {
  }

  return $tmp;
}

function return_for($expr)
{
  $init = current($expr);
  next($expr);
  eval_list($init);
  $cond = current($expr);
  next($expr);
  $inc = current($expr);
  next($expr);
  $exe = current($expr);
  next($expr);
  $tmp = '';
  try {
    while (eval_list($cond)) {
      $tmp .= eval_list($exe);
      eval_list($inc);
    }
  } catch (BreakException $e) {
  }

  return $tmp;
}

function return_break($expr)
{
  throw new BreakException();
}

function return_empty($expr)
{
  if (!isset($expr[0]))
    return TRUE;
  $tmp = eval_list($expr[0]);
  return empty($tmp);
}

function return_equal($expr)
{
  return (eval_list($expr[0]) == eval_list($expr[1]));
}

function return_typeequal($expr)
{
  $val = current($expr);
  next($expr);
  $tmp_1 = eval_list($val);
  $val = current($expr);
  next($expr);
  $tmp_2 = eval_list($val);

  return ($tmp_1 === $tmp_2);
}

function return_typenotequal($expr)
{
  $val = current($expr);
  next($expr);
  $tmp_1 = eval_list($val);
  $val = current($expr);
  next($expr);
  $tmp_2 = eval_list($val);

  return ($tmp_1 !== $tmp_2);
}

function return_notequal($expr)
{
  return (eval_list($expr[0]) != eval_list($expr[1]));
}

function return_unary_minus($expr)
{
  $val = current($expr);
  next($expr);
  return -eval_list($val);
}

function return_and($expr)
{
  reset($expr);
  $tmp = TRUE;
  while ($tmp && list(, $val) = each($expr))
    $tmp = eval_list($val);

  return $tmp;
}

function return_or($expr)
{
  reset($expr);
  $tmp = FALSE;
  while (!$tmp && list(, $val) = each($expr))
    $tmp = eval_list($val);

  return $tmp;
}

function return_not($expr)
{
  return !eval_list($expr[0]);
}

function return_binnot($expr)
{
  $val = current($expr);
  next($expr);
  return ~eval_list($val);
}


function return_sup($expr)
{
  $tmp_1 = current($expr);
  next($expr);
  $tmp_2 = current($expr);
  next($expr);

  return (eval_list($tmp_1) > eval_list($tmp_2));
}

function return_inf($expr)
{
  $tmp_1 = current($expr);
  next($expr);
  $tmp_2 = current($expr);
  next($expr);

  return (eval_list($tmp_1) < eval_list($tmp_2));
}

function return_supequal($expr)
{
  $tmp_1 = current($expr);
  next($expr);
  $tmp_2 = current($expr);
  next($expr);

  return (eval_list($tmp_1) >= eval_list($tmp_2));
}

function return_infequal($expr)
{
  $tmp_1 = current($expr);
  next($expr);
  $tmp_2 = current($expr);
  next($expr);

  return (eval_list($tmp_1) <= eval_list($tmp_2));
}

function return_plus($expr)
{
  $val = current($expr);
  next($expr);
  if (!isset($val))
    return;
  $tmp = eval_list($val);
  while (list(, $val) = each($expr))
    $tmp += eval_list($val);

  return $tmp;
}

function return_multiple($expr)
{
  $tmp = 1;
  while (list(, $val) = each($expr))
    $tmp = $tmp * eval_list($val);
  return $tmp;
}

function return_divide($expr)
{
  $val = current($expr);
  next($expr);
  $tmp = eval_list($val);
  while (list(, $val) = each($expr)) {
    $div = eval_list($val);
    if ($div != 0)
      $tmp /= $div;
    else
      $tmp = "NaN";
  }
  return $tmp;
}

function return_minus($expr)
{
  $val = current($expr);
  next($expr);
  $tmp = eval_list($val);
  while (list(, $val) = each($expr))
    $tmp -= eval_list($val);

  return $tmp;
}

function return_mod($expr)
{
  $val = current($expr);
  next($expr);
  $tmp = eval_list($val);
  while (list(, $val) = each($expr))
    $tmp %= eval_list($val);

  return $tmp;
}

function return_rem($p)
{
  while (list(, $val) = each($p)) {
    eval_list($val);
  }
  return;
}

function return_noeval($expr)
{
  return;
}

function &getref(&$var, $object, $noset = FALSE)
{
  $nullvar = NULL;

  $tmpvar = $var;
  if (is_string($object))
    $object = new vr($object);
  $null = NULL;
  switch (get_class($object)) {
    case 'vr':
      $name = $object->v;
      if ($name[0] == "#" || $name[0] == '$')
        $name = substr($name, 1);
      if (!isset($var->$name)) {
        if (!is_object($var)) {
          debug_print_r('getref vr :1 :', $name, '==', $var, "==\n");
          templeet_exit();
        }
        if ($noset)
          return $nullvar;

        $var->$name = NULL;
      }
      $var = &$var->$name;

      $p = $object->p;
      if (is_array($p)) {
        while (count($p)) {
          $tmp = array_shift($p);
          if (
            is_object($tmp)
            && get_class($tmp) == 'tf'
            && $tmp->f == 'arrayappend'
          ) {
            $var = &$var[];
          } else {
            $elt = eval_list($tmp);
            if (!is_array($var))
              $var = array();
            if (!isset($var[$elt])) {
              //                          if (!is_array($var))
              //                            debug_print_r("getref vr :3 :",$object,"==",$elt,"==",$tmpvar,"==",$var,"\n");
              if ($noset)
                return $nullvar;

              $var[$elt] = NULL;
            }
            $var = &$var[$elt];
          }
        }
      }
      return $var;
      break;

    case 'sc':
      if (is_string($object->p)) {
        if (defined($object->c . '::' . $object->p)) {
          $var = constant($object->c . '::' . $object->p);
          return $var;
        }
        $class = $object->c;
        $varname = $object->p;
        if (class_exists($class) && isset($class::$$varname))
          $var = &$class::$$varname;
        else {
          if (!property_exists($object->c, $object->p))
            throw new TempleetError("Property " . $object->c . "::" . $object->p . " doesn't exist!\n");

          $tmp = "\$var=&" . $object->c . "::\$" . $object->p . ";";
          eval($tmp);
        }
      } else {
        if (!is_object($object->p) || get_class($object->p) != "vr")
          throw new TempleetError("Unknown token " . print_r($object, TRUE) . " \n");

        if (!property_exists($object->c, $object->p->v))
          throw new TempleetError("Property " . $object->c . "::" . $object->p->v . " doesn't exist!\n");

        eval("\$var=&" . $object->c . "::\$" . $object->p->v . ";");

        foreach ($object->p->p as $param) {
          $value = eval_list($param);
          if (is_array($var) && isset($var[$value])) {
            $var = &$var[$value];
          } else {
            $varnull = NULL;
            if ($noset)
              return $varnull;

            $var[$value] = NULL;
            $var = &$var[$value];
          }
        }
      }
      return $var;
      break;

    case 'ob':
      $tmpobj = $object->o;
      while (is_countable($tmpobj) ? count($tmpobj) : 0) {
        $elt = array_shift($tmpobj);
        if (is_object($elt)) {
          switch (get_class($elt)) {
            case "vr":
              $var = &getref($var, $elt);
              break;

            case "tf":
              if (!is_callable(array($var, $elt->f)))
                throw new TempleetError("Method " . $elt->f . " doesn't exist!\n");

              $param = array();
              foreach ($elt->p as $val)
                $param[] = eval_list($val);
              $res = call_user_func_array(array($var, $elt->f), $param);
              $var = $res;
              break;

            case "sc":
              $class = $elt->c;
              $varname = $elt->p;
              if (class_exists($class) && isset($class::$$varname))
                $var = &$class::$$varname;
              else
                throw new TempleetError("getref: unknown object:<pre>" . print_r($elt, TRUE) . "</pre>\n");
              break;

            default:
              throw new TempleetError("getref: unknown object:<pre>" . print_r($elt, TRUE) . "</pre>\n");
          }
        } else
          $var = &$var->$elt;
      }
      return $var;
      break;

    case "tf":
      if (!is_callable(array($var, $object->f)))
        throw new TempleetError("Method " . $object->f . " doesn't exist!\n");

      $param = array();
      foreach ($object->p as $val) {
        $param[] = eval_list($val);
      }
      $res = call_user_func_array(array($var, $object->f), $param);
      $var = $res;
      break;

    default:
      throw new TempleetError("getref: unknown object:<pre>" . print_r($object, TRUE) . "</pre>\n");
  }
}

function unset_onevar($object)
{
  varname2object($object);

  $var = &getcontextvar($object);

  switch (get_class($object)) {
    case 'vr':
      $name = $object->v;
      if ($name[0] == "#" || $name[0] == '$')
        $name = substr($name, 1);
      if (!isset($var->$name))
        return;

      $p = $object->p;
      if (is_array($p)) {
        $var = &$var->$name;
        while (count($p)) {
          $tmp = array_shift($p);
          if (is_object($tmp) && get_class($tmp) == "tf" && $tmp->f == "arrayappend")
            return;

          $elt = eval_list($tmp);
          if (!isset($var[$elt]))
            return;
          if (count($p))
            $var = &$var[$elt];
          else
            unset($var[$elt]);
        }
      } else
        unset($var->$name);
      break;

    case 'sc':
      if (is_string($object->p))
        $object->p = new vr($object->p);
      $tmp = $object->c . "::\$" . $object->p->v;
      if (is_array($object->p->p)) {
        while (is_countable($object->p->p) ? count($object->p->p) : 0) {
          $tmp .= "[" . eval_list(array_shift($object->p->p)) . "]";
        }
        eval("unset($tmp);");
      } else
        eval("$tmp=NULL;");
      break;

    case 'ob':
      $tmpobj = $object->o;
      while (is_countable($tmpobj) ? count($tmpobj) : 0) {
        $elt = array_shift($tmpobj);
        if (is_object($elt)) {
          switch (get_class($elt)) {
            case "vr":
              if (is_countable($tmpobj) ? count($tmpobj) : 0)
                $var = &getref($var, $elt);
              else {
                $name = $elt->v;
                if (!isset($var->$name))
                  return;

                $p = $elt->p;
                if (is_array($p)) {
                  $var = &$var->$name;
                  while (count($p)) {
                    $elt = eval_list(array_shift($p));
                    if (!isset($var[$elt]))
                      return;
                    if (count($p))
                      $var = &$var[$elt];
                    else
                      unset($var[$elt]);
                  }
                } else
                  unset($var->$name);
              }
              break;

            case "tf":
              if (!is_callable(array($var, $elt->f)))
                throw new TempleetError("Method " . $elt->f . " doesn't exist!\n");

              $param = array();
              foreach ($elt->p as $val) {
                $param[] = eval_list($val);
              }
              $res = call_user_func_array(array($var, $elt->f), $param);
              $var = $res;
              break;

            case "sc":
              $class = $elt->c;
              $varname = $elt->p;
              if (class_exists($class) && isset($class::$$varname))
                $var = &$class::$$varname;
              else
                throw new TempleetError("getref: unknown object:<pre>" . print_r($elt, TRUE) . "</pre>\n");
              break;

            default:
              throw new TempleetError("getref: unknown object:<pre>" . print_r($elt, TRUE) . "</pre>\n");
          }
        } else {
          $var = &$var->$elt;
        }
      }
      break;

    default:
      throw new TempleetError("getref: unknown object:<pre>" . print_r($object, TRUE) . "</pre>\n");
  }
}

function return_unset($expr)
{
  while (list(, $object) = each($expr)) {
    unset_onevar($object);
  }
}

function getobject(&$object)
{
  if (is_object($object) && get_class($object) == "tf")
    $name = eval_list($object);
  else
    $name = $object;

  if (!is_string($name))
    throw new TempleetError("Don't know how to set variable variable name: ($name)\n");

  if (!preg_match("/^[\$\#]?\w+$/", $name))
    throw new TempleetError("Don't know how to set variable variable name of type:" .
      (is_object($name) ? " object " . get_class($name) : gettype($name)));

  $object = new vr($name);
}

function getvar($expr)
{
  $object = eval_list($expr);
  getobject($object);
  return eval_list($object);
}

function varname2object(&$object)
{
  if ((is_object($object) && get_class($object) == "tf") || is_string($object)) {
    global $config;
    if (!empty($config["variablesvariable"]))
      getobject($object);
    else {
      if (is_string($object))
        throw new TempleetError("Don't know how to set variable variable name: ($object)\n");
      else
        throw new TempleetError("Don't know how to set variable variable name of type:" .
          (gettype($object) == "object" ? " object " . get_class($object) : gettype($object)));
    }
  }
}

function return_setref($expr)
{
  $object = current($expr);
  next($expr);
  if (!is_object($object) && !is_string($object))
    throw new TempleetError("setref: parameter 0 not an object:" . $object . "\n");

  $val = current($expr);
  next($expr);
  $res = eval_list($val);

  if (isset($res)) {
    varname2object($object);

    $var = &getcontextvar($object);
    $var = &getref($var, $object);
    $var = $res;
  } else {
    return_unset(array($expr[0]));
    return NULL;
  }
  return $var;
}


function return_setdot($expr)
{
  varname2object($expr[0]);
  return return_setref(array($expr[0], eval_list($expr[0]) . eval_list($expr[1])));
}

function return_setmult($expr)
{
  varname2object($expr[0]);
  return return_setref(array($expr[0], eval_list($expr[0]) * eval_list($expr[1])));
}

function return_setdiv($expr)
{
  varname2object($expr[0]);
  return return_setref(array($expr[0], eval_list($expr[0]) / eval_list($expr[1])));
}

function return_setmod($expr)
{
  varname2object($expr[0]);
  return return_setref(array($expr[0], eval_list($expr[0]) % eval_list($expr[1])));
}

function return_setminus($expr)
{
  varname2object($expr[0]);
  return return_setref(array($expr[0], eval_list($expr[0]) - eval_list($expr[1])));
}

function return_setplus($expr)
{
  varname2object($expr[0]);
  return return_setref(array($expr[0], eval_list($expr[0]) + eval_list($expr[1])));
}

function return_setbinand($expr)
{
  varname2object($expr[0]);
  return return_setref(array($expr[0], eval_list($expr[0]) & eval_list($expr[1])));
}

function return_setbinxor($expr)
{
  varname2object($expr[0]);
  return return_setref(array($expr[0], eval_list($expr[0]) ^ eval_list($expr[1])));
}

function return_setbinor($expr)
{
  varname2object($expr[0]);
  return return_setref(array($expr[0], eval_list($expr[0]) | eval_list($expr[1])));
}

function return_array($array)
{

  $res = array();
  while (list(, $val) = each($array)) {
    $res[] = eval_list($val);
  }
  return $res;
}

function return_arraykey($expr)
{
  $res = array();
  while (list(, $val) = each($expr)) {
    if (is_object($val) && get_class($val) == 'tf' && $val->f == 'keyval')
      $res[eval_list($val->p[0])] = eval_list($val->p[1]);
    else
      $res[] = eval_list($val);
  }
  return $res;
}

function &return_new($expr)
{
  $class = current($expr);
  next($expr);
  $class = eval_list($class);

  if (!class_exists($class))
    throw new TempleetError("Class $class doesn't exist!\n");

  $param = array();
  while (list(, $val) = each($expr)) {
    $param[] = eval_list($val);
  }

  $reflection = new ReflectionClass($class);
  $output     = call_user_func_array(array(&$reflection, 'newInstance'), $param);

  return $output;
}

function return_exit($array)
{
  $val = current($array);
  next($array);
  echo eval_list($val);
  templeet_exit();
}

function return_executedtime($array)
{
  global $time_start;

  $time = getmicrotime() - $time_start;

  return round($time, 4);
}

function return_dumpvar()
{
  global $global_var;

  return $global_var;
}

function returncode($code)
{
  global $global_var;

  $global_var->_ = $code;
  return $code;
}

function return_include_php($expr)
{
  $include = eval_list($expr[0]);
  if (!include_once($include))
    throw new TempleetError("include_php: error including file: $include \n");
}

function return_cached($expr)
{
  global $cache;

  return $cache;
}

function return_try($array)
{
  $trycode = current($array);
  next($array);

  try {
    return eval_list($trycode);
  } catch (Exception $e) {
    while (list(, $exceptiontype) = each($array)) {
      $exceptioncode = current($array);
      next($array);
      $exceptiontype = eval_list($exceptiontype);
      if (!is_string($exceptiontype))
        throw new TempleetError("function try(): exception must be a string\n");

      if (get_class($e) == $exceptiontype) {
        global $global_var;

        $global_var->exception = $e;
        return eval_list($exceptioncode);
      }
    }
    throw $e;
  }
  return '';
}

function return_throw($array)
{
  $exception = current($array);
  next($array);

  throw eval_list($exception);
}

function return_checkargs($array)
{
  global $global_var;
  $returncode = current($array);
  next($array);

  $error = 0;
  if ((is_countable($array) ? count($array) : 0) - 1 != (is_countable($global_var->filenamevars) ? count($global_var->filenamevars) : 0))
    $error = 1;

  while (!$error && list(, $param) = each($array)) {
    if (!eval_list($param))
      $error = 1;
  }

  if ($error) {
    $res = eval_list($returncode);
    if (is_int($res) || ctype_digit($res))
      http_error($res);
    else
      throw new TempleetRedirect($res);
  }
}

function return_getconf($expr)
{
  global $config;

  $val = current($expr);
  next($expr);
  $name = eval_list($val);
  if (isset($config[$name]))
    return $config[$name];
}

function return_dont_cache($expr)
{
  global $cache, $global_var;
  $cache = 0;

  list(,$pragmanocache) = each($expr);
  if (isset($pragmanocache)) {
    $pragmanocache = eval_list($pragmanocache);
    $global_var->HTTP_nocache = $pragmanocache;
  } else
    $global_var->HTTP_nocache = 1;

  return;
}

function return_isset($expr)
{
  $var = &getcontextvar($expr[0]);
  $var = &getref($var, $expr[0], TRUE);
  return isset($var);
}

function crand($min = 0, $max = PHP_INT_MAX)
{
  global $config;

  $pr_bits = '';

  if (function_exists('openssl_random_pseudo_bytes'))
    $pr_bits = openssl_random_pseudo_bytes(16);

  if (strlen($pr_bits) < 16) {
    if (!$config['windows']) {
      // Unix/Linux platform?
      $fp = @fopen('/dev/urandom', 'rb');
      if ($fp !== FALSE) {
        $pr_bits .= @fread($fp, 16);
        @fclose($fp);
      }
    } else {
      if (@class_exists('COM')) {
        try {
          $CAPI_Util = new COM('CAPICOM.Utilities.1');
          $pr_bits .= $CAPI_Util->GetRandom(16, 0);
          if ($pr_bits) {
            $pr_bits = md5($pr_bits, TRUE);
          }
        } catch (Exception $ex) {
        }
      }
    }
  }

  if (strlen($pr_bits) < 16)
    $pr_bits = md5(mt_rand() . microtime(true) . uniqid('', true) . join('', stat(__FILE__)) . memory_get_usage() . getmypid(), TRUE);

  $array = unpack("V", $pr_bits);

  $d = abs($array[1]);
  $res = ($d % ($max - $min + 1)) + $min;

  return $res;
}

function random($length)
{
  $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  $l = strlen($chars) - 1;
  $out = '';
  for ($i = 0; $i < $length; $i++)
    $out .= $chars[crand(0, $l)];
  return $out;
}

function redirect($location, $status = 302)
{
  throw new TempleetRedirect($location, $status);
}

function seterrormessage()
{
  global $global_var;
  $error = error_get_last();
  $global_var->templeet_error = $error["message"];
}

function compactdir($dir)
{
  if ($dir == '')
    return $dir;
  $tmp = explode('/', $dir);
  $realdir = array();

  $i = $k = 0;
  if ($tmp[$i] == '')
    $realdir[$k++] = $tmp[$i++];

  for (; $i < count($tmp); $i++) {
    $tok = $tmp[$i];
    if ($tok == '..') {
      if ($k == 0 || $realdir[$k - 1] == '..')
        $realdir[$k++] = $tok;
      elseif ($realdir[$k - 1] != '')
        unset($realdir[--$k]);
    } elseif ($tok != '' && $tok != '.')
      $realdir[$k++] = $tok;
  }
  if ($tok == '')
    $realdir[$k] = $tok;
  if ($k == 1 && $realdir[0] == '')
    $realdir[1] = '';

  return join('/', $realdir);
}

function cloneObject($obj)
{
  return clone $obj;
}

class class_list
{
  public static array $handle = array();
  public static function create($type, $param)
  {
    global $config;

    if (include_once($config['modulesdir'] . 'list/list_' . $type . '.php')) {
      $classname = 'class_list_' . $type;
      return new $classname($param);
    }
    return FALSE;
  }
}
