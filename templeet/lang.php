<?php
include("serverconf.php");

function getserver($name)
{
  $val = '';
  if (isset($_SERVER[$name]))
    $val = $_SERVER[$name];
  else {
    global $HTTP_SERVER_VARS;
    if (isset($HTTP_SERVER_VARS[$name]))
      $val = $HTTP_SERVER_VARS[$name];
  }

  return $val;
}

setcookie("browser_lang", getserver('HTTP_ACCEPT_LANGUAGE'), 0, $config['dir_installed']);
echo "ok";
