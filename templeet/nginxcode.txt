$pathfromquerystring=0;
$pathinfo=0;
$querystring=$_SERVER['QUERY_STRING'];
if (getpost('_redo')!='') 
  $path=getpost('_redo');
else
if (!empty($_SERVER['PATH_INFO'])) 
  { /* pathinfo */
    $path=preg_replace("/^\//","",$_SERVER['PATH_INFO']);
    $pathinfo=1;
}
else
if (!empty($_SERVER['DOCUMENT_URI']) && !preg_match("/\/(?:templeet\.php|index\.php)\$/",$_SERVER['DOCUMENT_URI'])) 
  {
    $path=preg_replace("/^\//","",$_SERVER['DOCUMENT_URI']);
  }
else
if (preg_match('/^(?:file=)?([^&=]*)((?:&.*)?)$/',$_SERVER['QUERY_STRING'],$res_QUERY_STRING) 
)
  { /* page */
    $path=$res_QUERY_STRING[1];
    $querystring=preg_replace("/^&/","",$res_QUERY_STRING[2]);
    $pathfromquerystring=1;
} else {
  $path="";
}
