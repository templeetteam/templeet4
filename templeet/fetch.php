<?php
/*
 * This code has been developed by:
 *
 * Fabien Penso
 * Pascal Courtois
 *
 */
global $debug_template;
$debug_template = 0;

global $debug_info, $config;
$debug_info = isset($config["debug_info"]) && $config["debug_info"];

global $templeet_expression;
$templeet_expression = array(
  array('.', 'string'),
  array('*', 'multiple'),
  array('/', 'divide'),
  array('%', 'mod'),
  array('-', 'minus'),
  array('+', 'plus'),
  array('<<', 'bwlshift'),
  array('>>', 'bwrshift'),
  array('<=', 'infequal'),
  array('>=', 'supequal'),
  array('<', 'inf'),
  array('>', 'sup'),
  array('===', 'typeequal'),
  array('==', 'equal'),
  array('!==', 'typenotequal'),
  array('!=', 'notequal'),
  array('&&', 'and'),
  array('||', 'or'),
  array('&', 'bwand'),
  array('^', 'bwxor'),
  array('|', 'bwor'),
  array('.=', 'setdot'),
  array('*=', 'setmult'),
  array('/=', 'setdiv'),
  array('%=', 'setmod'),
  array('-=', 'setminus'),
  array('+=', 'setplus'),
  array('&=', 'setbinand'),
  array('^=', 'setbinxor'),
  array('|=', 'setbinor'),
  array('=', 'setref')
);

function gettemplateline($nb)
{
  global $template_splitlines, $last_line;

  if ($nb < $template_splitlines[$last_line])
    $last_line = 1;

  while (isset($template_splitlines[$last_line + 1]) && $nb > $template_splitlines[$last_line + 1])
    $last_line++;

  return $last_line;
}

function fetch_error($nb)
{
  global $config, $global_var, $contents, $template_length, $evalerror, $fetch_eval;

  $global_var->template_lines = gettemplateline($nb);
  $config['usepagecache'] = 0;
  $tmp_count = 0;
  if ($nb < $template_length)
    while ($nb >= 0 && $contents[$nb] != "\n") {
      $tmp_count += 1;
      $nb--;
    }
  else
    $global_var->endoffile = 1;

  $global_var->template_nb = $tmp_count;
  $global_var->errorLog = 'Syntax error in ' . $global_var->actual_template . ' at line ' . $global_var->template_lines;

  if ($fetch_eval)
    $global_var->eval_content = $contents;
  if (file_exists('template/error/syntax.html')) {
    $evalerror = 1;
    echo parsetemplate('template/error/syntax.html');
  } else
    echo $global_var->errorLog;

  templeet_exit();
}

function cleantemp_recurs($obj)
{
  if (!isset($obj))
    return;


  if (!is_object($obj))
    return $obj;

  if (get_class($obj) == "vr") {
    if (is_array($obj->p)) {
      $res = array();
      foreach ($obj->p as $val) {
        $val = cleantemp_recurs($val);
        $res[] = $val;
      }
      $obj->p = $res;
    }
    return $obj;
  }

  if (get_class($obj) == "ob") {
    $a = $obj->o;
    $obj->o = array();
    foreach ($a as $elt)
      $obj->o[] = cleantemp_recurs($elt);
    return $obj;
  }

  if (get_class($obj) == "sc") {
    $obj->p = cleantemp_recurs($obj->p);
    return $obj;
  }

  if (get_class($obj) != "tf") {
    debug_print_r("cleantemp_recurs: not a tf object!\n" . print_r($obj, TRUE) . "===\n");
    debug_tbacktrace();
    templeet_exit(1);
  }

  if ($obj->f == "string" && is_array($obj->p) && (is_countable($obj->p) ? count($obj->p) : 0) == 1 && is_string($obj->p[0])) {
    return $obj->p[0];
  }
  if ($obj->f == "array") {
    $constarray = TRUE;
    if (is_array($obj->p)) {
      while ($constarray && list(, $val) = each($obj->p)) {
        if (is_object($val))
          $constarray = FALSE;
      }

      if ($constarray)
        return $obj->p;
      reset($obj->p);
    }
    return $obj;
  }
  if (is_array($obj->p)) {
    if ((is_countable($obj->p) ? count($obj->p) : 0) == 1) {
      if ($obj->f == "string" && is_string($obj->p[0])) {
        return $obj->p[0];
      }

      if ($obj->f == "integer" && is_numeric($obj->p[0])) {
        if (floatval($obj->p[0]) == intval($obj->p[0]))
          return intval($obj->p[0]);
        else
          return floatval($obj->p[0]);
      }

      if (!is_object($obj->p[0]) || get_class($obj->p[0]) != "tf" || $obj->p[0]->f != "array")
        $obj->p = cleantemp_recurs($obj->p[0]);
    }
  }
  if (is_array($obj->p)) {
    $res = array();
    foreach ($obj->p as $val) {
      $res[] = cleantemp_recurs($val);
    }
    $obj->p = $res;
  } else {
    if (is_object($obj->p) && get_class($obj->p) == "tf")
      $obj->p = cleantemp_recurs($obj->p);
  }
  return $obj;
}

function cleantemp($template)
{
  $temp = cleantemp_recurs($template);

  if (!is_object($temp) || get_class($temp) != "tf")
    return $temp;
  if ($temp->f == "string" && is_array($temp->p) && (is_countable($temp->p) ? count($temp->p) : 0) == 1)
    return $temp->p[0];
  elseif ($temp->f == "string" && is_array($temp->p) && (is_countable($temp->p) ? count($temp->p) : 0) == 2 && is_string($temp->p[1]) && preg_match("/^\s+$/s", $temp->p[1]))
    return $temp->p[0];
  elseif ($temp->f == "string" && is_array($temp->p) && is_string($temp->p[(is_countable($temp->p) ? count($temp->p) : 0) - 1]) && preg_match("/^\s+$/s", $temp->p[(is_countable($temp->p) ? count($temp->p) : 0) - 1]))
    unset($temp->p[(is_countable($temp->p) ? count($temp->p) : 0) - 1]);

  return $temp;
}

function fetch_template()
{
  global $contents, $template_length, $template_nocache, $debug_info, $reduce_space, $config;
  $tmp_string = '';

  $reduce_space = isset($config["reduce_space"]) && $config["reduce_space"];
  global $template_splitlines, $last_line;
  $tmp = preg_split("/\r?\n/", $contents, -1, PREG_SPLIT_OFFSET_CAPTURE);

  $last_line = 1;
  foreach ($tmp as $numline => $line)
    $template_splitlines[$numline + 1] = $line[1];

  $nb = 0;
  $template_nocache = 0;

  $all = array();
  while ($nb < $template_length) {
    $startnb = $nb;
    $nb = strpos($contents, '~', $nb);
    if ((is_bool($nb) && ($nb == false)) || ($contents[$nb] != '~')) {
      $nb = $template_length;
      $txt_tmp = substr($contents, $startnb, $nb - $startnb);
      $tmp_string .= $txt_tmp;
    } else {
      if ($nb == 0 || $contents[$nb - 1] == "\n")
        $onlyfunc = 1;

      $txt_tmp = substr($contents, $startnb, $nb - $startnb);
      $tmp_string .= $txt_tmp;
      $startnb = $nb + 1;
      $nb = strpos($contents, '(', $startnb);
      if (((is_bool($nb)) && ($nb == false)) || ($contents[$nb] != '(') || $nb - $startnb == 0) {
        $tmp_string .= '~';
        $nb = $startnb;
      } else {
        $tmp_f = substr($contents, $startnb, $nb - $startnb);

        $tmp_nb = strpos($tmp_f, '~');
        if ($tmp_nb !== FALSE) {
          $tmp_string .= '~';
          $nb = $startnb;
        } else {
          $new = strtr(
            $tmp_f,
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789_ ',
            '000000000000000000000000000000000000000000000000000000000000001'
          );

          if ($new == '0') {
            if ($tmp_string != '') {
              $all[] = $tmp_string;
              $tmp_string = '';
            }
            list($nb, $tmp_b) = fetch_function($nb, $tmp_f);
            $all[] = $tmp_b;
            if ($nb < $template_length)
              if (isset($onlyfunc) && $onlyfunc == 1) {
                if (isset($contents[$nb]) && $contents[$nb] != '~')
                  $onlyfunc = 0;
                if (isset($contents[$nb + 1]) && $contents[$nb] == "\r" && $contents[$nb + 1] == "\n")
                  $nb += 2;
                if (isset($contents[$nb]) && $contents[$nb] == "\n")
                  $nb++;
              }
          } else
            $tmp_string .= '~' . $tmp_f;
        }
      }
    }
  }

  if ($tmp_string != '') {
    $all[] = $tmp_string;
  }

  if (count($all) > 1)
    $template = new tf("string", $all);
  else
    $template = $all[0];
  return cleantemp($template);
}

function fetch_function($nb, $tmp_f)
{
  global $contents, $template_length, $templeet_expression, $template_nocache, $config, $debug_info;

  $line = gettemplateline($nb);

  if ($tmp_f == "array")
    return fetch_array($nb);

  $tmp_array = array();
  if (is_string($tmp_f) && preg_match($config['nocachefunc'], $tmp_f))
    $template_nocache = 1;

  $nb++;
  $nb = fetch_removespace($nb);

  if ($nb >= $template_length)
    fetch_error($nb);

  if ($contents[$nb] != ')') {
    list($nb, $tmp_b) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
    $tmp_array[] = $tmp_b;
  }

  while ($nb < $template_length && $contents[$nb] != ')') {
    if ($contents[$nb] == ',' || $contents[$nb] == ';') {
      $nb++;
      $nb = fetch_removespace($nb);
      if ($contents[$nb] != ')') {
        list($nb, $tmp_b) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
        $tmp_array[] = $tmp_b;
      }
    } else {
      if (
        $contents[$nb] == ' ' ||
        $contents[$nb] == "\t" ||
        $contents[$nb] == "\n"
      )
        $nb++;
      else
        fetch_error($nb);
    }
  }

  if ($nb >= $template_length)
    fetch_error($nb);

  $nb++;
  if ($tmp_f == "debug_info") {
    $debug_info = 1;
    return array($nb, new tf("noeval", array()));
  }
  if ($tmp_f == "reduce_space") {
    global $reduce_space;
    $reduce_space = 1;
    return array($nb, new tf("noeval", array()));
  }
  if ($tmp_f == "noreduce_space") {
    global $reduce_space;
    $reduce_space = 0;
    return array($nb, new tf("noeval", array()));
  }

  return array($nb, new tf($tmp_f, $tmp_array, $debug_info ? $line : NULL));
}

function checkarrayappend($array)
{
  if (!is_array($array))
    return FALSE;

  if (isset($array[0]) && $array[0] == "arrayappend")
    return TRUE;

  $i = 1;
  while (isset($array[$i])) {
    if (checkarrayappend($array[$i]))
      return TRUE;
    $i++;
  }

  return FALSE;
}

function fetch_array($nb)
{
  global $contents, $template_length, $templeet_expression, $debug_info;

  $line = gettemplateline($nb);
  $nokey = 1;

  $tmp_array = array();

  $nb++;
  $nb = fetch_removespace($nb);

  while ($nb < $template_length && $contents[$nb] != ')') {
    $nb = fetch_removespace($nb);
    list($nb, $tmp) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);

    $nb = fetch_removespace($nb);
    if ($nb + 1 < $template_length && $contents[$nb] == '=' && $contents[$nb + 1] == '>') {
      $nokey = 0;
      $nb += 2;
      $nb = fetch_removespace($nb);

      list($nb, $tmp_value) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
      $tmp_array[] = new tf('keyval', array($tmp, $tmp_value, $debug_info ? $line : NULL));
    } else
      $tmp_array[] = $tmp;

    $nb = fetch_removespace($nb);

    if ($nb < $template_length) {
      if ($contents[$nb] == ',')
        $nb++;
      elseif ($contents[$nb] != ')')
        fetch_error($nb);
    }
  }

  if ($nokey)
    $func = 'array';
  else
    $func = 'arraykey';

  $nb++;
  return array($nb, new tf($func, $tmp_array, $debug_info ? $line : NULL));
}

function fetch_comp_expression($nb, $expr_level)
{
  global $contents, $template_length, $templeet_expression, $debug_info;

  $nb = fetch_removespace($nb);
  $line = gettemplateline($nb);

  if ($expr_level < 0) {
    if ($nb < $template_length && (ctype_alpha($contents[$nb]) || $contents[$nb] == '_' || $contents[$nb] == '#' || $contents[$nb] == '$') || $contents[$nb] == '@') {
      list($nb, $ident) = fetch_ident($nb);
      $nb = fetch_removespace($nb);
      return array($nb, $ident);
    }

    if ($nb < $template_length && $contents[$nb] == '-') {
      $nb++;
      list($nb, $tmp_expr) = fetch_comp_expression($nb, -1);
      $nb = fetch_removespace($nb);
      return array($nb, new tf('unary_minus', $tmp_expr, $debug_info ? $line : NULL));
    }

    if ($nb < $template_length && $contents[$nb] == '!') {
      $nb++;
      list($nb, $tmp_expr) = fetch_comp_expression($nb, -1);
      $nb = fetch_removespace($nb);
      return array($nb, new tf('not', $tmp_expr, $debug_info ? $line : NULL));
    }

    if ($nb < $template_length && $contents[$nb] == '(') {
      $nb++;
      list($nb, $tmp_expr) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
      $nb = fetch_removespace($nb);
      if ($contents[$nb] != ')') {
        fetch_error($nb);
      }
      $nb++;
      $nb = fetch_removespace($nb);
      if ($tmp_expr->f == "getref") {
        switch ($tmp_expr->p[0]) {
          case 'int':
            list($nb, $tmp_expr) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
            return array($nb, new tf('integer', $tmp_expr, $debug_info ? $line : NULL));
          case 'double':
            list($nb, $tmp_expr) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
            return array($nb, new tf('double', $tmp_expr, $debug_info ? $line : NULL));
          case 'string':
            list($nb, $tmp_expr) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
            return array($nb, new tf('caststring', $tmp_expr, $debug_info ? $line : NULL));
          case 'array':
            list($nb, $tmp_expr) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
            return array($nb, new tf('castarray', $tmp_expr, $debug_info ? $line : NULL));
        }
      }
      return array($nb, $tmp_expr);
    }

    if ($nb < $template_length && $contents[$nb] == '~') {
      $nb++;
      $tmp_f = '';

      while (
        $nb < $template_length &&
        ($contents[$nb] >= '0' && $contents[$nb] <= '9' || // 0-9
          is_alpha($contents[$nb]))
      ) {
        $tmp_f .= $contents[$nb];
        $nb++;
      }

      if ($contents[$nb] == '(') {
        list($nb, $tmp_array) = fetch_function($nb, $tmp_f);
        $nb = fetch_removespace($nb);

        return array($nb, $tmp_array);
      } else {
        fetch_error($nb);
      }
    }

    if ($nb + 1 < $template_length && $contents[$nb] == '0' && $contents[$nb + 1] != '.') {
      if (($contents[$nb + 1] == 'x') || ($contents[$nb + 1] == 'X'))
        list($nb, $tmp_array) = fetch_hexa($nb);
      else
        list($nb, $tmp_array) = fetch_octal($nb);

      $nb = fetch_removespace($nb);
      return array($nb, $tmp_array);
    } elseif (
      $nb < $template_length && $contents[$nb] >= '0' &&
      $contents[$nb] <= '9'
    ) {
      list($nb, $tmp_array) = fetch_num($nb);
      $nb = fetch_removespace($nb);
      return array($nb, $tmp_array);
    } elseif (
      ($nb + 2 < $template_length && substr($contents, $nb, 3) == '<![') ||
      ($nb < $template_length &&
        (($contents[$nb] == '`') || ($contents[$nb] == '"') || ($contents[$nb] == "'")))
    ) {
      list($nb, $tmp_array) = fetch_string($nb);
      $nb = fetch_removespace($nb);
      return array($nb, $tmp_array);
    } elseif ($nb < $template_length && $contents[$nb] == '{') {
      list($nb, $tmp_array) = fetch_block($nb);
      $nb = fetch_removespace($nb);
      return array($nb, $tmp_array);
    } elseif ($nb < $template_length && $contents[$nb] == ',') {
      $tmp_array = new tf('string', array(), $debug_info ? $line : NULL);
      $nb = fetch_removespace($nb);
      return array($nb, $tmp_array);
    }

    fetch_error($nb);
  }

  list($nb, $tmp_array) = fetch_comp_expression($nb, $expr_level - 1);
  while (
    (
      ($nb < $template_length && strlen($templeet_expression[$expr_level][0]) == 1 &&
        $contents[$nb] == $templeet_expression[$expr_level][0][0])
      ||
      ($nb + 1 < $template_length &&
        strlen($templeet_expression[$expr_level][0]) == 2 &&
        $contents[$nb] == $templeet_expression[$expr_level][0][0] &&
        $contents[$nb + 1] == $templeet_expression[$expr_level][0][1])
      ||
      ($nb + 2 < $template_length &&
        strlen($templeet_expression[$expr_level][0]) == 3 &&
        $contents[$nb] == $templeet_expression[$expr_level][0][0] &&
        $contents[$nb + 1] == $templeet_expression[$expr_level][0][1] &&
        $contents[$nb + 2] == $templeet_expression[$expr_level][0][2]))
    &&
    ($nb + 1 >= $template_length
      ||
      (
        (strstr(".+-/%*&^|", (string) $templeet_expression[$expr_level][0]) === FALSE)
        ||
        ((strstr(".+-/%*&^|", (string) $contents[$nb]) !== FALSE)
          &&
          $contents[$nb + 1] != "=")))
  ) {

    if ($nb < $template_length && $contents[$nb] == ')')
      return array($nb, $tmp_array);

    if ($nb + 1 < $template_length && $contents[$nb] == '=' &&  $contents[$nb + 1] == '>')
      return array($nb, $tmp_array);

    if (strlen($templeet_expression[$expr_level][0]) == 1) {
      if ($nb < $template_length && $contents[$nb] != $templeet_expression[$expr_level][0][0])
        return array($nb, $tmp_array);

      $nb = fetch_removespace($nb + 1);
    } elseif (strlen($templeet_expression[$expr_level][0]) == 2) {
      if (
        $nb + 1 < $template_length &&
        ($contents[$nb] != $templeet_expression[$expr_level][0][0] ||
          $contents[$nb + 1] != $templeet_expression[$expr_level][0][1])
      )
        return array($nb, $tmp_array);
      $nb = fetch_removespace($nb + 2);
    } else {
      if (
        $nb + 2 < $template_length &&
        ($contents[$nb] != $templeet_expression[$expr_level][0][0] ||
          $contents[$nb + 1] != $templeet_expression[$expr_level][0][1] ||
          $contents[$nb + 2] != $templeet_expression[$expr_level][0][2])
      )
        return array($nb, $tmp_array);
      $nb = fetch_removespace($nb + 3);
    }
    $tmp_array = new tf($templeet_expression[$expr_level][1], array($tmp_array), $debug_info ? $line : NULL);
    list($nb, $tmp) = fetch_comp_expression($nb, $expr_level - 1);
    $tmp_array->p[] = $tmp;
  }

  if ($nb < $template_length && $contents[$nb] == ')')
    return array($nb, $tmp_array);

  if ($nb + 1 < $template_length && $contents[$nb] == '=' && $contents[$nb + 1] == '>')
    return array($nb, $tmp_array);

  return array($nb, $tmp_array);
}

function fetch_removespace($nb)
{
  global $contents, $template_length;

  do {
    $newline = 0;
    while (
      $nb < $template_length &&
      ($contents[$nb] == ' ' ||
        $contents[$nb] == "\r" ||
        $contents[$nb] == "\t" ||
        $contents[$nb] == "\n")
    ) {

      if ($contents[$nb] == "\n") {
        $newline = 1;
      }

      $nb++;
    }
    if ($nb + 1 < $template_length && $contents[$nb] == "/") {
      if ($newline && $contents[$nb + 1] == "/") {
        $nb += 2;
        while ($nb < $template_length && $contents[$nb] != "\n")
          $nb++;
      } elseif ($contents[$nb + 1] == "*") {
        $nb += 2;
        $iscomment = 1;
        while ($nb < $template_length && $iscomment) {
          if ($nb + 1 >= $template_length)
            fetch_error($nb + 1);

          if ($contents[$nb] == "*" && $contents[$nb + 1] == "/") {
            $iscomment = 0;
            $nb++;
          }
          $nb++;
        }
      }
    }
  } while (
    $nb < $template_length &&
    ($contents[$nb] == ' ' ||
      $contents[$nb] == "\r" ||
      $contents[$nb] == "\t" ||
      $contents[$nb] == "\n")
  );

  return $nb;
}

function fetch_ident($nb)
{
  global $contents, $template_length, $templeet_expression;

  list($nb, $ident) = fetch_singleident($nb);

  if (is_string($ident) && defined($ident))
    return array($nb, constant($ident));

  if ($ident == "new" && $contents[$nb] != "(") {
    if ($nb >= $template_length)
      fetch_error($nb);

    $nb = fetch_removespace($nb + 1);

    if ($nb >= $template_length)
      fetch_error($nb);

    list($nb, $ident) = fetch_singleident($nb);

    if ($nb >= $template_length || $contents[$nb] != "(")
      fetch_error($nb);

    list($nb, $class) = fetch_function($nb, $ident);

    array_unshift($class->p, $class->f);
    $class->f = "new";
    return array($nb, $class);
  }

  if ($ident == "function") {
    if ($nb >= $template_length)
      fetch_error($nb);

    $nb = fetch_removespace($nb + 1);

    if ($nb >= $template_length)
      fetch_error($nb);

    list($nb, $ident) = fetch_singleident($nb);

    if ($nb >= $template_length || $contents[$nb] != "(")
      fetch_error($nb);

    $defunc = new tf("defunc", array($ident));

    $params = array();
    $nb = fetch_removespace($nb + 1);
    while ($contents[$nb] != ")") {
      if ($contents[$nb] != "#")
        fetch_error($nb);
      list($nb, $ident) = fetch_singleident($nb);
      $params[] = $ident;

      $nb = fetch_removespace($nb);
      if ($contents[$nb] != ")" && $contents[$nb] != ",")
        fetch_error($nb);

      if ($contents[$nb] == ",")
        $nb = fetch_removespace($nb + 1);
    }
    $nb = fetch_removespace($nb + 1);

    if ($nb >= $template_length || $contents[$nb] != "{")
      fetch_error($nb);

    list($nb, $block) = fetch_block($nb);

    if (count($params) != 0) {
      $mapparam = new tf("mapparam", array());
      foreach ($params as $param)
        $mapparam->p[] = new vr($param);

      array_unshift($block->p, $mapparam);
    }
    $defunc->p[1] = $block;
    return array($nb, $defunc);
  }

  if ($nb + 1 < $template_length && $contents[$nb] == ":" && $contents[$nb + 1] == ":") {
    $nb += 2;
    list($nb, $ident2) = fetch_compident($nb, $ident);

    if ((is_countable($ident2->o) ? count($ident2->o) : 0) == 2)
      return array($nb, new sc($ident2->o[0], $ident2->o[1]));
    else {
      $ident2->o[0] = new sc($ident2->o[0], $ident2->o[1]);
      unset($ident2->o[1]);
      $ident2->o = array_values($ident2->o);
      return array($nb, $ident2);
    }
  }

  if ($nb < $template_length && $contents[$nb] == "[") {
    list($nb, $ident) = fetch_arrayident($nb, $ident);
  }

  if ($nb < $template_length && $contents[$nb] == "(") {
    list($nb, $ident) = fetch_function($nb, $ident);
  }

  if ($nb + 1 < $template_length && $contents[$nb] == "-" && $contents[$nb + 1] == ">") {
    $nb += 2;
    list($nb, $ident) = fetch_compident($nb, $ident);
    return array($nb, $ident);
  }

  if (is_object($ident))
    return array($nb, $ident);
  else
    return array($nb, new vr($ident));
}

function fetch_arrayident($nb, $ident)
{
  global $contents, $template_length, $templeet_expression;

  $res = new vr($ident);

  while ($nb < $template_length && $contents[$nb] == "[") {
    $nb = fetch_removespace($nb + 1);
    if ($nb < $template_length && $contents[$nb] == "]") {
      $nb++;
      $res->p[] = new tf('arrayappend', array());
    } elseif ($nb >= $template_length) {
      fetch_error($nb);
    } else {
      list($nb, $tmp_b) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
      if ($nb < $template_length && $contents[$nb] == "]") {
        $nb++;
        $res->p[] = $tmp_b;
      } else
        fetch_error($nb);
    }
  }
  return array($nb, $res);
}

function fetch_singleident($nb)
{
  global $contents, $template_length, $templeet_expression;

  $ident = $contents[$nb++];
  while ($nb < $template_length && (ctype_alnum($contents[$nb]) || $contents[$nb] == "_")) {
    $ident .= $contents[$nb++];
  }
  return array($nb, $ident);
}

function fetch_compident($nb, $ident)
{
  global $contents, $template_length, $templeet_expression;

  $comparray = array($ident);
  list($nb, $ident) = fetch_singleident($nb);

  if ($nb < $template_length && $contents[$nb] == "[") {
    list($nb, $ident) = fetch_arrayident($nb, $ident);
  }
  if ($nb < $template_length && $contents[$nb] == "(") {
    list($nb, $ident) = fetch_function($nb, $ident);
  }
  while ($nb + 1 < $template_length && $contents[$nb] == "-" && $contents[$nb + 1] == ">") {
    $nb += 2;

    $comparray[] = $ident;
    list($nb, $ident) = fetch_singleident($nb);

    if ($nb < $template_length && $contents[$nb] == "[") {
      list($nb, $ident) = fetch_arrayident($nb, $ident);
    }

    if ($nb < $template_length && $contents[$nb] == "(") {
      list($nb, $ident) = fetch_function($nb, $ident);
    }
    $res = fetch_compident($nb, $comparray);
  }

  $comparray[] = $ident;
  $res = new ob($comparray);
  return array($nb, $res);
}

function fetch_block($nb)
{
  global $contents, $template_length, $templeet_expression, $debug_info;

  $nb++;
  $tmp_array = new tf('block', array(), $debug_info ? gettemplateline($nb) : NULL);

  if ($contents[$nb] != '}') {
    list($nb, $tmp_b) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
    $tmp_array->p[] = $tmp_b;
  }

  while ($nb < $template_length && $contents[$nb] != '}') {
    if ($contents[$nb] == ',' || $contents[$nb] == ';') {
      $nb++;
      $nb = fetch_removespace($nb);
      if ($contents[$nb] != '}') {
        list($nb, $tmp_b) = fetch_comp_expression($nb, (is_countable($templeet_expression) ? count($templeet_expression) : 0) - 1);
        $tmp_array->p[] = $tmp_b;
      }
    } else {
      if (
        $contents[$nb] == ' ' ||
        $contents[$nb] == "\t" ||
        $contents[$nb] == "\n"
      ) {
        $nb++;
      } else
        fetch_error($nb);
    }
  }
  $nb++;

  return array($nb, $tmp_array);
}

function fetch_string($nb)
{
  global $contents, $template_length, $debug_info;
  $startchar = $contents[$nb];
  $tmp_array = array();
  $tmp = '';
  $startpos = $nb;
  $line = gettemplateline($nb);

  $heredoc = false;

  if ($startchar == '<') {
    $nb += 3;
    $identifier = '';

    while (
      $nb < $template_length &&
      ($contents[$nb] >= '0' && $contents[$nb] <= '9' || // 0-9
        is_alpha($contents[$nb]) ||
        $contents[$nb] == '-')
    ) {
      $identifier .= $contents[$nb];
      $nb++;
    }

    if ($contents[$nb] != '[') {
      fetch_error($nb);
    }

    $startchar = '';
    $nb++;
    $heredoc = true;
    $end_id = "]$identifier]>";
    $end_id_len = strlen($end_id);
  } else {
    $nb++;
  }

  while (
    $nb < $template_length &&
    ($contents[$nb] != $startchar)
  ) {
    if ($startchar != '`' && $contents[$nb] == '~') {
      if ($nb == $startpos || $contents[$nb - 1] == "\n")
        $onlyfunc = 1;

      $startnb = $nb + 1;
      $nb = strpos($contents, '(', $startnb);
      if (((is_bool($nb)) && ($nb == false)) || ($contents[$nb] != '(')) {
        $tmp .= '~';
        $nb = $startnb + 1;
      } else {
        $tmp_f = substr($contents, $startnb, $nb - $startnb);
        $new = strtr(
          $tmp_f,
          'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789_ ',
          '000000000000000000000000000000000000000000000000000000000000001'
        );

        if ($new == '0') {
          if ($tmp != '')
            $tmp_array[] = $tmp;
          $tmp = '';
          list($nb, $tmp_b) = fetch_function($nb, $tmp_f);
          if ($nb >= $template_length)
            fetch_error($nb);
          $tmp_array[] = $tmp_b;
          if (isset($onlyfunc) && $onlyfunc) {
            if ($contents[$nb] != '~')
              $onlyfunc = 0;
            if ($contents[$nb] == "\n") {
              $nb++;
            }
          }
        } else {
          $tmp .= '~';
          $nb = $startnb;
        }
      }
    } elseif ($contents[$nb] == $startchar) {
      $tmp[(strlen($tmp) - 1)] = $startchar;
      $nb++;
    } elseif (!$heredoc && $contents[$nb] == '\\') {
      $nb++;

      switch ($contents[$nb]) {
        case '\\':
          $tmp .= '\\';
          $nb++;
          break;
        case "'":
          $tmp .= "'";
          $nb++;
          break;
        case '"':
          $tmp .= '"';
          $nb++;
          break;
        case '`':
          $tmp .= '`';
          $nb++;
          break;
        case '~':
          $tmp .= '~';
          $nb++;
          break;
        case 'n':
          $tmp .= "\n";
          $nb++;
          break;
        case 'r':
          $tmp .= "\r";
          $nb++;
          break;
        case 't':
          $tmp .= "\t";
          $nb++;
          break;
        case 'x':
          $nb++;
          $hex = '';
          if (
            ($contents[$nb] >= '0' && $contents[$nb] <= '9') ||
            ($contents[$nb] >= 'A' && $contents[$nb] <= 'F') ||
            ($contents[$nb] >= 'a' && $contents[$nb] <= 'f')
          ) {
            $hex = $contents[$nb++];
            if (
              ($contents[$nb] >= '0' && $contents[$nb] <= '9') ||
              ($contents[$nb] >= 'A' && $contents[$nb] <= 'F') ||
              ($contents[$nb] >= 'a' && $contents[$nb] <= 'f')
            )
              $hex .= $contents[$nb++];
            else
              $hex = "0$hex";
            $tmp .= pack('H2', $hex);
          }
          break;

        default:
          if ($contents[$nb] >= '0' && $contents[$nb] <= '7') {
            $oct = $contents[$nb++];
            if ($contents[$nb] >= '0' && $contents[$nb] <= '7') {
              $oct = $oct * 8 + $contents[$nb++];
              if ($contents[$nb] >= '0' && $contents[$nb] <= '7')
                $oct = $oct * 8 + $contents[$nb++];
            }
            $tmp .= chr($oct);
          } else {
            $tmp .= '\\' . $contents[$nb];
            $nb++;
          }
      }
    } else {
      $tmp .= $contents[$nb];
      $nb++;
    }

    if ($heredoc && substr($tmp, -$end_id_len) == $end_id) {
      $tmp = substr($tmp, 0, -$end_id_len);
      $nb--;
      break;
    }
  }

  if ($tmp != '')
    $tmp_array[] = $tmp;

  global $reduce_space;
  if ($reduce_space && isset($tmp_array[0]) && is_string($tmp_array[0])) {
    if (preg_match("/^([\t ]*)(\r\n|\n\r|\n|\r)([\t ]+)/", $tmp_array[0], $res)) {
      $tmp_array[0] = preg_replace("/^" . $res[1] . "/", '', $tmp_array[0]);
      $i = 0;
      $restmp = array();
      while (isset($tmp_array[$i])) {
        if (is_string($tmp_array[$i]))
          $tmp_array[$i] = preg_replace("/(\r\n|\n\r|\n|\r)" . $res[3] . "/", "$1", $tmp_array[$i]);
        if (!is_string($tmp_array[$i]) || $tmp_array[$i] != '')
          $restmp[] = $tmp_array[$i];
        $i++;
      }
      $tmp_array = $restmp;
      $tmp_array[0] = preg_replace("/^" . $res[2] . "/", '', $tmp_array[0]);
    }
  }

  $nb++;

  return array($nb, new tf('string', $tmp_array, $debug_info ? $line : NULL));
}

function fetch_num($nb)
{
  global $contents, $template_length;
  $tmp = '';
  while (
    $nb < $template_length &&
    $contents[$nb] >= '0' && $contents[$nb] <= '9'
  ) {
    $tmp .= $contents[$nb];
    $nb++;
  }

  if (
    $nb + 1 < $template_length && $contents[$nb] == '.' &&
    $contents[$nb + 1] >= '0' && $contents[$nb + 1] <= '9'
  ) {
    $tmp .= '.';
    $nb++;
    while (
      $nb < $template_length &&
      $contents[$nb] >= '0' && $contents[$nb] <= '9'
    ) {
      $tmp .= $contents[$nb];
      $nb++;
    }
    return array($nb, (float)$tmp);
  } else
    return array($nb, (int)$tmp);
}

function fetch_octal($nb)
{
  global $contents, $template_length;
  $tmp = '';
  while (
    $nb < $template_length &&
    $contents[$nb] >= '0' && $contents[$nb] <= '7'
  ) {
    $tmp .= $contents[$nb];
    $nb++;
  }
  $tmp = (int)base_convert($tmp, 8, 10);
  return array($nb, $tmp);
}

function fetch_hexa($nb)
{
  global $contents, $template_length;
  /*We skip contents already scanned */
  $nb += 2;
  $tmp = '';
  while (
    $nb < $template_length &&
    (($contents[$nb] >= '0' && $contents[$nb] <= '9') ||
      ($contents[$nb] >= 'a' && $contents[$nb] <= 'f') ||
      ($contents[$nb] >= 'A' && $contents[$nb] <= 'F'))
  ) {
    $tmp .= $contents[$nb];
    $nb++;
  }
  $tmp = (int)base_convert($tmp, 16, 10);
  return array($nb, $tmp);
}

function is_alpha($c)
{
  return ord($c) >= 65 && ord($c) <= 90 || // A-Z
    ord($c) >= 97 && ord($c) <= 122 ||
    $c == '_';
}
