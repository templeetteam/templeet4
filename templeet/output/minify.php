<?php

function minify_callback($matches)
{
  return $matches[1] . JSMin::minify($matches[2]) . $matches[3];
}

function minifycss_callback($matches)
{

  $minified = Minify_CSS_Compressor::process($matches[2], array());

  if ($minified === FALSE)
    $minified = $matches[2];

  return $matches[1] . $minified . $matches[3];
}

if (
  isset($global_var->HTTP_Content_type) &&
  (
    ($cache && !empty($config['user']['minify']['minify_cached'])) ||
    (!$cache && !empty($config['user']['minify']['minify_notcached']))) &&
  empty($global_var->nominify)
) {
  switch ($global_var->HTTP_Content_type) {
    case $config['extensions']['html']:
      if (!empty($config['user']['minify']['embedded_js']) && strlen($txt) < $config['user']['minify']['max_js_size']) {
        require 'minify/jsmin.php';
        $txt = preg_replace_callback(
          '/(<script[^>]*>)(.*?)(<\/script[^>]*>)/is',
          'minify_callback',
          $txt
        );
      }

      if (!empty($config['user']['minify']['embedded_css']) && strlen($txt) < $config['user']['minify']['max_css_size']) {
        include('minify/Compressor.php');

        $txt = preg_replace_callback(
          '/(<style[^>]*>)(.*?)(<\/style[^>]*>)/is',
          'minifycss_callback',
          $txt
        );
      }
      break;

    case $config['extensions']['js']:
      if (!empty($config['user']['minify']['standalone_js']) && strlen($txt) < $config['user']['minify']['max_js_size']) {
        require 'minify/jsmin.php';
        $txt = JSMin::minify($txt);
      }
      break;

    case $config['extensions']['css']:
      if (!empty($config['user']['minify']['standalone_css']) && strlen($txt) < $config['user']['minify']['max_css_size']) {
        include('minify/Compressor.php');

        $txt = Minify_CSS_Compressor::process($txt, array());
      }
      break;
  }
}
