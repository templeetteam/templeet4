<?php
require('serverconf.php');
if ($config['cgi_header']) {
  header('Status: 200');
} else {
  header('HTTP/1.0 200');
}

header("Content-type: image/gif");

while (list($key, $value) = each($_SERVER)) {
  if (is_string($value) && preg_match('/NOTEXISTINGURL/', $value)) {
    if ($fp = @fopen("server.txt", 'w+')) {
      @fwrite($fp, serialize($_SERVER));
      @fclose($fp);
    }

    echook();
    exit;
  }
}

echonotok();

function echook()
{
  echo pack("H*", "47494638396101000100800000000000ffffff2c00000000010001000002024c01003b");
}

function echonotok()
{
  echo pack("H*", "47494638396102000100800000000000ffffff2c00000000020001000002024c0a003b");
}

function getserver($name)
{
  if (isset($_SERVER[$name])) return $_SERVER[$name];
  global $HTTP_SERVER_VARS;
  if (isset($HTTP_SERVER_VARS[$name])) return $HTTP_SERVER_VARS[$name];
  return '';
}
