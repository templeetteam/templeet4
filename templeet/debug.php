<?php
/*
 * Debug functions
 *
 * $Id: debug.php 104 2002-11-04 10:30:09Z cvspenso $
 */

function debug_tree($tmp)
{
  global $tree;
  $tree = '';

  if (gettype($tmp) != 'array') {
    echo "scalar: ($tmp)\n";
  }
  debug_tree2($tmp);
}

function debug_tree2($tmp)
{
  global $tree;
  echo $tree . '> ';

  while (list($a, $b) = each($tmp)) {
    if (gettype($b) == 'array') {
      $tree .= '-';
      echo "\n";
      debug_tree2($b);
    } else {
      echo "($b)";
    }
  }
  echo "<$tree\n";
  $tree = substr($tree, 1);
}
