<?php

header("Content-type: text/plain");
include("serverconf.php");

if (!isset($_REQUEST["pass"]) || $_REQUEST["pass"] != $config['expirepassword'])
  exit();

chdir("..");

$hexnow = sprintf("%08x", time());
$time_start = getmicrotime();

expirefiles($config['expiredir'], $hexnow);

function getmicrotime($time = 0)
{
  if (!$time)
    $time = microtime();

  list($usec, $sec) = explode(" ", $time);
  return ((float)$usec + (float)$sec);
}

function expirefiles($dir, $now, $subdir = '')
{
  global $time_start, $config;

  if ($dh = @opendir($dir)) {
    while (($file = readdir($dh)) !== false) {
      if (is_dir($dir . $file) && preg_match("/^[\da-z][\da-z]$/", $file) && substr($subdir . $file . "000000", 0, 8) <= $now) {
        expirefiles($dir . $file . "/", $now, $subdir . $file);
        @rmdir($dir . $file . "/");
      } elseif (is_file($dir . $file)) {
        $content = file_get_contents($dir . $file);
        @unlink($dir . $file);
        @unlink($content);
        @unlink($config['shadowdir'] . $content);
      }
    }

    closedir($dh);
  }

  if (getmicrotime() - $time_start > $config['maxexecexpire']) {
    exit();
  }
}
