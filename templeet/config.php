<?php
/*
 * Configuration file for Templeet
 *
 * $Id: config.php 2529 2006-07-28 06:43:52Z courtois $
 */

	// Associate the url which is called with the template you want to use.
	// Use preg()
	// See http://www.php.net/manual/en/ref.pcre.php for informations
	//
	// This will allow you to have url like index.php?/dir1/dir2/ to be using
	// template/dir1.tmpl as template:
	// '/^([^\/]+)\/.+\.html$/'=>'\1.tmpl'

	$config['html2template_array'] = array(
			);

	$config['errortemplate_array'] = array(404 => 'template/error/404.html',
			403 => 'template/error/403.html');

	/*
	 * Note: this is usefull only if you want to use the list or the sql module.
	 *	 You don't need to fill theses values if you don't use SQL.
	 *
	 * First argument is the database, then user, password, host, and backend
	 * type. For backend you can use mysql or pgsql yet.
	*/
	$config['sqlconfig'] = array(
			'*' => array('type'=>'mysql','host'=>'localhost','database'=>'templeet','login'=>'','password'=>'','charset'=>'UTF8')
			);

	// Templeet has a multi-language support similar to multiviews in Apache,
	// do set the default language in case the client doesn't give any.
	$config['default_language'] = 'en';

	// Use 1 to set Last-Modified header when using cache to save bandwidth
	// Will work only if cache is activated.
	$config['uselastmodified'] = 1;

	// Error reporting. Set it to 0 if you want parsing errors to never
	// be displayed.
	$config['errorReporting'] = 1;

	// admin address  
	$config['emailadmin'] = '';

	// Authentication variables

	// name of the cookie set by the authentication module
	$config['authcookie'] = "TempleetUser_".dechex(crc32($config['dir_installed'])&0xFF);

	// expire time for cookie
	$config['authcookieexpire'] = 1800;

	// expire time for remembered cookie
	$config['authcookierememberexpire'] = 1000000000;

	// authentication config file
	// don't change this, you could burn in hell
	$config['authconfigfile']="templeet/auth/config.php";

	// users' area
	$config['maxuserarea'] = 0; 

	// create users 
	$config['createuserspriv'] = 1; 

	// create userspace 
	$config['createuserspacepriv'] = 1; 

		// verbose error 
	$config['verboseerror'] = 0; 

	// lost password protection 
	$config['lostpass_time'] = 604800; 
	$config['lostpass_retry'] = 2; 

	// remove authentification cookie when signing out
	$config['removeauthcookie'] = 0;

	// mime types 
	// you may add some mime types here
	//$config['extensions']['myextension'] = 'mymime/type';

	// save line numbers in template cache
	// set to 0 to save a little time and space
	$config["debug_info"]=1;

	// if set, an equal number of spaces are removed at 
	// the beginning of each line of strings in templates
	$config["reduce_space"]=1;

		// allow variable variable names
	$config["variablesvariable"]=0;

	// This is used for cuthtml
	// 
	$config['htmltags'] = array('b'  => '',
			'i'  => '',
			'br' => '/',
			'p'  => '',
			'img' => array('src' => '','/' => '', 'alt' => ''),
			'u'  => '',
			'em'  => '',
			'tt'  => '',
			'strong'  => '',
			'blockquote' => '',
			's' => '',
			'pre' => '',
			'ul' => '',
			'li' => '',
			'abbr' => array('title' => '', 'lang' => ''),
			'sub' => '',
			'sup' => '',
			'h1' => '',
			'h2' => '',
			'h3' => '',
			'h4' => '',
			'a' => array('href' => '')
			);

// BEGIN USER CONFIG
$config['user'] = array(
  'minify' =>
  array(
    'minify_cached' => 0,
    'minify_notcached' => 0,
    'embedded_js' => 0,
    'standalone_js' => 0,
    'embedded_css' => 0,
    'standalone_css' => 0,
    'max_js_size' => 100000,
    'max_css_size' => 100000,
  ),
);
// END USER CONFIG
