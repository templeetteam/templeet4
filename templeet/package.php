<?php

$filename = 'cache/packages/' . $_COOKIE['packageinstall'] . '.php';
if (
  !isset($_COOKIE['packageinstall']) ||
  !preg_match('/^\w+$/', $_COOKIE['packageinstall']) ||
  filemtime($filename) + 12000 < time() ||
  !$content = file_get_contents($filename)
) {
  sendhttpstatus(404);
  print 'bad cookie';
  exit();
}

$session = @unserialize(substr($content, 8));
if (!$session || !isset($session['package'])) {
  sendhttpstatus(404);
  print 'session error';
  exit();
}

chdir('..');
include('templeet/packages/' . $session['package'] . '.php');
$registry = @unserialize(substr(file_get_contents('templeet/registry.php'), 8));
if (!is_array($registry)) {
  sendhttpstatus(404);
  print 'bad registry';
  exit();
}

installpackage();

function sendhttpstatus($status)
{
  if (preg_match('/cgi/i', php_sapi_name()))
    header('Status: ' . $status);
  else
    header('HTTP/1.0 ' . $status);
}
