<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE doc SYSTEM "http://www.templeet.org/doc-t4.dtd">
<doc>
<title>Gestion de fichiers</title>
<bloc title="Fonctions">
<entry title="readfile" type="pseudofunction">&lt;p&gt;&amp;nbsp;&lt;/p&gt;
<warning>Cette fonction est obsolète. Utiliser à la place la fonction PHP &lt;span class="methodname"&gt;&lt;strong&gt;file_get_contents&lt;/strong&gt;&lt;/span&gt;</warning></entry>
<entry title="writefile" type="function">&lt;p&gt;La fonction prend deux arguments :&lt;br&gt;&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Le nom du fichier (chemin complet)&lt;/li&gt;
&lt;li&gt;Le contenu à écrire dans le fichier&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
<warning>si le fichier existe déjà, il est écrasé et &amp;#126;writefile() y insère le nouveau contenu.</warning>
&lt;p&gt;&lt;br&gt;Code d'erreur renvoyé par la fonction :&lt;br&gt;&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;-1 : impossible d'ouvrir le fichier en écriture&lt;/li&gt;
&lt;li&gt;-2 : un problème est survenu pendant l'écriture du fichier&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Exemple d'utilisation de la fonction :&lt;/p&gt;
<code>&amp;#126;writefile("liste.dat", "foobar")</code>
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
<tip>La fonction procède à un vérouillage informatif du fichier. Si deux processus utilisent cette fonction en même temps le fichier ne sera pas un mix des deux écritures mais bien le résultat de la seconde écriture.&lt;br&gt;</tip></entry>
<entry title="appendfile" type="pseudofunction">&lt;p&gt;&amp;nbsp;&lt;/p&gt;
<warning>Cette fonction est obsolète. Utiliser la fonction PHP &lt;span class="methodname"&gt;&lt;strong&gt;file_put_contents&lt;/strong&gt;&lt;/span&gt;</warning></entry>
<entry title="delete_files" type="pseudofunction">&lt;p&gt;&amp;nbsp;&lt;/p&gt;
<warning>Cette fonction est obsolète. Utiliser la fonction&lt;strong&gt; deletemask&lt;/strong&gt; à la place.</warning></entry>
<entry title="deletemask" type="function">&lt;p&gt;Cette fonction permet de supprimer des fichiers récursivement.&lt;/p&gt;
&lt;p&gt;Elle prend quatre paramètres:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;le répertoire de base&lt;/li&gt;
&lt;li&gt;un masque sur le nom des répertoires à &lt;strong&gt;NE PAS EFFACER&lt;/strong&gt;. (Optionnel. Par défaut l'effacement concerne tous les répertoires)&lt;br&gt;&lt;/li&gt;
&lt;li&gt;un masque sur le nom des fichiers à effacer (Optionnel. Par défaut tous les fichiers seront effacés)&lt;br&gt;&lt;/li&gt;
&lt;li&gt;effacement des répertoires vides. (Optionnel. Par défaut les répertoires vides ne sont pas effacés)&lt;br&gt;
&lt;blockquote&gt;0 ou FALSE: les répertoires vides ne sont pas effacés.&lt;br&gt;&lt;br&gt;&lt;/blockquote&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Exemple d'arborescence:&lt;br&gt;&lt;/p&gt;
<code>.&lt;br&gt;|-- 000.jpg&lt;br&gt;|-- musiques&lt;br&gt;|   |-- Beethoven, symphonie No 6.mp3&lt;br&gt;|   `-- Brassens, Le gorille.mp3&lt;br&gt;|-- photos&lt;br&gt;|   |-- alice&lt;br&gt;|   |   |-- 001.jpg&lt;br&gt;|   |   `-- 002.jpg&lt;br&gt;|   `-- bob&lt;br&gt;|       |-- 001.jpg&lt;br&gt;|       `-- 002.jpg&lt;br&gt;`-- videos&lt;br&gt;    |-- charlie.avi&lt;br&gt;    `-- fun.mpg&lt;br&gt;</code>
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;p&gt;Exemple 1:&lt;/p&gt;
<code>deletemask("videos")&lt;br&gt;</code>
&lt;p&gt;Tous les fichiers de "videos" sont effacés.&lt;/p&gt;
&lt;p&gt;Exemple 2:&lt;/p&gt;
<code>deletemask("./","/videos/")&lt;br&gt;</code>
&lt;p&gt;Tous les fichiers de "musiques" et de "photos" sont effacés ainsi que 000.jpg; les fichiers de "videos" sont conservés.&lt;/p&gt;
&lt;p&gt;Exemple 3:&lt;/p&gt;
<code>deletemask("./","","/\.jpg$/")</code>
&lt;p&gt;Tous les fichiers jpg sont effacés.&lt;/p&gt;
&lt;p&gt;Exemple 4:&lt;br&gt;&lt;/p&gt;
<code>deletemask("./","","/\.m/",TRUE)</code>
&lt;p&gt;Les fichiers mp3 et mpg sont effacés. Le répertoire "musiques" devenu vide est également supprimé.&lt;br&gt;&lt;/p&gt;
&lt;p&gt;Exemple 5:&lt;/p&gt;
<code>deletemask("./","","/\/00[^\/]*\.jpg$/")&lt;br&gt;</code>
&lt;p&gt;Tous les fichiers jpg dont le nom commence par 00 sont effacés.&lt;/p&gt;
<tip>Le masque sur le nom des répertoires et masque sur le nom de fichiers est testé en incluant le chemin complet depuis la base indiquée.&lt;br&gt;Les fichiers et répertoires sont vus par les masques avec un slash ('/') au début.&lt;br&gt;Ex: le fichier photo/alice/001.jpg est vu par le masque comme "/photo/alice/001.jpg" et le fichier 000.jpg est vu comme "/000.jpg"</tip>
&lt;p&gt;Exemple 6:&lt;/p&gt;
<code>deletemask("./","","/00")&lt;br&gt;</code>
&lt;div id="_mcePaste" class="mcePaste" style="overflow: hidden; position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px;"&gt;
&lt;pre class="doc_code code"&gt;deletemask("repertoire/de/base/")&lt;/pre&gt;
&lt;/div&gt;
&lt;p&gt;Tous les fichiers dont le nom commence par 00 sont effacés ainsi que ceux qui contiennent "/00" dans le nom complet relatif à la base indiquée.&lt;br&gt;&lt;/p&gt;
<tip>Si le masque n'est pas une expression rationnelle le masque est transformé en expression rationnelle.&lt;br&gt;Les caractères '/' et '.' (point) sont précédés d'un '\' et la chaine est encadrée de '/'&lt;br&gt;Dans l'exemple 6, le masque sera vu par preg_match comme "/\/00/"&lt;br&gt;</tip></entry>
<entry title="deletepath" type="function">&lt;p&gt;La fonction deletepath permet d'effacer une arborescence complète.&lt;/p&gt;
&lt;p&gt;Elle prend un seul paramètre qui est le chemin à effacer.&lt;/p&gt;
&lt;p&gt;Exemple:&lt;/p&gt;
<code>deletepath("photos")&lt;br&gt;</code>
&lt;p&gt;Tout le contenu de "photos" est effacé y compris les répertoires.&lt;/p&gt;
&lt;p&gt;Cette fonction est équivalent à:&lt;/p&gt;
<code>deletemask("photos","","",1)&lt;br&gt;</code></entry>
<entry title="copy" type="function">&lt;p&gt;La fonction copie récursivement une arborescence.&lt;br&gt;&lt;/p&gt;
&lt;p&gt;La fonction &amp;#126;copy peut prendre deux ou trois arguments :&lt;br&gt;&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;le fichier ou le répertoire source&lt;/li&gt;
&lt;li&gt;le répertoire destination&lt;/li&gt;
&lt;li&gt;copie non destructrice. (Optionel)&lt;br&gt;
&lt;ul&gt;
&lt;li&gt;TRUE ou différent de 0:&amp;nbsp; la fonction sauvegarde les fichiers existants lors de la copie des fichiers.&lt;/li&gt;
&lt;/ul&gt;
&lt;ul&gt;
&lt;li&gt;autre: la fonction écrase les fichiers existants lors de la copie des fichiers.&amp;nbsp;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Lorsque le paramètre de sauvegarde est activé, la fonction renvoie la liste des fichiers qui ont été sauvegardés.&lt;/p&gt;
&lt;p&gt;La fonction ne renvoie aucun code d'erreur&lt;/p&gt;
&lt;p&gt;Exemple d'utilisation de la fonction :&lt;/p&gt;
<code>&amp;#126;copy("template/", "backup/", 1)</code>
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;ul&gt;
&lt;/ul&gt;</entry>
<entry title="getwebfile" type="function">&lt;p&gt;Cette fonction permet de charger un fichier en HTTP.&lt;br&gt;&lt;/p&gt;
&lt;p&gt;Elle prend deux paramètres:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;l'url à télécharger&lt;/li&gt;
&lt;li&gt;un nom de répertoire ou de fichier optionnel.&lt;br&gt;
&lt;blockquote&gt;
&lt;ul&gt;
&lt;li&gt;Si le paramètre est un répertoire le fichier est sauvegardé dans ce répertoire sous le nom indiqué dans le protocole ou à défaut par le nom extrait de l'url.&lt;/li&gt;
&lt;li&gt;Si le paramère est un nom de fichier, le fichier est sauvegardé sous ce nom.&lt;/li&gt;
&lt;li&gt;Si le paramètre est absent la fonction retourne le fichier lui même&lt;br&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/blockquote&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;En cas d'erreur la fonction retourne:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;-1: erreur ouverture de la connexion&lt;/li&gt;
&lt;li&gt;-2: erreur en ouverture du fichier à écrire&lt;/li&gt;
&lt;li&gt;-3: erreur de syntaxe&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Exemple 1:&lt;/p&gt;
<code>&amp;#126;getwebfile("http://www.example.com/image.jpg","./download/")&lt;br&gt;</code>
&lt;p&gt;Le fichier image.jpg est sauvegardé dans le répertoire download sous le nom indiqué dans l'entête HTTP 'Content-disposition: attachment; filename=' si elle existe ou à défaut "image.jpg".&lt;/p&gt;
&lt;p&gt;Exemple 2:&lt;br&gt;&lt;/p&gt;
<code>&amp;#126;getwebfile("http://www.example.com/image.jpg","./download/monimage.jpg"</code>
&lt;p&gt;Le fichier est sauvegardé sous le nom "monimage.jpg" dans le répertoire "download".&lt;/p&gt;
&lt;p&gt;Exemple 3:&lt;/p&gt;
<code>xml=getwebfile("http://www.example.com/fichier.xml")&lt;br&gt;</code>
&lt;p&gt;Le contenu du fichier téléchargé est affecté à la variable xml pour pouvoir être analysé ensuite.&lt;br&gt;&lt;/p&gt;
<warning>L'ordre des paramètres est changé par rapport à Templeet 3.5&lt;br&gt;</warning></entry>
<entry title="sendfile" type="function">&lt;p&gt;La fonction sendfile permet d'envoyer le contenu d'un fichier.&lt;/p&gt;
&lt;p&gt;Fonctionnellement cette fonction est équivalente à &lt;span class="methodname"&gt;&lt;strong&gt;file_get_contents&lt;/strong&gt;. La différence se situe au niveau de l'occupation mémoire. Avec file_get_contents le contenu est évalué par Templeet; le contenu du fichier est donc chargé intégralement. Si la taille du fichier dépasse&amp;nbsp; la quantité de mémoire allouée à PHP alors celui ci s'arrête.&lt;/span&gt;&lt;/p&gt;
&lt;p&gt;Avec la fonction sendfile, le contenu d'un fichier peut être envoyé au client quelle que soit sa taille. Le contenu du fichier ne passe par l'évaluation de Templeet. Le fichier est ouvert mais n'est envoyé qu'à la fin du processus Templeet après les évaluations.&lt;br&gt;&lt;/p&gt;
&lt;p&gt;La fonction retourne:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&amp;nbsp;0 si le fichier est ouvert correctement&lt;/li&gt;
&lt;li&gt;-1 si il y a une erreur en ouverture du fichier&lt;/li&gt;
&lt;li&gt;-2 si la fonction sendfile a déjà été appelée. On ne peut envoyer qu'un fichier à la fois; La fonction ne peut donc être appelée qu'une seule fois. &lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Exemple:&lt;/p&gt;
<code>sendfile("./mon_fichier_a_envoyer")&lt;br&gt;</code>
&lt;p&gt;&lt;span class="methodname"&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;</entry>
</bloc>
</doc>
