<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE doc SYSTEM "http://www.templeet.org/doc-t4.dtd">
<doc>
<title>Extraction de texte HTML</title>
<bloc title="Fonction">
<entry title="cuthtml" type="function">&lt;p&gt;&amp;#126;cuthtml() permet de couper un texte HTML selon une certaine longueur, ainsi que d'en supprimer les tags et attributs HTML non autorisés.&lt;br&gt;La fonction prend 5 arguments :&lt;br&gt;&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;La chaîne de texte initiale&lt;/li&gt;
&lt;li&gt;La longueur de texte hors balises HTML à renvoyer (renvoie tout le texte lorsque l'argument est omis)&lt;/li&gt;
&lt;li&gt;La longueur de la fenêtre de recherche pour la balise de fermeture correspondant à la dernière balise rencontrée. (facultatif)&lt;/li&gt;
&lt;li&gt;Le texte de fermeture en cas de coupure (facultatif)&lt;/li&gt;
&lt;li&gt;La liste des balises HTML autorisées (toutes celles par défaut si l'argument est omis, aucune si l'argument est -1)&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp; La liste des balises et des attributs par défaut peut être configurée dans le fichier config.php.&lt;/li&gt;
&lt;li&gt;Le type de conversion des guillemets et apostrophes : &lt;strong&gt;&lt;tt&gt;ENT_COMPAT,&lt;/tt&gt;&lt;/strong&gt;&lt;strong&gt;&lt;tt&gt;ENT_QUOTES ou &lt;/tt&gt;&lt;/strong&gt;&lt;strong&gt;&lt;tt&gt;ENT_NOQUOTES&lt;/tt&gt;&lt;/strong&gt;&lt;strong&gt;&amp;nbsp;&lt;/strong&gt;&lt;tt&gt;. &lt;/tt&gt;&lt;strong&gt;&lt;tt&gt;ENT_COMPAT&lt;/tt&gt;&lt;/strong&gt; par défaut)&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp; Pour plus détails voir:&amp;nbsp; &lt;a href="http://www.php.net/manual/en/function.htmlentities.php"&gt;http://www.php.net/manual/en/function.htmlentities.php&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Exemples:&lt;/p&gt;
&lt;p&gt;1. Aucune coupure:&lt;/p&gt;
&lt;blockquote&gt;
&lt;pre class="doc_code code"&gt;&amp;#126;cuthtml(&amp;lt;![TEXT["Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Monde!&amp;lt;/u&amp;gt;", hurla l'écho.]TEXT]&amp;gt;)&lt;/pre&gt;
&lt;p&gt;résultat: conversion des guillemets (paramètre &lt;strong&gt;ENT_COMPAT&lt;/strong&gt; par défaut du paramètre 6), fermeture du tag &amp;lt;b&amp;gt;&lt;/p&gt;
&lt;pre class="doc_code code"&gt;&amp;amp;quot;Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Monde!&amp;lt;/u&amp;gt;&amp;amp;quot;, hurla l'écho.&amp;lt;/b&amp;gt;&lt;/pre&gt;
&lt;/blockquote&gt;
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;p&gt;2. Coupure à exactement 15 caractères:&lt;/p&gt;
&lt;blockquote&gt;
&lt;pre class="doc_code code"&gt;&amp;#126;cuthtml(&amp;lt;![TEXT["Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Monde!&amp;lt;/u&amp;gt;", hurla l'écho.]TEXT]&amp;gt;,15)&lt;/pre&gt;
&lt;p&gt;résultat:&lt;/p&gt;
&lt;pre class="doc_code code"&gt;&amp;nbsp;&amp;amp;quot;Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Mo&amp;lt;/u&amp;gt;&amp;lt;/b&amp;gt;&lt;/pre&gt;
&lt;/blockquote&gt;
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;p&gt;3. Coupure à exactement 15 caractères, ajout de ... après la coupure,&amp;nbsp;tous les tags définis par défaut sont autorisés:&lt;/p&gt;
&lt;blockquote&gt;
&lt;pre class="doc_code code"&gt;&amp;#126;cuthtml(&amp;lt;![TEXT["Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Monde!&amp;lt;/u&amp;gt;", hurla l'écho.]TEXT]&amp;gt;,15,,'...','')&lt;/pre&gt;
&lt;p&gt;résultat:&lt;/p&gt;
&lt;pre class="doc_code code"&gt;&amp;nbsp;&amp;amp;quot;Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Mo&amp;lt;/u&amp;gt;&amp;lt;/b&amp;gt;&lt;/pre&gt;
&lt;/blockquote&gt;
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;p&gt;4. Coupure à exactement 15 caractères,ajout de ... après la coupure, seul le tag &amp;lt;b&amp;gt; est autorisé:&lt;/p&gt;
&lt;blockquote&gt;
&lt;pre class="doc_code code"&gt;&amp;#126;cuthtml(&amp;lt;![TEXT["Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Monde!&amp;lt;/u&amp;gt;", hurla l'écho.]TEXT]&amp;gt;,15,,'...','b')&lt;/pre&gt;
&lt;p&gt;résultat:&lt;/p&gt;
&lt;pre class="doc_code code"&gt;&amp;amp;quot;Bonjour, &amp;lt;b&amp;gt;Le Mo...&amp;lt;/b&amp;gt;&lt;/pre&gt;
&lt;/blockquote&gt;
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;p&gt;4 bis. Mêmes contraintes que pour l'exemple précédent mais avec passage du paramètre des tags sous forme de tableau: &lt;br&gt;&lt;/p&gt;
&lt;blockquote&gt;
&lt;pre class="doc_code code"&gt;&amp;#126;cuthtml(&amp;lt;![TEXT["Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Monde!&amp;lt;/u&amp;gt;", hurla l'écho.]TEXT]&amp;gt;,15,,'...',array('b' =&amp;gt; ''))&lt;/pre&gt;
&lt;p&gt;résultat:&lt;/p&gt;
&lt;pre class="doc_code code"&gt;&amp;amp;quot;Bonjour, &amp;lt;b&amp;gt;Le Mo...&amp;lt;/b&amp;gt;&lt;/pre&gt;
&lt;/blockquote&gt;
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;p&gt;5. Coupure au premier séparateur rencontré à partir du caractère 15 sur une fenêtre de 10 caractères:&lt;/p&gt;
&lt;blockquote&gt;
&lt;pre class="doc_code code"&gt;&amp;#126;cuthtml(&amp;lt;![TEXT["Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Monde!&amp;lt;/u&amp;gt;", hurla l'écho.]TEXT]&amp;gt;,15,10,'...','')&lt;/pre&gt;
&lt;p&gt;résultat:&lt;/p&gt;
&lt;pre class="doc_code code"&gt;&amp;amp;quot;Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Monde...&amp;lt;/u&amp;gt;&amp;lt;/b&amp;gt;&lt;/pre&gt;
&lt;/blockquote&gt;
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;p&gt;6. Aucune coupure, suppression de tous les tags:&lt;/p&gt;
&lt;blockquote&gt;
&lt;pre class="doc_code code"&gt;&amp;#126;cuthtml(&amp;lt;![TEXT["Bonjour, &amp;lt;b&amp;gt;&amp;lt;u&amp;gt;Le Monde!&amp;lt;/u&amp;gt;", hurla l'écho.]TEXT]&amp;gt;,,,,-1)&lt;/pre&gt;
&lt;p&gt;résultat:&lt;/p&gt;
&lt;/blockquote&gt;
&lt;blockquote&gt;
&lt;pre class="doc_code code"&gt;&amp;amp;quot;Bonjour, Le Monde!&amp;amp;quot;, hurla l'écho.&lt;/pre&gt;
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;/blockquote&gt;
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
<warning>Les tags ne sont pas comptés dans le nombre de caractères pour la coupure. Les caractères espace, tabulation, retour à la ligne successifs comptent pour un seul caractère.&lt;br&gt;</warning></entry>
</bloc>
</doc>
