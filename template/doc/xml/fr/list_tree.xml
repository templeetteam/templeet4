<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE doc SYSTEM "http://www.templeet.org/doc-t4.dtd">
<doc>
<title>Extraction d'arborescence depuis une base de données</title>
<bloc title="Fonctions">
<entry title="list_tree" type="function">&lt;p&gt;La fonction &amp;#126;list_tree permet d'effectuer une extraction sur une base de  données, selon une structure arborescente.&lt;br&gt;&lt;br&gt; Avec les séquences LF, LM, LP, LL, LTB, LTT, etc, on peut effectuer un  traitement (le plus souvent un affichage) différent selon qu'on affiche la première  ligne, les suivantes ou la dernière. Ceci peut s'avérer utile pour générer un tableau.&lt;/p&gt;
&lt;p&gt;La fonction &amp;#126;list_tree prend comme arguments :&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;le nom indiqué dans templeet/config.php pour la base  de données,&lt;/li&gt;
&lt;li&gt;la requête SQL à exécuter,&lt;/li&gt;
&lt;li&gt;le sens de classement des résultats : 'T' pour l'order croissant,  'TR' pour l'ordre décroissant ou '' (chaine vide) pour utliser l'ordre retourné par la requête SQL,&lt;br&gt;&lt;/li&gt;
&lt;li&gt;puis un nombre d'arguments couplés par deux dont le premier 	argument du couple peut être soit:  		&lt;strong&gt;LM&lt;/strong&gt;, 		&lt;strong&gt;LF&lt;/strong&gt;, 		&lt;strong&gt;LL&lt;/strong&gt;, 		&lt;strong&gt;L1&lt;/strong&gt;, 		&lt;strong&gt;LP&lt;/strong&gt;,  		&lt;strong&gt;LTT&lt;/strong&gt;,  		&lt;strong&gt;LTB&lt;/strong&gt;,  		&lt;strong&gt;LD&lt;/strong&gt; ou  		&lt;strong&gt;LE&lt;/strong&gt;. &lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Appelons A le premier argument du couple, B le deuxième argument du  couple.&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Si A est égal à &lt;strong&gt;LF&lt;/strong&gt;, B est évalué lors du premier  traitement, avant celui de LM et LTT&lt;/li&gt;
&lt;li&gt;Si A est égal à &lt;strong&gt;LL&lt;/strong&gt;, B est évalué pour le dernier  traitement, après celui de LP et LTB.&lt;/li&gt;
&lt;li&gt;Si A est égal à &lt;strong&gt;LM&lt;/strong&gt;, B est évalué pour chaque  traitement y compris le premier mais il sera alors évalué après celui de  LF, et avant celui de LL. LM est évalué avant une descente dans une  ramification.&lt;/li&gt;
&lt;li&gt;Si A est égal à &lt;strong&gt;LP&lt;/strong&gt;, B est évalué pour chaque  traitement y compris le premier mais il sera alors évalué après celui de  LF, et avant celui de LL. LP est évalué après une descente dans une  ramification, et donc après le traitement équivalent à LM. Les données  disponibles par &amp;#126;fld sont identiques à celles de LM pour un bloc donné.&lt;/li&gt;
&lt;li&gt;Si A est égal à &lt;strong&gt;LTT&lt;/strong&gt;, B est évalué lors du premier  traitement d'un sous-niveau, avant LM.&lt;/li&gt;
&lt;li&gt;Si A est égal à &lt;strong&gt;LTB&lt;/strong&gt;, B est évalué lors du dernier  traitement d'un sous-niveau, après LP.&lt;/li&gt;
&lt;li&gt;Si A est égal à &lt;strong&gt;LD&lt;/strong&gt;, B est évalué en cas de non  traitement d'information.&lt;/li&gt;
&lt;li&gt;Si A est égal à &lt;strong&gt;L1&lt;/strong&gt;, B est évalué lors du  traitement d'une seule information.&lt;/li&gt;
&lt;li&gt;Si A est égal à &lt;strong&gt;LE&lt;/strong&gt;, B est évalué en cas d'erreur  lors du traitement. Ceci comprend aussi les erreurs de connexion à la  bases de données.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Exemple pour une table :&lt;/p&gt;
<code>CREATE TABLE my_tree (&lt;br&gt;&amp;nbsp;&amp;nbsp; &amp;nbsp;id integer NOT NULL auto_increment,&lt;br&gt;&amp;nbsp;&amp;nbsp; &amp;nbsp;parent_id integer NOT NULL DEFAULT '0',&lt;br&gt;&amp;nbsp;&amp;nbsp; &amp;nbsp;title varchar(255),&lt;br&gt;&amp;nbsp;&amp;nbsp; &amp;nbsp;PRIMARY KEY(id),&lt;br&gt;&amp;nbsp;&amp;nbsp; &amp;nbsp;KEY parent_id(parent_id)&lt;br&gt;);&lt;br&gt;INSERT INTO my_tree(parent_id, title) VALUES (0, 'Liens');&lt;br&gt;INSERT INTO my_tree(parent_id, title) VALUES (1, 'Musique');&lt;br&gt;INSERT INTO my_tree(parent_id, title) VALUES (2, 'Instruments');&lt;br&gt;INSERT INTO my_tree(parent_id, title) VALUES (1, 'Informatique');&lt;br&gt;INSERT INTO my_tree(parent_id, title) VALUES (4, 'Linux');&lt;br&gt;INSERT INTO my_tree(parent_id, title) VALUES (2, 'Partitions');&lt;br&gt;INSERT INTO my_tree(parent_id, title) VALUES (2, 'Compositeurs');&lt;br&gt;INSERT INTO my_tree(parent_id, title) VALUES (7, 'J.S. Bach');&lt;br&gt;INSERT INTO my_tree(parent_id, title) VALUES (7, 'An Piele');&lt;br&gt;INSERT INTO my_tree(parent_id, title) VALUES (4, 'Templeet');</code>
&lt;p&gt;Ici, le champ &lt;em&gt;parent_id&lt;/em&gt; référence le champ &lt;em&gt;id&lt;/em&gt;,  afin de créer une structure arborescente, comme une structure de  rubriques, ou une hiérarchie de répertoires.&lt;br&gt; &lt;br&gt; Exemple, avec (2, 1) représentant (id, parent_id) :&lt;/p&gt;
<code>Liens (1, 0)&lt;br&gt;&amp;nbsp; |__ Musique (2,1)&lt;br&gt;&amp;nbsp; |&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |&lt;br&gt;&amp;nbsp; |&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |__ Instruments (3, 2)&lt;br&gt;&amp;nbsp; |&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |&lt;br&gt;&amp;nbsp; |&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |__ Partitions (6, 2)&lt;br&gt;&amp;nbsp; |&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |&lt;br&gt;&amp;nbsp; |&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |__ Compositeurs (7, 2)&lt;br&gt;&amp;nbsp; |&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |&lt;br&gt;&amp;nbsp; |&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |__ J.S. Bach (8, 7)&lt;br&gt;&amp;nbsp; |&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |&lt;br&gt;&amp;nbsp; |&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |__ An Pierle (9, 7)&lt;br&gt;&amp;nbsp; |&lt;br&gt;&amp;nbsp; |__ Informatique (4, 1)&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |__ Linux (5, 4)&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |__ Templeet (10, 4)&lt;br&gt;&lt;br&gt;</code>
&lt;p&gt;Ensuite, chaque champ objet d'une recherche est nommé par la fonction &amp;#126;fld('nom_du_champ') pour le reste de la fonction.&lt;/p&gt;</entry>
<entry title="fld" type="pseudofunction">&lt;p&gt;La fonction &amp;#126;fld utilisée avec list_tree est décrite dans la page de documentation de &lt;a href="list.html.fr#fld"&gt;list&lt;/a&gt;.&lt;/p&gt;</entry>
<entry title="gfld" type="pseudofunction">&lt;p&gt;La fonction &amp;#126;gfld utilisée avec list_tree est décrite dans la page de  documentation de &lt;a href="list.html.fr#gfld"&gt;list&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;Son utilisation avec list_tree est faite avec un argument propre à  list_tree:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&amp;#126;gfld("&lt;strong&gt;numrows&lt;/strong&gt;") : permet d'avoir le nombre de  ligne renvoyé par la requête&lt;/li&gt;
&lt;li&gt;&amp;#126;gfld("&lt;strong&gt;tnumrows&lt;/strong&gt;") : permet d'avoir le nombre de  ligne renvoyé par la requête pour ce niveau&lt;/li&gt;
&lt;li&gt;&amp;#126;gfld("&lt;strong&gt;counter&lt;/strong&gt;") : permet d'avoir le numéro de  ligne courante&lt;/li&gt;
&lt;li&gt;&amp;#126;gfld("&lt;strong&gt;tcounter&lt;/strong&gt;") : permet d'avoir le numéro de  ligne courante dans ce niveau&lt;/li&gt;
&lt;li&gt;&amp;#126;gfld("&lt;strong&gt;tdepth&lt;/strong&gt;") : permet d'obtenir la profondeur  de l'arborescence. La profondeur commence à 1. &lt;/li&gt;
&lt;li&gt;&amp;#126;gfld("&lt;strong&gt;error&lt;/strong&gt;") : permet d'avoir le message  d'erreur&lt;/li&gt;
&lt;li&gt;&amp;#126;gfld("&lt;strong&gt;executedtime&lt;/strong&gt;") : permet d'avoir le temps  d'éxécution (pour LF, LD, L1, LL seulement)&lt;/li&gt;
&lt;/ul&gt;
<warning>&lt;strong&gt;&amp;nbsp;&lt;/strong&gt;Les champs tdepth, tnumrows et tcounter ne   sont disponible que pour LM, LP, LTT, LTB.</warning>
<warning>Le champs tcount en 3.5 a été renommé en tcounter en 4.0 pour homogénéiser.&lt;br&gt;</warning>
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;ul&gt;
&lt;/ul&gt;</entry>
</bloc>
<text>&lt;p&gt;&lt;strong&gt;&amp;nbsp;&lt;/strong&gt;La requête SQL doit être construite de manière particulière afin de  permettre à &amp;#126;list_tree de construire la structure arborescente. La  première colonne sélectionnée doit être le champ servant d'identifiant  (une colonne &lt;em&gt;id&lt;/em&gt; par exemple), la deuxième colonne doit être  la valeur de l'identifiant de l'élément parent. L'élément parent doit  également être retourné par la requête afin d'obtenir la structure  nécessaire.&lt;br&gt; Par exemple, pour la table donnée en exemple précédemment, on pourrait  faire la requête suivante :&lt;/p&gt;
<code>&amp;#126;list_tree('database',&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; "SELECT id, parent_id, title FROM my_tree",&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 'T',&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 'LF','Mon arbre : &amp;lt;br/&amp;gt;',&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 'LL','&amp;lt;br/&amp;gt;',&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 'LTT','&amp;lt;ul style="background-color: #&amp;#126;string({level=dechex(255-gfld('tdepth')*10);level.level.level});&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; border: 1px solid black;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; list-style-position: inside;"&amp;gt;\n',&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 'LTB','&amp;lt;/ul&amp;gt;',&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 'LM','&amp;lt;li&amp;gt;&amp;#126;fld('title') (&amp;#126;fld('id'), &amp;#126;fld('parent_id'), &amp;#126;gfld('tdepth'), &amp;#126;gfld('counter'), &amp;#126;gfld('tcounter'))',&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 'LP','&amp;lt;/li&amp;gt;',&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; 'LD','J\'ai perdu mon chêne, mon alter égo...'&lt;br&gt;)</code>
&lt;p&gt;Ce qui donne :&lt;/p&gt;
&lt;p&gt;Mon arbre : &lt;br&gt;&lt;/p&gt;
&lt;ul style="border: 1px solid black; background-color: rgb(245, 245, 245); list-style-position: inside;"&gt;
&lt;li&gt;Liens (1, 0, 1, 1, 1)
&lt;ul style="border: 1px solid black; background-color: rgb(235, 235, 235); list-style-position: inside;"&gt;
&lt;li&gt;Musique (2, 1, 2, 2, 1)
&lt;ul style="border: 1px solid black; background-color: rgb(225, 225, 225); list-style-position: inside;"&gt;
&lt;li&gt;Instruments (3, 2, 3, 3, 1)&lt;/li&gt;
&lt;li&gt;Partitions (6, 2, 3, 4, 2)&lt;/li&gt;
&lt;li&gt;Compositeurs (7, 2, 3, 5, 3)
&lt;ul style="border: 1px solid black; background-color: rgb(215, 215, 215); list-style-position: inside;"&gt;
&lt;li&gt;J.S. Bach (8, 7, 4, 6, 1)&lt;/li&gt;
&lt;li&gt;An Piele (9, 7, 4, 7, 2)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;Informatique (4, 1, 2, 8, 2)
&lt;ul style="border: 1px solid black; background-color: rgb(225, 225, 225); list-style-position: inside;"&gt;
&lt;li&gt;Linux (5, 4, 3, 9, 1)&lt;/li&gt;
&lt;li&gt;Templeet (10, 4, 3, 10, 2)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;La même chose, mais à l'envers :&lt;/p&gt;
<code>&amp;#126;list_tree('database',&lt;br&gt;        "SELECT id, parent_id, title FROM my_tree",&lt;br&gt;        'T',&lt;br&gt;        'LF','Mon arbre : &amp;lt;br/&amp;gt;',&lt;br&gt;        'LL','&amp;lt;br/&amp;gt;',&lt;br&gt;        'LTT','&amp;lt;ul style="background-color: #&amp;#126;string({level=dechex(255-gfld('tdepth')*10);level.level.level}); &lt;br&gt;                          border: 1px solid black;&lt;br&gt;                          list-style-position: inside;"&amp;gt;',&lt;br&gt;        'LTB','&amp;lt;/ul&amp;gt;',&lt;br&gt;        'LM','&amp;lt;li&amp;gt;',&lt;br&gt;        'LP','&amp;#126;fld('title') (&amp;#126;fld('id'), &amp;#126;fld('parent_id'), &amp;#126;gfld('tdepth'), &amp;#126;gfld('counter'), &amp;#126;gfld('tcounter'))&amp;lt;/li&amp;gt;',&lt;br&gt;        'LD','J\'ai perdu mon chêne, mon alter égo...'&lt;br&gt;)</code>
&lt;p&gt;Ce qui donne :&lt;/p&gt;
&lt;p&gt;Mon arbre : &lt;br&gt;&lt;/p&gt;
&lt;ul style="border: 1px solid black; background-color: rgb(245, 245, 245); list-style-position: inside;"&gt;
&lt;li&gt;
&lt;ul style="border: 1px solid black; background-color: rgb(235, 235, 235); list-style-position: inside;"&gt;
&lt;li&gt;
&lt;ul style="border: 1px solid black; background-color: rgb(225, 225, 225); list-style-position: inside;"&gt;
&lt;li&gt;Instruments (3, 2, 3, 3, 1)&lt;/li&gt;
&lt;li&gt;Partitions (6, 2, 3, 4, 2)&lt;/li&gt;
&lt;li&gt;
&lt;ul style="border: 1px solid black; background-color: rgb(215, 215, 215); list-style-position: inside;"&gt;
&lt;li&gt;J.S. Bach (8, 7, 4, 6, 1)&lt;/li&gt;
&lt;li&gt;An Piele (9, 7, 4, 7, 2)&lt;/li&gt;
&lt;/ul&gt;
Compositeurs (7, 2, 3, 7, 3)&lt;/li&gt;
&lt;/ul&gt;
Musique (2, 1, 2, 7, 1)&lt;/li&gt;
&lt;li&gt;
&lt;ul style="border: 1px solid black; background-color: rgb(225, 225, 225); list-style-position: inside;"&gt;
&lt;li&gt;Linux (5, 4, 3, 9, 1)&lt;/li&gt;
&lt;li&gt;Templeet (10, 4, 3, 10, 2)&lt;/li&gt;
&lt;/ul&gt;
Informatique (4, 1, 2, 10, 2)&lt;/li&gt;
&lt;/ul&gt;
Liens (1, 0, 1, 10, 1)&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&amp;nbsp;&lt;/p&gt;
&lt;p&gt;Soit de manière schématique, en représentant l'ordre d'exéxution de haut  en bas et de gauche à droite, en respectant les profondeurs de  l'arborescence donnée en exemple en premier :&lt;/p&gt;
<code>LF, LTT, LM...............................&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp; .&lt;br&gt;.&amp;nbsp; |__ LTT, LM.......................... .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; |&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; |__ LTT, LM, LP&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; |&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; |__ LM, LP&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; |&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; |__ LM .................... . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |__ LTT, LM, LP&amp;nbsp; . . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; |__ LM, LP, LTB&amp;nbsp; . . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; ..LP, LTB.................. . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; ..LP, LTB........................ .&lt;br&gt;.&amp;nbsp; |&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp; .&lt;br&gt;.&amp;nbsp; |__ LM............................... .&lt;br&gt;.&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; |&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; |__ LTT, LM, LP&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; |&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; |__ LM, LP, LTB&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; .&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; . .&lt;br&gt;.&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; ..LP, LTB........................ .&lt;br&gt;.&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp; .&lt;br&gt;..LP, LTB, LF.............................&lt;br&gt;&lt;br&gt;</code>
&lt;div id="_mcePaste" class="mcePaste" style="overflow: hidden; position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px;"&gt;Mon arbre : &lt;br&gt;
&lt;ul style="border: 1px solid black; background-color: rgb(245, 245, 245); list-style-position: inside;"&gt;
&lt;li&gt;Liens (1, 0, 1, 1, 1)
&lt;ul style="border: 1px solid black; background-color: rgb(235, 235, 235); list-style-position: inside;"&gt;
&lt;li&gt;Musique (2, 1, 2, 2, 1)
&lt;ul style="border: 1px solid black; background-color: rgb(225, 225, 225); list-style-position: inside;"&gt;
&lt;li&gt;Instruments (3, 2, 3, 3, 1)&lt;/li&gt;
&lt;li&gt;Partitions (6, 2, 3, 4, 2)&lt;/li&gt;
&lt;li&gt;Compositeurs (7, 2, 3, 5, 3)
&lt;ul style="border: 1px solid black; background-color: rgb(215, 215, 215); list-style-position: inside;"&gt;
&lt;li&gt;J.S. Bach (8, 7, 4, 6, 1)&lt;/li&gt;
&lt;li&gt;An Piele (9, 7, 4, 7, 2)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;Informatique (4, 1, 2, 8, 2)
&lt;ul style="border: 1px solid black; background-color: rgb(225, 225, 225); list-style-position: inside;"&gt;
&lt;li&gt;Linux (5, 4, 3, 9, 1)&lt;/li&gt;
&lt;li&gt;Templeet (10, 4, 3, 10, 2)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;/div&gt;</text></doc>
