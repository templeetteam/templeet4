var th=0;

function getAbsolutePosition(element) {
    var r = { x: element.offsetLeft, y: element.offsetTop };
    if (element.offsetParent) {
      var tmp = getAbsolutePosition(element.offsetParent);
      r.x += tmp.x;
      r.y += tmp.y;
    }
    return r;
  }
  
function getElementSize(elem,prop) 
{
    if (typeof(elem)=="string")
      elem = document.getElementById(elem);
        
    switch(prop)
      {
        case 'width':
          return elem.offsetWidth;
          break;
          
        case 'height':
          return elem.offsetHeight;
          break;
          
                
      }
    return "";      
}

function floatwin_showdiv(id)
{
  if (document.getElementById) 
    document.getElementById(''+id+'').style.visibility = "visible";
}

function floatwin(id,flagit,e) {
if (flagit=="1")
  {
    if (e.clientX)
      {
	x = e.clientX;
	y = e.clientY;
      }
	
    if (e.pageX)
      {
	x = e.pageX;
	y = e.pageY;
      }

    scrollX=document.body.parentNode.scrollLeft;
    scrollY=document.body.parentNode.scrollTop;
        
    if (document.body.parentNode.clientWidth)
        {
          windowwidth=document.body.parentNode.clientWidth;
          windowheight=document.body.parentNode.clientHeight;
          scrollX=document.body.parentNode.scrollLeft;
          scrollY=document.body.parentNode.scrollTop;
        }
      else  
        {
          windowwidth=document.body.clientWidth;
          windowheight=document.body.clientHeight;
          scrollX=document.body.scrollLeft;
          scrollY=document.body.scrollTop;
        }

//    document.getElementById('trace').innerHTML+="windowheight:"+windowheight+"<br />";

    if (document.getElementById)
      {
	if (x-scrollX>windowwidth/2)
	    newleft = x-20-getElementSize(id,'width');
	  else
	    newleft = x+20;
	if (y-scrollY>windowheight/2)
	    newtop = y-20-getElementSize(id,'height');
	  else	
	    newtop = y+20;
	    
	document.getElementById(id).style.top=newtop+"px";
	document.getElementById(id).style.left=newleft+"px";

	if (th!=0)
	  clearTimeout(th);
		  
	th=setTimeout("floatwin_showdiv('"+id+"')",200);
      }				
  }
else
 if (flagit=="0")
   {
     if (th!=0)
       {
	 clearTimeout(th);
	 th=0;
       }
     if (document.getElementById) document.getElementById(id).style.visibility = "hidden"							
   }
}

var submenu="";
var submenu_icon=0;

function submenu_overimage_on(icon_id,submenu_id,e,callbackmenu)
{
    if (e.clientX)
      {
	x = e.clientX;
	y = e.clientY;
      }
	
    if (e.pageX)
      {
	x = e.pageX;
	y = e.pageY;
      }

    if (submenu!="")
      {
        if (submenu!=submenu_id)
            {
              if (document.getElementById) document.getElementById(submenu).style.visibility = "hidden"							
              if (callbackmenu)
                callbackmenu(0,x,y,icon_id,submenu_id);
           }
          else
            return false;
      }
      
    submenu_icon=document.getElementById(icon_id);
            
    
    if (document.body.parentNode.clientWidth)
        {
          windowwidth=document.body.parentNode.clientWidth;
          windowheight=document.body.parentNode.clientHeight;
          scrollX=document.body.parentNode.scrollLeft;
          scrollY=document.body.parentNode.scrollTop;
        }
      else  
        {
          windowwidth=document.body.clientWidth;
          windowheight=document.body.clientHeight;
          scrollX=document.body.scrollLeft;
          scrollY=document.body.scrollTop;
        }
    
    menuwidth=getElementSize(submenu_id,'width');
    menuheight=getElementSize(submenu_id,'height');

    pagewidth=document.body.offsetWidth;
    pageheight=document.body.offsetheight;
    
    if (document.getElementById)
      {
	 newleft=Math.floor(x-(menuwidth/2));
	 if (newleft<scrollX+10)
	   newleft=scrollX+10;
	 if (newleft+menuwidth>scrollX+windowwidth-10)
	   newleft=scrollX+windowwidth-10-menuwidth; 

	 newtop=y;
	 if (newtop<scrollY+10)
	   newtop=scrollY+10;
	 if (newtop+menuheight>scrollY+windowheight-10)
	   newtop=scrollY+windowheight-10-menuheight; 
    	   	    
	document.getElementById(submenu_id).style.top=newtop+"px";
	document.getElementById(submenu_id).style.left=newleft+"px";

      }	
    submenu=submenu_id;  
    if (document.getElementById) 
      {
        document.getElementById(submenu_id).style.visibility = "visible";
        if (callbackmenu)
          callbackmenu(1,x,y,icon_id,submenu_id);
      }  
}

function submenu_overimage_off(icon_id,submenu_id,e,callbackmenu) {
    if (e.clientX)
      {
	x = e.clientX;
	y = e.clientY;
      }
	
    if (e.pageX)
      {
	x = e.pageX;
	y = e.pageY;
      }
     
    inmenu=(x>=document.getElementById(submenu_id).offsetLeft) && 
           (x<document.getElementById(submenu_id).offsetLeft+document.getElementById(submenu_id).offsetWidth) &&
           (y>=document.getElementById(submenu_id).offsetTop) && 
           (y<document.getElementById(submenu_id).offsetTop+document.getElementById(submenu_id).offsetHeight);
    
    submenu_icon=document.getElementById(icon_id);  
    r_menuicon=getAbsolutePosition(submenu_icon)
           
    inmenuicon=(x>=r_menuicon.x) && 
           (x<r_menuicon.x+submenu_icon.offsetWidth) &&
           (y>=r_menuicon.y) && 
           (y<r_menuicon.y+submenu_icon.offsetHeight);
           
    if (!inmenu && !inmenuicon)
      {

        if (document.getElementById) document.getElementById(''+submenu_id+'').style.visibility = "hidden";
        submenu="";	
        if (callbackmenu)
          callbackmenu(0,x,y,icon_id,submenu_id);
      }  
}

function submenu_overmenu_on(icon_id,submenu_id,callbackmenu) {
}

function submenu_overmenu_off(icon_id,submenu_id,e,callbackmenu) {
    if (e.clientX)
      {
	x = e.clientX;
	y = e.clientY;
      }
	
    if (e.pageX)
      {
	x = e.pageX;
	y = e.pageY;
      }
     
    inmenu=(x>=document.getElementById(submenu).offsetLeft) && 
           (x<document.getElementById(submenu).offsetLeft+document.getElementById(submenu).offsetWidth) &&
           (y>=document.getElementById(submenu).offsetTop) && 
           (y<document.getElementById(submenu).offsetTop+document.getElementById(submenu).offsetHeight);
           
    submenu_icon=document.getElementById(icon_id);  
    r_menuicon=getAbsolutePosition(submenu_icon)
           
    inmenuicon=(x>=r_menuicon.x) && 
           (x<r_menuicon.x+submenu_icon.offsetWidth) &&
           (y>=r_menuicon.y) && 
           (y<r_menuicon.y+submenu_icon.offsetHeight);

    if (!inmenu && !inmenuicon)
       {
         if (document.getElementById) document.getElementById(''+submenu_id+'').style.visibility = "hidden";
         submenu="";
         if (callbackmenu)
           callbackmenu(0,x,y,icon_id,submenu_id);
						
       }  

}