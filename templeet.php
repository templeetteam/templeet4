<?php
/*
 * This code has been developed by:
 *
 * Fabien Penso
 * Pascal Courtois
 *
 *
 */

require_once('templeet.decl.php');

// If user hit the STOP button, we don't stop PHP
ignore_user_abort(true);
ini_set('magic_quotes_runtime', 0);
global $sendfile;

global $global_var;
$global_var = new vars();
$global_var->_POST = &$_POST;
$global_var->_GET = &$_GET;
$global_var->_REQUEST = &$_REQUEST;
$global_var->_ENV = &$_ENV;
$global_var->_SERVER = &$_SERVER;
$global_var->_FILES = &$_FILES;
$global_var->_SESSION = &$_SESSION;
$global_var->_COOKIE = &$_COOKIE;

if (function_exists('mb_internal_encoding'))
  mb_internal_encoding('ASCII');

global $debug_trace_var;
$debug_trace_var = '';

function debug_print($format)
{
  global $debug_trace_var;
  $param = func_get_args();
  $debug_trace_var .= vsprintf($format, $param);
  flush();
  return '';
}

function debug_print_r($var)
{
  global $debug_trace_var;

  $numargs = func_num_args();
  $arg_list = func_get_args();
  for ($i = 0; $i < $numargs; $i++) {
    if (is_array($arg_list[$i]) || is_object($arg_list[$i])) {
      $debug_trace_var .= print_r($arg_list[$i], TRUE);
    } else {
      $debug_trace_var .= $arg_list[$i];
    }
  }
  return '';
}

function debug_tbacktrace($pre = 0)
{
  global $debug_trace_var;
  ob_start();
  debug_print_backtrace();
  if ($pre)
    $debug_trace_var .= '<pre>';
  $debug_trace_var .= ob_get_contents();
  if ($pre)
    $debug_trace_var .= '</pre>';
  ob_end_clean();
  flush();
  return '';
}

function templeet_exit()
{
  global $config, $path, $lockfile, $debug_trace_var;
  if ($debug_trace_var != '')
    print "<pre>\n" . $debug_trace_var . "</pre>\n";

  if (isset($lockfile) && $lockfile != '')
    @unlink($lockfile);
  exit;
}

function getmicrotime($time = 0)
{
  if (!$time) {
    $time = microtime();
  }
  list($usec, $sec) = explode(' ', $time);
  return ((float)$usec + (float)$sec);
}

function template_fileexists($path)
{
  global $config;
  return @is_file($config['templatedir'] . $path);
}

function template_isdir($path)
{
  global $config;
  return @is_dir($config['templatedir'] . $path);
}

function findlanguages($dir, $reg)
{
  global $dirname, $config, $global_var;

  if ($dh = opendir($config['templatedir'] . $dir)) {
    while (($file = readdir($dh)) !== false) {
      if (preg_match("/$reg/", $file, $res))
        $global_var->all_lang[$res[1]] = 1;
    }
    closedir($dh);
  }
}

function setheaders($textout)
{
  global $global_var;

  if (isset($global_var->HTTP_Content_type)) {
    $charset = '';
    if (isset($global_var->HTTP_charset))
      $charset = "; charset=" . $global_var->HTTP_charset;
    elseif (preg_match('|<?xml\s+[^>]*encoding="(.*?)"|', $textout, $res))
      $charset = "; charset=" . $res[1];
    elseif (preg_match('|<meta\s+http-equiv="Content-Type"\s+content="text/html;\s+charset=(.*?)"\s+/>|', $textout, $res))
      $charset = "; charset=" . $res[1];

    header('Content-type: ' . $global_var->HTTP_Content_type . $charset);
  }

  if (isset($global_var->HTTP_filename)) {
    $name = $global_var->HTTP_filename;
    if ($name[0] != '"')
      $name = '"' . $name . '"';
    header('Content-disposition: attachment; filename=' . $name);
  }

  if (!empty($global_var->HTTP_nocache)) {
    header('Expires: Mon, 18 Jul 1978 05:00:00 GMT');
    $ie = isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/MSIE \d/', $_SERVER['HTTP_USER_AGENT']);
    if (!$ie)
      header('Cache-Control: no-store, no-cache, must-revalidate'); //HTTP/1.1
    header('Cache-Control: post-check=0, pre-check=0', false);
    if (!isset($_SERVER['HTTPS']) || !$ie)
      header('Pragma: no-cache'); //HTTP/1.0
  }
}

function expirefiles($dir, $now, $subdir = '')
{
  global $time_start, $config;

  if ($dh = @opendir($dir)) {
    while (($file = readdir($dh)) !== false) {
      if (is_dir($dir . $file) && preg_match("/^[\da-z][\da-z]$/", $file) && substr($subdir . $file . "000000", 0, 8) <= $now) {
        expirefiles($dir . $file . '/', $now, $subdir . $file);
        @rmdir($dir . $file . '/');
      } elseif (is_file($dir . $file)) {
        $content = file_get_contents($dir . $file);
        @unlink($dir . $file);
        @unlink($content);
        @unlink($config['shadowdir'] . $content);
      }
    }

    closedir($dh);
  }

  if (getmicrotime() - $time_start > $config['maxexecexpire']) {
    exit();
  }
}

$time_start = getmicrotime();

require('templeet/templeet.php');

$location = getfileinfo();
if ($location != '') {
  header("Location: $location");
  sendhttpstatus(302);
  if ($config['fallbackused']) // workaround for Apache FallbackResource bug
  {
    header('Content-Length: 1');
    print ' ';
  }
  templeet_exit();
}

try {
  $output = parsetemplate($global_var->template);
} catch (TempleetRedirect $r) {
  header('Location: ' . $r->getMessage());
  sendhttpstatus($r->getCode());
  if ($config['fallbackused']) // workaround for Apache FallbackResource bug
  {
    header('Content-Length: 1');
    print ' ';
  }
  templeet_exit();
}

if (getpost('_redo') != '') {
  header("Location: $path");
  sendhttpstatus(302);
  if ($config['fallbackused']) // workaround for Apache FallbackResource bug
  {
    header("Content-Length: 1");
    print ' ';
  }
  templeet_exit();
}

if (!is_string($output)) {
  if (is_array($output))
    $output = print_r($output, TRUE);
  else
    $output = (string)$output;
}

setheaders($output);

if ($debug_trace_var != '')
  $output .= "<pre>\n" . $debug_trace_var . "</pre>\n";

$expirefiles = $config['usepageexpire'] &&
  getmicrotime() - $time_start < $config['maxexecexpire'] &&
  (!file_exists($config['expiredir'] . 'last') ||
    @filemtime($config['expiredir'] . 'last') < time() + $config['maxcycleexpire']);

$ob = ob_list_handlers();
if (
  !is_array($ob) ||
  count($ob) == 0 ||
  (count($ob) == 1 && $ob[0] == 'default output handler')
) {
  $length = 0;
  if (isset($sendfile)) {
    $stat = fstat($sendfile);
    $length = $stat['size'];
  }

  $length += strlen($output) + ob_get_length();
  header("Content-Length: $length");
  if ($expirefiles)
    header('Connection: close');
}

echo $output;

if (isset($sendfile) && is_resource($sendfile)) {
  while (!feof($sendfile)) {
    $buffer = fread($sendfile, 65536);
    echo $buffer;
  }
  fclose($sendfile);
}

if ($expirefiles) {
  if (count($ob) > 0) {
    ob_end_flush();
    flush();
  }
  $hexnow = sprintf("%08x", time());
  expirefiles($config['expiredir'], $hexnow);
  @touch($config['expiredir'] . "last");
}
